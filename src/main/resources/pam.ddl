DROP TABLE IF EXISTS tbl_error_log;
DROP TABLE IF EXISTS tbl_qna;
DROP TABLE IF EXISTS tbl_version;
DROP TABLE IF EXISTS tbl_card_limit;
DROP TABLE IF EXISTS tbl_collect_type;
DROP TABLE IF EXISTS tbl_collect_log;
DROP TABLE IF EXISTS tbl_bank_account_balance;
DROP TABLE IF EXISTS tbl_property;
DROP TABLE IF EXISTS tbl_card_bill_mapping;
DROP TABLE IF EXISTS tbl_notice;
DROP TABLE IF EXISTS tbl_finace_company_regist;
DROP TABLE IF EXISTS tbl_notification;
DROP TABLE IF EXISTS tbl_notification_small_category;
DROP TABLE IF EXISTS tbl_notification_large_category;
DROP TABLE IF EXISTS tbl_challenge;
DROP TABLE IF EXISTS tbl_challenge_type;
DROP TABLE IF EXISTS tbl_money_calendar;
DROP TABLE IF EXISTS tbl_calendar_pay_type;
DROP TABLE IF EXISTS tbl_calendar_repeat_type;
DROP TABLE IF EXISTS tbl_budget_list;
DROP TABLE IF EXISTS tbl_budget;
DROP TABLE IF EXISTS tbl_expense;
DROP TABLE IF EXISTS tbl_pay_type;
DROP TABLE IF EXISTS tbl_car;
DROP TABLE IF EXISTS tbl_real_estate;
DROP TABLE IF EXISTS tbl_real_estate_type;
DROP TABLE IF EXISTS tbl_real_estate_deal_type;
DROP TABLE IF EXISTS tbl_stock_transaction;
DROP TABLE IF EXISTS tbl_monthly_asset;
DROP TABLE IF EXISTS tbl_daily_asset;
DROP TABLE IF EXISTS tbl_cash_receipt;
DROP TABLE IF EXISTS tbl_card_bill_detail;
DROP TABLE IF EXISTS tbl_card_bill_summary;
DROP TABLE IF EXISTS tbl_card_bill;
DROP TABLE IF EXISTS tbl_card_approval;
DROP TABLE IF EXISTS tbl_card;
DROP TABLE IF EXISTS tbl_bank_transaction;
DROP TABLE IF EXISTS tbl_category;
DROP TABLE IF EXISTS tbl_large_category;
DROP TABLE IF EXISTS tbl_bank_account;
DROP TABLE IF EXISTS tbl_asset_small_category;
DROP TABLE IF EXISTS tbl_asset_large_category;
DROP TABLE IF EXISTS tbl_finance_info;
DROP TABLE IF EXISTS tbl_finance_type;
DROP TABLE IF EXISTS tbl_user;

/**********************************/
/* Table Name: 사용자 */
/**********************************/
CREATE TABLE tbl_user(
		user_id                       		VARCHAR(64)		 NOT NULL		 PRIMARY KEY COMMENT '사용자 아이디',
		password                      		VARCHAR(255)		 NOT NULL COMMENT '비밀번호',
		name                          		VARCHAR(255)		 NOT NULL COMMENT '이름',
		birth_date                    		VARCHAR(8)		 NOT NULL COMMENT '생년월일',
		gender                        		CHAR(1)		 NOT NULL COMMENT '성별',
		mrrd_yn                       		CHAR(1)		 NOT NULL COMMENT '결혼 여부',
		os_type                       		INT		 NULL  COMMENT 'os 종류',
		push_key                      		VARCHAR(255)		 NULL  COMMENT '푸시 키',
		push_yn                       		CHAR(1)		 NOT NULL COMMENT '푸시 여부',
		use_yellopass_yn              		CHAR(1)		 NOT NULL COMMENT '옐로패스 사용자 여부',
		yellopass_id                  		BIGINT		 NULL  COMMENT '옐로패스 아이디',
		use_yn                        		CHAR(1)		 DEFAULT 'Y'		 NULL  COMMENT '사용 여부',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일'
) COMMENT='사용자';

/**********************************/
/* Table Name: 금융기관 종류 */
/**********************************/
CREATE TABLE tbl_finance_type(
		fnnc_type                     		CHAR(2)		 NOT NULL		 PRIMARY KEY COMMENT '금융기관 종류',
		fnnc_name                     		VARCHAR(30)		 NOT NULL COMMENT '금융기관 종류 이름',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일'
) COMMENT='금융기관 종류';

/**********************************/
/* Table Name: 금융 기관 */
/**********************************/
CREATE TABLE tbl_finance_info(
		cmpny_code                    		CHAR(3)		 NOT NULL		 PRIMARY KEY COMMENT '금융기관 코드',
		fnnc_type                     		CHAR(2)		 NOT NULL COMMENT '금융기관 종류',
		cmpny_name                    		VARCHAR(30)		 NOT NULL COMMENT '금융기관 이름',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (fnnc_type) REFERENCES tbl_finance_type (fnnc_type)
) COMMENT='금융 기관';

/**********************************/
/* Table Name: 자산 대분류 */
/**********************************/
CREATE TABLE tbl_asset_large_category(
		large_ctgry_code              		CHAR(2)		 NOT NULL		 PRIMARY KEY COMMENT '대분류 코드',
		large_ctgry_name              		VARCHAR(20)		 NOT NULL COMMENT '자산 대분류 이름',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일'
) COMMENT='자산 대분류';

/**********************************/
/* Table Name: 자산 소분류 */
/**********************************/
CREATE TABLE tbl_asset_small_category(
		small_ctgry_code              		CHAR(2)		 NOT NULL		 PRIMARY KEY COMMENT '소분류 코드',
		large_ctgry_code              		CHAR(2)		 NOT NULL COMMENT '대분류 코드',
		small_ctgry_name              		VARCHAR(30)		 NOT NULL COMMENT '소분류 이름',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (large_ctgry_code) REFERENCES tbl_asset_large_category (large_ctgry_code)
) COMMENT='자산 소분류';

/**********************************/
/* Table Name: 은행 계좌 */
/**********************************/
CREATE TABLE tbl_bank_account(
		accnt_id                      		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '계좌 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		cmpny_code                    		CHAR(3)		 NOT NULL COMMENT '금융기관 코드',
		accnt_num                     		VARCHAR(255)		 NOT NULL COMMENT '계좌번호',
		accnt_name                    		VARCHAR(100)		 NULL  COMMENT '계좌 이름',
		accnt_host_name               		VARCHAR(30)		 NULL  COMMENT '예금주 이름',
		small_ctgry_code              		CHAR(2)		 NOT NULL COMMENT '자산 종류',
		cur_blnc                      		BIGINT		 NULL  COMMENT '현재 잔액',
		pssbl_blnc                    		BIGINT		 NULL  COMMENT '출금가능 액수',
		open_date                     		VARCHAR(14)		 NULL  COMMENT '개설일',
		close_date                    		VARCHAR(14)		 NULL  COMMENT '만료일',
		use_yn                        		CHAR(1)		 NOT NULL COMMENT '사용 여부',
		cur_cd                        		VARCHAR(30)		 NULL  COMMENT '외화통화 코드',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  FOREIGN KEY (cmpny_code) REFERENCES tbl_finance_info (cmpny_code),
  FOREIGN KEY (small_ctgry_code) REFERENCES tbl_asset_small_category (small_ctgry_code),
  CONSTRAINT IDX_tbl_bank_account_1 UNIQUE (user_id, cmpny_code, accnt_num)
) COMMENT='은행 계좌';

/**********************************/
/* Table Name: 카테고리 대분류 */
/**********************************/
CREATE TABLE tbl_large_category(
		large_ctgry_code              		CHAR(2)		 NOT NULL		 PRIMARY KEY COMMENT '대분류 코드',
		large_ctgry_name              		VARCHAR(30)		 NOT NULL COMMENT '대분류 이름',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일'
) COMMENT='카테고리 대분류';

/**********************************/
/* Table Name: 카테고리 */
/**********************************/
CREATE TABLE tbl_category(
		ctgry_code                    		CHAR(4)		 NOT NULL		 PRIMARY KEY COMMENT '카테고리 코드',
		ctgry_name                    		VARCHAR(30)		 NULL  COMMENT '카테고리 이름',
		large_ctgry_code              		CHAR(2)		 NOT NULL COMMENT '카테고리 대분류',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (large_ctgry_code) REFERENCES tbl_large_category (large_ctgry_code)
) COMMENT='카테고리';

/**********************************/
/* Table Name: 은행 거래 내역 */
/**********************************/
CREATE TABLE tbl_bank_transaction(
		trans_id                      		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '거래 아이디',
		accnt_id                      		BIGINT		 NOT NULL COMMENT '계좌 아이디',
		trans_date                    		CHAR(14)		 NULL  COMMENT '거래일자',
		ctgry_code                    		CHAR(4)		 NULL  COMMENT '거래 종류',
		sum                           		BIGINT		 NULL  COMMENT '금액',
		trans_type                    		INT		 NOT NULL COMMENT '입출금 분류',
		after_value                   		BIGINT		 NULL  COMMENT '거래 후 잔액',
		oppnnt                        		VARCHAR(50)		 NOT NULL COMMENT '거래 상대',
		trans_method                  		VARCHAR(30)		 NULL  COMMENT '이체매체',
		dstrb                         		VARCHAR(30)		 NULL  COMMENT '취급점',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (accnt_id) REFERENCES tbl_bank_account (accnt_id),
  FOREIGN KEY (ctgry_code) REFERENCES tbl_category (ctgry_code),
  CONSTRAINT IDX_tbl_bank_transaction_1 UNIQUE (accnt_id, trans_date, ctgry_code, sum)
) COMMENT='은행 거래 내역';

/**********************************/
/* Table Name: 카드 */
/**********************************/
CREATE TABLE tbl_card(
		card_id                       		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '카드 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		cmpny_code                    		CHAR(3)		 NOT NULL COMMENT '카드사 코드',
		card_num                      		VARCHAR(30)		 NOT NULL COMMENT '카드번호',
		nick_name                     		VARCHAR(30)		 NULL  COMMENT '별명',
		card_code                     		CHAR(6)		 NULL  COMMENT '카드 코드',
		bill_yn                       		CHAR(1)		 NOT NULL COMMENT '청구서 카드 여부',
		use_yn                        		CHAR(1)		 NOT NULL COMMENT '사용 여부',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  FOREIGN KEY (cmpny_code) REFERENCES tbl_finance_info (cmpny_code),
  CONSTRAINT IDX_tbl_card_1 UNIQUE (user_id, cmpny_code, card_num, bill_yn)
) COMMENT='카드';

/**********************************/
/* Table Name: 카드 승인 내역 */
/**********************************/
CREATE TABLE tbl_card_approval(
		aprvl_id                      		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '승인 아이디',
		card_id                       		BIGINT		 NOT NULL COMMENT '카드 아이디',
		istm_yn                       		CHAR(1)		 NOT NULL COMMENT '할부 여부',
		aprvl_date                    		VARCHAR(14)		 NOT NULL COMMENT '승인일자',
		aprvl_num                     		VARCHAR(20)		 NOT NULL COMMENT '승인 번호',
		aprvl_yn                      		CHAR(1)		 NOT NULL COMMENT '승인/취소 여부',
		istm_mon                      		INT		 NULL  COMMENT '할부 개월',
		aprvl_amt                     		BIGINT		 NOT NULL COMMENT '승인 금액',
		store_name                    		VARCHAR(30)		 NULL  COMMENT '가명점 이름',
		crncy_type                    		VARCHAR(30)		 NULL  COMMENT '통화 종류',
		store_num                     		VARCHAR(30)		 NULL  COMMENT '가맹점 사업자 번호',
		ctgry_code                    		CHAR(4)		 NULL  COMMENT '가맹점 분류 코드',
		store_tel                     		VARCHAR(20)		 NULL  COMMENT '가맹점 전화번호',
		store_addr                    		TEXT		 NULL  COMMENT '가맹점 주소',
		store_host_name               		VARCHAR(20)		 NULL  COMMENT '가맹점 대표자 명',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (card_id) REFERENCES tbl_card (card_id),
  FOREIGN KEY (ctgry_code) REFERENCES tbl_category (ctgry_code),
  CONSTRAINT IDX_tbl_card_approval_1 UNIQUE (card_id, aprvl_date, aprvl_num)
) COMMENT='카드 승인 내역';

/**********************************/
/* Table Name: 카드 청구서 */
/**********************************/
CREATE TABLE tbl_card_bill(
		bill_id                       		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '청구서 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		cmpny_code                    		CHAR(3)		 NOT NULL COMMENT '업체 코드',
		pay_date                      		CHAR(8)		 NOT NULL COMMENT '결제 예정일',
		pay_month                     		CHAR(6)		 NOT NULL COMMENT '결제 예정월',
		pay_amt                       		BIGINT		 NOT NULL COMMENT '결제 예정 금액',
		use_yn                        		CHAR(1)		 NOT NULL COMMENT '사용 여부',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  FOREIGN KEY (cmpny_code) REFERENCES tbl_finance_info (cmpny_code),
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  CONSTRAINT IDX_tbl_card_bill_1 UNIQUE (user_id, cmpny_code, pay_month)
) COMMENT='카드 청구서';

/**********************************/
/* Table Name: 카드 청구서 요약 */
/**********************************/
CREATE TABLE tbl_card_bill_summary(
		smry_id                       		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '요약 아이디',
		bill_id                       		BIGINT		 NOT NULL COMMENT '청구서 아이디',
		smry_name                     		VARCHAR(255)		 NOT NULL COMMENT '청구서 요약 종류',
		sum                           		BIGINT		 NOT NULL COMMENT '금액',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (bill_id) REFERENCES tbl_card_bill (bill_id),
  CONSTRAINT IDX_tbl_card_bill_summary_1 UNIQUE (bill_id, smry_name)
) COMMENT='카드 청구서 요약';

/**********************************/
/* Table Name: 카드 청구서 내역 */
/**********************************/
CREATE TABLE tbl_card_bill_detail(
		detail_id                     		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '청구서 상세 아이디',
		bill_id                       		BIGINT		 NOT NULL COMMENT '청구서 아이디',
		card_id                       		BIGINT		 NOT NULL COMMENT '카드 아이디',
		istm_yn                       		CHAR(1)		 NOT NULL COMMENT '할부여부',
		istm_period                   		VARCHAR(30)		 NULL  COMMENT '할부기간',
		istm_month                    		INT		 NULL  COMMENT '할부개월',
		use_date                      		VARCHAR(14)		 NOT NULL COMMENT '이용일자',
		store_name                    		VARCHAR(30)		 NULL  COMMENT '가맹점 이름',
		aprvl_num                     		VARCHAR(20)		 NOT NULL COMMENT '승인 번호',
		srtx                          		BIGINT		 NULL  COMMENT '부가세',
		tip                           		BIGINT		 NULL  COMMENT '봉사료',
		excg_rate                     		FLOAT		 NULL  COMMENT '환율',
		store_num                     		VARCHAR(20)		 NULL  COMMENT '가맹점 사업자 번호',
		ctgry_code                    		CHAR(4)		 NOT NULL COMMENT '가맹점 분류',
		store_addr                    		VARCHAR(100)		 NULL  COMMENT '가맹점 주소',
		store_tel                     		VARCHAR(20)		 NULL  COMMENT '가맹점 전화번호',
		frgn_yn                       		CHAR(1)		 NULL  COMMENT '국외여부',
		store_host_name               		VARCHAR(20)		 NULL  COMMENT '대표자 명',
		crncy_code                    		VARCHAR(30)		 NULL  COMMENT '통화 코드',
		ko_sum                        		BIGINT		 NOT NULL COMMENT '금액',
		frgn_sum                      		BIGINT		 NULL  COMMENT '현지 금액',
		fee                           		BIGINT		 NULL  COMMENT '수수료',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (bill_id) REFERENCES tbl_card_bill (bill_id),
  FOREIGN KEY (ctgry_code) REFERENCES tbl_category (ctgry_code),
  FOREIGN KEY (card_id) REFERENCES tbl_card (card_id),
  CONSTRAINT IDX_tbl_card_bill_detail_1 UNIQUE (bill_id, card_id, use_date, aprvl_num, store_name, ko_sum)
) COMMENT='카드 청구서 내역';

/**********************************/
/* Table Name: 현금영수증 */
/**********************************/
CREATE TABLE tbl_cash_receipt(
		rcpt_id                       		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '영수증 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		trans_date                    		VARCHAR(14)		 NOT NULL COMMENT '거래날짜',
		store_name                    		VARCHAR(30)		 NOT NULL COMMENT '거래점',
		sum                           		BIGINT		 NOT NULL COMMENT '금액',
		ctgry_code                    		CHAR(4)		 NOT NULL COMMENT '카테고리 코드',
		aprvl_num                     		VARCHAR(20)		 NOT NULL COMMENT '승인번호',
		idnty                         		VARCHAR(20)		 NULL  COMMENT '카드구분',
		aprvl_yn                      		CHAR(1)		 NOT NULL COMMENT '거래종류',
		ddct_yn                       		CHAR(1)		 NOT NULL COMMENT '공제여부',
		issue_type                    		INT		 NOT NULL COMMENT '이슈구분',
		expense                       		VARCHAR(30)		 NULL  COMMENT '비용',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  FOREIGN KEY (ctgry_code) REFERENCES tbl_category (ctgry_code),
  CONSTRAINT IDX_tbl_cash_receipt_1 UNIQUE (user_id, trans_date, store_name, aprvl_num)
) COMMENT='현금영수증';

/**********************************/
/* Table Name: 일별 개인 자산 */
/**********************************/
CREATE TABLE tbl_daily_asset(
		asset_id                      		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '자산 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		base_date                     		CHAR(8)		 NOT NULL COMMENT '기준 날짜',
		small_ctgry_code              		CHAR(2)		 NOT NULL COMMENT '자산 종류',
		sum                           		BIGINT		 NOT NULL COMMENT '금액',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  FOREIGN KEY (small_ctgry_code) REFERENCES tbl_asset_small_category (small_ctgry_code)
) COMMENT='일별 개인 자산';

/**********************************/
/* Table Name: 월별 개인 자산 */
/**********************************/
CREATE TABLE tbl_monthly_asset(
		asset_id                      		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '자산 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		base_date                     		CHAR(6)		 NOT NULL COMMENT '기준 날짜',
		small_ctgry_code              		CHAR(2)		 NOT NULL COMMENT '자산 종류',
		sum                           		BIGINT		 NOT NULL COMMENT '금액',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  FOREIGN KEY (small_ctgry_code) REFERENCES tbl_asset_small_category (small_ctgry_code)
) COMMENT='월별 개인 자산';

/**********************************/
/* Table Name: 개인 주식 거래 내역 */
/**********************************/
CREATE TABLE tbl_stock_transaction(
		trans_id                      		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '거래 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		trans_type                    		INT(1)		 NOT NULL COMMENT '거래 종류',
		stock_code                    		VARCHAR(20)		 NOT NULL COMMENT '종목코드',
		stock_name                    		VARCHAR(30)		 NOT NULL COMMENT '종목 이름',
		trans_date                    		CHAR(8)		 NOT NULL COMMENT '거래일',
		price                         		BIGINT		 NOT NULL COMMENT '가격',
		amt                           		BIGINT		 NOT NULL COMMENT '수량',
		sum                           		BIGINT		 NOT NULL COMMENT '거래 금액',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id)
) COMMENT='개인 주식 거래 내역';

/**********************************/
/* Table Name: 부동산 거래 유형 */
/**********************************/
CREATE TABLE tbl_real_estate_deal_type(
		deal_type                     		CHAR(2)		 NOT NULL		 PRIMARY KEY COMMENT '거래유형',
		deal_name                     		VARCHAR(10)		 NOT NULL COMMENT '거래유형 이름',
		reg_date                      		DATETIME		 NULL  COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일'
) COMMENT='부동산 거래 유형';

/**********************************/
/* Table Name: 부동산 종류 */
/**********************************/
CREATE TABLE tbl_real_estate_type(
		estate_type                   		CHAR(2)		 NOT NULL		 PRIMARY KEY COMMENT '부동산 종류',
		info                          		VARCHAR(10)		 NOT NULL COMMENT '설명',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일'
) COMMENT='부동산 종류';

/**********************************/
/* Table Name: 부동산 */
/**********************************/
CREATE TABLE tbl_real_estate(
		estate_id                     		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '부동산 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		estate_name                   		VARCHAR(30)		 NULL  COMMENT '부동산 이름',
		estate_type                   		CHAR(2)		 NOT NULL COMMENT '부동산 종류',
		deal_type                     		CHAR(2)		 NOT NULL COMMENT '거래 유형',
		price                         		BIGINT		 NOT NULL COMMENT '가격',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  FOREIGN KEY (deal_type) REFERENCES tbl_real_estate_deal_type (deal_type),
  FOREIGN KEY (estate_type) REFERENCES tbl_real_estate_type (estate_type)
) COMMENT='부동산';

/**********************************/
/* Table Name: 중고차 보유 내역 */
/**********************************/
CREATE TABLE tbl_car(
		car_id                        		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '자동차 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		maker_code                    		VARCHAR(30)		 NOT NULL COMMENT '제조사 코드',
		car_code                      		VARCHAR(30)		 NOT NULL COMMENT '차종',
		made_year                     		CHAR(4)		 NOT NULL COMMENT '연식',
		sub_code                      		VARCHAR(30)		 NOT NULL COMMENT '세부차명',
		mrkt_price                    		BIGINT		 NULL  COMMENT '시세',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id)
) COMMENT='중고차 보유 내역';

/**********************************/
/* Table Name: 결제수단 */
/**********************************/
CREATE TABLE tbl_pay_type(
		pay_type                      		CHAR(2)		 NOT NULL		 PRIMARY KEY COMMENT '결제수단',
		info                          		VARCHAR(10)		 NOT NULL COMMENT '설명',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일'
) COMMENT='결제수단';

/**********************************/
/* Table Name: 소비 */
/**********************************/
CREATE TABLE tbl_expense(
		expns_id                      		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '소비 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		expns_date                    		VARCHAR(14)		 NOT NULL COMMENT '소비일',
		oppnnt                        		VARCHAR(30)		 NOT NULL COMMENT '소비대상',
		sum                           		BIGINT		 NOT NULL COMMENT '금액',
		ctgry_code                    		CHAR(4)		 NOT NULL COMMENT '카테고리 분류',
		pay_type                      		CHAR(2)		 NOT NULL COMMENT '결제수단',
		cancel_yn                     		CHAR(1)		 NOT NULL COMMENT '취소여부',
		edit_yn                       		CHAR(1)		 NOT NULL COMMENT '사용자 등록 여부',
		cmpny_code                    		CHAR(3)		 NULL  COMMENT '업체코드',
		use_yn                        		CHAR(1)		 NOT NULL COMMENT '사용 여부',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  FOREIGN KEY (pay_type) REFERENCES tbl_pay_type (pay_type),
  CONSTRAINT IDX_tbl_expense_1 UNIQUE (user_id, expns_date, sum)
) COMMENT='소비';

/**********************************/
/* Table Name: 예산 */
/**********************************/
CREATE TABLE tbl_budget(
		bdgt_id                       		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '예산 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		base_month                    		CHAR(6)		 NOT NULL COMMENT '기준 월',
		total_sum                     		BIGINT		 NOT NULL COMMENT '예산 합계',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  CONSTRAINT IDX_tbl_budget_1 UNIQUE (user_id, base_month)
) COMMENT='예산';

/**********************************/
/* Table Name: 예산 내역 */
/**********************************/
CREATE TABLE tbl_budget_list(
		list_id                       		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '내역 아이디',
		bdgt_id                       		BIGINT		 NOT NULL COMMENT '예산 아이디',
		large_ctgry_code              		CHAR(2)		 NOT NULL COMMENT '카테고리',
		sum                           		BIGINT		 NULL  COMMENT '금액',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (bdgt_id) REFERENCES tbl_budget (bdgt_id),
  FOREIGN KEY (large_ctgry_code) REFERENCES tbl_large_category (large_ctgry_code),
  CONSTRAINT IDX_tbl_budget_list_1 UNIQUE (bdgt_id, large_ctgry_code)
) COMMENT='예산 내역';

/**********************************/
/* Table Name: 머니캘린더 반복 종류 */
/**********************************/
CREATE TABLE tbl_calendar_repeat_type(
		rpt_type                      		CHAR(2)		 NOT NULL		 PRIMARY KEY COMMENT '반복 종류',
		rpt_name                      		VARCHAR(10)		 NOT NULL COMMENT '반복 이름',
		reg_date                      		DATETIME		 NULL  COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일'
) COMMENT='머니캘린더 반복 종류';

/**********************************/
/* Table Name: 머니 캘린더 납부 유형 */
/**********************************/
CREATE TABLE tbl_calendar_pay_type(
		pay_type                      		CHAR(2)		 NOT NULL		 PRIMARY KEY COMMENT '유형 코드',
		name                          		VARCHAR(30)		 NOT NULL COMMENT '유형 이름',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일'
) COMMENT='머니 캘린더 납부 유형';

/**********************************/
/* Table Name: 머니 캘린더 */
/**********************************/
CREATE TABLE tbl_money_calendar(
		cal_id                        		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '캘린더 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		title                         		VARCHAR(30)		 NOT NULL COMMENT '제목',
		oppnnt                        		VARCHAR(30)		 NULL  COMMENT '청구기관',
		amt                           		BIGINT		 NOT NULL COMMENT '금액',
		pay_type                      		CHAR(2)		 NOT NULL COMMENT '납부유형',
		pay_date                      		VARCHAR(8)		 NOT NULL COMMENT '납부 기한',
		pay_day                       		VARCHAR(2)		 NOT NULL COMMENT '납부 일',
		rpt_type                      		CHAR(2)		 NOT NULL COMMENT '반복 유형',
		use_yn                        		CHAR(1)		 NOT NULL COMMENT '사용 여부',
		expt_yn                       		CHAR(1)		 NOT NULL COMMENT '예상/확정여부',
		mtchd_tag                     		VARCHAR(30)		 NULL  COMMENT '매칭 된 태그',
		del_date                      		DATETIME		 NULL  COMMENT '삭제일',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  FOREIGN KEY (rpt_type) REFERENCES tbl_calendar_repeat_type (rpt_type),
  FOREIGN KEY (pay_type) REFERENCES tbl_calendar_pay_type (pay_type),
  CONSTRAINT IDX_tbl_money_calendar_1 UNIQUE (user_id, oppnnt, pay_date)
) COMMENT='머니 캘린더';

/**********************************/
/* Table Name: 챌린지 유형 */
/**********************************/
CREATE TABLE tbl_challenge_type(
		chlng_type                    		CHAR(2)		 NOT NULL		 PRIMARY KEY COMMENT '챌린지 유형',
		chlng_name                    		VARCHAR(30)		 NOT NULL COMMENT '챌린지 이름',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일'
) COMMENT='챌린지 유형';

/**********************************/
/* Table Name: 챌린지 */
/**********************************/
CREATE TABLE tbl_challenge(
		chlng_id                      		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '챌린지 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		chlng_type                    		CHAR(2)		 NOT NULL COMMENT '챌린지 유형',
		title                         		VARCHAR(30)		 NOT NULL COMMENT '제목',
		goal_date                     		VARCHAR(8)		 NULL  COMMENT '목표일',
		accnt_id                      		BIGINT		 NULL  COMMENT '연동 계좌',
		goal_amt                      		BIGINT		 NULL  COMMENT '목표 금액',
		monthly_amt                   		BIGINT		 NOT NULL COMMENT '월납입 금액',
		cmplt_yn                      		CHAR		 NULL  COMMENT '달성 여부',
		cmplt_date                    		VARCHAR(8)		 NULL  COMMENT '달성일',
		image_path                    		VARCHAR(100)		 NULL  COMMENT '이미지 경로',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  FOREIGN KEY (chlng_type) REFERENCES tbl_challenge_type (chlng_type)
) COMMENT='챌린지';

/**********************************/
/* Table Name: 알림 대분류 */
/**********************************/
CREATE TABLE tbl_notification_large_category(
		large_ctgry_code              		CHAR(2)		 NOT NULL		 PRIMARY KEY COMMENT '대분류 코드',
		large_ctgry_name              		VARCHAR(30)		 NOT NULL COMMENT '대분류 이름',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일'
) COMMENT='알림 대분류';

/**********************************/
/* Table Name: 알림 소분류 */
/**********************************/
CREATE TABLE tbl_notification_small_category(
		small_ctgry_code              		CHAR(4)		 NOT NULL		 PRIMARY KEY COMMENT '소분류 코드',
		small_ctgry_name              		VARCHAR(100)		 NOT NULL COMMENT '소분류 설명',
		msg                           		VARCHAR(500)		 NOT NULL COMMENT '메시지',
		large_ctgry_code              		CHAR(2)		 NOT NULL COMMENT '대분류 코드',
		list_yn                       		CHAR(1)		 NOT NULL COMMENT '리스트 포함 여부',
		push_yn                       		CHAR(1)		 NOT NULL COMMENT '푸쉬 보낼지 여부',
		push_time                     		CHAR(4)		 NULL  COMMENT '푸쉬 보낼 시간',
		popup_yn                      		CHAR(1)		 NOT NULL COMMENT '팝업 여부',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (large_ctgry_code) REFERENCES tbl_notification_large_category (large_ctgry_code)
) COMMENT='알림 소분류';

/**********************************/
/* Table Name: 알림 */
/**********************************/
CREATE TABLE tbl_notification(
		noti_id                       		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '알림 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		small_ctgry_code              		CHAR(4)		 NOT NULL COMMENT '소분류',
		msg                           		VARCHAR(500)		 NOT NULL COMMENT '메시지',
		make_month                    		CHAR(6)		 NOT NULL COMMENT '등록월',
		make_day                      		CHAR(2)		 NOT NULL COMMENT '등록 날짜',
		read_yn                       		CHAR(1)		 NOT NULL COMMENT '확인 여부',
		push_time                     		DATETIME		 NULL  COMMENT '푸시 보낸 시간',
		identifier                    		VARCHAR(20)		 NULL  COMMENT '구분자',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (small_ctgry_code) REFERENCES tbl_notification_small_category (small_ctgry_code),
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id)
) COMMENT='알림';

/**********************************/
/* Table Name: 사용자 금융기관 등록 */
/**********************************/
CREATE TABLE tbl_finace_company_regist(
		regist_id                     		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '등록 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		cmpny_code                    		CHAR(3)		 NOT NULL COMMENT '금융기관 코드',
		cert_yn                       		CHAR(1)		 NOT NULL COMMENT '공인인증서 사용여부',
		login_id                      		VARCHAR(30)		 NULL  COMMENT '로그인 아이디',
		psswd                         		VARCHAR(255)		 NULL  COMMENT '비밀번호',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (cmpny_code) REFERENCES tbl_finance_info (cmpny_code),
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id)
) COMMENT='사용자 금융기관 등록';

/**********************************/
/* Table Name: 공지사항 */
/**********************************/
CREATE TABLE tbl_notice(
		notice_id                     		INT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '공지사항 아이디',
		subject                       		VARCHAR(100)		 NOT NULL COMMENT '제목',
		contents                      		VARCHAR(1000)		 NOT NULL COMMENT '내용',
		writer                        		VARCHAR(100)		 NULL  COMMENT '작성자',
		emgnc_yn                      		CHAR(1)		 NOT NULL COMMENT '긴급공지 여부',
		use_yn                        		CHAR(1)		 NOT NULL COMMENT '사용 여부',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  CONSTRAINT IDX_tbl_notice_1 UNIQUE (emgnc_yn, use_yn)
) COMMENT='공지사항';

/**********************************/
/* Table Name: 카드 청구서 매핑 */
/**********************************/
CREATE TABLE tbl_card_bill_mapping(
		bill_id                       		BIGINT		 NOT NULL COMMENT '청구서 아이디',
		card_id                       		BIGINT		 NOT NULL COMMENT '카드 아이디',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  PRIMARY KEY (bill_id, card_id),
  FOREIGN KEY (bill_id) REFERENCES tbl_card_bill (bill_id),
  FOREIGN KEY (card_id) REFERENCES tbl_card (card_id),
  CONSTRAINT IDX_tbl_card_bill_mapping_1 UNIQUE (bill_id, card_id)
) COMMENT='카드 청구서 매핑';

/**********************************/
/* Table Name: 재산 */
/**********************************/
CREATE TABLE tbl_property(
		ppty_id                       		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '재산 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		ppty_date                     		VARCHAR(8)		 NOT NULL COMMENT '기준날짜',
		small_ctgry_code              		CHAR(2)		 NOT NULL COMMENT '자산 카테고리 코드',
		detail_id                     		BIGINT		 NULL  COMMENT '세부내역 아이디',
		sum                           		BIGINT		 NOT NULL COMMENT '금액',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  FOREIGN KEY (small_ctgry_code) REFERENCES tbl_asset_small_category (small_ctgry_code)
) COMMENT='재산';

/**********************************/
/* Table Name: 은행 계좌 잔액 */
/**********************************/
CREATE TABLE tbl_bank_account_balance(
		blnc_id                       		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '잔액 아이디',
		accnt_id                      		BIGINT		 NOT NULL COMMENT '계좌 아이디',
		blnc_date                     		VARCHAR(8)		 NOT NULL COMMENT '기준 날짜',
		blnc_amt                      		BIGINT		 NOT NULL COMMENT '잔액',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (accnt_id) REFERENCES tbl_bank_account (accnt_id),
  CONSTRAINT IDX_tbl_bank_account_balance_1 UNIQUE (accnt_id, blnc_date)
) COMMENT='은행 계좌 잔액';

/**********************************/
/* Table Name: 수집 로그 */
/**********************************/
CREATE TABLE tbl_collect_log(
		log_id                        		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '로그 아이디',
		user_id                       		VARCHAR(64)		 NULL  COMMENT '사용자 아이디',
		cllct_type                    		INT		 NOT NULL COMMENT '수집 종류',
		cmpny_code                    		CHAR(3)		 NULL  COMMENT '업체 코드',
		accd_id                       		BIGINT		 NULL  COMMENT '계좌/카드 아이디',
		upload_id                     		VARCHAR(30)		 NULL  COMMENT '업로드 아이디',
		res_code                      		CHAR(3)		 NULL  COMMENT '결과코드',
		res_desc                      		VARCHAR(100)		 NULL  COMMENT '결과 설명',
		err_desc                      		VARCHAR(100)		 NULL  COMMENT '에러 내용',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  CONSTRAINT IDX_tbl_collect_log_1 UNIQUE (user_id, upload_id)
) COMMENT='수집 로그';

/**********************************/
/* Table Name: 데이터 수집 종류 */
/**********************************/
CREATE TABLE tbl_collect_type(
		cllct_type                    		INT		 NOT NULL		 PRIMARY KEY COMMENT '수집 종류',
		cllct_name                    		VARCHAR(10)		 NOT NULL COMMENT '수집 내용',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일'
) COMMENT='데이터 수집 종류';

/**********************************/
/* Table Name: 카드 한도 */
/**********************************/
CREATE TABLE tbl_card_limit(
		limit_id                      		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '한도 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		cmpny_code                    		CHAR(3)		 NOT NULL COMMENT '은행사 코드',
		limit_amt                     		BIGINT		 NOT NULL COMMENT '한도',
		use_amt                       		BIGINT		 NULL  COMMENT '사용액',
		psbl_amt                      		BIGINT		 NULL  COMMENT '사용 가능 금액',
		use_yn                        		CHAR(1)		 NOT NULL COMMENT '사용 여부',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  FOREIGN KEY (cmpny_code) REFERENCES tbl_finance_info (cmpny_code),
  CONSTRAINT IDX_tbl_card_limit_1 UNIQUE (user_id, cmpny_code)
) COMMENT='카드 한도';

/**********************************/
/* Table Name: 버전 테이블 */
/**********************************/
CREATE TABLE tbl_version(
		version                       		VARCHAR(30)		 NOT NULL		 PRIMARY KEY COMMENT '버전',
		dscrpt                        		VARCHAR(100)		 NULL  COMMENT '내용',
		url                           		VARCHAR(500)		 NULL  COMMENT '다운로드 url',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일'
) COMMENT='버전 테이블';

/**********************************/
/* Table Name: 큐앤에이 */
/**********************************/
CREATE TABLE tbl_qna(
		qna_id                        		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '큐앤에이 아이디',
		qstn                          		VARCHAR(1000)		 NOT NULL COMMENT '질문',
		ansr                          		VARCHAR(1000)		 NOT NULL COMMENT '답변',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일'
) COMMENT='큐앤에이';

/**********************************/
/* Table Name: 에러 로그 */
/**********************************/
CREATE TABLE tbl_error_log(
		log_id                        		BIGINT		 NOT NULL		 PRIMARY KEY AUTO_INCREMENT COMMENT '로그 아이디',
		user_id                       		VARCHAR(64)		 NOT NULL COMMENT '사용자 아이디',
		cmpny_code                    		CHAR(3)		 NULL  COMMENT '금융기관 코드',
		err_desc                      		VARCHAR(1000)		 NOT NULL COMMENT '에러 내용',
		reg_date                      		DATETIME		 NOT NULL COMMENT '등록일',
		mod_date                      		DATETIME		 NOT NULL COMMENT '수정일',
  FOREIGN KEY (user_id) REFERENCES tbl_user (user_id),
  FOREIGN KEY (cmpny_code) REFERENCES tbl_finance_info (cmpny_code)
) COMMENT='에러 로그';


CREATE INDEX IDX_tbl_property_1 ON tbl_property (user_id, detail_id, ppty_date);


INSERT INTO tbl_finance_type (fnnc_type, fnnc_name, reg_date, mod_date) VALUES ('01', '은행', now(), now());
INSERT INTO tbl_finance_type (fnnc_type, fnnc_name, reg_date, mod_date) VALUES ('02', '카드사', now(), now());
INSERT INTO tbl_finance_type (fnnc_type, fnnc_name, reg_date, mod_date) VALUES ('03', '증권사', now(), now());
INSERT INTO tbl_finance_type (fnnc_type, fnnc_name, reg_date, mod_date) VALUES ('04', '보험사', now(), now());
INSERT INTO tbl_finance_type (fnnc_type, fnnc_name, reg_date, mod_date) VALUES ('11', '정부(국세청)', now(), now());

INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('039', '01', '경남은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('034', '01', '광주은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('004', '01', '국민은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('003', '01', '기업은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('011', '01', '농협', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('031', '01', '대구은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('032', '01', '부산은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('002', '01', '산업은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('045', '01', '새마을금고', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('007', '01', '수협', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('088', '01', '신한은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('048', '01', '신협', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('027', '01', '씨티은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('005', '01', '외환은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('020', '01', '우리은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('037', '01', '전북은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('023', '01', 'SC은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('035', '01', '제주은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('502', '02', '국민카드', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('514', '02', '농협카드', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('508', '02', '롯데카드', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('504', '02', '삼성카드', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('506', '02', '신한카드', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('501', '02', '씨티카드', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('511', '02', '우리카드', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('509', '02', '하나SK카드', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('505', '02', '현대카드', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('503', '02', 'BC카드', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('081', '01', '하나은행', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('071', '01', '우체국', now(), now());
INSERT INTO tbl_finance_info (cmpny_code, fnnc_type, cmpny_name, reg_date, mod_date) VALUES ('054', '11', '국세청', now(), now());

INSERT INTO tbl_asset_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('01', '자산', now(), now());
INSERT INTO tbl_asset_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('02', '부채', now(), now());
INSERT INTO tbl_asset_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('99', '기타', now(), now());

INSERT INTO tbl_asset_small_category (small_ctgry_code, large_ctgry_code, small_ctgry_name, reg_date, mod_date) VALUES ('01', '01', '예금', now(), now());
INSERT INTO tbl_asset_small_category (small_ctgry_code, large_ctgry_code, small_ctgry_name, reg_date, mod_date) VALUES ('02', '01', '적금', now(), now());
INSERT INTO tbl_asset_small_category (small_ctgry_code, large_ctgry_code, small_ctgry_name, reg_date, mod_date) VALUES ('03', '01', '주식', now(), now());
INSERT INTO tbl_asset_small_category (small_ctgry_code, large_ctgry_code, small_ctgry_name, reg_date, mod_date) VALUES ('04', '01', '펀드', now(), now());
INSERT INTO tbl_asset_small_category (small_ctgry_code, large_ctgry_code, small_ctgry_name, reg_date, mod_date) VALUES ('11', '02', '대출', now(), now());
INSERT INTO tbl_asset_small_category (small_ctgry_code, large_ctgry_code, small_ctgry_name, reg_date, mod_date) VALUES ('12', '02', '카드', now(), now());
INSERT INTO tbl_asset_small_category (small_ctgry_code, large_ctgry_code, small_ctgry_name, reg_date, mod_date) VALUES ('99', '99', '기타', now(), now());

INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('01', '음식', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('02', '카페/간식', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('03', '술/유흥', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('04', '생활/마트', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('05', '주거/통신', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('06', '교통/차량', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('07', '패션/미용', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('08', '온라인 쇼핑', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('09', '교육', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('10', '문화/예술', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('11', '스포츠', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('12', '건강', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('13', '여행/숙박', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('14', '육아', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('15', '애완동물', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('16', '금융/보험', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('17', '기타', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('18', '미분류', now(), now());
INSERT INTO tbl_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('19', '수입', now(), now());

INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0101', '주식', '01', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0102', '패스트푸드', '01', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0103', '식료품', '01', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0104', '기타음식', '01', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0201', '커피/음료', '02', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0202', '부식', '02', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0301', '주류/유흥', '03', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0401', '마트', '04', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0402', '백화점', '04', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0403', '편의점', '04', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0404', '기타생활/마트', '04', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0501', '유선전화', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0502', '통신비', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0503', '인터넷', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0504', '월세', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0505', '관리비', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0506', '가전/디지털', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0507', '가구/생활용품', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0508', '수리/수선', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0509', '경조사', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0510', '종교활동', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0511', '전문서비스', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0512', '케이블TV', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0513', 'TV수신료', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0514', '수도료', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0515', '전기료', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0516', '도시가스', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0517', '배달/택배', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0518', '기타주거/통신', '05', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0601', '대중교통', '06', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0602', '주유비', '06', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0603', '차량관리', '06', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0604', '주차비', '06', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0605', '차량구매', '06', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0606', '택시비', '06', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0607', '배달/운송', '06', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0608', '기타교통/차량', '06', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0701', '의류비', '07', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0702', '패션잡화', '07', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0703', '세탁/수선', '07', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0704', '헤어/뷰티', '07', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0705', '기타패션/미용', '07', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0801', '온라인쇼핑', '08', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0901', '학용품', '09', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0902', '학원비', '09', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0903', '학자금 대출', '09', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0904', '등록금', '09', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0905', '시설이용', '09', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('0906', '기타교육', '09', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1001', '영화', '10', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1002', '공연예술', '10', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1003', '전시/관람', '10', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1004', '도서비', '10', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1005', '취미/오락', '10', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1006', '온라인컨텐츠', '10', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1007', '기타문화/예술', '10', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1101', '운동/용품', '11', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1102', '기타스포츠', '11', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1201', '병원', '12', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1202', '치과', '12', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1203', '시력관리', '12', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1204', '약값', '12', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1205', '기타건강', '12', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1301', '항공권', '13', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1302', '숙박', '13', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1303', '렌트카', '13', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1304', '기타여행/숙박', '13', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1401', '육아용품', '14', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1402', '보육시설', '14', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1403', '교육/놀이', '14', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1404', '기타육아', '14', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1501', '애완용품', '15', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1502', '애완미용', '15', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1503', '동물병원', '15', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1504', '기타애완동뮬', '15', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1601', '예금', '16', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1602', '적금', '16', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1603', '펀드', '16', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1604', '보험', '16', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1605', '대출이자', '16', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1606', '계좌이체', '16', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1607', '현금인출', '16', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1608', '기타금융/보험', '16', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1701', '기타', '17', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1702', '해외결제', '17', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1703', '소득세', '17', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1704', '재산세', '17', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1705', '지방세 (자동차세)', '17', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1706', '주민세', '17', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1707', '과태료 (세외)', '17', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1708', '국민연금', '17', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1709', '건강보험', '17', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1710', '기타공과금', '17', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1711', '은행 수수료', '17', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1712', '금융 수수료', '17', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1713', '서비스 수수료', '17', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1714', '연체료', '17', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1801', '미분류', '18', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1901', '급여', '19', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1902', '상여', '19', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1903', '사업소득', '19', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1904', '임대소득', '19', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1905', '적금', '19', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1906', '보험', '19', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1907', '이자', '19', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1908', '배당금', '19', now(), now());
INSERT INTO tbl_category (ctgry_code, ctgry_name, large_ctgry_code, reg_date, mod_date) VALUES ('1909', '기타수입', '19', now(), now());

INSERT INTO tbl_real_estate_deal_type (deal_type, deal_name, reg_date, mod_date) VALUES ('01', '매매', now(), now());
INSERT INTO tbl_real_estate_deal_type (deal_type, deal_name, reg_date, mod_date) VALUES ('02', '전세', now(), now());
INSERT INTO tbl_real_estate_deal_type (deal_type, deal_name, reg_date, mod_date) VALUES ('03', '월세', now(), now());

INSERT INTO tbl_real_estate_type (estate_type, info, reg_date, mod_date) VALUES ('01', '아파트', now(), now());
INSERT INTO tbl_real_estate_type (estate_type, info, reg_date, mod_date) VALUES ('02', '빌라', now(), now());
INSERT INTO tbl_real_estate_type (estate_type, info, reg_date, mod_date) VALUES ('03', '단독', now(), now());
INSERT INTO tbl_real_estate_type (estate_type, info, reg_date, mod_date) VALUES ('04', '상가', now(), now());
INSERT INTO tbl_real_estate_type (estate_type, info, reg_date, mod_date) VALUES ('05', '오피스텔', now(), now());
INSERT INTO tbl_real_estate_type (estate_type, info, reg_date, mod_date) VALUES ('06', '대지', now(), now());

INSERT INTO tbl_pay_type (pay_type, info, reg_date, mod_date) VALUES ('01', '신용카드', now(), now());
INSERT INTO tbl_pay_type (pay_type, info, reg_date, mod_date) VALUES ('02', '체크카드', now(), now());
INSERT INTO tbl_pay_type (pay_type, info, reg_date, mod_date) VALUES ('03', '현금', now(), now());
INSERT INTO tbl_pay_type (pay_type, info, reg_date, mod_date) VALUES ('04', '계좌이체', now(), now());
INSERT INTO tbl_pay_type (pay_type, info, reg_date, mod_date) VALUES ('99', '기타/미분류', now(), now());

INSERT INTO tbl_calendar_repeat_type (rpt_type, rpt_name, reg_date, mod_date) VALUES ('NO', '없음', now(), now());
INSERT INTO tbl_calendar_repeat_type (rpt_type, rpt_name, reg_date, mod_date) VALUES ('1W', '1주', now(), now());
INSERT INTO tbl_calendar_repeat_type (rpt_type, rpt_name, reg_date, mod_date) VALUES ('1M', '1개월', now(), now());
INSERT INTO tbl_calendar_repeat_type (rpt_type, rpt_name, reg_date, mod_date) VALUES ('3M', '3개월', now(), now());
INSERT INTO tbl_calendar_repeat_type (rpt_type, rpt_name, reg_date, mod_date) VALUES ('6M', '6개월', now(), now());
INSERT INTO tbl_calendar_repeat_type (rpt_type, rpt_name, reg_date, mod_date) VALUES ('1Y', '1년', now(), now());

INSERT INTO tbl_calendar_pay_type (pay_type, name, reg_date, mod_date) VALUES ('01', '계좌이체', now(), now());
INSERT INTO tbl_calendar_pay_type (pay_type, name, reg_date, mod_date) VALUES ('02', '자동납부', now(), now());

INSERT INTO tbl_challenge_type (chlng_type, chlng_name, reg_date, mod_date) VALUES ('01', '여행자금 마련', now(), now());
INSERT INTO tbl_challenge_type (chlng_type, chlng_name, reg_date, mod_date) VALUES ('02', '학비 마련', now(), now());
INSERT INTO tbl_challenge_type (chlng_type, chlng_name, reg_date, mod_date) VALUES ('03', '비상금 마련', now(), now());
INSERT INTO tbl_challenge_type (chlng_type, chlng_name, reg_date, mod_date) VALUES ('04', '주택자금 마련', now(), now());
INSERT INTO tbl_challenge_type (chlng_type, chlng_name, reg_date, mod_date) VALUES ('05', '새 차 자금 마련', now(), now());
INSERT INTO tbl_challenge_type (chlng_type, chlng_name, reg_date, mod_date) VALUES ('06', '기타 저축하기', now(), now());

INSERT INTO tbl_notification_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('01', '소비', now(), now());
INSERT INTO tbl_notification_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('02', '투자', now(), now());
INSERT INTO tbl_notification_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('03', '머니캘린더', now(), now());
INSERT INTO tbl_notification_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('04', '챌린지', now(), now());
INSERT INTO tbl_notification_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('05', '금융기관연동', now(), now());
INSERT INTO tbl_notification_large_category (large_ctgry_code, large_ctgry_name, reg_date, mod_date) VALUES ('06', '버전업데이트', now(), now());

INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0101', '결제 카테고리 미분류(기본)', '파악이 어려운 지출이 3개 이상입니다. 카테고리 분류로 브로콜리를 도와주세요.', '01', 'Y', 'Y', '', 'N', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0102', '결제 카테고리 미분류(지난 달)', '지난 달 파악이 어려운 지출이 있습니다. 새로운 달이 시작하기 전에 분류를 완성해보면 어떨까요?', '01', 'N', 'Y', '', 'Y', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0103', '월초 예산 설정 알림', '새로운 달이 시작되었습니다. 지난 달과 같은 예산으로 이번 달을 시작하실건가요?', '01', 'N', 'Y', '', 'Y', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0104', '총 예산 근접 알림', '이번 달 예산에 가까워지고 있습니다. (확인해보시겠어요?)', '01', 'Y', 'Y', '', 'Y', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0105', '총 예산 초과 알림', '지출금액이 이번 달 예산을 초과했습니다, (조정이 필요할까요?)', '01', 'Y', 'Y', '', 'Y', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0106', '카테고리별 예산 초과 알림', 'OOO 지출이 예산을 초과했습니다. (조정이 필요할까요?)', '01', 'Y', 'Y', '', 'Y', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0201', '투자자산 확인 유도', '오늘 나의 주식 기상도는 어떨까요?', '01', 'N', 'Y', '', 'N', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0301', '금융기관 등록 후 최초 설정 (3개월 데이터 기반)', '출금내역을 통해 머니캘린더에 OOO 일정을 자동등록하였습니다. (확인해보시겠어요?)', '01', 'Y', 'Y', '', 'Y', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0302', '일정 예정 알림 (1일 전)', 'OOO 일정이 내일입니다.', '01', 'N', 'Y', '', 'Y', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0303', '일정 예정 알림 (당일)', 'OOO 일정이 오늘입니다.', '01', 'N', 'Y', '', 'Y', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0304', '납부 완료 알림', 'OOO 일정이 실제 납부한 일정으로 수정되었습니다.', '01', 'Y', 'Y', '', 'N', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0305', '미납 알림', 'OOO 일정이 아직 납부되지 않은 것 같습니다. (확인해보시겠어요?)', '01', 'Y', 'Y', '', 'Y', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0401', '진행 시', 'OOO 챌린지를 달성해나가고 있군요! 현재 NN% 달성 중입니다. 조금 더 힘내세요!', '01', 'Y', 'Y', '', 'N', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0402', '챌린지 달성 시', 'OOO 챌린지를 달성했습니다. 대단하시네요!', '01', 'Y', 'Y', '', 'N', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0403', '계좌 연동 촉구 시', '계좌연동이 필요한 챌린지가 있습니다. 더 적극적인 챌린지 달성을 위해 계좌연동을 부탁드려요.', '01', 'Y', 'Y', '', 'N', now(), now());
INSERT INTO tbl_notification_small_category (small_ctgry_code, small_ctgry_name, msg, large_ctgry_code, list_yn, push_yn, push_time, popup_yn, reg_date, mod_date) VALUES ('0501', '금융기관 연동 실패 시', '금융기관(OOOO) 연동 중 오류가 발생하여 데이터 수집이 정상적으로 이루어 지지 않을 수 있습니다.', '05', 'Y', 'Y', '', 'N', now(), now());

INSERT INTO tbl_notice (notice_id, subject, contents, writer, emgnc_yn, use_yn, reg_date, mod_date) VALUES (1, '긴급공지', '현재 서버 장애로 브로콜리 앱을 사용할 수 없습니다.', 'admin', 'Y', 'N', now(), now());

INSERT INTO tbl_collect_type (cllct_type, cllct_name, reg_date, mod_date) VALUES (0, 'No type', now(), now());
INSERT INTO tbl_collect_type (cllct_type, cllct_name, reg_date, mod_date) VALUES (1, '은행 계좌', now(), now());
INSERT INTO tbl_collect_type (cllct_type, cllct_name, reg_date, mod_date) VALUES (2, '은행 거래 내역', now(), now());
INSERT INTO tbl_collect_type (cllct_type, cllct_name, reg_date, mod_date) VALUES (3, '카드 승인 내역', now(), now());
INSERT INTO tbl_collect_type (cllct_type, cllct_name, reg_date, mod_date) VALUES (4, '카드 청구 내역', now(), now());
INSERT INTO tbl_collect_type (cllct_type, cllct_name, reg_date, mod_date) VALUES (5, '카드 한도', now(), now());
INSERT INTO tbl_collect_type (cllct_type, cllct_name, reg_date, mod_date) VALUES (6, '카드 결제 예정', now(), now());
INSERT INTO tbl_collect_type (cllct_type, cllct_name, reg_date, mod_date) VALUES (7, '현금 영수증', now(), now());

INSERT INTO tbl_version (version, dscrpt, url, reg_date, mod_date) VALUES ('1.0.2', '최신', '', now(), now());
