package com.pam.security;

public class Session {
	
	private String sessionId;
	private String currentStatus;
	private String serverName;
	private String certificate;
	private String serverRandom;
	private String clientRandom;
	private String secretKeyAlgorithm;
	private String publicKeyAlgorithm;
	private String hashAlgorithm;
	private String privateKey;
	private String sessionKey;
	private Integer timeout;

	public Session() {
		// TODO Auto-generated constructor stub
	}
	
	public String getSessionId() {
		return this.sessionId;
	}
	
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	public String getCurrentStatus() {
		return this.currentStatus;
	}
	
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	
	public String getServerName() {
		return this.serverName;
	}
	
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}
	
	public String getCertificate() {
		return this.certificate;
	}
	
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	
	public String getServerRandom() {
		return this.serverRandom;
	}
	
	public void setServerRandom(String serverRandom) {
		this.serverRandom = serverRandom;
	}
	
	public String getClientRandom() {
		return this.clientRandom;
	}
	
	public void setClientRandom(String clientRandom) {
		this.clientRandom = clientRandom;
	}
	
	public String getSecretKeyAlgorithm() {
		return this.secretKeyAlgorithm;
	}
	
	public void setSecretKeyAlgorithm(String secretKeyAlgorithm) {
		this.secretKeyAlgorithm = secretKeyAlgorithm;
	}
	
	public String getPublicKeyAlgorithm() {
		return this.publicKeyAlgorithm;
	}
	
	public void setPublicKeyAlgorithm(String publicKeyAlgorithm) {
		this.publicKeyAlgorithm = publicKeyAlgorithm;
	}
	
	public String getHashAlgorithm() {
		return this.hashAlgorithm;
	}
	
	public void setHashAlgorithm(String hashAlgorithm) {
		this.hashAlgorithm = hashAlgorithm;
	}
	
	public String getPrivateKey() {
		return this.privateKey;
	}
	
	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
	
	public String getSessionKey() {
		return this.sessionKey;
	}
	
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	
	public Integer getTimeout() {
		return this.timeout;
	}
	
	public void setTimeout(Integer timeout) {
		this.timeout = timeout;
	}
}

