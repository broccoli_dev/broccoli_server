package com.pam.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("/WEB-INF/setting.properties")
public class CouchbaseConfig {
	@Autowired
	Environment env;
	
    private String url;
    private String bucket;
    private String passwd;
    private String expire;
    
    @PostConstruct
    public void init() {
    	setUrl(env.getProperty("couchbase.url"));
    	setBucket(env.getProperty("couchbase.bucket"));
    	setPasswd(env.getProperty("couchbase.passwd"));
    	setExpire(env.getProperty("couchbase.expire"));
    }

	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getExpire() {
		return expire;
	}
	public void setExpire(String expire) {
		this.expire = expire;
	}
}