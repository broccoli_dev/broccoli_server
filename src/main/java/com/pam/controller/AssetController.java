package com.pam.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.entity.ReturnMessage;
import com.pam.mapper.domain.TblCar;
import com.pam.mapper.domain.TblRealEstate;
import com.pam.mapper.domain.TblStockTransaction;
import com.pam.service.AssetService;
import com.pam.util.HttpUtil;
import com.pam.util.Utils;

@Controller
@RequestMapping("/asset")
public class AssetController {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    AssetService assetService;
    	
    /**
     * 자산 메인 조회
     */
	@RequestMapping(value="/main",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getAssetMain(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				ret = assetService.getAssetMainInfo(userId);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}

    /**
     * 자산 기타 조회
     */
	@RequestMapping(value="/etc",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getAssetEtc(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				ret = assetService.getAssetEtcInfo(userId);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}

	/**
	 * 주식 리스트
	 */
	@RequestMapping(value="/stock/list",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getStockList(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				ret = assetService.getStockList(userId);	
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}

	/**
	 * 주식 내역 등록
	 */
	@RequestMapping(value="/stock/save",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object setStockInfo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String transId = (String) requestParam.get("transId");
		Integer transType = (Integer) requestParam.get("transType");
		String transDate = (String) requestParam.get("transDate");
		String stockCode = (String) requestParam.get("stockCode");
		String stockName = assetService.getStockNameFromCode(stockCode);
		String price = (String) requestParam.get("price");
		String amt = (String) requestParam.get("amount");
		String sum = (String) requestParam.get("sum");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(transId) || transType == null || Utils.isStringEmpty(transDate) 
				|| Utils.isStringEmpty(stockCode) || Utils.isStringEmpty(price) || Utils.isStringEmpty(amt) || Utils.isStringEmpty(sum)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				TblStockTransaction stockTransaction = new TblStockTransaction();
				stockTransaction.setUserId(userId);
				Long _transId = Long.parseLong(transId);
				stockTransaction.setTransId(_transId);
				stockTransaction.setTransType(transType);
				stockTransaction.setStockCode(stockCode);
				stockTransaction.setStockName(stockName);
				stockTransaction.setTransDate(transDate);
				Long _price = Long.parseLong(price);
				stockTransaction.setPrice(_price);
				Long _amt = Long.parseLong(amt);
				if(transType == 2) {
					_amt = -1 * _amt;
				}
				stockTransaction.setAmt(_amt);
				Long _sum = Long.parseLong(sum);
				stockTransaction.setSum(_sum);
				assetService.setStockTransInfo(stockTransaction);

				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("ok");	
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}

	/**
	 * 주식 내역 삭제
	 */
	@RequestMapping(value="/stock/delete",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object deleteStockInfo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String transId = (String) requestParam.get("transId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(transId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				TblStockTransaction stockTransaction = new TblStockTransaction();
				stockTransaction.setUserId(userId);
				Long _transId = Long.parseLong(transId);
				stockTransaction.setTransId(_transId);
				assetService.deleteStockTransInfo(stockTransaction);

				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("ok");	
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}

	/**
	 * 부동산 등록
	 */
	@RequestMapping(value="/real_estate/save",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object setRealEstateInfo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String estateId = (String) requestParam.get("estateId");
		String estateName = (String) requestParam.get("estateName");
		String estateType = (String) requestParam.get("estateType");
		String dealType = (String) requestParam.get("dealType");
		String price = (String) requestParam.get("price");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(estateId) || Utils.isStringEmpty(estateName) 
				|| Utils.isStringEmpty(estateType) || Utils.isStringEmpty(dealType) || Utils.isStringEmpty(price)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				TblRealEstate realEstate = new TblRealEstate();
				realEstate.setUserId(userId);
				Long _estateId = Long.parseLong(estateId);
				realEstate.setEstateId(_estateId);
				realEstate.setEstateName(estateName);
				realEstate.setEstateType(estateType);
				realEstate.setDealType(dealType);
				Long _price = Long.parseLong(price);
				realEstate.setPrice(_price);
				assetService.setRealEstateInfo(realEstate);

				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("ok");	
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}

	/**
	 * 부동산 삭제
	 */
	@RequestMapping(value="/real_estate/delete",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object deleteRealEstateInfo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String estateId = (String) requestParam.get("estateId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(estateId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				TblRealEstate realEstate = new TblRealEstate();
				realEstate.setUserId(userId);
				Long _estateId = Long.parseLong(estateId);
				realEstate.setEstateId(_estateId);
				assetService.deleteRealEstateInfo(realEstate);

				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("ok");	
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}

	/**
	 * 자동차 등록
	 */
	@RequestMapping(value="/car/save",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object setCarInfo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String carId = (String) requestParam.get("carId");
		String makerCode = (String) requestParam.get("makerCode");
		String carCode = (String) requestParam.get("carCode");
		String madeYear = (String) requestParam.get("madeYear");
		String subCode = (String) requestParam.get("subCode");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(carId) || Utils.isStringEmpty(makerCode) 
				|| Utils.isStringEmpty(carCode) || Utils.isStringEmpty(madeYear) || Utils.isStringEmpty(subCode)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				TblCar car = new TblCar();
				car.setUserId(userId);
				Long _carId = Long.parseLong(carId);
				car.setCarId(_carId);
				car.setMakerCode(makerCode);
				car.setCarCode(carCode);
				car.setMadeYear(madeYear);
				car.setSubCode(subCode);
				assetService.setCarInfo(car);

				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("ok");	
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}

	/**
	 * 자동차 삭제
	 */
	@RequestMapping(value="/car/delete",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object deleteCarInfo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String carId = (String) requestParam.get("carId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(carId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				TblCar car = new TblCar();
				car.setUserId(userId);
				Long _carId = Long.parseLong(carId);
				car.setCarId(_carId);
				assetService.deleteCarInfo(car);

				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("ok");	
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}

	/**
	 * 은행 거래 내역 상세 리스트
	 */
	@RequestMapping(value="/bank/detail",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getBankDetailInfo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String accountId = (String) requestParam.get("accountId");
		String startDate = (String) requestParam.get("startDate");
		String endDate = (String) requestParam.get("endDate");
		Integer mode = (Integer) requestParam.get("mode");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(accountId) || Utils.isStringEmpty(startDate) 
				|| Utils.isStringEmpty(endDate) || mode == null) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				ret = assetService.getBankDetailInfo(userId, accountId, startDate, endDate, mode);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}

	/**
	 * 카드 청구서 리스트
	 */
	@RequestMapping(value="/card/bill",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getCardBillList(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				ret = assetService.getCardBillList(userId);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}

	/**
	 * 카드 청구내역 상세
	 */
	@RequestMapping(value="/card/detail",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getCardBillDetailInfo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String companyCode = (String) requestParam.get("companyCode");
		String date = (String) requestParam.get("date");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(companyCode) || Utils.isStringEmpty(date)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				ret = assetService.getCardBillDetail(userId, companyCode, date);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			} 
		}
		
		return ret;
	}
 
	/**
	 * 주식 거래 내역
	 */
	@RequestMapping(value="/stock/detail",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getStockDetailInfo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String stockCode = (String) requestParam.get("stockCode");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(stockCode)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				ret = assetService.getStockDetailInfo(userId, stockCode);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}
}
