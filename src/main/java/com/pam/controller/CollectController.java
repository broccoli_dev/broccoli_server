package com.pam.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.entity.ReturnMessage;
import com.pam.mapper.domain.TblErrorLog;
import com.pam.queue.MQService;
import com.pam.service.CollectService;
import com.pam.util.HttpUtil;
import com.pam.util.Utils;

@Controller
@RequestMapping("/collect")
public class CollectController {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    CollectService collectService;
    
    @Autowired
	private MQService mqService;
	
    /**
     * 은행 리스트 
     */
	@RequestMapping(value="/bank/list",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object collectMyBankInfo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());
		
		Object ret = null;
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );

		mqService.send(requestParam);
					
		ret = new ReturnMessage();
		((ReturnMessage) ret).setCode("100");
		((ReturnMessage) ret).setDesc("성공");	
		
		return ret;
	}

	/**
	 * 은행 거래 내역
	 */
	@RequestMapping(value="/bank/trans",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object collectBankTransactionInfo(HttpServletRequest request, HttpServletResponse response) {		
		logger.info("request from {}", request.getRemoteHost());
		
		Object ret = null;
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		
		mqService.send(requestParam);
					
		ret = new ReturnMessage();
		((ReturnMessage) ret).setCode("100");
		((ReturnMessage) ret).setDesc("성공");	
		
		return ret;
	}

	/**
	 * 카드 승인 내역
	 */
	@RequestMapping(value="/card/approval",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object collectCardApprovalInfo(HttpServletRequest request, HttpServletResponse response) {		
		logger.info("request from {}", request.getRemoteHost());
		
		Object ret = null;		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );

		mqService.send(requestParam);
					
		ret = new ReturnMessage();
		((ReturnMessage) ret).setCode("100");
		((ReturnMessage) ret).setDesc("성공");	
		
		return ret;
	}

	/**
	 * 카드 청구서
	 */
	@RequestMapping(value="/card/bill",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object collectCardBillInfo(HttpServletRequest request, HttpServletResponse response) {		
		logger.info("request from {}", request.getRemoteHost());
		
		Object ret = null;		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );

		mqService.send(requestParam);
					
		ret = new ReturnMessage();
		((ReturnMessage) ret).setCode("100");
		((ReturnMessage) ret).setDesc("성공");	
		
		return ret;
	}

	/**
	 * 카드 한도
	 */
	@RequestMapping(value="/card/limit",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object collectCardLimitInfo(HttpServletRequest request, HttpServletResponse response) {		
		logger.info("request from {}", request.getRemoteHost());
		
		Object ret = null;		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );

		mqService.send(requestParam);
					
		ret = new ReturnMessage();
		((ReturnMessage) ret).setCode("100");
		((ReturnMessage) ret).setDesc("성공");	
		
		return ret;
	}

	/**
	 * 카드 결제 예정
	 */
	@RequestMapping(value="/card/schedule",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object collectCardScheduleInfo(HttpServletRequest request, HttpServletResponse response) {		
		logger.info("request from {}", request.getRemoteHost());
		
		Object ret = null;		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );

		mqService.send(requestParam);
					
		ret = new ReturnMessage();
		((ReturnMessage) ret).setCode("100");
		((ReturnMessage) ret).setDesc("성공");	
		
		return ret;
	}

	/**
	 * 현금 영수증
	 */
	@RequestMapping(value="/cash",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object collectMyCashInfo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());
		
		Object ret = null;		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		
		mqService.send(requestParam);
					
		ret = new ReturnMessage();
		((ReturnMessage) ret).setCode("100");
		((ReturnMessage) ret).setDesc("성공");	
		
		return ret;		
	}

	/**
	 * 스크래핑 에러 로그 수집
	 */
	@RequestMapping(value="/error",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object collectScrapingError(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());
		
		Object ret = null;		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );

		String userId = (String) requestParam.get("userId");
		String companyCode = (String) requestParam.get("companyCode");
		Object error = (Object) requestParam.get("error");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(companyCode) || error == null) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			String errorStr = Utils.toJsonString(error);
			if(errorStr != null && errorStr.length() > 499) {
				errorStr = errorStr.substring(0, 499);
			}

			Date now = Calendar.getInstance(Locale.KOREA).getTime();
			TblErrorLog errorLog = new TblErrorLog();
			errorLog.setUserId(userId);
			errorLog.setCmpnyCode(companyCode);
			errorLog.setErrDesc(Utils.toJsonString(error));
			errorLog.setRegDate(now);
			errorLog.setModDate(now);
			
			try {
				collectService.addErrorLog(errorLog);
				
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("성공");
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");
			}				
		}		
		
		return ret;		
	}
}
