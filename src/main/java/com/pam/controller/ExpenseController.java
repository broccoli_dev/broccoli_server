package com.pam.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.constant.Const;
import com.pam.entity.ReturnMessage;
import com.pam.mapper.domain.TblBudget;
import com.pam.mapper.domain.TblBudgetList;
import com.pam.mapper.domain.TblCard;
import com.pam.mapper.domain.TblExpense;
import com.pam.service.ExpenseService;
import com.pam.util.HttpUtil;
import com.pam.util.Utils;

@Controller
@RequestMapping("/expense")
public class ExpenseController {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    ExpenseService expenseService;

    /**
     * 소비 메인
     */
	@RequestMapping(value="/main", method = RequestMethod.POST, headers = "accept=*/*")
	public @ResponseBody Object getExpenseMainInfo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				ret = expenseService.getExpenseMainInfo(userId);
			} catch (MySQLIntegrityConstraintViolationException
					| RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error"); 
			}
		}

		return ret;
	}
    
    /**
     * 소비 상세 내역 조회
     */
	@RequestMapping(value="/detail", method = RequestMethod.POST, headers = "accept=*/*")
	public @ResponseBody Object getExpenseDetail(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String date = (String) requestParam.get("date");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(date)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				TblExpense expense = new TblExpense();
				expense.setUserId(userId);
				expense.setExpnsDate(date);
				expense.setCtgryCode(Const.CATEGORY_CODE_NOT_DEFINED.substring(0, 2)); // 미분류는 소비 합계에서 제외한다.
				ret = expenseService.getExpenseDetailList(expense);
			} catch (MySQLIntegrityConstraintViolationException
					| RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error"); 
			}
		}

		return ret;
	}

	/**
	 * 소비 등록/수정
	 */
	@RequestMapping(value="/save",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object addExpense(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );

		String userId = (String) requestParam.get("userId");
		String expenseId = (String) requestParam.get("expenseId");
		String expenseDate = (String) requestParam.get("expenseDate");
		String opponent = (String) requestParam.get("opponent");
		String categoryCode = (String) requestParam.get("categoryCode");
		String payType = (String) requestParam.get("payType");
		String editYn = (String) requestParam.get("editYn");
		String sum = (String) requestParam.get("sum");
		
		Date now = new Date();

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(expenseId) || Utils.isStringEmpty(expenseDate) || Utils.isStringEmpty(opponent)
				 || Utils.isStringEmpty(categoryCode) || Utils.isStringEmpty(payType) || Utils.isStringEmpty(editYn) || Utils.isStringEmpty(sum)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			// 예산 설정
			TblExpense expense = new TblExpense();
			expense.setExpnsId(Long.parseLong(expenseId));
			expense.setUserId(userId);
			expense.setExpnsDate(expenseDate);
			expense.setOppnnt(opponent);
			expense.setCtgryCode(categoryCode);
			expense.setPayType(payType);
			expense.setEditYn(editYn);
			expense.setSum(Long.parseLong(sum));
			expense.setCancelYn("N");
			expense.setUseYn("Y");
			expense.setRegDate(now);
			expense.setModDate(now);
							
			try {
				expenseService.addExpense(expense);

				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("성공");	
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error"); 
			} 
		}

		return ret;
	}

	/**
	 * 소비 내역 삭제
	 */
	@RequestMapping(value="/delete",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object delExpense(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );

		String userId = (String) requestParam.get("userId");
		String expenseId = (String) requestParam.get("expenseId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(expenseId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			// 예산 설정
			TblExpense expense = new TblExpense();
			expense.setExpnsId(Long.parseLong(expenseId));
			expense.setUserId(userId);
			expense.setUseYn("N");
							
			try {
				expenseService.deleteExpense(expense);

				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("성공");	
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error"); 
			} 
		}

		return ret;
	}

	/**
	 * 예산 / 소비 리스트 조회
	 */
	@RequestMapping(value="/budget/list",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getBudgetDetail(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String baseMonth = (String) requestParam.get("baseMonth");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(baseMonth)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				TblBudget budget = new TblBudget();
				budget.setUserId(userId);
				budget.setBaseMonth(baseMonth);
				ret = expenseService.getBudgetDetailList(budget);
			} catch (MySQLIntegrityConstraintViolationException
					| RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error"); 
			}
		}

		return ret;
	}
	
	/**
	 * 예산 등록/수정
	 */
	@RequestMapping(value="/budget/save",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object addBudget(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String budgetId = (String) requestParam.get("budgetId");
		String userId = (String) requestParam.get("userId");
		String baseMonth = (String) requestParam.get("baseMonth");
		String totalSum = (String) requestParam.get("totalSum");
		
		Date now = new Date();

		// 필수 요소 확인
		if(Utils.isStringEmpty(budgetId) || Utils.isStringEmpty(userId) || Utils.isStringEmpty(baseMonth) || Utils.isStringEmpty(totalSum)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			// 예산 설정
			TblBudget budget = new TblBudget();
			budget.setBdgtId(Long.parseLong(budgetId));
			budget.setUserId(userId);
			budget.setBaseMonth(baseMonth);
			budget.setTotalSum(Long.parseLong(totalSum));
			budget.setRegDate(now);
			budget.setModDate(now);
			
			@SuppressWarnings("unchecked")
			ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>>) requestParam.get("list");

			if(list != null && list.size() > 0) {
				// 예산 리스트
				List<TblBudgetList> budgetList = new ArrayList<TblBudgetList>();
	
				for(HashMap<String, String> map : list) {
					String categoryCode = map.get("categoryCode");
					String sum = map.get("sum");
					
					if(Utils.isStringEmpty(categoryCode) || Utils.isStringEmpty(sum)) {
						ret = new ReturnMessage();
						((ReturnMessage) ret).setCode("603");
						((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
						return ret;
					} else {	
						TblBudgetList budget_list = new TblBudgetList();
						budget_list.setLargeCtgryCode(categoryCode);
						budget_list.setSum(Long.parseLong(sum));
						budget_list.setRegDate(now);
						budget_list.setModDate(now);
						budgetList.add(budget_list);
					}
				}				
				
				try {
					expenseService.addBudget(budget, budgetList);

					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("100");
					((ReturnMessage) ret).setDesc("성공");	
				} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
					logger.info("error = {}", e.toString());
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("501");
					((ReturnMessage) ret).setDesc("DB access error"); 
				} 
			} else {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("603");
				((ReturnMessage) ret).setDesc("No LISTs.");					
			}
		}

		return ret;
	}

	/**
	 * 카테고리 별 소비 금액 및 비중 조회
	 */
	@RequestMapping(value="/category/list",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getExpenseCategoryList(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String date = (String) requestParam.get("date");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(date)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				TblExpense expense = new TblExpense();
				expense.setUserId(userId);
				expense.setExpnsDate(date);
				ret = expenseService.getExpenseCategoryList(expense);
			} catch (MySQLIntegrityConstraintViolationException
					| RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error"); 
			}
		}

		return ret;
	}

	/**
	 * 카테고리 별 소비 내역 조회
	 */
	@RequestMapping(value="/category/detail",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getExpenseCategoryDetail(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String categoryCode = (String) requestParam.get("categoryCode");
		String date = (String) requestParam.get("date");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(categoryCode) || Utils.isStringEmpty(date)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				TblExpense expense = new TblExpense();
				expense.setUserId(userId);
				expense.setCtgryCode(categoryCode);
				expense.setExpnsDate(date);
				ret = expenseService.getExpenseDetail(expense);
			} catch (MySQLIntegrityConstraintViolationException
					| RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error"); 
			}
		}

		return ret;
	}

	/**
	 * 소비 카드 리스트
	 */
	@RequestMapping(value="/card/list",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getExpenseCardListInfo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				ret = expenseService.getExpenseCardListInfo(userId);
			} catch (MySQLIntegrityConstraintViolationException
					| RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error"); 
			}
		}

		return ret;
	}

	/**
	 * 카드에 카드코드 저장
	 */
	@RequestMapping(value="/card/map",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object setCardCode(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String cardId = (String) requestParam.get("cardId");
		String cardCode = (String) requestParam.get("cardCode");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(cardId) || Utils.isStringEmpty(cardCode)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				TblCard card = new TblCard();
				card.setCardId(Long.parseLong(cardId));
				card.setUserId(userId);
				card.setCardCode(cardCode);
				Integer result = expenseService.setCardCode(card);
				
				if(result > 0) {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("100");
					((ReturnMessage) ret).setDesc("성공");						
				} else {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("503");
					((ReturnMessage) ret).setDesc("No such data.");						
				}
			} catch (MySQLIntegrityConstraintViolationException
					| RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error"); 
			}
		}

		return ret;
	}
}
