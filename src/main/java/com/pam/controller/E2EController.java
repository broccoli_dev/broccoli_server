package com.pam.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.pam.security.Contents;
import com.pam.security.E2EProtocol;
import com.pam.service.E2EService;

@Controller
@RequestMapping("/e2e")
public class E2EController {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    E2EService e2eService;
	
	@RequestMapping(value="/handshake",
			method = RequestMethod.POST,
			headers = "accept=application/json")
	public @ResponseBody void handshake(HttpServletRequest request, HttpServletResponse response) throws IOException {	
		ObjectMapper mapper = new ObjectMapper();
		E2EProtocol protocol = null;
		HttpSession session = request.getSession();
		String httpSessionId = session.getId();
		String remoteIp = request.getRemoteHost();
		logger.info("handshake from ip = {}, session_id = {}", remoteIp, httpSessionId);

		e2eService.setHttpSessionId(httpSessionId);
		
		try {
			protocol = mapper.readValue(request.getInputStream(), E2EProtocol.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		E2EProtocol e2e = new E2EProtocol();
		try {
			e2e = e2eService.doHandShake(protocol);
		} catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException
				| InvalidKeySpecException | NoSuchAlgorithmException | RuntimeException e) {
			// TODO Auto-generated catch block
			logger.info("Handshake error = {}", e.toString());
			Contents cont = new Contents();
			cont.setType("99"); // 오류 발생
			cont.setServerName("Decryption Error.");
			e2e.getContents().add(cont);
		} 

		Gson gson = new Gson();
		String ret = gson.toJson(e2e);
		
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.print(ret);
		out.flush();
	}	

	@RequestMapping(value="/check",
			method = RequestMethod.POST,
			headers = "accept=application/json")
	public @ResponseBody Object haveSession(HttpServletRequest request, HttpServletResponse response) throws IOException {		
		HttpSession session = request.getSession();
		String httpSessionId = session.getId();
		
		HashMap<String, String> ret = new HashMap<String, String>();
		if(e2eService.sessionExpired(httpSessionId)) {
			ret.put("code", "101");
			ret.put("desc", "expired");			
		} else {
			ret.put("code", "100");
			ret.put("desc", "ok");						
		}
		
		return ret;
	}
}