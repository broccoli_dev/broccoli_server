package com.pam.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.entity.ReturnMessage;
import com.pam.service.InvestService;
import com.pam.util.HttpUtil;
import com.pam.util.Utils;

@Controller
@RequestMapping("/invest")
public class InvestController {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    InvestService investService;

    /**
     * 투자 메인 정보를 가져온다.
     */
	@RequestMapping(value="/main",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getInvestMain(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId)");
		} else {			
			try {
				ret = investService.getInvestMain(userId);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");		
			}
		}
		
		return ret;
	}
}
