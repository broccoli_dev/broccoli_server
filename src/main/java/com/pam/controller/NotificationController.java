package com.pam.controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.entity.ReturnMessage;
import com.pam.mapper.domain.TblNotification;
import com.pam.service.NotificationService;
import com.pam.util.HttpUtil;
import com.pam.util.Utils;

@Controller
@RequestMapping("/notification")
public class NotificationController {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    NotificationService notificationService;

    /**
     * 알림 리스트를 가져온다.
     */
	@RequestMapping(value="/list",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getNotificationList(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId)");
		} else {			
			try {
				// 일단 알림 내역을 조회하여 등록한다.
//				notificationService.makeNotification(userId);
				ret = notificationService.getNotificationList(userId);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");		
			}
		}
		
		return ret;
	}

	/**
	 * 알림 내역을 읽음으로 설정한다.
	 */
	@RequestMapping(value="/read",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object setNotificationRead(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId)");
		} else {		
			@SuppressWarnings("unchecked")
			ArrayList<String> list = (ArrayList<String>) requestParam.get("list");
			Long rows = 0L;

			if(list != null && list.size() > 0) {
				try {
					rows = notificationService.setNotificationRead(userId, list);
				} catch (MySQLIntegrityConstraintViolationException
						| RuntimeException e) {
					// TODO Auto-generated catch block
					logger.info("error = {}", e.toString());
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("501");
					((ReturnMessage) ret).setDesc("DB access error");	
					return ret;
				}
				
				if(rows != 0L) {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("100");
					((ReturnMessage) ret).setDesc("ok");					
				} else {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("503");
					((ReturnMessage) ret).setDesc("No such data");							
				}
			} else {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("603");
				((ReturnMessage) ret).setDesc("No list.");
			}
		}
		
		return ret;
	}

	/**
	 * 알림 등록
	 */
	@RequestMapping(value="/add",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object addNotification(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String categoryCode = (String) requestParam.get("categoryCode");
		String message = (String) requestParam.get("message");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(categoryCode) || Utils.isStringEmpty(message)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId|categoryCode|message)");
		} else {		
			TblNotification notification = new TblNotification();
			notification.setUserId(userId);
			notification.setSmallCtgryCode(categoryCode);
			notification.setMsg(message);
			String date = Utils.getCurrentDate("yyyymmdd");
			String makeMonth = date.substring(0, 6);
			String makeDay = date.substring(6, 8);
			notification.setMakeMonth(makeMonth);
			notification.setMakeDay(makeDay);
			notification.setReadYn("N");
			Date now = Calendar.getInstance(Locale.KOREA).getTime();
			notification.setRegDate(now);
			notification.setModDate(now);
			
			try {
				notificationService.addNotification(notification);
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("ok");	
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");
			}
		}
		
		return ret;
	}
}
