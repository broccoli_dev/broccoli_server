package com.pam.controller;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.entity.CardInfo;
import com.pam.entity.ReturnMessage;
import com.pam.service.AdminInquiryService;
import com.pam.util.HttpUtil;
import com.pam.util.Utils;

@Controller
@RequestMapping("/admininquiry")
public class AdminInquiryController {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    AdminInquiryService adminInquiryService;
	
	/**
	 * 태그 목록 조회 (관리자 페이지 용)
	 */
	@RequestMapping(value="/tag/list",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getTag(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = adminInquiryService.getTag();
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 카테고리 목록 조회 (관리자 페이지 용)
	 */
	@RequestMapping(value="/category",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getCategory(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = adminInquiryService.getCategory();
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 태그 목록 업데이트 (관리자 페이지 용)
	 */
	@RequestMapping(value="/tag/save",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object saveTag(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				adminInquiryService.saveTag(requestParam);
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("OK");
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");
			}
		}
		return ret;		
	}
	
	/**
	 * 미분류 카테고리 목록 조회 (관리자 페이지 용)
	 */
	@RequestMapping(value="/noncategory",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object nonCategory(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = adminInquiryService.getNonCategory();
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;			
	}
	
	/**
	 * 미분류 항목 태그 추가 (관리자 페이지 용)
	 */
	@RequestMapping(value="/tag/addnoncategory",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object addNonCategoryTag(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		String tag = (String) requestParam.get("tag");
		String ctgryCode = (String) requestParam.get("ctgryCode");
		Integer isMoneyCalendar = (Integer) requestParam.get("isMoneyCalendar");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin) || Utils.isStringEmpty(tag) || Utils.isStringEmpty(ctgryCode) || isMoneyCalendar == null) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				adminInquiryService.addNonCategoryTag(tag,ctgryCode,isMoneyCalendar);
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("OK");
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");
			}
		}
		return ret;		
	}
	
	/**
	 * 모든 카드 리스트 검색 (관리자 페이지 용)
	 */
	@RequestMapping(value="/card/alllist",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getCardAllList(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = adminInquiryService.getCardAllList();
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 카드 삭제 (관리자 페이지 용)
	 */
	@RequestMapping(value="/card/delete",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object deleteCard(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		String cardCode = (String) requestParam.get("cardCode");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin) || Utils.isStringEmpty(cardCode)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				adminInquiryService.deleteCard(cardCode);
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("OK");
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 카드 회사,타입,브랜드,카테고리 코드 요청 (관리자 페이지 용)
	 */
	@RequestMapping(value="/card/codes",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getCardCodes(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = adminInquiryService.getCardCodes();
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 카드 전체 정보 요청 (관리자 페이지 용)
	 */
	@RequestMapping(value="/card/info",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getCardInfo(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		String cardCode = (String) requestParam.get("cardCode");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin) || Utils.isStringEmpty(cardCode)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = adminInquiryService.getCardInfo(cardCode);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 카드 추가,업데이트 (관리자 페이지 용)
	 */
	@RequestMapping(value="/card/save",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object saveCard(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		String cardCode = (String) requestParam.get("cardCode");
		Gson gson = new Gson();
		CardInfo cardData = gson.fromJson(gson.toJson(requestParam.get("cardData")), CardInfo.class);
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin) || Utils.isStringEmpty(cardCode) || cardData == null) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				adminInquiryService.saveCard(cardCode, cardData);
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("OK");
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");
			}
		}
		return ret;		
	}
	
	/**
	 * 스토어 이름-카테고리 매치 검색 (관리자 페이지 용)
	 */
	@RequestMapping(value="/storematch",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getStoreNameMatchCategory(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		String storeName = (String) requestParam.get("storeName");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin) || Utils.isStringEmpty(storeName)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = adminInquiryService.getStoreNameMatchCategory(storeName);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 스토어 삭제(관리자 페이지 용)
	 */
	@RequestMapping(value="/store/delete",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object deleteStore(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		String storeName = (String) requestParam.get("storeName");
		String storeTel = (String) requestParam.get("storeTel");
		String source = (String) requestParam.get("source");
		String categoryRaw = (String) requestParam.get("categoryRaw");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin) || Utils.isStringEmpty(storeName) || Utils.isStringEmpty(storeTel) ||
				Utils.isStringEmpty(source) || Utils.isStringEmpty(categoryRaw)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				adminInquiryService.deleteStore(storeName, storeTel, source, categoryRaw);
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("OK");
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 관리자 리스트 조회 (관리자 페이지 용)
	 */
	@RequestMapping(value="/admin/list",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getAdminList(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = adminInquiryService.getAdminList();
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 관리자 계정 추가 (관리자 페이지 용)
	 */
	@RequestMapping(value="/admin/add",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object addAdminAccount(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		String adminId = (String) requestParam.get("adminId");
		String adminAuth = (String) requestParam.get("adminAuth");
		String password = (String) requestParam.get("password");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin) || Utils.isStringEmpty(adminId) || Utils.isStringEmpty(adminAuth) || Utils.isStringEmpty(password)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else if (adminAuth.equals("S")){
			// 루트 관리자 계정 ('S'권한)은 오직 1개만 존재. 추가 불가.
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("601");
			((ReturnMessage) ret).setDesc("Improper input value");
		}
		else{
			try{
				adminInquiryService.addAdminAccount(adminId,adminAuth,password);
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("OK");
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("Password hashing error");
			}
		}
		return ret;		
	}
	
	/**
	 * 관리자 계정 삭제(관리자 페이지 용)
	 */
	@RequestMapping(value="/admin/delete",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object deleteAdminAccount(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		String adminId = (String) requestParam.get("adminId");
		String adminAuth = (String) requestParam.get("adminAuth");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin) || Utils.isStringEmpty(adminId) || Utils.isStringEmpty(adminAuth)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else if (adminAuth.equals("S")){
			// 루트 관리자 계정 ('S'권한)은 삭제 불가.
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("601");
			((ReturnMessage) ret).setDesc("Improper input value");
		} else {
			try{
				adminInquiryService.deleteAdminAccount(adminId, adminAuth);
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("OK");
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
}
