package com.pam.controller;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.entity.ReturnMessage;
import com.pam.manager.scraping.ScrapingUtil;
import com.pam.mapper.domain.TblUser;
import com.pam.security.PasswordHash;
import com.pam.service.E2EService;
import com.pam.service.UserService;
import com.pam.util.HttpUtil;
import com.pam.util.Utils;

@Controller
@RequestMapping("/user")
public class UserController {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    UserService userService;

    @Autowired
    E2EService e2eService;
	
    /**
     * 사용자 등록
     */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/add",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object insertUser(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userName = (String) requestParam.get("userName");
		String birthDate = (String) requestParam.get("birthDate");
		String gender = (String) requestParam.get("gender");
		String marriedYn = (String) requestParam.get("marriedYn");
		String password = (String) requestParam.get("password");
//		String deviceKey = (String) requestParam.get("device_key");
		String pushYn = (String) requestParam.get("pushYn");
		String yellopassId = (String) requestParam.get("yellopassId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userName) || Utils.isStringEmpty(birthDate) || Utils.isStringEmpty(gender) || Utils.isStringEmpty(marriedYn) || Utils.isStringEmpty(password)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			Date now = Calendar.getInstance(Locale.KOREA).getTime();
			
			TblUser user = new TblUser();
			/*String encryptedName = "";
			
			String key = ByteUtil.byteArrayToHex(SecureUtil.genSaltFromDate(now));
			encryptedName = SecureUtil.encryptString(userName, key);*/
			
			String passHash = "";
			
			try {
				passHash = PasswordHash.createHash(password, now);
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			user.setName(userName);
			user.setPassword(passHash);
			user.setBirthDate(birthDate);
			user.setGender(gender);
			user.setMrrdYn(marriedYn);
			user.setRegDate(now);
			user.setModDate(now);
			user.setPushYn(pushYn);
			if(Utils.isStringEmpty(pushYn)){
				user.setPushYn("N");
			}
			if(!Utils.isStringEmpty(yellopassId)){
				user.setYellopassId(Long.parseLong(yellopassId));
			}
			
			String userId = "";
			
			try {
				userId = userService.addUser(user);
				
				ret = new HashMap<String, String>();
				((HashMap<String, String>) ret).put("userId", userId);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}
	
	/**
     * 사용자 아이디 조회
     */
	@RequestMapping(value="/userid",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getUserId(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String yellopassId = (String) requestParam.get("yellopassId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(yellopassId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				ret = userService.getUserId(yellopassId);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");
			}
		}
		
		return ret;
	}
	
	/**
     * 사용자 정보 요청
     */
	@RequestMapping(value="/info",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getUser(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				ret = userService.getUserInfo(userId);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");
			}
		}
		
		return ret;
	}
	
	/**
     * 푸쉬 사용여부 수정
     */
	@RequestMapping(value="/push",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object setPush(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String pushYn = (String) requestParam.get("pushYn");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(pushYn) ) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				Integer result = userService.setPush(userId, pushYn);
				if(result == 1) {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("100");
					((ReturnMessage) ret).setDesc("ok");
				} else {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("503");
					((ReturnMessage) ret).setDesc("No such user");					
				}
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");
			}
		}
		
		return ret;
	}

	/**
	 * 사용자 정보 수정
	 */
	@RequestMapping(value="/modify",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object modifyUser(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String userName = (String) requestParam.get("userName");
		String birthDate = (String) requestParam.get("birthDate");
		String gender = (String) requestParam.get("gender");
		String marriedYn = (String) requestParam.get("marriedYn");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(userName) || Utils.isStringEmpty(birthDate) || Utils.isStringEmpty(gender) || Utils.isStringEmpty(marriedYn)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			TblUser user = new TblUser();
			user.setUserId(userId);
			user.setName(userName);
			user.setBirthDate(birthDate);
			user.setGender(gender);
			user.setMrrdYn(marriedYn);
						
			try {
				Integer result = userService.setUser(user);
				
				if(result == 1) {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("100");
					((ReturnMessage) ret).setDesc("ok");
				} else {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("503");
					((ReturnMessage) ret).setDesc("No such user");					
				}
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}

	/**
	 * 비밀번호 수정
	 */
	@RequestMapping(value="/password/change",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object changePassword(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String password = (String) requestParam.get("password");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(password)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			TblUser user = new TblUser();
			user.setUserId(userId);
			user.setPassword(password);
						
			try {
				Integer result = userService.setPassword(user);
				
				if(result == 1) {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("100");
					((ReturnMessage) ret).setDesc("ok");
				} else {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("503");
					((ReturnMessage) ret).setDesc("No such user");					
				}
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}

	/**
	 * 로그인
	 */
	@RequestMapping(value="/login",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody void login(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;

		// request에서 암호화 여부에 따라 json string을 읽어온다.
		String recvJson = e2eService.getJsonStringFromHttpServlet(request);
		
		if(recvJson == null) {
			/*ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("101");
			((ReturnMessage) ret).setDesc("No session or expired");	*/
		} else {
			HashMap<String, Object> map = Utils.getMapFromJsonString(recvJson);		
			
			String userId = (String) map.get("userId");
			String password = (String) map.get("password");
			Integer type = (Integer) map.get("type");
			String pushKey = (String) map.get("pushKey");
	
			// 필수 요소 확인
			if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(password) || (type == null) || Utils.isStringEmpty(pushKey)) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("603");
				((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
			} else {
				TblUser user = new TblUser();
				user.setUserId(userId);
				user.setPassword(password);
				user.setOsType(type);
				user.setPushKey(pushKey);
				
				try {
					try {
						if(userService.authUser(user)) {
							ret = new ReturnMessage();
							((ReturnMessage) ret).setCode("100");
							((ReturnMessage) ret).setDesc("ok");
						} else {
							ret = new ReturnMessage();
							((ReturnMessage) ret).setCode("503");
							((ReturnMessage) ret).setDesc("Invalid user id or password");							
						}
					} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
						// TODO Auto-generated catch block
						logger.info("login error1 = {}", e.toString());
						ret = new ReturnMessage();
						((ReturnMessage) ret).setCode("501");
						((ReturnMessage) ret).setDesc("DB access error");	
					}
				} catch (MySQLIntegrityConstraintViolationException | RuntimeException e1) {
					// TODO Auto-generated catch block
					logger.info("login error2 = {}", e1.toString());
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("501");
					((ReturnMessage) ret).setDesc("DB access error");
				}
			}
		}

		e2eService.sendData(request, response, ret);
	}

	@RequestMapping(value="/company/delete",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object deleteCompany(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String companyType = (String) requestParam.get("companyType");
		String companyCode = (String) requestParam.get("companyCode");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(companyType)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				if(ScrapingUtil.checkCompanyTypeCodes(companyType, companyCode)) {
					userService.deleteCompanyData(userId, companyType, companyCode);
					
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("100");
					((ReturnMessage) ret).setDesc("ok");
				} else {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("601");
					((ReturnMessage) ret).setDesc("Company type or code is incorrect.");			
				}
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}
	
	/**
     * 이름으로 사용자 아이디 조회
     */
	@RequestMapping(value="/decryptuser",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getDecryptUser(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userName = (String) requestParam.get("userName");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userName)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				ret = userService.getDecryptUser(userName);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");
			}
		}
		
		return ret;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value="/key",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody void getSecretKey(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;

		// request에서 암호화 여부에 따라 json string을 읽어온다.
		String recvJson = e2eService.getJsonStringFromHttpServlet(request);
		
		if(recvJson == null) {
			/*ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("101");
			((ReturnMessage) ret).setDesc("No session or expired");	*/
		} else {
			HashMap<String, Object> map = Utils.getMapFromJsonString(recvJson);		
			String userId = (String) map.get("userId");

			// 필수 요소 확인
			if(Utils.isStringEmpty(userId)) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("603");
				((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
			} else {
				try {
					String key = userService.getSecretKey(userId);
					
					if(key.equals("")) {
						ret = new ReturnMessage();
						((ReturnMessage) ret).setCode("503");
						((ReturnMessage) ret).setDesc("No such user.");						
					} else {
						ret = new HashMap<String, String>();
						((HashMap<String, String>) ret).put("key", key);
					}
				} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
					// TODO Auto-generated catch block
					logger.info("error = {}", e.toString());
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("501");
					((ReturnMessage) ret).setDesc("DB access error");
				} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
					// TODO Auto-generated catch block
					logger.info("error = {}", e.toString());
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("902");
					((ReturnMessage) ret).setDesc("Security error");
				}
			}
		}

		e2eService.sendData(request, response, ret);
	}
	
	/**
     * 회원 탈퇴 (사용자 삭제)
     */
	@RequestMapping(value="/deleteuser",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object deleteUser(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				userService.deleteUser(userId);
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("OK");
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");
			}
		}
		
		return ret;
	}
}
