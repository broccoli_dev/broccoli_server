package com.pam.controller;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.entity.ReturnMessage;
import com.pam.entity.UserDB;
import com.pam.mapper.TblAdminMapper;
import com.pam.mapper.domain.TblAdmin;
import com.pam.security.PasswordHash;
import com.pam.service.AdminInquiryService;
import com.pam.util.HttpUtil;
import com.pam.util.Utils;

@Controller
@RequestMapping("/admin")
public class AdminController {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    TblAdminMapper adminMapper;
    
    @Autowired
    AdminInquiryService adminInquiryService;
    
	@RequestMapping(value={"","/"},
			method = RequestMethod.GET,
			headers = "accept=*/*")
	public String admin(HttpServletRequest request, HttpServletResponse response) {
		return "redirect:/pam/admin/home";
	}
	
	@RequestMapping(value={"/system","/system/"},
			method = RequestMethod.GET,
			headers = "accept=*/*")
	public String system(HttpServletRequest request, HttpServletResponse response) {
		return "redirect:/pam/admin/system/store";
	}
	
	/**
     * 관리자 로그인 체크
     */
	@RequestMapping(value={"/logincheck","/logincheck/"},
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public String loginCheck(HttpServletRequest request) {
		String returnURL = "redirect:/pam/admin/login";
		
		String inputID = request.getParameter("inputID");
		String inputPW = request.getParameter("inputPW");
		
		TblAdmin admin = adminMapper.selectByPrimaryKey(inputID);
		
		if (admin != null){
			String checkPW = admin.getPassword();
			Date regDate = admin.getRegDate();
			try {
				if( PasswordHash.validatePassword(inputPW, checkPW, regDate) ){
					//로그인 성공, admin이라는 session key 생성
			        request.getSession().setAttribute("admin", inputID);
			        returnURL = "redirect:/pam/admin/home";
				}
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
	    return returnURL;
	}
	
	/**
     * 관리자 로그아웃 처리
     */
	@RequestMapping(value={"/logout","/logout/"},
			method = RequestMethod.GET,
			headers = "accept=*/*")
	public String logout(HttpServletRequest request) {
		request.getSession().setAttribute("admin", null);
	    return "redirect:/pam/admin/login";
	}
	
	/**
	 * 관리자 비밀번호 변경
	 */
	@RequestMapping(value={"/changepw","/changepw/"},
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object addNonCategoryTag(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String admin = (String) requestParam.get("admin");
		String nowPW = (String) requestParam.get("nowPW");
		String newPW = (String) requestParam.get("newPW");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(admin) || Utils.isStringEmpty(nowPW) || Utils.isStringEmpty(newPW)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				String adminID = (String) request.getSession().getAttribute("admin");
				TblAdmin adminData = adminMapper.selectByPrimaryKey(adminID);
				
				String checkPW = adminData.getPassword();
				Date regDate = adminData.getRegDate();
				
				try {
					if( PasswordHash.validatePassword(nowPW, checkPW, regDate) ){
						String newPassHash= PasswordHash.createHash(newPW, regDate);
						Date now = Calendar.getInstance(Locale.KOREA).getTime();
						adminData.setPassword(newPassHash);
						adminData.setModDate(now);
						adminMapper.updateByPrimaryKeySelective(adminData);
						
						ret = new ReturnMessage();
						((ReturnMessage) ret).setCode("100");
						((ReturnMessage) ret).setDesc("OK");
					}
					else {
						ret = new ReturnMessage();
						((ReturnMessage) ret).setCode("701");
						((ReturnMessage) ret).setDesc("Incorrect account");
					}
				} catch (NoSuchAlgorithmException | InvalidKeySpecException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} catch (RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");
			}
		}
		return ret;		
	}
	
	
	/**
     * 로그인 화면
     */
	@RequestMapping(value={"/login","/login/"},
			method = RequestMethod.GET,
			headers = "accept=*/*")
	public String login(HttpServletRequest request, HttpServletResponse response) {
		return "login";
	}
    	
    /**
     * 메인 화면
     */
	@RequestMapping(value={"/home","/home/"},
			method = RequestMethod.GET,
			headers = "accept=*/*")
	public ModelAndView home(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());
		Map<String,Object> model = new HashMap<String,Object>();
		model.put("admin_id", request.getSession().getAttribute("admin"));
		
		return new ModelAndView("home", model);
	}
	
	/**
     * 태그 관리 화면
     */
	@RequestMapping(value={"/system/tag","/system/tag/"},
			method = RequestMethod.GET,
			headers = "accept=*/*")
	public String tag(HttpServletRequest request, HttpServletResponse response) {
		return "tag";
	}
	
	/**
     * 스토어 관리 화면
     */
	@RequestMapping(value={"/system/store","/system/store/"},
			method = RequestMethod.GET,
			headers = "accept=*/*")
	public String store(HttpServletRequest request, HttpServletResponse response) {
		return "store";
	}
	
	/**
     * 미분류 카테고리 관리 화면
     */
	@RequestMapping(value={"/system/ncategory","/system/ncategory/"},
			method = RequestMethod.GET,
			headers = "accept=*/*")
	public String ncategory(HttpServletRequest request, HttpServletResponse response) {
		return "ncategory";
	}
	
	/**
     * 카드 혜택 관리 화면
     */
	@RequestMapping(value={"/system/cardbenefit","/system/cardbenefit/"},
			method = RequestMethod.GET,
			headers = "accept=*/*")
	public String cardbenefit(HttpServletRequest request, HttpServletResponse response) {
		return "cardbenefit";
	}
	
	/**
     * 카드 혜택 관리 화면
     */
	@RequestMapping(value={"/system/cardbenefit/{cardCode}","/system/cardbenefit/{cardCode}/"},
			method = RequestMethod.GET,
			headers = "accept=*/*")
	public ModelAndView cardbenefitEdit(HttpServletRequest request, HttpServletResponse response, @PathVariable String cardCode) {
		logger.info("{}", cardCode);
		Map<String,Object> model = new HashMap<String,Object>();
		model.put("cardCode", cardCode);
		return new ModelAndView("cardbenefitEdit", model);
	}
	
	/**
     * 회원 DB 출력 화면
     */
	@RequestMapping(value={"/system/userdb","/system/userdb/"},
			method = RequestMethod.GET,
			headers = "accept=*/*")
	public String userdb(HttpServletRequest request, HttpServletResponse response) {
		String adminID = (String) request.getSession().getAttribute("admin");
		TblAdmin adminData = adminMapper.selectByPrimaryKey(adminID);
		if (!(adminData.getAuthCode().equals("S") || adminData.getAuthCode().equals("A"))){
			return "notAuth";
		}
		
		return "userdb";
	}
	
	/**
     * 엑셀파일 다운로드
     */
	@RequestMapping(value="/system/userdb/excel",
			method = RequestMethod.GET,
			headers = "accept=*/*")
	public ModelAndView userdbExcelGET(HttpServletRequest request, HttpServletResponse response) {
		String adminID = (String) request.getSession().getAttribute("admin");
		TblAdmin adminData = adminMapper.selectByPrimaryKey(adminID);
		if (!(adminData.getAuthCode().equals("S") || adminData.getAuthCode().equals("A"))){
			ModelAndView mav = new ModelAndView("notAuth", null);
			return mav;
		}
		
		Map<String,List<UserDB>> model = new HashMap<String,List<UserDB>>();
		
		String minAge = (String) request.getParameter("minAge");
		String maxAge = (String) request.getParameter("maxAge");
		String sex = (String) request.getParameter("sex");
		String married = (String) request.getParameter("married");
		logger.info("{} {} {} {}", minAge, maxAge, sex, married);
		
		HashMap<String,String> map = new HashMap<String,String>();
		if (!minAge.equals("ALL")){
			map.put("minAge", minAge);
		}
		if (!maxAge.equals("ALL")){
			map.put("maxAge", maxAge);
		}
		if (!sex.equals("ALL")){
			map.put("sex", sex);
		}
		if (!married.equals("ALL")){
			map.put("married", married);
		}
		
		try {
			List<UserDB> users = adminInquiryService.getUserDB(map);
			model.put("users", users);
		} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ModelAndView mav = new ModelAndView("userExcelView", model);
		return mav;
	}
	
	/**
     * 관리자 계정 관리 화면
     */
	@RequestMapping(value={"/system/adminaccount","/system/adminaccount/"},
			method = RequestMethod.GET,
			headers = "accept=*/*")
	public String adminAccount(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());
		String adminID = (String) request.getSession().getAttribute("admin");
		TblAdmin adminData = adminMapper.selectByPrimaryKey(adminID);
		if (!adminData.getAuthCode().equals("S")){
			return "notAuth";
		}
		
		return "adminAccount";
	}
}
