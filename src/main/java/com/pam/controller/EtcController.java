package com.pam.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.entity.ReturnMessage;
import com.pam.manager.scraping.ScrapingUtil;
import com.pam.mapper.domain.TblChallenge;
import com.pam.mapper.domain.TblMoneyCalendar;
import com.pam.service.EtcService;
import com.pam.util.HttpUtil;
import com.pam.util.Utils;

@Controller
@RequestMapping("/etc")
public class EtcController {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    EtcService etcService;

	@RequestMapping(value="/main",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getEtcMain(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId)");
		} else {			
			try {
				ret = etcService.getEtcMain(userId);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");		
			}
		}
		
		return ret;
	}

	/**
	 * 머니 캘린더 리스트 조회
	 */
	@RequestMapping(value="/calendar/list",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getMoneyCalendarList(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String date = (String) requestParam.get("date");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(date)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId|date)");
		} else {			
			try {
				ret = etcService.getMonthlyMoneyCalendarList(userId, date);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");		
			}
		}
		
		return ret;
	}

	/**
	 * 머니 캘린더 은행 거래 내역 조회
	 */
	@RequestMapping(value="/calendar/transaction",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getMoneyCalendarTransactionList(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId)");
		} else {			
			try {
				ret = etcService.getMoneyCalendarTransactionList(userId);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");		
			}
		}
		
		return ret;
	}


    /**
     * 머니 캘린더 등록/수정
     */
	@RequestMapping(value="/calendar/save",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object addMoneyCalendar(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String calendarId = (String) requestParam.get("calendarId");
		String title = (String) requestParam.get("title");
		String opponent = (String) requestParam.get("opponent");
		String amt = (String) requestParam.get("amt");
		String payType = (String) requestParam.get("payType");
		String payDate = (String) requestParam.get("payDate");
		String repeatType = (String) requestParam.get("repeatType");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(calendarId) || Utils.isStringEmpty(title) // || Utils.isStringEmpty(transactionId) 
				|| Utils.isStringEmpty(amt) || Utils.isStringEmpty(payType) || Utils.isStringEmpty(payDate) || Utils.isStringEmpty(repeatType)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId|calendarId|title|transactionId|amt|payType|payDate|repeatType)");
		} else {
			String useYn = "Y";
			String expectYn = "Y"; // 수동입력 머니캘린더의 경우 기본 "예상"
			TblMoneyCalendar moneyCalendar = new TblMoneyCalendar();
			moneyCalendar.setCalId(Long.parseLong(calendarId));
			moneyCalendar.setUserId(userId);
			moneyCalendar.setTitle(title);
			moneyCalendar.setOppnnt(opponent);
			moneyCalendar.setAmt(ScrapingUtil.getLongValueFromString(amt));
			moneyCalendar.setPayType(payType);
			moneyCalendar.setPayDate(payDate);
			moneyCalendar.setPayDay(payDate.substring(6, 8));
			moneyCalendar.setRptType(repeatType);
			moneyCalendar.setUseYn(useYn);
			moneyCalendar.setExptYn(expectYn);
			
			try {
				etcService.setMoneyCalendar(moneyCalendar);
				
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("ok");
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");		
			}
		}
		
		return ret;
	}

	/**
	 * 머니캘린더 삭제
	 */
	@RequestMapping(value="/calendar/delete",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object deleteMoneyCalendar(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String calendarId = (String) requestParam.get("calendarId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(calendarId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId|calendarId)");
		} else {
			try {
				Integer rows = etcService.deleteMoneyCaleldar(userId, Long.parseLong(calendarId));
				
				if(rows > 0) {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("100");
					((ReturnMessage) ret).setDesc("ok");
				} else {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("503");
					((ReturnMessage) ret).setDesc("No such data");					
				}
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");		
			}
		}
		
		return ret;
	}

    /**
     * 챌린지 리스트 조회
     * @param request
     * @param response
     * @return
     */
	@RequestMapping(value="/challenge/list",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getChallengeList(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId)");
		} else {			
			try {
				ret = etcService.getChallengeList(userId);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");		
			}
		}
		
		return ret;
	}

	/**
	 * 챌린지 은행 정보 조회
	 */
	@RequestMapping(value="/challenge/bank",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getChallengeBankList(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		Integer accountType = (Integer) requestParam.get("accountType");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || accountType == null) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId|accountType)");
		} else {			
			try {
				ret = etcService.getChallengeBankAccount(userId, accountType);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");		
			}
		}
		
		return ret;
	}
	
    /**
     * 챌린지를 등록/수정 한다.
     */
	@RequestMapping(value="/challenge/save",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object addChallenge(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String challengeId = (String) requestParam.get("challengeId");
		String challengeType = (String) requestParam.get("challengeType");
		String title = (String) requestParam.get("title");
		String goalDate = (String) requestParam.get("goalDate");
		String accountId = (String) requestParam.get("accountId");
		String goalAmt = (String) requestParam.get("goalAmt");
		String monthlyAmt = (String) requestParam.get("monthlyAmt");
		String imagePath = (String) requestParam.get("imagePath");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(challengeId) || Utils.isStringEmpty(challengeType) || Utils.isStringEmpty(title) || Utils.isStringEmpty(goalDate)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId|challengeId|challengeType|title|goalDate)");
		} else {
			TblChallenge challenge = new TblChallenge();
			challenge.setChlngId(Long.parseLong(challengeId));
			challenge.setUserId(userId);
			challenge.setChlngType(challengeType);
			challenge.setTitle(title);
			challenge.setGoalDate(goalDate);
			challenge.setCmpltYn("N");
			challenge.setImagePath(imagePath);
			
			if(!Utils.isStringEmpty(accountId)) {
				challenge.setAccntId(Long.parseLong(accountId));
			}
			if(!Utils.isStringEmpty(goalAmt)) {
				challenge.setGoalAmt(ScrapingUtil.getLongValueFromString(goalAmt));
			}
			if(!Utils.isStringEmpty(monthlyAmt)) {
				challenge.setMonthlyAmt(ScrapingUtil.getLongValueFromString(monthlyAmt));
			}
			
			try {
				etcService.setChallenge(challenge);
				
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("100");
				((ReturnMessage) ret).setDesc("ok");
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");		
			}
		}
		
		return ret;
	}

	/**
	 * 챌린지 삭제
	 */
	@RequestMapping(value="/challenge/delete",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object deleteChallenge(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String challengeId = (String) requestParam.get("challengeId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(challengeId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId|challengeId)");
		} else {
			TblChallenge challenge = new TblChallenge();
			challenge.setChlngId(Long.parseLong(challengeId));
			challenge.setUserId(userId);
			
			try {
				Integer rows = etcService.deleteChallenge(challenge);

				if(rows > 0) {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("100");
					((ReturnMessage) ret).setDesc("ok");
				} else {
					ret = new ReturnMessage();
					((ReturnMessage) ret).setCode("503");
					((ReturnMessage) ret).setDesc("No such data");					
				}
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");		
			}
		}
		
		return ret;
	}
}
