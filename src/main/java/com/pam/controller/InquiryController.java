package com.pam.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.entity.ReturnMessage;
import com.pam.service.InquiryService;
import com.pam.service.PushService;
import com.pam.util.HttpUtil;
import com.pam.util.Utils;

@Controller
@RequestMapping("/inquiry")
public class InquiryController {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    InquiryService inquiryService;
    
    @Autowired
    PushService pushService;

	@RequestMapping(value="/version",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getClientLatestVersion(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		try {
			ret = inquiryService.getClientLatestVersion();
		} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
			// TODO Auto-generated catch block
			logger.info("error = {}", e.toString());
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("501");
			((ReturnMessage) ret).setDesc("DB access error");		
		}
		
		return ret;
	}
	
    /**
     * 종류별로 최종 성공 했던 uploadId를 return한다.
     */
	@RequestMapping(value="/success/latest",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object insertUser(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId)");
		} else {			
			try {
				ret = inquiryService.getCollectLogInfo(userId);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");		
			}
		}
		
		return ret;
	}

	/**
	 * uploadId로 성공 여부 조회
	 */
	@RequestMapping(value="/success",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object isSuccessOrError(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String uploadId = (String) requestParam.get("uploadId");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)|| Utils.isStringEmpty(uploadId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.(userId|uploadId)");
		} else {			
			try {
				ret = inquiryService.getCollectResult(userId, uploadId);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");		 
			}
		}
		
		return ret;
	}


	/**
	 * 주식 거래 내역
	 */
	@RequestMapping(value="/stock/find",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getStockCode(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String keyWord = (String) requestParam.get("keyWord");

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(keyWord)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try {
				ret = inquiryService.getStockMap(keyWord);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");	
			}
		}
		
		return ret;
	}
	
	/**
	 * 카드사 코드로 카드 리스트 검색
	 */
	@RequestMapping(value="/card/list",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getCardList(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String companyCode = (String) requestParam.get("companyCode");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(companyCode)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = inquiryService.getCardList(userId, companyCode);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 카드 혜택 조회
	 */
	@RequestMapping(value="/card/benefit",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getCardBenefit(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String cardCode = (String) requestParam.get("cardCode");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(cardCode)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = inquiryService.getCardBenefit(userId, cardCode);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 차량 제조사 조회
	 */
	@RequestMapping(value="/car/maker",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getCarMaker(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(userId)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = inquiryService.getCarMakerList(userId);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 차량 대표차명 조회
	 */
	@RequestMapping(value="/car/model",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getCarModel(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String makerCode = (String) requestParam.get("makerCode");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(makerCode)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = inquiryService.getCarModelList(userId, makerCode);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 차량 세부차명 조회
	 */
	@RequestMapping(value="/car/submodel",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getCarSubModel(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String makerCode = (String) requestParam.get("makerCode");
		String carCode = (String) requestParam.get("carCode");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(makerCode) || Utils.isStringEmpty(carCode)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = inquiryService.getCarSubModelList(userId, makerCode, carCode);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 차량 연식 조회
	 */
	@RequestMapping(value="/car/year",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getCarMadeYear(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String makerCode = (String) requestParam.get("makerCode");
		String carCode = (String) requestParam.get("carCode");
		String subCode = (String) requestParam.get("subCode");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(makerCode) || Utils.isStringEmpty(carCode) || Utils.isStringEmpty(subCode)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = inquiryService.getCarMadeYearList(userId, makerCode, carCode, subCode);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}
	
	/**
	 * 차량 시세 조회
	 */
	@RequestMapping(value="/car/marketprice",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getCarMarketPrice(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());	
		Object ret = null;
		
		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		String makerCode = (String) requestParam.get("makerCode");
		String carCode = (String) requestParam.get("carCode");
		String subCode = (String) requestParam.get("subCode");
		String madeYear = (String) requestParam.get("madeYear");
		
		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(makerCode) || Utils.isStringEmpty(carCode)
				|| Utils.isStringEmpty(subCode) || Utils.isStringEmpty(madeYear)) {
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("603");
			((ReturnMessage) ret).setDesc("Some essential parameters are empty.");
		} else {
			try{
				ret = inquiryService.getCarMarketPrice(userId, makerCode, carCode, subCode, madeYear);
			} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
				logger.info("error = {}", e.toString());
				ret = new ReturnMessage();
				((ReturnMessage) ret).setCode("501");
				((ReturnMessage) ret).setDesc("DB access error");					
			}
		}
		
		return ret;		
	}

	/**
	 * 긴급 공지 조회
	 */
	@RequestMapping(value="/notice/emergency",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getEmergencyNotice(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		try {
			ret = inquiryService.getEmergencyNotice();
		} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
			// TODO Auto-generated catch block
			logger.info("error = {}", e.toString());
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("501");
			((ReturnMessage) ret).setDesc("DB access error");		
		}
		
		return ret;
	}

	/**
	 * qna 조회
	 */
	@RequestMapping(value="/qna",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object getQna(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;
		
		try {
			ret = inquiryService.getQnaList();
		} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
			// TODO Auto-generated catch block
			logger.info("error = {}", e.toString());
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("501");
			((ReturnMessage) ret).setDesc("DB access error");		
		}
		
		return ret;
	}

	@RequestMapping(value="/push",
			method = RequestMethod.POST,
			headers = "accept=*/*")
	public @ResponseBody Object sendPush(HttpServletRequest request, HttpServletResponse response) {
		logger.info("request from {}", request.getRemoteHost());		
		Object ret = null;

		Map<String, ?> requestParam = HttpUtil.getRequestVariables( request );
		String userId = (String) requestParam.get("userId");
		
		try {
			pushService.SendPush(userId, "android", "푸시 받았어?");

			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("100");
			((ReturnMessage) ret).setDesc("ok");
		} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
			// TODO Auto-generated catch block
			logger.info("error = {}", e.toString());
			ret = new ReturnMessage();
			((ReturnMessage) ret).setCode("501");
			((ReturnMessage) ret).setDesc("DB access error");	
		}
		
		return ret;
	}
}
