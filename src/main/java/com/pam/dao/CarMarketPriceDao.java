package com.pam.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.entity.CarMadeYear;
import com.pam.entity.CarMaker;
import com.pam.entity.CarMarketPrice;
import com.pam.entity.CarModel;
import com.pam.entity.CarSubModel;

@Repository
public class CarMarketPriceDao {
	@Autowired
    private SqlSession sqlSession;
	
	public List<CarMaker> getCarMakerList() {
		return sqlSession.selectList("com.pam.mapper.CarMarketPriceMapper.selectCarMaker");
	}
	
	public List<CarModel> getCarModelList(HashMap<String, String> map) {
		return sqlSession.selectList("com.pam.mapper.CarMarketPriceMapper.selectCarModel", map);
	}
	
	public List<CarSubModel> getCarSubModelList(HashMap<String, String> map) {
		return sqlSession.selectList("com.pam.mapper.CarMarketPriceMapper.selectCarSubModel", map);
	}
	
	public List<CarMadeYear> getCarMadeYearList(HashMap<String, String> map) {
		return sqlSession.selectList("com.pam.mapper.CarMarketPriceMapper.selectCarMadeYear", map);
	}
	
	public CarMarketPrice getCarMarketPrice(HashMap<String, String> map) {
		return sqlSession.selectOne("com.pam.mapper.CarMarketPriceMapper.selectMarketPrice", map);
	}
}
