package com.pam.dao;

import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.mapper.domain.TblBankAccount;
import com.pam.mapper.domain.TblBankTransaction;

@Repository
public class BankDao {
	
	@Autowired
    private SqlSession sqlSession;

	public Long inserBankAccount(TblBankAccount bankAccount) {
		 sqlSession.insert("com.pam.mapper.BankMapper.insertBankAccountAndReturnAccntId", bankAccount);		 
		 return bankAccount.getAccntId();
	}
	
	public Integer inserBankTransaction(TblBankTransaction bankTransaction) {
		 return sqlSession.insert("com.pam.mapper.BankMapper.updateIfDuplicationElseInsert", bankTransaction);
	}

	public Integer inserBankTransactionInOneSql(HashMap<String, String> map) {
		 return sqlSession.insert("com.pam.mapper.BankMapper.insertIgnoreInto", map);
	}

	public Integer inserBankAccountBalance(HashMap<String, String> map) {
		 return sqlSession.insert("com.pam.mapper.BankMapper.insertBankAccountBalance", map);
	}
}
