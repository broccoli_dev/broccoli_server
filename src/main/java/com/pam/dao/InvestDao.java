package com.pam.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.entity.StockPrice;
import com.pam.mapper.domain.TblStockTransaction;

@Repository
public class InvestDao {
	
	@Autowired
    private SqlSession sqlSession;
	
	/**
	 * 사용자 전체 주식 리스트를 가져온다.
	 * @param userId
	 * @return
	 */
	public List<String> getUserStockList(String userId) {
		return sqlSession.selectList("com.pam.mapper.investMapper.selectUserStockList", userId);
	}

	/**
	 * 일별 주식 가격 정보를 가져온다.
	 * @param sqlMap
	 * @return
	 */
	public List<StockPrice> getDailyStockPriceInfo(HashMap<String, String> sqlMap) {
		return sqlSession.selectList("com.pam.mapper.investMapper.selectDailyStockPrice", sqlMap);
	}

	/**
	 * 주식 날짜 리스트를 조회한다.
	 * @param sqlMap
	 * @return
	 */
	public List<String> getStockDateList(Integer period) {
		return sqlSession.selectList("com.pam.mapper.investMapper.selectStockDateList", period);
	}
	

	public Long getStockAmtBeforeStartDate(TblStockTransaction trans) {
		return sqlSession.selectOne("com.pam.mapper.investMapper.selectStockAmt", trans);
	}
}