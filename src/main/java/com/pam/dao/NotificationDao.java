package com.pam.dao;

import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NotificationDao {
	
	@Autowired
    private SqlSession sqlSession;
	
	/**
	 * 사용자 전체 주식 리스트를 가져온다.
	 * @param userId
	 * @return
	 */
	public Long makeNotificationRead(HashMap<String, String> map) {
		return (long) sqlSession.update("com.pam.mapper.notificationMapper.updateNotificationRead", map);
	}
}