package com.pam.dao;


import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.entity.BudgetExpense;
import com.pam.mapper.domain.TblBudget;

@Repository
public class BudgetDao {
	
	@Autowired
    private SqlSession sqlSession;

	public Long inserBudget(TblBudget budget) {
		 sqlSession.insert("com.pam.mapper.budgetMapper.insertBudget", budget);		 
		 return budget.getBdgtId();
	}

	public Integer inserBudgetList(HashMap<String, String> map) {
		 return sqlSession.insert("com.pam.mapper.budgetMapper.insertBudgetList", map);
	}

	public List<BudgetExpense> getBudgetList(TblBudget budget) {		
		return sqlSession.selectList("com.pam.mapper.budgetMapper.selectBudgetList", budget);
	}
}
