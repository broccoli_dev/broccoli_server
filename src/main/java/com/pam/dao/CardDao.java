package com.pam.dao;

import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.mapper.domain.TblCard;
import com.pam.mapper.domain.TblCardApproval;
import com.pam.mapper.domain.TblCardBill;
import com.pam.mapper.domain.TblCardBillDetail;
import com.pam.mapper.domain.TblCardBillMapping;
import com.pam.mapper.domain.TblCardBillSummary;
import com.pam.mapper.domain.TblCardLimit;

@Repository
public class CardDao {
	
	@Autowired
    private SqlSession sqlSession;
	
	public Long insertCard(TblCard card) {
		Integer ret = sqlSession.insert("com.pam.mapper.cardMapper.insertCard", card);
		return (long) ret;
	}

	public Long insertCardApproval(TblCardApproval cardApproval) {
		Integer ret = sqlSession.insert("com.pam.mapper.cardMapper.updateCardApprovalIfDuplicationElseInsert", cardApproval);
		return (long) ret;
	}

	public Long insertCardApprovalInOneSql(HashMap<String, String> map) {
		Integer ret = sqlSession.insert("com.pam.mapper.cardMapper.inserttCardApprovalIgnoreInto", map);
		return (long) ret;
	}

	public Integer insertCardBillDetailInOneSql(HashMap<String, String> map) {
		Integer ret = sqlSession.insert("com.pam.mapper.cardMapper.insertCardBillDetailIgnoreInto", map);
		return ret;
	}

	public Integer insertCardBillSummaryInOneSql(HashMap<String, String> map) {
		Integer ret = sqlSession.insert("com.pam.mapper.cardMapper.insertCardBillSummaryIgnoreInto", map);
		return ret;
	}

	public void insertCardBill(TblCardBill cardBill) {
		sqlSession.insert("com.pam.mapper.cardMapper.updateCardBillIfDuplicationElseInsert", cardBill);
	}

	public void insertCardBillMapping(TblCardBillMapping cardBillMapping) {
		sqlSession.insert("com.pam.mapper.cardMapper.updateCardBillMappingIfDuplicationElseInsert", cardBillMapping);
	}

	public void insertCardBillSummary(TblCardBillSummary cardBillSummary) {
		sqlSession.insert("com.pam.mapper.cardMapper.updateCardBillSummaryIfDuplicationElseInsert", cardBillSummary);
	}

	public Long insertCardBillDetail(TblCardBillDetail cardBillDetail) {
		Integer ret = sqlSession.insert("com.pam.mapper.cardMapper.updateCardBillDetailIfDuplicationElseInsert", cardBillDetail);
		return (long) ret;
	}

	public TblCardApproval getLastCardApproval(String userId) {
		return sqlSession.selectOne("com.pam.mapper.cardMapper.selectLastCardApproval", userId);
	}

	public Integer insertCardLimit(TblCardLimit cardLimit) {
		return sqlSession.insert("com.pam.mapper.cardMapper.insertCardLimit", cardLimit);
	}
}
