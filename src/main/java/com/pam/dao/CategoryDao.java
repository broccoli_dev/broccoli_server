package com.pam.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.entity.Category;
import com.pam.entity.LargeCategory;
import com.pam.entity.NonCategory;
import com.pam.entity.StoreNameMatchCategory;

@Repository
public class CategoryDao {
	
	@Autowired
    private SqlSession sqlSession;
	
	public List<Category> getMatchedCategoryList(HashMap<String, String> map) {
		return sqlSession.selectList("com.pam.mapper.CategoryMapper.selectMatchedCategory", map);
	}
	
	public List<LargeCategory> getCategoryList() {
		return sqlSession.selectList("com.pam.mapper.CategoryMapper.selectCategory");
	}
	
	public List<NonCategory> getNonCategoryList() {
		return sqlSession.selectList("com.pam.mapper.CategoryMapper.selectNonCategory");
	}
	
	public List<StoreNameMatchCategory> getStoreNameMatchCategoryList(HashMap<String, String> map){
		return sqlSession.selectList("com.pam.mapper.CategoryMapper.selectStoreNameMatchCategory", map);
	}
	
	public int deleteStore(HashMap<String,String> map){
		return sqlSession.delete("com.pam.mapper.CategoryMapper.deleteStore", map);
	}
}
