package com.pam.dao;

import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.mapper.domain.TblCashReceipt;

@Repository
public class CashReceiptDao {
	
	@Autowired
    private SqlSession sqlSession;
	
	public Long inserCashReceipt(TblCashReceipt cashReceipt) {
		Integer ret = sqlSession.insert("com.pam.mapper.cashReceiptMapper.updateCashReceiptIfDuplicationElseInsert", cashReceipt);
		return (long) ret;
	}

	public Long insertCashReceiptInOneSql(HashMap<String, String> map) {
		Integer ret = sqlSession.insert("com.pam.mapper.cashReceiptMapper.insertIgnoreInto", map);
		return (long) ret;
	}
}
