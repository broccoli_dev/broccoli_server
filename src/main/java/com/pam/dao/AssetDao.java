package com.pam.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.entity.Bank;
import com.pam.entity.AssetMonthlyProperty;
import com.pam.entity.BankBalance;
import com.pam.entity.CardBill;
import com.pam.entity.CardBillDetail;
import com.pam.entity.Debt;
import com.pam.entity.StockAmt;
import com.pam.entity.StockDailyPrice;
import com.pam.entity.StockDetailOfDate;
import com.pam.mapper.domain.TblCardBill;

@Repository
public class AssetDao {
	
	@Autowired
    private SqlSession sqlSession;
	
	public List<Bank> getAssetBankInfoList(String userId) {
		return sqlSession.selectList("com.pam.mapper.assetMapper.selectAssetBankInfo", userId);
	}

	public List<AssetMonthlyProperty> getAssetMonthlyPropertyInfoList(String userId) {
		return sqlSession.selectList("com.pam.mapper.assetMapper.selectAssetMonthlyPropertyInfo", userId);
	}

	public Long getCardAccSumValue(String userId) {
		return sqlSession.selectOne("com.pam.mapper.assetMapper.selectCardAccSum", userId);
	}

	public List<Debt> getDebtInfo(String userId) {
		return sqlSession.selectList("com.pam.mapper.assetMapper.selectDebtInfo", userId);
	}

	public List<AssetMonthlyProperty> getDailyBankDebtInfo(String userId) {
		return sqlSession.selectList("com.pam.mapper.assetMapper.selectDailyBankDebt", userId);
	}
	
	public List<AssetMonthlyProperty> getMonthlyBankDebtInfo(String userId) {
		return sqlSession.selectList("com.pam.mapper.assetMapper.selectMonthlyBankDebt", userId);
	}

	public List<AssetMonthlyProperty> getMonthlyCardDebtInfo(String userId) {
		return sqlSession.selectList("com.pam.mapper.assetMapper.selectMonthlyCardDebt", userId);
	}

	public List<AssetMonthlyProperty> getDailyBankBalanceInfo(String userId) {
		return sqlSession.selectList("com.pam.mapper.assetMapper.selectDailyBankBalance", userId);
	}

	public List<BankBalance> getDailyBankPropertyInfo(String userId) {
		return sqlSession.selectList("com.pam.mapper.assetMapper.selectDailyBankProperty", userId);
	}

	public List<CardBill> getCardBillList(String userId) {
		return sqlSession.selectList("com.pam.mapper.assetMapper.selectCardBillList", userId);
	}

	public List<CardBillDetail> getCardBillDetail(TblCardBill cardBill) {		
		return sqlSession.selectList("com.pam.mapper.assetMapper.selectCardBillDetail", cardBill);
	}

	public List<StockAmt> getStockAmtList(String userId) {		
		return sqlSession.selectList("com.pam.mapper.assetMapper.selectStockList", userId);
	}
	
	public String getStockNameFromCode(String stockCode) {
		return sqlSession.selectOne("com.pam.mapper.assetMapper.selectStockNameFromCode", stockCode);
	}
	
	public List<StockDailyPrice> getStockDailyPriceList(HashMap<String, String> map) {		
		return sqlSession.selectList("com.pam.mapper.assetMapper.selectStockDailyPriceList", map);
	}
	
	public List<StockDailyPrice> getStockDatePrice2DaysList(HashMap<String, String> map) {		
		return sqlSession.selectList("com.pam.mapper.assetMapper.selectStock2DaysPrice", map);
	}
	
	public StockDetailOfDate getStockDetailOfDate(HashMap<String, String> map) {
		return sqlSession.selectOne("com.pam.mapper.assetMapper.selectStockDetailOfDate", map);
	}

	public List<String> getLast2Day() {
		return sqlSession.selectList("com.pam.mapper.assetMapper.selectLast2Day");
	}
}