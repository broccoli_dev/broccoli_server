package com.pam.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.entity.BankTransactionOfMoneyCalendar;
import com.pam.entity.ChallengeBank;
import com.pam.entity.ChallengeList;
import com.pam.entity.MoneyCalendarList;
import com.pam.entity.MoneyCalendarTransaction;
import com.pam.mapper.domain.TblExpense;
import com.pam.mapper.domain.TblMoneyCalendar;

@Repository
public class EtcDao {
	
	@Autowired
    private SqlSession sqlSession;
	
	/**
	 * 챌린지 리스트를 가져온다.
	 * @param userId
	 * @return
	 */
	public List<ChallengeList> getChallengeList(String userId) {
		return sqlSession.selectList("com.pam.mapper.etcMapper.selectChallengeList", userId);
	}

	/**
	 * 챌린지 계좌 정보를 가져온다.
	 * @param userId
	 * @return
	 */
	public List<ChallengeBank> getChallengeBankInfo(HashMap<String, String> paramMap) {
		return sqlSession.selectList("com.pam.mapper.etcMapper.selectBankAccount", paramMap);
	}

	/**
	 * 머니 캘린더 등록
	 * @param moneyCalendar
	 * @return
	 */
	public Long insertMoneyCalendar(TblMoneyCalendar moneyCalendar) {
		sqlSession.insert("com.pam.mapper.etcMapper.insertMoneyCalendar", moneyCalendar);
		return moneyCalendar.getCalId();
	}

	/**
	 * 머니 캘린더 리스트 조회
	 * @param userId
	 * @return
	 */
	public List<MoneyCalendarList> getMoneyCalendarList(String userId) {
		return sqlSession.selectList("com.pam.mapper.etcMapper.selectMoneyCalendarList", userId);
	}

	/**
	 * 머니 캘린더 총 갯수 조회
	 * @param userId
	 * @return
	 */
	public Integer getMoneyCalendarTotalCount(String userId) {
		return sqlSession.selectOne("com.pam.mapper.etcMapper.selectMoneyCalendarTotalCount", userId);
	}

	/**
	 * 월별 머니 캘린더 리스트 조회
	 * @param moneyCalendar
	 * @return
	 */
	public List<MoneyCalendarList> getMonthlyMoneyCalendarList(TblMoneyCalendar moneyCalendar) {
		return sqlSession.selectList("com.pam.mapper.etcMapper.selectMonthlyMoneyCalendar", moneyCalendar);
	}

	/**
	 * 머니캘린더 은행 거래 내역 조회
	 * @param userId
	 * @return
	 */
	public List<MoneyCalendarTransaction> getMoneyCalendarTransactionList(String userId) {
		return sqlSession.selectList("com.pam.mapper.etcMapper.selectMoneyCalendarTransactionList", userId);
	}
	
	/**
	 * 머니캘린더 은행 거래 내역 조회
	 * @param userId
	 * @return
	 */
	public List<BankTransactionOfMoneyCalendar> getBankTransactionOfMoneyCalendar(HashMap<String, String> map) {
		return sqlSession.selectList("com.pam.mapper.etcMapper.selectBankTransactionOfMoneyCalendar", map);
	}
	
	/**
	 * 머니 캘린더로부터 소비 등록
	 * @param expense
	 * @return
	 */
	public int insertExpenseFromMoneyCalendar(TblExpense expense) {
		return sqlSession.insert("com.pam.mapper.etcMapper.insertExenseFromMoneyCalendar", expense);
	}
}