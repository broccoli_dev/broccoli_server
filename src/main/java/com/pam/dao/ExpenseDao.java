package com.pam.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.entity.AssetMonthlyProperty;
import com.pam.entity.Budget;
import com.pam.entity.CardUse;
import com.pam.entity.ExpenseBudget;
import com.pam.entity.ExpenseCategory;
import com.pam.entity.ExpenseDetail;
import com.pam.entity.ExpensePaytype;
import com.pam.mapper.domain.TblExpense;

@Repository
public class ExpenseDao {
	
	@Autowired
    private SqlSession sqlSession;
	
	/**
	 * 소비내역 추가
	 * @param map
	 * @return
	 */
	public Long insertExpenseInOneSql(HashMap<String, String> map) {
		Integer ret = sqlSession.insert("com.pam.mapper.expenseMapper.insertExpense", map);
		return (long) ret;
	}

	/**
	 * 소비 상세 리스트를 가져온다.
	 * @param expense
	 * @return
	 */
	public List<ExpenseDetail> getExpenseDetail(TblExpense expense) {		
		return sqlSession.selectList("com.pam.mapper.expenseMapper.selectExpenseDetail", expense);
	}
	
	public Long getExpenseMonthlySum(TblExpense expense) {		
		return sqlSession.selectOne("com.pam.mapper.expenseMapper.selectExpenseMonthlySum", expense);
	}

	/**
	 * 카테고리 별 소비 합계를 가져온다. 
	 * @param map
	 * @return
	 */
	public List<Budget> getCategoryExpense(HashMap<String, String> map) {		
		return sqlSession.selectList("com.pam.mapper.expenseMapper.selectCategoryExpense", map);
	}

	/**
	 * 카테고리 별 소비 합계 및 percent를 가져온다.
	 * @param expense
	 * @return
	 */
	public List<ExpenseCategory> getExpensePerCategory(TblExpense expense) {		
		return sqlSession.selectList("com.pam.mapper.expenseMapper.selectExpensePerCategory", expense);
	}

	/**
	 * 카테고리 별 소비 내역을 가져온다.
	 * @param expense
	 * @return
	 */
	public List<ExpenseDetail> getExpenseCategoryDetailInfo(TblExpense expense) {		
		return sqlSession.selectList("com.pam.mapper.expenseMapper.selectExpenseCategoryDetail", expense);
	}

	/**
	 * 월간 예산, 소비 정보를 가져온다.
	 * @param expense
	 * @return
	 */
	public ExpenseBudget getMonthlyExpenseBudgetInfo(TblExpense expense) {		
		return sqlSession.selectOne("com.pam.mapper.expenseMapper.selectMonthlyExpenseBudget", expense);
	}

	/**
	 * 월간 소비 총액을 가져온다.
	 * @param expense
	 * @return
	 */
	public Long getMonthlyExpenseInfo(TblExpense expense) {		
		return sqlSession.selectOne("com.pam.mapper.expenseMapper.selectMonthlyExpense", expense);
	}

	/**
	 * 월간 예산 총액을 가져온다.
	 * @param expense
	 * @return
	 */
	public Long getMonthlyBudgetInfo(TblExpense expense) {		
		return sqlSession.selectOne("com.pam.mapper.expenseMapper.selectMonthlyBudget", expense);
	}

	/**
	 * 이번 달 전체 소비 숫자를 가져온다.
	 * @param userId
	 * @return
	 */
	public Integer getTotalExpenseCount(String userId) {		
		return sqlSession.selectOne("com.pam.mapper.expenseMapper.selectTotalExpenseCountInThisMonth", userId);
	}	

	/**
	 * 이번 달 최근 소비 3개 리스트를 가져온다.
	 * @param userId
	 * @return
	 */
	public List<ExpenseDetail> getLast3Expenses(String userId) {		
		return sqlSession.selectList("com.pam.mapper.expenseMapper.selectLast3ExpenseListInThisMonth", userId);
	}

	/**
	 * 이번 달 예산 숫자를 가져온다.
	 * @param userId
	 * @return
	 */
	public Integer getBudgetCount(String userId) {		
		return sqlSession.selectOne("com.pam.mapper.expenseMapper.selectBudgetCountInThisMonth", userId);
	}

	/**
	 * 이번 달 예산, 소비 합계를 가져온다.
	 * @param expense
	 * @return
	 */
	public ExpenseBudget getExpenseAndBudgetInfo(String userId) {		
		return sqlSession.selectOne("com.pam.mapper.expenseMapper.selectExpenseAndBudgetSumInThisMonth", userId);
	}

	/**
	 * 이번 달 예산 합계를 가져온다.
	 * @param expense
	 * @return
	 */
	public Long getExpenseSum(TblExpense expense) {		
		return sqlSession.selectOne("com.pam.mapper.expenseMapper.selectExpenseSumInThisMonth", expense);
	}

	public Long getBudgetSum(String userId) {		
		return sqlSession.selectOne("com.pam.mapper.expenseMapper.selectBudgetSumInThisMonth", userId);
	}

	/**
	 * 이번달 소비 분류별 소비액 리스트를 가져온다.
	 * @param userId
	 * @return
	 */
	public List<Budget> getExpenseCategory(String userId) {		
		return sqlSession.selectList("com.pam.mapper.expenseMapper.selectExpenseCategoryInThisMonth", userId);
	}

	/**
	 * 소비 방법 별 소비액 리스트를 가져온다.
	 * @param userId
	 * @return
	 */
	public List<ExpensePaytype> getExpensePaytype(String userId) {		
		return sqlSession.selectList("com.pam.mapper.expenseMapper.selectExpensePaytypeInThisMonth", userId);
	}

	/**
	 * 일별 소비액 합계를 가져온다.
	 * @param userId
	 * @return
	 */
	public List<AssetMonthlyProperty> getDailyExpenseSumInfo(TblExpense expense) {		
		return sqlSession.selectList("com.pam.mapper.expenseMapper.selectDailyExpenseSum", expense);
	}

	/**
	 * 이번달 카드별 사용 금액 합계를 가져온다.
	 * @param userId
	 * @return
	 */
	public List<CardUse> getCardUseListInThisMonth(String userId) {		
		return sqlSession.selectList("com.pam.mapper.expenseMapper.selectCardUseSumInThisMonth", userId);
	}

	/**
	 * 이번달 카드 사용 금액 합계를 구한다.
	 * @param cardId
	 * @return
	 */
	public Long getCardUseSum(Long cardId) {		
		return sqlSession.selectOne("com.pam.mapper.expenseMapper.selectCardUseSum", cardId);
	}
}
