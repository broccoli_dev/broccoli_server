package com.pam.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.entity.CategoryTag;
import com.pam.entity.MoneyCalendarTag;
import com.pam.entity.Tag;

@Repository
public class CategoryTagDao {
	
	@Autowired
    private SqlSession sqlSession;
	
	public List<CategoryTag> getAllTag() {
		return sqlSession.selectList("com.pam.mapper.ctgryTagMapper.selectAllCategoryTag");
	}
	
	public List<MoneyCalendarTag> getAllMoneyCalendar() {
		return sqlSession.selectList("com.pam.mapper.ctgryTagMapper.selectAllMoneyCalendarTag");
	}
	
	public List<Tag> getAllTagInfo() {
		return sqlSession.selectList("com.pam.mapper.ctgryTagMapper.selectAlltag");
	}
	
	public int insertNewTag(HashMap<String, String> map) {
		return sqlSession.insert("com.pam.mapper.ctgryTagMapper.insertNewTag", map);
	}
	
	public int deleteTag(HashMap<String, String> map){
		return sqlSession.delete("com.pam.mapper.ctgryTagMapper.deleteTag", map);
	}
	
	public int updateTag(HashMap<String, String> map){
		return sqlSession.update("com.pam.mapper.ctgryTagMapper.updateTag", map);
	}
	
	public Integer getLastCtgryPriority() {
		return sqlSession.selectOne("com.pam.mapper.ctgryTagMapper.selectLastCtgryPriority");
	}
}
