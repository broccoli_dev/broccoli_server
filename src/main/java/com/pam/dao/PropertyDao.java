package com.pam.dao;

import java.util.HashMap;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PropertyDao {
	
	@Autowired
    private SqlSession sqlSession;
	
	public Long insertProperty(HashMap<String, String> map) {
		Integer ret = sqlSession.insert("com.pam.mapper.propertyMapper.insertIgnoreInto", map);
		return (long) ret;
	}
}
