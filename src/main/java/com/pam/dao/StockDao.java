package com.pam.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.entity.StockCodeName;

@Repository
public class StockDao {
	
	@Autowired
    private SqlSession sqlSession;
		
	public List<StockCodeName> selectStockCodeName(HashMap<String, String> itemMap) {
		return sqlSession.selectList("com.pam.mapper.stockMapper.selectStockList", itemMap);
	}
}
