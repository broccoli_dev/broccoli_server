package com.pam.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.mapper.domain.TblBankTransaction;
import com.pam.mapper.domain.TblMoneyCalendar;

@Repository
public class MoneyCalDao {
	
	@Autowired
    private SqlSession sqlSession;
	
	public long insertMoneyCalFromBrankTransaction(HashMap<String, String> map) {
		return sqlSession.insert("com.pam.mapper.MoneyCalMapper.insertMoneyCalFromBankTransaction", map);
	}
	
	public List<TblMoneyCalendar> getNextMoneyCalendarList(String userId) {
		return sqlSession.selectList("com.pam.mapper.MoneyCalMapper.selectNextMoneyCalendarList", userId);
	}
	
	public List<TblMoneyCalendar> getMonthlyMoneyCalendarList(HashMap<String,String> map) {
		return sqlSession.selectList("com.pam.mapper.MoneyCalMapper.selectMonthlyMoneyCalendarList", map);
	}
	
	public List<TblBankTransaction> getMonthlyBankTransaction(HashMap<String,String> map) {
		return sqlSession.selectList("com.pam.mapper.MoneyCalMapper.selectMonthlyBankTransaction", map);
	}
	
	public long insertMoneyCal(TblMoneyCalendar moneyCal) {
		return sqlSession.insert("com.pam.mapper.MoneyCalMapper.insertMoneyCal", moneyCal);
	}
	
	public long updateMoneyCal(TblMoneyCalendar moneyCal) {
		return sqlSession.update("com.pam.mapper.MoneyCalMapper.updateMoneyCal", moneyCal);
	}
	
	public int deleteRepeatedMoneyCal(TblMoneyCalendar moneyCal) {
		return sqlSession.update("com.pam.mapper.MoneyCalMapper.deleteRepeatedMoneyCal", moneyCal);
	}
}
