package com.pam.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.entity.CardAllList;
import com.pam.entity.CardBenefit;
import com.pam.entity.CardBenefitCode;
import com.pam.entity.CardBenefitLargeCategory;
import com.pam.entity.CardBenefitLargeCode;
import com.pam.entity.CardBrandCode;
import com.pam.entity.CardCompanyCode;
import com.pam.entity.CardInfo;
import com.pam.entity.CardInfoBenefit;
import com.pam.entity.CardList;
import com.pam.entity.CardTypeCode;

@Repository
public class CardListDao {
	@Autowired
    private SqlSession sqlSession;
	
	public List<CardList> getCardList(HashMap<String, String> map) {
		return sqlSession.selectList("com.pam.mapper.CardListMapper.selectCardList", map);
	}
	
	public CardBenefit getCardBenefit(HashMap<String, String> map) {
		return sqlSession.selectOne("com.pam.mapper.CardListMapper.selectCardBenefit", map);
	}
	
	public List<CardBenefitLargeCategory> getCardBenefitLargeCategoryList(HashMap<String, String> map) {
		return sqlSession.selectList("com.pam.mapper.CardListMapper.selectCardBenefitLargeCategory", map);
	}
	
	public List<String> getCardBenefitDetailList(HashMap<String, String> map) {
		return sqlSession.selectList("com.pam.mapper.CardListMapper.selectCardBenefitDetail", map);
	}
	
	public List<CardAllList> getCardAllList() {
		return sqlSession.selectList("com.pam.mapper.CardListMapper.selectCardAllList");
	}
	
	public int deleteCardBrandByCardCode(HashMap<String, String> map) {
		return sqlSession.delete("com.pam.mapper.CardListMapper.deleteCardBrandByCardCode", map);
	}
	
	public int deleteCardBenefitByCardCode(HashMap<String, String> map) {
		return sqlSession.delete("com.pam.mapper.CardListMapper.deleteCardBenefitByCardCode", map);
	}
	
	public int deleteCard(HashMap<String, String> map) {
		return sqlSession.delete("com.pam.mapper.CardListMapper.deleteCard", map);
	}
	
	public List<CardCompanyCode> getCardCompanyCode() {
		return sqlSession.selectList("com.pam.mapper.CardListMapper.selectCardCompanyCode");
	}
	
	public List<CardTypeCode> getCardTypeCode() {
		return sqlSession.selectList("com.pam.mapper.CardListMapper.selectCardTypeCode");
	}
	
	public List<CardBrandCode> getCardBrandCode() {
		return sqlSession.selectList("com.pam.mapper.CardListMapper.selectCardBrandCode");
	}
	
	public List<CardBenefitLargeCode> getCardBenefitLargeCode() {
		return sqlSession.selectList("com.pam.mapper.CardListMapper.selectCardBenefitLargeCode");
	}
	
	public List<CardBenefitCode> getCardBenefitCode(String benefitLargeCtgryCode) {
		return sqlSession.selectList("com.pam.mapper.CardListMapper.selectCardBenefitCode", benefitLargeCtgryCode);
	}
	
	public CardInfo getCardInfo(String cardCode){
		return sqlSession.selectOne("com.pam.mapper.CardListMapper.selectCardInfo", cardCode);
	}
	
	public List<String> getCardInfoBrand(String cardCode){
		return sqlSession.selectList("com.pam.mapper.CardListMapper.selectCardInfoBrand", cardCode);
	}
	
	public List<CardInfoBenefit> getCardInfoBenefit(String cardCode){
		return sqlSession.selectList("com.pam.mapper.CardListMapper.selectCardInfoBenefit", cardCode);
	}
	
	public String getLatestManualInsertedCardCode(String companyCode){
		return sqlSession.selectOne("com.pam.mapper.CardListMapper.selectLatestManualInsertedCardCode", companyCode);
	}
	
	public int insertManualCardData(HashMap<String,String> map){
		return sqlSession.insert("com.pam.mapper.CardListMapper.insertManualCardData", map);
	}
	
	public int insertManualCardBrand(HashMap<String,String> map){
		return sqlSession.insert("com.pam.mapper.CardListMapper.insertManualCardBrand", map);
	}
	
	public int deleteManualOldCardBrand(HashMap<String,String> map){
		return sqlSession.delete("com.pam.mapper.CardListMapper.deleteManualOldCardBrand", map);
	}
	
	public int deleteManualOldCardBenefit(HashMap<String,String> map){
		return sqlSession.delete("com.pam.mapper.CardListMapper.deleteManualOldCardBenefit", map);
	}
	
	public String getBenefitSeq(HashMap<String,String> map){
		return sqlSession.selectOne("com.pam.mapper.CardListMapper.selectBenefitSeq", map);
	}
	
	public String getLatestManualInsertedBenefitSeq(HashMap<String,String> map){
		return sqlSession.selectOne("com.pam.mapper.CardListMapper.selectLatestManualInsertedBenefitSeq", map);
	}
	
	public int insertManualCardBenefit(HashMap<String,String> map){
		return sqlSession.insert("com.pam.mapper.CardListMapper.insertManualCardBenefit", map);
	}
}
