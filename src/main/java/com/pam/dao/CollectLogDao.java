package com.pam.dao;


import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pam.entity.CollectSuccessInfo;

@Repository
public class CollectLogDao {
	
	@Autowired
    private SqlSession sqlSession;

	public List<CollectSuccessInfo> getCollectLogInfo(String userId) {		
		return sqlSession.selectList("com.pam.mapper.collectLogMapper.selectSuccessCollectLogInfo", userId);
	}
}
