package com.pam.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.regex.Pattern;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;


/**
 * ���� ��ƿ��Ƽ Ŭ����.
 * @author jin
 */
public final class Utils {

    /**
     * Utility class�� ���� �⺻ ������.
     */
    private Utils() {
        // Nothing.
    }

    /**
     * Logger (static).
     */
    private final static Logger logger = LoggerFactory.getLogger(Utils.class);

    /**
     * OS�� ���� ���ڿ� ������.
     * �����ڿ��� �ʱ�ȭ �ȴ�.
     */
    public static final String STRING_LINE_SEPARATOR;

    /**
     * OS�� ���� ���丮 ������.
     * �����ڿ��� �ʱ�ȭ �ȴ�.
     */
    public static final String STRING_FILE_SEPARATOR;

    /**
     * ������ ����.
     */
    public static final int EIGHT_HEX = 0xff;

    /**
     * 4byte ���� for checkstyle.
     */
    public static final int FOUR_BYTE_LENGTH = 4;

    /**
     * ����Ʈ ����� ���� ����Ʈ ũ��.
     */
    public static final int SHIFT_SIZE_8 = 8;

    /**
     * ����Ʈ ����� ���� ����Ʈ ũ��.
     */
    public static final int SHIFT_SIZE_16 = 16;

    /**
     * ����Ʈ ����� ���� ����Ʈ ũ��.
     */
    public static final int SHIFT_SIZE_24 = 24;
    
    /**
     * 24��.
     */
    public static final int HOURS_ONE_DAY = 24;
    
    /**
     * 60��.
     */
    public static final int MINUTES_ONE_HOUR = 60;
    
    /**
     * 60��.
     */
    public static final int SECONDS_ONE_MINUTES = 60;
    
    /**
     * �Ͽ���.
     */
    public static final int SUN_DAY = 1;
    
    /**
     * ������.
     */
    public static final int MON_DAY = 2;
    
    /**
     * ȭ����.
     */
    public static final int TUE_DAY = 3;
    
    /**
     * ������.
     */
    public static final int WED_DAY = 4;
    
    /**
     * �����.
     */
    public static final int THU_DAY = 5;
    
    /**
     * �ݿ���.
     */
    public static final int FRI_DAY = 6;
    
    /**
     * �����.
     */
    public static final int SAT_DAY = 7;
    
    /**
     * week ����.
     */
    public static final Integer[] WEEKDAYS = new Integer[]{SUN_DAY, MON_DAY, TUE_DAY, WED_DAY, THU_DAY, FRI_DAY, SAT_DAY};
    
    /**
     * week ���ϸ�.
     */
    public static final String[] WEEKDAYS_NAME = new String[]{"��", "��", "ȭ", "��", "��", "��", "��"};
    
    /**
     * �ݾ��� group ����.
     */
    public static final int PRICE_GROUP_SIZE = 3;
    
    /**
     * stacktrace �ִ� ���� ���� ��.
     */
    public static final int STACKTRACE_MAX_LINE = 10;
    
    /**
     * The short hostname length.
     */
    public static final int SHORT_HOSTNAME_LENGTH = 8;
    
    /**
     * The short node.name length.
     */
    public static final int SHORT_NODENAME_LENGTH = 10;
    
    static {
        String lineSeparator = System.getProperty("line.separator");
        String fileSeparator = System.getProperty("file.separator");
        if (lineSeparator == null) {
            lineSeparator = "\n";
        }
        STRING_LINE_SEPARATOR = lineSeparator;
        STRING_FILE_SEPARATOR = fileSeparator;
    }

    /**
     * for use with ' checkDirectoryIsWritable() '.
     */
    private static final TreeSet<String> DIRECTORY_WRITABLE = new
            TreeSet<String>();

    /**
     * ���� ����Ÿ�� 4byte�� ��ȯ.
     * @param data ��������Ÿ
     * @return byte[4] - ��ȯ ���
     */
    public static byte[] intTo4Byte(final int data) {
        byte[] byteData = new byte[FOUR_BYTE_LENGTH];
        int byteIdx = 0;
        byteData[byteIdx++] = (byte) ((data >> SHIFT_SIZE_24) & EIGHT_HEX);
        byteData[byteIdx++] = (byte) ((data >> SHIFT_SIZE_16) & EIGHT_HEX);
        byteData[byteIdx++] = (byte) ((data >> SHIFT_SIZE_8) & EIGHT_HEX);
        byteData[byteIdx++] = (byte) (data & EIGHT_HEX);
        return byteData;
    }

    /**
     * ���� ����Ÿ�� 1byte�� ��ȯ.
     * @param data ��������Ÿ
     * @return byte[2] - ��ȯ ���
     */
    public static byte[] intTo2Byte(final int data) {
        byte[] byteData = new byte[2];
        byteData[0] = (byte) ((data >> SHIFT_SIZE_8) & EIGHT_HEX);
        byteData[1] = (byte) (data & EIGHT_HEX);
        return byteData;
    }

    /**
     * ���� ����Ÿ�� 1byte�� ��ȯ.
     * @param data ��������Ÿ
     * @return byte[1] - ��ȯ ���
     */
    public static byte[] intToByte(final int data) {
        byte[] byteData = new byte[1];
        byteData[0] = (byte) (data & EIGHT_HEX);
        return byteData;
    }

    /**
     * �⺻ ȯ�� ���� ������ �����ش�.
     */
    public static void showBaseInformations() {

        StringBuffer print = null;

        String startStr = STRING_LINE_SEPARATOR
                + STRING_LINE_SEPARATOR
                + " - - - - - - - - - - - - - - - - - - - - "
                + STRING_LINE_SEPARATOR
                + "   Uhome Community               "
                + STRING_LINE_SEPARATOR
                + "   C / O / R / E                         "
                + STRING_LINE_SEPARATOR
                + " - - - - - - - - - - - - - - - - - - - - "
                + STRING_LINE_SEPARATOR
                + "                        (C)2015 NomadConnection Inc "
                + STRING_LINE_SEPARATOR
                + STRING_LINE_SEPARATOR
                + STRING_LINE_SEPARATOR + ".";
        logger.info(startStr);

        // JVM version
        print = new StringBuffer();
        print.append("[Env 01/10] JVM version=");
        print.append(System.getProperty("java.vm.version"));
        logger.info(print.toString());

        // Runtime version
        print = new StringBuffer();
        print.append("[Env 02/10] Runtime version=");
        print.append(System.getProperty("java.runtime.version"));
        logger.info(print.toString());

        // Java Spec. version
        print = new StringBuffer();
        print.append("[Env 03/10] Support Java version=");
        print.append(System.getProperty("java.specification.version"));
        logger.info(print.toString());

        // JVM classpath
        print = new StringBuffer();
        print.append("[Env 04/10] Java Classpath=");
        print.append(System.getProperty("java.class.path"));
        logger.info(print.toString());

        // JAVA_HOME directory
        print = new StringBuffer();
        print.append("[Env 05/10] Java Home=");
        print.append(System.getProperty("java.home"));
        logger.info(print.toString());

        // HOSTNAME
        print = new StringBuffer();
        print.append("[Env 06/10] Hostname=");
        print.append(getHostname() + " (short: " + getShortHostname() + ")");
        logger.info(print.toString());

        // TEMP directory
        print = new StringBuffer();
        print.append("[Env 07/10] tmp file path=");
        print.append(System.getProperty("java.io.tmpdir"));
        logger.info(print.toString());

        // USER_HOME directory
        print = new StringBuffer();
        print.append("[Env 08/10] User Home=");
        print.append(System.getProperty("user.home"));
        logger.info(print.toString());

        // USER_NAME
        print = new StringBuffer();
        print.append("[Env 09/10] User Name=");
        print.append(System.getProperty("user.name"));
        logger.info(print.toString());

        // APPLICATION_VERSION
        /*
        print = new StringBuffer();
        print.append("[Env 10/10] Module version=");
        print.append(Utils.getVersion());
        logger.info(print.toString());
        */

        logger.debug("DEBUG level test message.");
        logger.info("INFO  level test message.");
        logger.error("ERROR level test message.");

    }

    /**
     * @return hostname string.
     */
    public static String getHostname() {
        
        String host = System.getenv("COMPUTERNAME");
        if (host != null) {
            return host;
        }
        
        host = System.getenv("HOSTNAME");
        if (host != null) {
            return host;
        }
        
        try {
            InetAddress addr = InetAddress.getLocalHost();
            host = addr.getHostName();
            if (host != null) {
                return host;
            }
        } catch (UnknownHostException e) {
            logger.error("Error at read hostname from InetAddress.");
        }
        
        // undetermined.
        return null;
    }
    
    /**
     * @return ª�� ����? �ڿ��� n�ڸ���.. hostname.
     * @see SHORT_HOSTNAME_LENGTH
     */
    public static String getShortHostname() {
        String hostname = getHostname();
        if (hostname.length() > SHORT_HOSTNAME_LENGTH) {
            return getHostname().substring(hostname.length() - SHORT_HOSTNAME_LENGTH, hostname.length());
        }
        return hostname;
    }
    
    /**
     * nodeName ���� ��.
     * ������ ���� ���� ��� �ڿ��� n�ڸ��� ��ȯ �Ѵ�.
     * @param fullNodeName ���� node name.
     * @return ���ѵ� ���̷� ���� node name.
     */
    public static String getShortNodename(final String fullNodeName) {
        String nodeName = fullNodeName;
        if (nodeName.length() > SHORT_NODENAME_LENGTH) {
            return getHostname().substring(nodeName.length() - SHORT_NODENAME_LENGTH, nodeName.length());
        }
        return nodeName;
    }
    
    /**
     * ���� ����.
     * @param filename ������ ���丮.
     * @return boolean ó�� ���
     */
    public static boolean makeSureDirectoryExists(final String filename) {

        if (filename == null) {
            return false;
        }

        File parent = new File(getParent(filename));
        if (!parent.exists()) {
            return parent.mkdirs();
        }
        return false;
    }

    /**
     * @return version ���ڿ��� ��ȯ.
     */
    public static String getVersion() {
        return ResourceBundle.getBundle("version").getString("version");
    }

    /**
     * @param makeDir ������ ��� ���ڿ�.
     * @return ���� ���.
     */
    public static boolean makeDirectoryIfNotExist(final String makeDir) {
        if (makeDir == null) {
            return false;
        }

        File directory = new File(makeDir);
        if (!directory.isDirectory()) {
            return directory.mkdirs();
        }

        return true;
    }

    /**
     * @param filePath �θ� ��θ� Ȯ���� ��� ���ڿ�.
     * @return �θ� ��� ���ڿ�.
     */
    public static String getParent(final String filePath) {
        File f = new File(filePath);
        return f.getParent();
    }

    /**
     * ������ ���丮�� ������ �� �� �ִ��� Ȯ���Ѵ�.
     * @param dirPath ���� ������ Ȯ���� ��� ���ڿ�.
     * @return ���� ����.
     */
    public static boolean checkDirectoryIsWritable(final String dirPath) {

        if (isStringEmpty(dirPath)) {
            return false;
        }

        // has it checked before?
        if (DIRECTORY_WRITABLE.contains(dirPath)) {
            return true;
        }

        String testFileName = "TEMP_WRITABILITY_TEST__REMOVE_IT_IF_YOU_WANT";

        File dir = new File(dirPath);
        if (!dir.exists() && !dir.mkdirs()) {
            return false;
        }

        BufferedWriter writer = null;
        boolean writerClosed = false;

        try {

            File fileInDir = new File(dirPath, testFileName);
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(fileInDir), "EUCKR"));

            writer.write("TEST");
            writer.flush();
            writer.close();
            writerClosed = true;

            if (fileInDir.exists()) {
                boolean isDelete = fileInDir.delete();
                DIRECTORY_WRITABLE.add(dirPath);
                // the test was successful
                return isDelete;
            }
        } catch (Throwable e) {
            logger.debug("Utils.checkDirectoryIsWritable() exception occured. ("
                    + e
                    + ")");
        } finally {
            if (!writerClosed) {
                try {
                    writer.close();
                } catch (Throwable ee) {
                    logger.debug("Utils.checkDirectoryIsWritable()"
                            + " exception occured. ("
                            + ee + ")");
                }
            }
        }
        return false;
    }

    /**
     * @param toCheck �˻��� ���ڿ�.
     * @return ���ڿ� ����.
     */
    public static boolean isStringEmpty(final String toCheck) {
    	boolean ret = false;
    	
    	if(toCheck != null) {
    		String tmp = toCheck.trim();
    		int length = tmp.length();
    		if(length <= 0) {
    			ret = true;
    		}
    	} else {
    		ret = true;
    	}
    	
        return ret;
    }
    
    /**
     * ����ð� ���ڿ�.
     * @param dateFormat ��¥ ����ǥ�� DateFormat
     * @return ����ð� ���ڿ�
     */
    public static String getNowTime(final String dateFormat) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(Calendar.getInstance().getTime());
    }
    
    /**
     * ����ð� ���ڿ�.
     * @return ����ð� ���ڿ�
     */
    public static String getNowTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
        return sdf.format(Calendar.getInstance().getTime());
    }
    
    /**
     * �Ϸ� �� ���ڿ�.
     * @return �Ϸ� �� ���ڿ�
     */
    public static String getPriviousTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        cal.setTime(Calendar.getInstance().getTime());
        cal.add(Calendar.DATE, -1);
        return sdf.format(cal.getTime()) + "000000";
    }
    
    /**
     * ����ð� ���ڿ�.
     * @return ����ð� ���ڿ�
     */
    public static String getNowMills() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        return sdf.format(Calendar.getInstance().getTime());
    }
    
    /**
     * file Upload.
     * @param inputStream inputStream
     * @param outputStream outputStream
     * @param limitsize limitsize
     * @throws Exception Exception
     */
    public static void fileUpload(final InputStream inputStream, final OutputStream outputStream, final int limitsize) throws Exception {
        try {
            byte[] buf = new byte[limitsize];
            int readByte = 0;
            while ((readByte = inputStream.read(buf, 0, limitsize - 1)) != -1) {
                outputStream.write(buf, 0, readByte);
            }
        } catch (Exception e) {
            logger.error(Utils.getStacktraceString(e));
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }
    
    /**
     * @param in InputStream
     * @param limitSize limitSize
     * @return ByteArray
     * @throws IOException IOException
     */
    public static byte[] readStreamData(final InputStream in, final int limitSize) throws IOException {
        ByteArrayOutputStream baos = null;
        try {
            baos = new ByteArrayOutputStream();
            int c;

            byte[] buf = new byte[limitSize];
            while ((c = in.read(buf)) != -1) {
                baos.write(buf, 0, c);
            }

            byte[] ret = baos.toByteArray();

            return ret;
        } finally {
            if (baos != null) {
                   baos.close();
            }
        }
    }
    
    /**
     * int�� 1byteHexString�� ����.
     * @param inputData inputData
     * @return 1byteHexString
     */
    public static String intToHexStringByOneByte(final int inputData) {
        if (inputData <= 0) {
            return "00";
        }

        // Dongho Lee 2009/06/23
        // 1 byte�� �����ϱ� ���ؼ��� 0xff �� bit-wise AND ������ ����� ��
        // �׷��� ���� ��� hexstring���� ����� 3�ڸ��� �̻��� ���� �� ����
        // ���� 1 byte �̻��� ���� ����
        int result = inputData & EIGHT_HEX;
        String resultText = Integer.toHexString(result);
        // String resultText = Integer.toHexString(inputData);
        while (resultText.length() < 2) {
            resultText = "0" + resultText;
        }
        return resultText;
    }
    
    /**
     * ������ �����ϴ� �޼ҵ�.
     * @param inFileName source.
     * @param outFileName target.
     * @return result ������ ���� ���
     */
    public static boolean fileCopy(final String inFileName, final String outFileName) {
        boolean result = false;
        FileInputStream fis = null;
        FileOutputStream fos = null;
        try {
            fis = new FileInputStream(inFileName);
            fos = new FileOutputStream(outFileName);
            
            int data = 0;
            while ((data = fis.read()) != -1) {
                fos.write(data);
            }
            result = true;
        } catch (IOException e) {
            logger.error(e.getMessage());
            result = false;
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException io) {
                    logger.error(Utils.getStacktraceString(io));
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException io) {
                    logger.error(Utils.getStacktraceString(io));
                }
            }
        }
        return result;
    }
    
    /**
     * ���� Ȯ���� ����.
     * @param fileNm �����̸�
     * @return extension ���� Ȯ����
     */
    public static String getFileExtension(final String fileNm) {
        StringTokenizer st = new StringTokenizer(fileNm, ".");
        String extension = "";
        while (st.hasMoreTokens()) {
            extension = st.nextToken();
        }
        return extension;
    }
    
    /**
     * ���� ���� ����.
     * @param fileNames �������Ͽ� �� ���ϸ� ����Ʈ
     * @param zipFileRelativePath �������� Relative ���
     * @param limitSize ������ ���� �ִ� �뷮
     * @return zipFileNm ���� ���ϸ�
     * @throws IOException IOException
     */
    public static String writeBlobIntoZip(final String[] fileNames, final String zipFileRelativePath, final int limitSize) throws IOException {
        byte[] imageBytes = null;
        ZipOutputStream zos = null;
        FileInputStream fin = null;
        String zipFileNm = getNowMills() + ".zip";
        
        makeDirectoryIfNotExist(zipFileRelativePath);
        
        String rename = zipFileRelativePath + STRING_FILE_SEPARATOR + zipFileNm;
        logger.debug("rename >>>>>>>>>> " + rename);
        try {
            zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(rename)));
            
            for (int i = 0; fileNames[i] != null && i < fileNames.length; i++) {
                fin = new FileInputStream(fileNames[i]);
                byte[] buffer = new byte[limitSize];
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                int read = -1;
                
                while ((read = fin.read(buffer, 0, buffer.length)) != -1) {
                    baos.write(buffer, 0, read);
                }
                
                imageBytes = baos.toByteArray();
                
                if (imageBytes == null) {
                    continue;
                }
                
                CRC32 imageCRC = new CRC32();
                ZipEntry imageEntry = new ZipEntry(fileNames[i].substring(fileNames[i].lastIndexOf(STRING_FILE_SEPARATOR)));
                imageEntry.setCrc(imageCRC.getValue());
                imageEntry.setSize(imageBytes.length);
                
                zos.putNextEntry(imageEntry);
                zos.write(imageBytes);
                
                zos.flush();
                zos.closeEntry();
            }
        } catch (Exception e) {
            logger.error(Utils.getStacktraceString(e));
        } finally {
            if (zos != null) {
                zos.close();
            }
            if (fin != null) {
                fin.close();
            }
        }
        
        return zipFileNm;
    }
    
    /**
     * Object �� ByteArray ��ȯ.
     * @param obj Object
     * @return ByteArray
     */
    public static byte[] getObjectConvertByteArray(final Object obj) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ObjectOutputStream os = new ObjectOutputStream(out);
            os.writeObject(obj);
            os.flush();
            os.close();
        } catch (Exception e) {
            logger.error(Utils.getStacktraceString(e));
        }
        return out.toByteArray();
    }
    
    
    /**
     * ByteArray �� Object ��ȯ.
     * @param data ByteArray ������
     * @return Object
     */
    public static Object getByteArrayConvertObject(final byte[] data) {
        if (data == null || data.length == 0) {
            return null;
        }
        ObjectInputStream ois = null;
        Object objectData = null;
        try {
            ois = new ObjectInputStream(new BufferedInputStream(
                    new ByteArrayInputStream(data)));
            objectData = ois.readObject();

        } catch (Exception e) {
            logger.error(Utils.getStacktraceString(e));
        }
        return objectData;
    }
    
    /**
     * ���ڿ��� ���Ϸ� ����.
     * @param string ������ ����.
     * @param pathName ������ ������ ��ο� �̸�.
     */
    public static void saveString2File(final String string, final String pathName) {
        // ���Ϸ� ���� �Ѵ�.
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(pathName));
            writer.write(string);
        } catch (IOException e) {
            logger.error(e.getMessage());
            //throw e;
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                logger.error("Error at close writer. Message: " + e.getMessage());
            }
        }
    }
    
    /**
     * ��¥ Ÿ���� ��ȯ���ش�.
     * @param date date.
     * @param formatType 0: yyyy-MM-dd, 1: yyyy-MM-dd HH:mm:ss, 2: yyyy-MM-dd HH:mm, 3: mm
     * @return string value.
     */
    public static String convertDateFormat(final Date date, final String formatType) {
        String strDate = "";
        try {
            if (date != null) {
                String dateFormat = "";
                if (formatType.equals("0")) {
                        dateFormat = "yyyy-MM-dd";
                } else if (formatType.equals("1")) {
                        dateFormat = "yyyy-MM-dd HH:mm:ss";
                } else if (formatType.equals("2")) {
                    dateFormat = "yyyy-MM-dd HH:mm";
                } else if (formatType.equals("3")) {
                    dateFormat = "yyyy.MM.dd";
                } else if (formatType.equals("4")) {
                    dateFormat = "MM";
                } else if (formatType.equals("5")) {
                    dateFormat = "dd";
                } else if (formatType.equals("6")) {
                    dateFormat = "HH";
                } else if (formatType.equals("7")) {
                    dateFormat = "mm";
                } else if (formatType.equals("8")) {
                    dateFormat = "yyyyMMddHHmm";
                } else if (formatType.equals("9")) {
                    dateFormat = "yyyy-MM-dd-HH-mm-ss";
                }
                SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
                strDate = sdf.format(date);
            }
        } catch (Exception e) {
            logger.error(Utils.getStacktraceString(e));
        }
        return strDate;
    }
    
    /** ���ڿ� ������ DATE �������� �������ݴϴ�.
     * @param strDate strDate.
     * @param formatType formatType.
     * @return DATE.
     */
    public static Date convertStringToDate(final String strDate, final String formatType) {
     
        // �Է��� ��¥�� ���ڿ��� yyyy-MM-dd �����̹Ƿ� �ش� �������� �����͸� �����Ѵ�.
        SimpleDateFormat format = null;
        if (formatType != null && formatType.equals("0")) {
            format = new SimpleDateFormat("yyyyMMddHHmmss");
        } else if (formatType != null && formatType.equals("1")) {
            format = new SimpleDateFormat("yyyyMMddHHmm");
        } else if (formatType != null && formatType.equals("2")) {
            format = new SimpleDateFormat("yyyyMMdd");
        }

        //SimpleDateFormat.parse()�޼ҵ带 ���� Date��ü�� �����Ѵ�.
        //SimpleDateFormat.parse()�޼ҵ�� �Է��� ���ڿ� ������ ��¥��
        //���˰� �ٸ���� java.text.ParseException�� �߻��Ѵ�.
        Date date = null;
        try {
            date = format.parse(strDate);
        } catch (ParseException e) {
            logger.error(e.getMessage());
        }
        return date;
    }
    
    /**
     * ���� �⵵�� ���� ��ȯ�մϴ�.
     * @param type yyyy:�⵵, mm: �� �� ���� ��ȯ�մϴ�.
     * @return type�� ���� ���� �⵵�� ���� ��ȯ�մϴ�.
     */
    public static String getCurrentDate(final String type) {
        String currentDate = "";
        Calendar calendar = Calendar.getInstance();
        if (type.equals("yyyy")) {
            currentDate = String.valueOf(calendar.get(Calendar.YEAR));
        } else if (type.equals("mm")) {
            currentDate = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        } else if (type.equals("yyyy.mm")) {
            SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy.MM", Locale.KOREA);
            Date currentTime = new Date();
            currentDate = mSimpleDateFormat.format(currentTime);
        } else if (type.equals("yyyy.mm.dd")) {
            SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy.MM.dd", Locale.KOREA);
            Date currentTime = new Date();
            currentDate = mSimpleDateFormat.format(currentTime);
        }  else if (type.equals("yyyymmdd")) {
            SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyyMMdd", Locale.KOREA);
            Date currentTime = new Date();
            currentDate = mSimpleDateFormat.format(currentTime);
        }  else if (type.equals("yymmdd")) {
            SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyMMdd", Locale.KOREA);
            Date currentTime = new Date();
            currentDate = mSimpleDateFormat.format(currentTime);
        }  else if (type.equals("yyyy-mm-dd")) {
            SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
            Date currentTime = new Date();
            currentDate = mSimpleDateFormat.format(currentTime);
        }
        
        return currentDate;
    }
    
    /**
     * ���� ��¥�� ���� ������ ��¥�� ��ȯ.
     * @param type 0:year, 1:month, 2:day
     * @param value ������ ����
     * @param format ��¥ ����. yyyy, mm, yyyy.mm, yyyy.mm.dd, yyyy-mm-dd, yyyymmdd
     * @return format�� �°� ��¥ ���� ��ȯ.
     */
    public static String getPrevNextDateString(final String type, final int value, final String format) {
        String currentDate = "";
        Calendar calendar = Calendar.getInstance();       
        
        if (type.equals("0")) {
        	calendar.add(Calendar.YEAR, value);
        } else if (type.equals("1")) {
        	calendar.add(Calendar.MONTH, value);
        } else if (type.equals("2")) {
        	calendar.add(Calendar.DATE, value);
        }
        
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        String month = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        if (month.length() < 2) {
        	month = "0" + month;
        }
        String day = String.valueOf(calendar.get(Calendar.DATE));
        if (day.length() < 2) {
        	day = "0" + day;
        }
        
        if (format.equals("yyyy")) {
            currentDate = year;
        } else if (format.equals("mm")) {
            currentDate = month;
        } else if (format.equals("yyyy.mm")) {
            currentDate = year + "." + month;
        } else if (format.equals("yyyy.mm.dd")) {
            currentDate = year + "." + month + "." + day;
        } else if (format.equals("yyyy-mm")) {
        	currentDate = year + "-" + month;
        } else if (format.equals("yyyy-mm-dd")) {
        	currentDate = year + "-" + month + "-" + day;
        } else if (format.equals("yyyymmdd")) {
        	currentDate = year + month + day;
        } else if (format.equals("yyyymm")) {
        	currentDate = year + month;
        }
        
        return currentDate;
    }
    
    /**
     * ���� ��¥�� ���� ������ ��¥�� ��ȯ.
     * @param type 0:year, 1:month, 2:day
     * @param value ������ ����
     * @param format ��¥ ����. yyyy, mm, yyyy.mm, yyyy.mm.dd, yyyy-mm-dd, yyyymmdd
     * @param fromDate 기준 날짜
     * @return format�� �°� ��¥ ���� ��ȯ.
     */
    public static String getPrevNextDateString(final String type, final int value, final String format, final Date fromDate) {
        String currentDate = "";
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fromDate);
        
        if (type.equals("0")) {
        	calendar.add(Calendar.YEAR, value);
        } else if (type.equals("1")) {
        	calendar.add(Calendar.MONTH, value);
        } else if (type.equals("2")) {
        	calendar.add(Calendar.DATE, value);
        }
        
        String year = String.valueOf(calendar.get(Calendar.YEAR));
        String month = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        if (month.length() < 2) {
        	month = "0" + month;
        }
        String day = String.valueOf(calendar.get(Calendar.DATE));
        if (day.length() < 2) {
        	day = "0" + day;
        }
        
        if (format.equals("yyyy")) {
            currentDate = year;
        } else if (format.equals("mm")) {
            currentDate = month;
        } else if (format.equals("yyyy.mm")) {
            currentDate = year + "." + month;
        } else if (format.equals("yyyy.mm.dd")) {
            currentDate = year + "." + month + "." + day;
        } else if (format.equals("yyyy-mm")) {
        	currentDate = year + "-" + month;
        } else if (format.equals("yyyy-mm-dd")) {
        	currentDate = year + "-" + month + "-" + day;
        } else if (format.equals("yyyymmdd")) {
        	currentDate = year + month + day;
        } else if (format.equals("yyyymm")) {
        	currentDate = year + month;
        }
        
        return currentDate;
    }

    /**
     * 00전, 00후 시간 정보를 가져온다.
     * @param type 0:year, 1:month, 2:day
     * @param value 정수
     * @return Date
     */
    public static Date getPrevNextDate(final String type, final int value) {
        Calendar calendar = Calendar.getInstance();       
        
        if (type.equals("0")) {
        	calendar.add(Calendar.YEAR, value);
        } else if (type.equals("1")) {
        	calendar.add(Calendar.MONTH, value);
        } else if (type.equals("2")) {
        	calendar.add(Calendar.DATE, value);
        }
        
        return new Date(calendar.getTimeInMillis());
    }
    
    /**
     * �ݾ�ǥ�÷� �����Ͽ� ��ȯ�մϴ�.
     * @param price ����.
     * @return �ݾ�ǥ�÷� ��ȯ�� ��.
     */
    public static String convertMoneyType(final Integer price) {
        String strPrice = "0";
        if (price != null) {
            String moneyString = price.toString(); 
            String format = "#,##0"; 
            DecimalFormat df = new DecimalFormat(format); 
            DecimalFormatSymbols dfs = new DecimalFormatSymbols(); 

            dfs.setGroupingSeparator(','); // �����ڸ� ,�� 
            df.setGroupingSize(PRICE_GROUP_SIZE); //3�ڸ� �������� ������ó�� �Ѵ�. 
            df.setDecimalFormatSymbols(dfs); 

            strPrice = df.format(Double.parseDouble(moneyString)).toString();
        }
        return strPrice + "��";
    } 
    
    /**
     * ���ڿ��� null�ϰ�� ���ڿ��� ��ȯ���ش�.
     * @param str str.
     * @return str.
     */
    public static String initNullString(final String str) {
        String tmpStr = str;
        if (!StringUtils.hasLength(str)) {
            tmpStr = "";
        }
        return tmpStr;
    }
    
    /**
     * ����ð� timestamp �� Date�� ��ȯ�Ѵ�.
     * @return currentDate ����ð� Date
     */
    public static Date getCurrentDateFromTimeStamp() {
        Timestamp stamp = new Timestamp(System.currentTimeMillis());
        Date currentDate = new Date(stamp.getTime());
        
        return currentDate;
    }
    
    /**
     * Exception stacktrace ���ڿ�.
     * @param e exception
     * @return stacktrace ���ڿ�. 
     */
    public static String getStacktraceString(final Exception e) {
        String stacktrace = e.getMessage() + "\r\n";
        StackTraceElement[] stack = e.getStackTrace();
        if (e.getStackTrace() != null) {
            for (int i = 0; i < stack.length && i < STACKTRACE_MAX_LINE; i++) {
                stacktrace += stack[i].toString() + "\r\n";
            }
        }
        return stacktrace; 
    }
        
    /**
     * Object�� get Property ���� ������ List�� �����մϴ�.
     * @param <T> Generic ����.
     * @param obj ���� Object.
     * @param arrMethod List�� Property�� (get���ڴ� �����մϴ�.)
     * @return List.
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static <T> LinkedHashMap<String, String> convertObjectToList(final T obj, final String[] arrMethod) {
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        Class clsTemp = obj.getClass();
        try {
            
            for (int i = 0; i < arrMethod.length; i++) {
                String methodTemp = arrMethod[i];
                Method targetMethod = clsTemp.getDeclaredMethod(makeGeneralGetterFunctionName(methodTemp));
                Object objTemp = targetMethod.invoke(obj);
                String value = "";
                if (objTemp != null) {
                    value = objTemp.toString();
                }
                map.put(methodTemp, value);
            }
        } catch (Exception e) {
            logger.error(Utils.getStacktraceString(e));
        }
        return map;
    }
    
    /** 
     * ù���� �빮�� ���� �� Get���ڸ� �ٿ��ݴϴ�.
     * @param methodName Method Name.
     * @return Method Name.
     */
    private static String makeGeneralGetterFunctionName(final String methodName) {
     // 1. �켱 fieldName �� ���ڸ� �빮�ڷ� ġȯ�Ѵ�.
     String methodNameTemp = methodName.substring(0, 1).toUpperCase() + methodName.substring(1);
     
     // 2. �� �տ� get�� �ٿ��ش�.
     methodNameTemp = "get" + methodNameTemp;
     
     return methodNameTemp;
    }
    
    /**
     * �Է� ���� ����� ��� 1�� true�� ��ȯ, �׷��� ���� ��� 0�� false�� ��ȯ.
     * <p>
     * 
     * @since 	1.0
     * @author 	firecell - 2014. 10. 31.
     * @param value value
     * @return int
     */
    public static int getResultValue(final int value) {
    	if (value > 0) {
    		return 1;
    	} else {
    		return 0;
    	}
    }
    
    /**
     * ���丮 ����(���� ��� ���� �ؼ�...).
     * @param path ������ ���.
     * @return ���.
     */
    public static boolean deleteDirectory(final File path) {
        
        if (!path.exists()) {
            return false;
        }
        
        File[] files = path.listFiles();
            for (File file : files) {
                if (file.isDirectory()) {
                    deleteDirectory(file);
                } else {
                    file.delete();
                }
            }
            
        return path.delete();
    }
    
    /**
     * List Object �� List<Map<String, Object>> ���� convert.
     * @param objectList List Object
     * @return listMap
     */
    public static List<Map<String, Object>> convertListObjectToListMap(final List<?> objectList) {
        final List<String> excludeFields = new ArrayList<String>();
        excludeFields.add("userId");
        excludeFields.add("regDate");
        excludeFields.add("modDate");
        
        return convertListObjectToListMap(objectList, excludeFields);
    }
    
    /**
     * List Object �� List<Map<String, Object>> ���� convert.
     * @param objectList List Object
     * @param excludeFields map ������ ���� �ʵ�� ����Ʈ
     * @return listMap
     */
    public static List<Map<String, Object>> convertListObjectToListMap(final List<?> objectList, final List<String> excludeFields) {
        List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
        
        for (Object obj : objectList) {
            Field[] fields = obj.getClass().getDeclaredFields();
            Map<String, Object> defaultCh = new LinkedHashMap<String, Object>();
            
            for (int i = 0; i < fields.length; i++) {
                fields[i].setAccessible(true);
                String fieldName = fields[i].getName();
                
                if (!excludeFields.contains(fieldName)) {
                    try {
                        Object fieldValue = fields[i].get(obj);
                        defaultCh.put(fieldName, fieldValue);
                    } catch (IllegalArgumentException e) {
                        logger.info("error = {}", e.toString());
                    } catch (IllegalAccessException e) {
                        logger.info("error = {}", e.toString());
                    }
                }
            }
            listMap.add(defaultCh);
        }
        
        return listMap;
    }
    
    /**
     * Object �� Map<String, Object> ���� convert.
     * @param object object
     * @return map
     */
    public static Map<String, Object> convertObjectToMap(final Object object) {
        final List<String> excludeFields = new ArrayList<String>();
        excludeFields.add("userId");
        excludeFields.add("regDate");
        excludeFields.add("modDate");
        return convertObjectToMap(object, excludeFields);
    }
    
    /**
     * Object �� Map<String, Object> ���� convert.
     * @param object object
     * @param excludeFields map ������ ���� �ʵ�� ����Ʈ
     * @return map
     */
    public static Map<String, Object> convertObjectToMap(final Object object, final List<String> excludeFields) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        
        Field[] fields = object.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            String fieldName = fields[i].getName();
            if (!excludeFields.contains(fieldName)) {
                try {
                    Object fieldValue = fields[i].get(object);
                    map.put(fieldName, fieldValue);
                } catch (IllegalArgumentException e) {
                    logger.info("error = {}", e.toString());
                } catch (IllegalAccessException e) {
                    logger.info("error = {}", e.toString());
                }
            }
        }
        
        return map;
    }

    /**
     * onairTime���� ��ȯ���ݴϴ�. (ex. 201410100000)
     * @param onairDate onairDate.
     * @param onairHour onairHour.
     * @param onairMin onairMin.
     * @return onairTime onairTime.
     */
    public static String convertOnairTime(final String onairDate, final String onairHour, final String onairMin) {
    	return onairDate.replace("-", "") + onairHour.replace("��", "") + onairMin.replace("��", "");
    }
    
    /**
     * �������� üũ.
     * @param str ���ڿ�
     * @return ���� ����
     */
    public static boolean isNumeric(final String str) { 
        Pattern pattern = Pattern.compile("[+-]?\\d+");
        
        return pattern.matcher(str).matches(); 
    }
    
    /**
     * ���� ������ ���ϴ� ���ڷ� ä���ݴϴ�.
     * @param str ���ڿ�.
     * @param len ä�� ����.
     * @param addStr ä�� ���ڿ�.
     * @return ä�� ����.
     */
    public static String lpad(final String str, final int len, final String addStr) {
        String result = str;
        int templen   = len - result.length();
        for (int i = 0; i < templen; i++) {
              result = addStr + result;
        }
        return result;
    }
    
    
    public static int getTotalPage(final int totalCount, final int pageSize) {
    	if(totalCount % pageSize == 0) {
    		return totalCount / pageSize;
    	}
    	else {
    		return totalCount / pageSize + 1;
    	}
    } 
    
    public static int getStartPage(final int pageGroupSize, final int currentPage) {
    	int startPage = 0;
    	for(int num=1; pageGroupSize*num < currentPage; num++) {
    		if(pageGroupSize + startPage < currentPage) {
    			startPage += pageGroupSize;
    		} else {
    			break;
    		}
    	}
    	
    	return startPage+1;
    }
    
    public static int getEndPage(final int totalPage, final int pageGroupSize, final int startPage, final int currentPage) {
    	int endPage = startPage + pageGroupSize - 1;
    	for(int num=endPage/pageGroupSize; pageGroupSize*num < totalPage; num++) {
    		if(pageGroupSize*num > currentPage) {
    			endPage = pageGroupSize*num;
    		} else {
    			break;
    		}
    	}
    	
    	if(endPage > totalPage) {
    		endPage = totalPage;
    	}
    	
    	return endPage;
    }
    
    public static boolean checkHsid(final String hsid) {
    	boolean result = true;
    	
    	if(!hsid.contains("-")) {
    		result = false;
    	} else {
    		String[] arr = hsid.split("-");
    		if(arr.length != 2) {
    			result = false;
    		} else {
    			if(!isNumber(arr[0]) || !isNumber(arr[1])) {
    				result = false;
    			}
    		}
    	}
    	
    	return result;
    }

    public static boolean isNumber(String str){
        boolean result = false; 
        try{
            Double.parseDouble(str) ;
            result = true ;
        }catch(Exception e){}
         
        return result ;
    }
    
    public static boolean isEmail(String email) {
        if (email==null) return false;
        boolean b = Pattern.matches("[\\w\\~\\-\\.]+@[\\w\\~\\-]+(\\.[\\w\\~\\-]+)+",email.trim());
        return b;
    }
    
    public static boolean isPrevMonth(Date prevDate, int month) {
    	boolean ret = false;
    	
    	Date prevMonth = Utils.getPrevNextDate("1", month);
    	
    	if(prevMonth.getTime() > prevDate.getTime()) {
    		ret = true;
    	}
    	
    	return ret;
    }
    
    public static String toJsonString(Object obj) {
		String ret = "";
		Gson gson = new Gson();
		ret = gson.toJson(obj);
		
		return ret;
    }
    
    public static String getDateString(Date date) {
    	DateFormat sdFormat = new SimpleDateFormat("yyyyMMddHHmmss");
    	String tempDate = sdFormat.format(date);
    	
    	return tempDate;
    }
    
    public static HashMap<String, Object> getMapFromJsonString(String json) {
    	HashMap<String, Object> map = new HashMap<String, Object>();
    	ObjectMapper mapper = new ObjectMapper();
    	
    	try {
			map = mapper.readValue(json, new TypeReference<Map<String, Object>>(){});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.info("error = {}", e.toString());
		}
    	
    	return map;
    }
    
    public static Date getDateFromString(String from, String format) {
    	Date ret = null;
    	SimpleDateFormat transFormat = new SimpleDateFormat(format);
    	try {
			ret = transFormat.parse(from);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.info("error = {}", e.toString());
		}
    	
    	return ret;
    }
    
    public static String getPrevDate(String date, int val) {
    	Date day = Utils.getDateFromString(date, "yyyyMMdd");

        Calendar cal = Calendar.getInstance();
        cal.setTime(day);
        cal.add(Calendar.DATE, val);

        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        String strDate = df.format(cal.getTime());
        
        return strDate;
    }
}
