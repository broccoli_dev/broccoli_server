package com.pam.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

public class ByteUtil {

	public ByteUtil() {
		// TODO Auto-generated constructor stub
	}

	// byte[] to hex
	public static String byteArrayToHex(byte[] ba) {
	    if (ba == null || ba.length == 0) {
	        return null;
	    }
	 
	    StringBuffer sb = new StringBuffer(ba.length * 2);
	    String hexNumber;
	    for (int x = 0; x < ba.length; x++) {
	        hexNumber = "0" + Integer.toHexString(0xff & ba[x]);
	 
	        sb.append(hexNumber.substring(hexNumber.length() - 2));
	    }
	    return sb.toString();
	} 
	
	// hex to byte[]
	public static byte[] hexToByteArray(String hex) {
	    if (hex == null || hex.length() == 0) {
	        return null;
	    }
	 
	    byte[] ba = new byte[hex.length() / 2];
	    for (int i = 0; i < ba.length; i++) {
	        ba[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
	    }
	    return ba;
	}

    public static byte[] concat(byte[] b1, byte[] b2) {
        int n1 = b1.length;
        int n2 = b2.length;
        byte[] b = new byte[n1 + n2];
        System.arraycopy(b1, 0, b, 0, n1);
        System.arraycopy(b2, 0, b, n1, n2);
        return b;
    }
    
    public static byte[] inputStreamToByteArray(InputStream is) {        
        byte[] resBytes = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
         
        byte[] buffer = new byte[1024];
        int read = -1;
        try {
            while ( (read = is.read(buffer)) != -1 ) {
               bos.write(buffer, 0, read);
            }
             
            resBytes = bos.toByteArray();
            bos.close();
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
         
        return resBytes;
    }
    
    public static byte[] longToByteArray(long in) {
        byte[] resBytes = ByteBuffer.allocate(Long.SIZE / Byte.SIZE).putLong(in).array();
        
        return resBytes;
    }
}
