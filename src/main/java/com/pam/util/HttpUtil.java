package com.pam.util;

import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.map.ObjectMapper;


public class HttpUtil {
	
	//@@public static LookupService _CounrtyLookup;
	
	@SuppressWarnings("unchecked")
	public static Map<String, ?> getRequestVariables( HttpServletRequest request ) {
		Map<String, ? > requestData = Collections.emptyMap();		
	    if ( request.getContentLength() > 0 ) {	 
	    	try {
	    		requestData= new ObjectMapper().readValue( request.getInputStream(), Map.class );
	    	}
	    	catch( Exception e ) {
	    		;
	    	}
	    }
	    
	    return requestData;
	}
}
