package com.pam.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Date;
/*
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;*/

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pam.security.AES256Util;
import com.pam.util.ByteUtil;

public class SecureUtil {
    private final static Logger logger = LoggerFactory.getLogger(SecureUtil.class);
    
	/**
	 * generate random value
	 * @param length random value length
	 * @return (length) bytes random value string
	 */
	public static String genRandom(int length) {
    	SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[length];
        random.nextBytes(bytes);
        
        return ByteUtil.byteArrayToHex(bytes);
	}
	
	public static int getRandom(int length){
		String number = "1";
		for(int i=0;i<length;i++) number += "0";
		return (int)(Math.random()*Integer.parseInt(number));
	}
	
	public static byte[] genSaltFromDate(Date date) {
		long timemillis = date.getTime();
		timemillis = timemillis / 10000L;
		timemillis = timemillis * timemillis * timemillis;
		byte[] salt = ByteUtil.longToByteArray(timemillis);
		
		return salt;
	}
	
	public static String encryptString(String str, String key) {
		String ret = "";
		
		try {
			AES256Util util = new AES256Util(key);
			try {
				ret = util.aesEncode(str);
			} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
					| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			logger.info("error = {}", e.toString());
		}
		
		return ret;
	}
	
	public static String decryptString(String str, String key) {
		String ret = "";
		
		try {
			AES256Util util = new AES256Util(key);
			
			try {
				ret = util.aesDecode(str);
			} catch (InvalidKeyException | UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException
					| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
				// TODO Auto-generated catch block
				logger.info("error = {}", e.toString());
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			logger.info("error = {}", e.toString());
		}
		
		return ret;
	}
}
