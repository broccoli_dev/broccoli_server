package com.pam.util;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "HttpResponse")
public class HttpResponse<T> implements Serializable {
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 7392084466075771236L;
	@XmlElement(name = "code", required=true)
	private String _code;
	@XmlElement(name = "data", required=false)
	private T _data;
 
	public HttpResponse() {
		_code = null;
		_data = null;
	}

	public HttpResponse( String code, T data ) {
		_code = code;
		_data = data;
	}
	
	public String getCode() {
		return _code;
	}
	
	public void setCode(String code) {
		this._code = code;
	}
	
	public void setData( T data ) {
		_data = data;
	}
	
	public T getData() {
		return _data;
	}
}
