package com.pam.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.pam.entity.CategoryTag;
import com.pam.entity.MoneyCalendarTag;


public class Global {
	protected final static Log _logger = LogFactory.getLog("Global");

	public static List<CategoryTag> tagList = new ArrayList<CategoryTag>();
	public static List<MoneyCalendarTag> mcList = new ArrayList<MoneyCalendarTag>();

	public static HashMap<String, String> secritKeyEncryptionAlgorithm;
	public static HashMap<String, String> publicKeyEncryptionAlgorithm;
	public static HashMap<String, String> hashAlgorithm;
	public static Integer HTTP_SESSION_EXPIRE_MIN = 30;
		
	public static void init() {	
	}
}
