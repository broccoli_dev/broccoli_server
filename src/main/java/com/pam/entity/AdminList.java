package com.pam.entity;

import lombok.Data;

public @Data class AdminList {
	private String adminId;
	private String authCode;
	private String regDate;
	private String modDate;
}
