package com.pam.entity;

import lombok.Data;

public @Data class LargeCategory {
	private String largeCtgryCode;
	private String largeCtgryName;
	private String ctgryCode;
	private String ctgryName;
}