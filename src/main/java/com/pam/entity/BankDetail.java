package com.pam.entity;

import lombok.Data;

public @Data class BankDetail {
	private String transDate;
	private String transMethod;
	private String opponent;
	private String sum;
}
