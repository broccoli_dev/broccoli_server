package com.pam.entity;

import lombok.Data;

public @Data class Category {
	private String ctgryCode;
	private String ctgryName;
}