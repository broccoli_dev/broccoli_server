package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class CardCodes {
	private List<CardCompanyCode> company;
	private List<CardTypeCode> type;
	private List<CardBrandCode> brand;
	private List<CardBenefitLargeCode> largeBenefit;
}
