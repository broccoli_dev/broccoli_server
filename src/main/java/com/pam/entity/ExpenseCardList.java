package com.pam.entity;

import lombok.Data;

public @Data class ExpenseCardList {
	private String cardId;
	private String companyCode;
	private String cardName;
	private String cardCode;
	private String isCredit;
	private String sum;
	private String totalLimit;
	private CardBenefit benefit;
	
	public void setCardUse(CardUse cardUse) {
		this.cardId = cardUse.getCardId();
		this.companyCode = cardUse.getCompanyCode();
		this.cardName = cardUse.getCardName();
		this.cardCode = cardUse.getCardCode();
		this.isCredit = cardUse.getIsCredit();
		this.sum = cardUse.getSum();
		this.totalLimit = cardUse.getTotalLimit();
	}
}
