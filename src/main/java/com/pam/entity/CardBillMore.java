package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class CardBillMore {
	private String sum;
	private List<CardBillDetail> list;
}
