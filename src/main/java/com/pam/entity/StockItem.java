package com.pam.entity;

import lombok.Data;

public @Data class StockItem {
	private String stockCode;
	private String stockName;
	private String sum;
	private String prevSum;
}
