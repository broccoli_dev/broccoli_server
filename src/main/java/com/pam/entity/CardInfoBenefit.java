package com.pam.entity;

import lombok.Data;

public @Data class CardInfoBenefit {
	private String benefitLargeCtgry;
	private String benefitCtgry;
	private String benefitName;
}
