package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class ExpenseCategoryMonth {
	private String budget;
	private String expense;
	private List<ExpenseDetail> list;
}
