package com.pam.entity;

import java.util.Date;

import lombok.Data;

public @Data class MoneyCal {
	private Long calId;
	private String userId;
	private String title;
	private Long transId;
	private Long amt;
	private String payType;
	private String payDate;
	private String rptType;
	private String useYn;
	private String exptYn;
	private Date regDate;
	private Date modDate;
	private String matchedTag;
	private String oppnnt;
}