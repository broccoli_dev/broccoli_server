package com.pam.entity;

import lombok.Data;

public @Data class UserIdNameDate {
	private String userId;
	private String userName;
	private String regDate;
}
