package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class AssetEtc {
	private List<RealEstate> realEstate;
	private List<Car> car;
}
