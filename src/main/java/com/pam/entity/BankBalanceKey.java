package com.pam.entity;

import lombok.Data;

public @Data class BankBalanceKey {
	private Long accountId;
	private String date;

	@Override
 	public boolean equals(Object object) {
		boolean result = false;
		if (object == null || object.getClass() != getClass()) {
			result = false;
		} else {
			BankBalanceKey key = (BankBalanceKey) object;
			Long _accountId = key.getAccountId();
			String _date = key.getDate();
			
			if(this.accountId == _accountId && this.date.equals(_date)) {
				result = true;
			}
		}
		
		return result;
	}

	  @Override
	  public int hashCode() {
	    int hash = 3;
	    hash = 7 * hash + this.accountId.hashCode();
	    hash = 7 * hash + this.date.hashCode();
	    return hash;
	  }
}
