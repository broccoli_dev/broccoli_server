package com.pam.entity;

import lombok.Data;

public @Data class ChallengeList {
	private String challengeId;
	private String challengeType;
	private String title;
	private String startDate;
	private String endDate;
	private String amt;
	private String goalAmt;
	private String monthlyAmt;
	private String accountId;
	private String imagePath;
}
