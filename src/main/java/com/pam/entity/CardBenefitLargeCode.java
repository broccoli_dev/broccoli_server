package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class CardBenefitLargeCode {
	private String benefitLargeCtgryCode;
	private String benefitLargeCtgryName;
	private List<CardBenefitCode> benefit;
}
