package com.pam.entity;

import lombok.Data;

public @Data class Payload {
	private String alert;
	private Integer badge;
}