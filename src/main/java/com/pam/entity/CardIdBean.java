package com.pam.entity;

import lombok.Data;

public @Data class CardIdBean {
	private String userId;
	private String cmpnyCode;
	private String cardNum;
	private String billYn;
	private String useYn;
	
	@Override
 	public boolean equals(Object object) {
		boolean result = false;
		if (object == null || object.getClass() != getClass()) {
			result = false;
		} else {
			CardIdBean bean = (CardIdBean) object;
			String _userId = bean.getUserId();
			String _cmpnyCode = bean.getCmpnyCode();
			String _cardNum = bean.getCardNum();
			String _billYn = bean.getBillYn();
			String _useYn = bean.getUseYn();
			
			if(this.userId.equals(_userId) && this.cmpnyCode.equals(_cmpnyCode) && this.cardNum.equals(_cardNum) && this.billYn.equals(_billYn) && this.useYn.equals(_useYn)) {
				result = true;
			}
		}
		
		return result;
	}

	  @Override
	  public int hashCode() {
	    int hash = 3;
	    hash = 7 * hash + this.userId.hashCode();
	    hash = 7 * hash + this.cmpnyCode.hashCode();
	    hash = 7 * hash + this.cardNum.hashCode();
	    hash = 7 * hash + this.billYn.hashCode();
	    hash = 7 * hash + this.useYn.hashCode();
	    return hash;
	  }
}
