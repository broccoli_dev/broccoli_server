package com.pam.entity;

import lombok.Data;

public @Data class CardList {
	private String cardCode;
	private String cardName;
}
