package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class BudgetList {
	private String budgetId;
	private String budgetSum;
	private String expenseSum;
	private List<BudgetExpense> list;
}
