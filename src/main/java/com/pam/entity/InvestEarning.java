package com.pam.entity;

import lombok.Data;

public @Data class InvestEarning {
	private String date;
	private String earningRate;
}
