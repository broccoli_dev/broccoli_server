package com.pam.entity;

import lombok.Data;

public @Data class CollectSuccessInfo {
	private Integer type;
	private String uploadId;
}
