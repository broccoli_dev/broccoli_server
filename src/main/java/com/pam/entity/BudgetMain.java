package com.pam.entity;

import lombok.Data;

public @Data class BudgetMain {
	private Integer count;
	private String budgetSum;
	private String expenseSum;
}
