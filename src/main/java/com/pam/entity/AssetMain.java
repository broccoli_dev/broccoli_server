package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class AssetMain {
	private List<AssetMonthlyProperty> monthlyProperty;
	private List<AssetMonthlyProperty> monthlyDebt;
	private List<Bank> bank;
	private Card card;
	private Stock stock;
	private List<Debt> debt;
	private List<RealEstate> realEstate;
	private List<Car> car;
}
