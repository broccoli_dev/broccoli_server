package com.pam.entity;

import lombok.Data;

public @Data class CardCompanyCode {
	private String companyCode;
	private String companyName;
}
