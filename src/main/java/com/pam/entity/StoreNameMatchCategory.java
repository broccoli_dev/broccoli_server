package com.pam.entity;

import lombok.Data;

public @Data class StoreNameMatchCategory {
	private String storeName;
	private String storeTel;
	private String source;
	private String categoryRaw;
	private String categoryName;
}
