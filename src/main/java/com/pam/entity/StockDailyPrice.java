package com.pam.entity;

import lombok.Data;

public @Data class StockDailyPrice {
	private String stockCode;
	private String date;
	private String price;
}
