package com.pam.entity;

import lombok.Data;

public @Data class CardTypeCode {
	private String cardType;
	private String typeName;
}
