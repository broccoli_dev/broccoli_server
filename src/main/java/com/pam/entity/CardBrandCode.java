package com.pam.entity;

import lombok.Data;

public @Data class CardBrandCode {
	private String brandCode;
	private String brandName;
}
