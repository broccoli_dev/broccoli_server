package com.pam.entity;

import lombok.Data;

public @Data class Debt {
	private String accountId;
	private String companyCode;
	private String accountNum;
	private String accountName;
	private String sum;
}
