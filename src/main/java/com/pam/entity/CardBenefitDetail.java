package com.pam.entity;

import lombok.Data;

public @Data class CardBenefitDetail {
	private String benefitName;
}
