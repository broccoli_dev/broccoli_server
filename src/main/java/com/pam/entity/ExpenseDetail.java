package com.pam.entity;

import lombok.Data;

public @Data class ExpenseDetail {
	private String expenseId;
	private String expenseDate;
	private String opponent;
	private String categoryCode;
	private String payType;
	private String editYn;
	private String sum;
}
