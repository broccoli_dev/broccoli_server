package com.pam.entity;

import lombok.Data;

public @Data class CardAllList {
	private String cardCode;
	private String companyName;
	private String cardType;
	private String cardName;
	private String internalAnnualFee;
	private String foreignAnnualFee;
	private String cardImg;
	private String requirement;
	private String headcopy;
}
