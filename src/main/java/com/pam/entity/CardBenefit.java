package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class CardBenefit {
	private String realName;
	private String requireResult;
	private String annualFee;
	private List<CardBenefitLargeCategory> list;
}
