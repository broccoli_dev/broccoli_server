package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class ExpenseMain {
	private List<AssetMonthlyProperty> dailyExpense;
	private ExpenseList expenseList;
	private BudgetMain budget;
	private List<Budget> expenseCategoryList;
	private List<ExpensePaytype> expensePaytypeList;
	private List<ExpenseCardList> cardList;
}
