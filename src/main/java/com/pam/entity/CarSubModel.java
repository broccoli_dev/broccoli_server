package com.pam.entity;

import lombok.Data;

public @Data class CarSubModel {
	private String subCode;
}
