package com.pam.entity;

import lombok.Data;

public @Data class ChallengeBank {
	private String accountId;
	private String companyName;
	private String accountName;
	private String accountNum;
	private String addedYn;
}
