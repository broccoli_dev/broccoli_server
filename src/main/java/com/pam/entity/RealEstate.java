package com.pam.entity;

import lombok.Data;

public @Data class RealEstate {
	private String estateId;
	private String estateName;
	private String estateType;
	private String dealType;
	private String price;
}
