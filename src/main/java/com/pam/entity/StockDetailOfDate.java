package com.pam.entity;

import lombok.Data;

public @Data class StockDetailOfDate {
	private String highPrice;
	private String lowPrice;
	private String totalPrice;
	private String tradeAmount;
	private String totalAmount;
	private Float foreignerPercent;
	private Float per;
	private Float pbr;
}
