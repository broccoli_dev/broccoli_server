package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class MoneyCalendarMain {
	private Integer totalCount;
//	private List<MoneyCalendarList> list;
	private List<MoneyCalList> list;
}
