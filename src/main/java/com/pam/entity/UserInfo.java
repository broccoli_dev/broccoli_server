package com.pam.entity;

import lombok.Data;

public @Data class UserInfo {
	private String userName;
	private String birthDate;
	private String gender;
	private String marriedYn;
	private String pushYn;
}
