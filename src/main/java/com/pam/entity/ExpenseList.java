package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class ExpenseList {
	private Integer totalCount;
	private List<ExpenseDetail> list;
}
