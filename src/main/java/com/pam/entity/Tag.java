package com.pam.entity;

import lombok.Data;

public @Data class Tag {
	private String tag;
	private String ctgryCode;
	private Integer ctgryPriority;
	private Integer isMoneyCalendar;
}
