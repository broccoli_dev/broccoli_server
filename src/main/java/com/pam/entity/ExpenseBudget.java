package com.pam.entity;

import lombok.Data;

public @Data class ExpenseBudget {
	private String budget;
	private String expense;
}
