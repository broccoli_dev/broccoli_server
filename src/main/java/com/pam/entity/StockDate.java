package com.pam.entity;

import lombok.Data;

public @Data class StockDate {
	private String stockCode;
	private String date;	

	@Override
 	public boolean equals(Object object) {
		boolean result = false;
		if (object == null || object.getClass() != getClass()) {
			result = false;
		} else {
			StockDate stockDate = (StockDate) object;
			String _stockCode = stockDate.getStockCode();
			String _date = stockDate.getDate();
			
			if(this.stockCode.equals(_stockCode) && this.date.equals(_date)) {
				result = true;
			}
		}
		
		return result;
	}

	  @Override
	  public int hashCode() {
	    int hash = 3;
	    hash = 7 * hash + this.stockCode.hashCode();
	    hash = 7 * hash + this.date.hashCode();
	    return hash;
	  }
}
