package com.pam.entity;

import lombok.Data;

public @Data class Qna {
	private String qnaId;
	private String question;
	private String answer;
}
