package com.pam.entity;

import lombok.Data;

public @Data class ExpensePaytype {
	private String payType;
	private String sum;
}
