package com.pam.entity;

import lombok.Data;

public @Data class MoneyCalList {
	private String calendarId;
	private String title;
	private String opponent;
	private String amt;
	private String payType;
	private String expectOrPaid;
	private String payDate;
	private String repeatType;
	private String matchedTag;
}