package com.pam.entity;

import lombok.Data;

public @Data class PushData {
	private String deviceToken;
	private String deviceType;
	private Payload payload;
}