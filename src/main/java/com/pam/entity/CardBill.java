package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class CardBill {
	private String companyCode;
	private Integer cardCount;
	private String date;
	private String sum;
	private List<String> list;
}
