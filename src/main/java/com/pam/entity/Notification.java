package com.pam.entity;

import lombok.Data;

public @Data class Notification {
	private Long notificationId;
	private String categoryCode;
	private String message;
	private String date;
	private String readYn;
}
