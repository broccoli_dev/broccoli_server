package com.pam.entity;

import lombok.Data;

public @Data class CarMaker {
	private String makerCode;
}
