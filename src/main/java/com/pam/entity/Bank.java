package com.pam.entity;

import lombok.Data;

public @Data class Bank {
	private String accountId;
	private String companyCode;
	private String accountNum;
	private String accountName;
	private String afterValue;
	private String lastTransDate;
	private String lastTransOpponent;
	private String lastTransSum;
}
