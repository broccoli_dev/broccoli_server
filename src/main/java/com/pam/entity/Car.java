package com.pam.entity;

import lombok.Data;

public @Data class Car {
	private String carId;
	private String makerCode;
	private String carCode;
	private String madeYear;
	private String subCode;
	private String marketPrice;
}
