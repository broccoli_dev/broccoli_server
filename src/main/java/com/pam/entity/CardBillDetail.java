package com.pam.entity;

import lombok.Data;

public @Data class CardBillDetail {
	private String cardName;
	private String transDate;
	private String transType;
	private String opponent;
	private String sum;
}
