package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class CardInfo {
	private String companyCode;
	private String cardType;
	private String cardName;
	private String internalAnnualFee;
	private String foreignAnnualFee;
	private String cardImg;
	private String requirement;
	private String headcopy;
	private List<String> brand;
	private List<CardInfoBenefit> benefit;
}
