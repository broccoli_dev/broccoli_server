package com.pam.entity;

import lombok.Data;

public @Data class Budget {
	private String categoryCode;
	private String sum;
}
