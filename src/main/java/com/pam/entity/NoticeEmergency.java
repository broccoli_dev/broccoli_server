package com.pam.entity;

import lombok.Data;

public @Data class NoticeEmergency {
	private String subject;
	private String contents;
}
