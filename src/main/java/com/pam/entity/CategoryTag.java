package com.pam.entity;

import lombok.Data;

public @Data class CategoryTag {
	private String tag;
	private String ctgryCode;
}
