package com.pam.entity;

import lombok.Data;

public @Data class MoneyCalendarTag {
	private String tag;
	private String isMoneyCalendar;
}
