package com.pam.entity;

import lombok.Data;

public @Data class BankBalance {
	private Long accountId;
	private String date;
	private Long sum;
}
