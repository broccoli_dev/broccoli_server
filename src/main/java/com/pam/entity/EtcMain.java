package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class EtcMain {
	private List<ChallengeList> challenge;
	private MoneyCalendarMain moneyCalendar;
}
