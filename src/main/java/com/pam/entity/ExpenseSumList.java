package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class ExpenseSumList {
	String totalSum;
	List<ExpenseDetail> list;
}
