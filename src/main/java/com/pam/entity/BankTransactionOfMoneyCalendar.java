package com.pam.entity;

import lombok.Data;

public @Data class BankTransactionOfMoneyCalendar {
	private String userId;
	private String transDate;
	private String oppnnt;
	private String ctgryCode;
	private String transMethod;
	private String cmpnyCode;
	private Long sum;
}