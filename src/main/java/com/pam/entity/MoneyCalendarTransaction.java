package com.pam.entity;

import lombok.Data;

public @Data class MoneyCalendarTransaction {
	private String transactionId;
	private String transactionDate;
	private String transactionMethod;
	private String opponent;
	private String sum;
	private String addedYn;
}