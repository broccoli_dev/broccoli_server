package com.pam.entity;

import lombok.Data;

public @Data class CarModel {
	private String carCode;
}
