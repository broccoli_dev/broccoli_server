package com.pam.entity;

import lombok.Data;

public @Data class CarMadeYear {
	private String madeYear;
}
