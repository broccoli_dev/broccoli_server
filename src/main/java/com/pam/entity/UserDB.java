package com.pam.entity;

import lombok.Data;

public @Data class UserDB {
	private String name;
	private String age;
	private String gender;
	private String mrrdYn;
	private String bankCount;
	private String accountCount;
	private String bankBalance;
	private String debtCount;
	private String debtBalance;
	private String cardCount;
	private String cardExpectPayment;
	private String car;
	private String realEstate;
	private String expenseThisMon;
	private String expensePrvOneMon;
	private String expensePrvTwoMon;
	private String expensePrvThreeMon;
	private String expensePrvFourMon;
	private String expensePrvFiveMon;
	private String expenseFood;
	private String expenseCafe;
	private String expenseDrink;
	private String expenseLife;
	private String expenseHouse;
	private String expenseTraffic;
	private String expenseFashion;
	private String expenseOnline;
	private String expenseEducation;
	private String expenseCulture;
	private String expenseSports;
	private String expenseHealth;
	private String expenseTravel;
	private String expenseParenting;
	private String expensePet;
	private String expenseFinance;
	private String expenseCategoryEtc;
	private String expenseCredit;
	private String expenseCheck;
	private String expenseCash;
	private String expenseUsageEtc;
}
