package com.pam.entity;

import lombok.Data;

public @Data class StockPrice {
	private String stockCode;
	private String date;
	private String startPrice;
	private String endPrice;
}
