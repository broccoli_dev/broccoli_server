package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class StockDetail {
	private String count;
	private String totalSum;
	private String nowPrice;
	private String prevPrice;
	private List<StockDailyPrice> dailyPrice;
	private List<StockDetailItem> list;
	private String highPrice;
	private String lowPrice;
	private String totalPrice;
	private String tradeAmount;
	private String totalAmount;
	private Float foreignerPercent;
	private Float per;
	private Float pbr;
}
