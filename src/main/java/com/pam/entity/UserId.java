package com.pam.entity;

import lombok.Data;

public @Data class UserId {
	private String userId;
}
