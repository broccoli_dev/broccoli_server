package com.pam.entity;

import lombok.Data;

public @Data class Card {
	private Integer cardCount;
	private String sum;
	private String lastTransDate;
	private String lastTransOpponent;
	private String lastSum;
}
