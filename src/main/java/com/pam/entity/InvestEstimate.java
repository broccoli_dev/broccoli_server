package com.pam.entity;

import lombok.Data;

public @Data class InvestEstimate {
	private String date;
	private String estimate;
}
