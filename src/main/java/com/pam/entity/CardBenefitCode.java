package com.pam.entity;

import lombok.Data;

public @Data class CardBenefitCode {
	private String benefitCtgryCode;
	private String benefitCtgryName;
}
