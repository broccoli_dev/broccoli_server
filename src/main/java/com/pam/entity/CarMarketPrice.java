package com.pam.entity;

import lombok.Data;

public @Data class CarMarketPrice {
	private String marketPrice;
}
