package com.pam.entity;

import lombok.Data;

public @Data class CardUse {
	private String cardId;
	private String companyCode;
	private String cardName;
	private String cardCode;
	private String isCredit;
	private String sum;
	private String totalLimit;
}
