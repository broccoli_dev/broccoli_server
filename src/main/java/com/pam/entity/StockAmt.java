package com.pam.entity;

import lombok.Data;

public @Data class StockAmt {
	private String stockCode;
	private Long amt;
}
