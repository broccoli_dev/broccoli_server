package com.pam.entity;

import lombok.Data;

public @Data class StockCodeName {
	private String stockCode;
	private String stockName;
}
