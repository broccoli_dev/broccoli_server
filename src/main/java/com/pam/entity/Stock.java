package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class Stock {
	private String date;
	private List<StockItem> list;
}
