package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class CardBenefitLargeCategory {
	private String benefitLargeCategoryCode;
	private String benefitLargeCategoryCodeName;
	private List<String> detail;
}
