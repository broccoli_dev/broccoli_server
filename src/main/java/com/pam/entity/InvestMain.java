package com.pam.entity;

import java.util.List;

import lombok.Data;

public @Data class InvestMain {
	private String totalEstimate;
	private String difference;
	private String rate;
	private List<InvestEstimate> dailyEstimate;
	private List<InvestEarning> dailyEarningRate;
	private Stock stock;
}
