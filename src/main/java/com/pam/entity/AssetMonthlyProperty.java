package com.pam.entity;

import lombok.Data;

public @Data class AssetMonthlyProperty {
	private String date;
	private String sum;
}
