package com.pam.entity;

import lombok.Data;

public @Data class StockDetailItem {
	private String transId;
	private Integer transType;
	private String transDate;
	private String price;
	private String amount;
	private String sum;
}
