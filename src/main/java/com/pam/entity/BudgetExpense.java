package com.pam.entity;

import lombok.Data;

public @Data class BudgetExpense {
	private String categoryCode;
	private String budget;
	private String expense;
}
