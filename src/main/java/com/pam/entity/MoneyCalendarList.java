package com.pam.entity;

import lombok.Data;

public @Data class MoneyCalendarList {
	private String calendarId;
	private String title;
	private String transactionId;
	private String requestCompany;
	private String amt;
	private String payType;
	private String expectYn;
	private String payDate;
	private String repeatType;
	private String payYn;
}