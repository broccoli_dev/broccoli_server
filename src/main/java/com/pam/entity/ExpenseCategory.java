package com.pam.entity;

import lombok.Data;

public @Data class ExpenseCategory {
	private String categoryCode;
	private String sum;
	private String percent;
}
