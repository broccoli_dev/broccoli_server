package com.pam.view;

import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import com.pam.entity.UserDB;
import com.pam.util.Utils;

public class UserExcelView extends AbstractXlsxView{

	@SuppressWarnings("unchecked")
	@Override
	protected void buildExcelDocument(Map<String, Object> ModelMap, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// set Initial worksheet
		Sheet sheet = workbook.createSheet("USER");
		initialUserWorksheet(workbook, sheet);
		
		// set data
		Row row = null;
		int row_n = 2;
		
		Font font = workbook.createFont();
		font.setFontHeightInPoints((short)9);
		font.setFontName("돋움");
		
		CellStyle style = workbook.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setFont(font);
		
		List<UserDB> users = (List<UserDB>) ModelMap.get("users");
		for (UserDB user:users){
			row = sheet.createRow(row_n);
			createStringCell(row, 0, style, user.getName());
			createStringCell(row, 1, style, user.getAge());
			createStringCell(row, 2, style, user.getGender());
			createStringCell(row, 3, style, user.getMrrdYn());
			createStringCell(row, 4, style, user.getBankCount());
			createStringCell(row, 5, style, user.getAccountCount());
			createStringCell(row, 6, style, user.getBankBalance());
			createStringCell(row, 7, style, user.getDebtCount());
			createStringCell(row, 8, style, user.getDebtBalance());
			createStringCell(row, 9, style, user.getCardCount());
			createStringCell(row, 10, style, user.getCardExpectPayment());
			createStringCell(row, 11, style, user.getCar());
			createStringCell(row, 12, style, user.getRealEstate());
			createStringCell(row, 13, style, user.getExpenseThisMon());
			createStringCell(row, 14, style, user.getExpensePrvOneMon());
			createStringCell(row, 15, style, user.getExpensePrvTwoMon());
			createStringCell(row, 16, style, user.getExpensePrvThreeMon());
			createStringCell(row, 17, style, user.getExpensePrvFourMon());
			createStringCell(row, 18, style, user.getExpensePrvFiveMon());
			createStringCell(row, 19, style, user.getExpenseFood());
			createStringCell(row, 20, style, user.getExpenseCafe());
			createStringCell(row, 21, style, user.getExpenseDrink());
			createStringCell(row, 22, style, user.getExpenseLife());
			createStringCell(row, 23, style, user.getExpenseHouse());
			createStringCell(row, 24, style, user.getExpenseTraffic());
			createStringCell(row, 25, style, user.getExpenseFashion());
			createStringCell(row, 26, style, user.getExpenseOnline());
			createStringCell(row, 27, style, user.getExpenseEducation());
			createStringCell(row, 28, style, user.getExpenseCulture());
			createStringCell(row, 29, style, user.getExpenseSports());
			createStringCell(row, 30, style, user.getExpenseHealth());
			createStringCell(row, 31, style, user.getExpenseTravel());
			createStringCell(row, 32, style, user.getExpenseParenting());
			createStringCell(row, 33, style, user.getExpensePet());
			createStringCell(row, 34, style, user.getExpenseFinance());
			createStringCell(row, 35, style, user.getExpenseCategoryEtc());
			createStringCell(row, 36, style, user.getExpenseCredit());
			createStringCell(row, 37, style, user.getExpenseCheck());
			createStringCell(row, 38, style, user.getExpenseCash());
			createStringCell(row, 39, style, user.getExpenseUsageEtc());
			
			row_n++;
		}
		
		// set Response
		Date today = new Date();
		String wbName=URLEncoder.encode("BroccoliUsers-"+Utils.convertDateFormat(today, "8"), "UTF-8");
		response.setHeader("Content-Type", "application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename="+wbName+".xlsx");
		OutputStream os = null;
		os = response.getOutputStream();
		workbook.write(os);
		os.flush();
		os.close();
	}
	
	private void createStringCell(Row row, int i, CellStyle style, String value){
		Cell cell = row.createCell((short) i);
		cell.setCellStyle(style);
		cell.setCellValue(value);
	}
	
	private void initialUserWorksheet(Workbook workbook, Sheet sheet){
		Font font = workbook.createFont();
		font.setFontHeightInPoints((short)9);
		font.setFontName("돋움");
		
		CellStyle style = workbook.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setFont(font);
		
		Row row = sheet.createRow(0);
		createStringCell(row, 0, style, "회원정보");
		createStringCell(row, 4, style, "은행");
		createStringCell(row, 9, style, "신용카드");
		createStringCell(row, 13, style, "월별 소비액");
		createStringCell(row, 19, style, "전월 카테고리별 소비액");
		createStringCell(row, 36, style, "소비방법별 사용액");
		sheet.addMergedRegion(new CellRangeAddress(0,0,0,3));
		sheet.addMergedRegion(new CellRangeAddress(0,0,4,8));
		sheet.addMergedRegion(new CellRangeAddress(0,0,9,12));
		sheet.addMergedRegion(new CellRangeAddress(0,0,13,18));
		sheet.addMergedRegion(new CellRangeAddress(0,0,19,35));
		sheet.addMergedRegion(new CellRangeAddress(0,0,36,39));
		
		row = sheet.createRow(1);
		createStringCell(row, 0, style, "이름");
		createStringCell(row, 1, style, "연령");
		createStringCell(row, 2, style, "성별");
		createStringCell(row, 3, style, "결혼여부");
		createStringCell(row, 4, style, "등록은행수");
		createStringCell(row, 5, style, "예적금계좌수");
		createStringCell(row, 6, style, "예적금총액");
		createStringCell(row, 7, style, "대출계좌수");
		createStringCell(row, 8, style, "대출금총액");
		createStringCell(row, 9, style, "등록신용카드수");
		createStringCell(row, 10, style, "신용카드결제예정금액");
		createStringCell(row, 11, style, "자동차");
		createStringCell(row, 12, style, "부동산");
		createStringCell(row, 13, style, "당월소비액");
		createStringCell(row, 14, style, "당월-1 소비액");
		createStringCell(row, 15, style, "당월-2 소비액");
		createStringCell(row, 16, style, "당월-3 소비액");
		createStringCell(row, 17, style, "당월-4 소비액");
		createStringCell(row, 18, style, "당월-5 소비액");
		createStringCell(row, 19, style, "음식");
		createStringCell(row, 20, style, "카페/간식");
		createStringCell(row, 21, style, "술/유흥");
		createStringCell(row, 22, style, "생활/마트");
		createStringCell(row, 23, style, "주거/통신");
		createStringCell(row, 24, style, "교통/차량");
		createStringCell(row, 25, style, "패션/미용");
		createStringCell(row, 26, style, "온라인쇼핑");
		createStringCell(row, 27, style, "교육");
		createStringCell(row, 28, style, "문화/예술");
		createStringCell(row, 29, style, "스포츠");
		createStringCell(row, 30, style, "건강");
		createStringCell(row, 31, style, "여행/숙박");
		createStringCell(row, 32, style, "육아");
		createStringCell(row, 33, style, "애완동물");
		createStringCell(row, 34, style, "금융/보험");
		createStringCell(row, 35, style, "기타");
		createStringCell(row, 36, style, "신용카드");
		createStringCell(row, 37, style, "체크카드");
		createStringCell(row, 38, style, "현금");
		createStringCell(row, 39, style, "기타");
	}
}
