package com.pam.manager.scraping;

import java.util.Arrays;

import com.pam.constant.Const;
import com.pam.util.Utils;

public class ScrapingUtil {
	public static String getBankAcctTypeFromString(String accName) {
		String ret = "";
		
		if(accName.contains(Const.ACCT_TYPE_STRING_DEPOSIT)) {
			ret = Const.ACCT_TYPE_DEPOSIT;
		} else if(accName.contains(Const.ACCT_TYPE_STRING_ISTMNT_SAVINGS)) {
			ret = Const.ACCT_TYPE_ISTMNT_SAVINGS;
		} else if(accName.contains(Const.ACCT_TYPE_STRING_STOCK)) {
			ret = Const.ACCT_TYPE_STOCK;
		} else if(accName.contains(Const.ACCT_TYPE_STRING_FUND)) {
			ret = Const.ACCT_TYPE_FUND;
		} else if(accName.contains(Const.ACCT_TYPE_STRING_CARD)) {
			ret = Const.ACCT_TYPE_CARD;
		} else if(accName.contains(Const.ACCT_TYPE_STRING_LOAN)) {
			ret = Const.ACCT_TYPE_LOAN;
		} else {
			ret = Const.ACCT_TYPE_ETC;
		}
		
		return ret;
	}
	
	public static Long getLongValueFromString(String value) {
		Long ret = 0L;
	    String tmp = value.replaceAll("[^0-9]", "");
	    
		if(tmp != null && !tmp.equals("")) {
			ret = Long.parseLong(tmp);
		} 
		
		if(value.startsWith("-")) { // 음수면 -를 붙힌다.
			ret = (-1) * ret;
		}
		
		return ret;
	}

	public static Float getFloatValueFromString(String value) {
		Float ret = 0F;
		if(value != null && !value.equals("")) {
			value = value.replace(",", "").replace(" ", "");
			ret = Float.parseFloat(value);
		}		
		
		return ret;
	}
	
	public static String getSmallCtgryCodeFromAcctKindAndType(String acctKind, String acctType) {
		String ret = "";
		
		if(acctKind.contains(Const.ACCT_TYPE_STRING_LOAN) || acctType.contains(Const.ACCT_TYPE_LOAN)) {
			ret = Const.ACCT_TYPE_LOAN;
		} else if(acctKind.contains(Const.ACCT_TYPE_STRING_FUND) || acctType.contains(Const.ACCT_TYPE_STRING_FUND)) {
			ret = Const.ACCT_TYPE_FUND;
		} else if(acctKind.contains(Const.ACCT_TYPE_STRING_DEP_AND_IST) || acctKind.contains(Const.ACCT_TYPE_STRING_DEP_AND_IST)) { // "예적금"은 일단 예금으로
			ret = Const.ACCT_TYPE_DEPOSIT;
		} else if(acctKind.contains(Const.ACCT_TYPE_STRING_ISTMNT_SAVINGS) || acctType.contains(Const.ACCT_TYPE_STRING_ISTMNT_SAVINGS)) {
			ret = Const.ACCT_TYPE_ISTMNT_SAVINGS;
		} else if(acctKind.contains(Const.ACCT_TYPE_STRING_STOCK) || acctType.contains(Const.ACCT_TYPE_STRING_STOCK)) {
			ret = Const.ACCT_TYPE_STOCK;
		} else {
			ret = Const.ACCT_TYPE_DEPOSIT;
		}
		
		return ret;
	}
	
	public static Long getSumFromInOutBal(String inBal, String outBal) {
		Long ret = null;
		
		Long in = ScrapingUtil.getLongValueFromString(inBal);
		Long out = ScrapingUtil.getLongValueFromString(outBal);
		
		if(in > 0) {
			ret = in;
		} else if(out > 0) {
			ret = Long.valueOf(-1) * out;
		} else {
			ret = Long.valueOf(0);
		}
		
		return ret;
	}
	
	public static String getIstmYnFromUseDis(String useDis) {
		String ret = "N";
		
		if(useDis.contains("일시불")) {
			ret = "N";
		} else if(useDis.contains("할부") || useDis.contains("개월")) {
			ret = "Y";
		}
		
		return ret;
	}
	
	public static String getAprvlDate(String appDate, String appTime) {
		String ret = "";
		
		appDate = appDate.replace("-", "").replace(" ", "");
		ret += appDate;
		
		if(appTime != null) {
			appTime.replace(":", "").replace(" ", "");
		} else {
			appTime = "000000";
		}
		
		ret += appTime;
		
		// appTime에 14자리 들어있는 경우도 있음
		if(appTime.length() == 14) {
			ret = appTime;
		}
		
		return ret;
	}
	
	public static String getAprvlYnFromAppGubun(String appGubun) {
		String ret = "1";
		
		if(appGubun.contains("승인")) {
			ret = "1";
		} else {
			ret = "0";
		}
		
		return ret;
	}

	public static String getDdctYnFromDeductYn(String deductYn) {
		String ret = "N";
		
		if(deductYn.contains("공제")) {
			ret = "Y";
		} 
		
		return ret;
	}

	public static Integer getIssueType(String issueType) {
		Integer ret = 1;
		
		if(issueType.contains("일반")) {
			ret = 1;
		} else {
			ret = 0;
		}
		
		return ret;
	}
	
	public static Integer getIstmMon(String Qouta) {
		Integer ret = null;
		
		if(Utils.isStringEmpty(Qouta)) {
			ret = 0;
		} else {
			ret = Integer.parseInt(Qouta);
		}
		
		return ret;
	}
	
	public static String getFrgnYnFromFxUse(String fxUse) {
		String ret = "N";
				
		return ret;
	}

	public static String getPayTypeFromTransDes(String transDes) {
		String ret = "";
		
		if(transDes.contains("체크") || transDes.contains("신한체")) {
			ret = Const.PAY_TYPE_CHECK;
		} else {
			ret = Const.PAY_TYPE_ETC;
		}
		
		return ret;
	}
	
	public static String makeUseDate(String useDate) {
		String ret = "";
		
		if(useDate.length() == 6 && useDate.startsWith("1")) {
			ret = "20" + useDate;
		} else {
			ret = useDate;
		}
		
		return ret;
	}

	
	public static boolean checkCompanyTypeCodes(String companyType, String companyCode) {
		boolean ret = false;
		
		if(companyType.equals(Const.COMPANY_TYPE_BANK)) { // 은행
			if(Arrays.asList(Const.COMPANY_BANK_LIST).contains(companyCode)) {
				ret = true;
			}
		} else if(companyType.equals(Const.COMPANY_TYPE_CARD)) { // 카드
			if(Arrays.asList(Const.COMPANY_CARD_LIST).contains(companyCode)) {
				ret = true;
			}
			
		} else if(companyType.equals(Const.COMPANY_TYPE_GO)) { // 국세청
			if(Arrays.asList(Const.COMPANY_GO_LIST).contains(companyCode)) {
				ret = true;
			}
		} else {
			
		}
		
		return ret;
	}
	
	public static String getNoSpaceString(String org) {
		String ret = "";
		
		if(org != null) {
			ret = ScrapingUtil.toHalfChar(org);
			ret = ret.replaceAll(" ", "").replaceAll("\\s", "");
		}
		
		return ret;
	}
	
	private static String toHalfChar(String src)
    {
        StringBuffer strBuf = new StringBuffer();
        char c = 0;
        int nSrcLength = src.length();
        for (int i = 0; i < nSrcLength; i++)
        {
            c = src.charAt(i);
            //영문이거나 특수 문자 일경우.
            if (c >= '！' && c <= '～')
            {
                c -= 0xfee0;
            }
            else if (c == '　')
            {
                c = 0x20;
            }
            // 문자열 버퍼에 변환된 문자를 쌓는다
            strBuf.append(c);
        }
        return strBuf.toString();
    }    
}