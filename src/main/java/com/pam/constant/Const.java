package com.pam.constant;

public class Const {
	// 스크래핑 데이터 종류
	public static final Integer COLLECT_TYPE_BANK_ACCOUNT = 1; // 은행 계좌 목록
	public static final Integer COLLECT_TYPE_BANK_TRANSACTION = 2; // 은행 거래 내역
	public static final Integer COLLECT_TYPE_CARD_APPROVAL = 3; // 카드 승인 내역
	public static final Integer COLLECT_TYPE_CARD_BILL = 4; // 카드 청구서
	public static final Integer COLLECT_TYPE_CARD_LIMIT = 5; // 카드 한도
	public static final Integer COLLECT_TYPE_CARD_SCHEDULE = 6; // 카드 결제 예정
	public static final Integer COLLECT_TYPE_CASH_RECEIPT = 7; // 현금 영수증	
	
	// 자산 종류 이름
	public static final String ACCT_TYPE_STRING_DEPOSIT = "예금"; // 예금
	public static final String ACCT_TYPE_STRING_CHUNG = "청약"; // 청약
	public static final String ACCT_TYPE_STRING_ISTMNT_SAVINGS = "적금"; // 적금
	public static final String ACCT_TYPE_STRING_STOCK = "주식"; // 주식
	public static final String ACCT_TYPE_STRING_FUND = "펀드"; // 펀드
	public static final String ACCT_TYPE_STRING_CARD = "카드"; // 카드
	public static final String ACCT_TYPE_STRING_LOAN = "대출"; // 대출
	public static final String ACCT_TYPE_STRING_DEP_AND_IST = "예적금"; // 예적금
	
	// 자산 종류 코드
	public static final String ACCT_TYPE_DEPOSIT = "01"; // 예금
	public static final String ACCT_TYPE_ISTMNT_SAVINGS = "02"; // 적금
	public static final String ACCT_TYPE_STOCK = "03"; // 주식
	public static final String ACCT_TYPE_FUND = "04"; // 펀드
	public static final String ACCT_TYPE_CARD = "11"; // 카드
	public static final String ACCT_TYPE_LOAN = "12"; // 대출
	public static final String ACCT_TYPE_ETC = "99"; // 기타
	
	// 은행 입출금
	public static final Integer BANK_TRANS_TYPE_IN = 1; // 입금
	public static final Integer BANK_TRANS_TYPE_OUT = 2; // 출금
	
	// 카테고리 코드
	public static final String CATEGORY_CODE_TOTAL = "00";
	public static final String CATEGORY_CODE_ETC = "99";
	public static final String CATEGORY_CODE_NOT_DEFINED = "1801"; // 미분류
	
	// 결제 수단	01:신용카드, 02:체크카드, 03:현금, 04:계좌이체, 99:미분류
	public static final String PAY_TYPE_CARD = "01";
	public static final String PAY_TYPE_CHECK = "02";
	public static final String PAY_TYPE_CASH = "03";
	public static final String PAY_TYPE_TRANS = "04";
	public static final String PAY_TYPE_ETC = "99";
	
	// 업체 대 분류
	public static final String COMPANY_TYPE_BANK = "01"; // 은행
	public static final String COMPANY_TYPE_CARD = "02"; // 카드사
	public static final String COMPANY_TYPE_STOCK = "03"; // 증권사
	public static final String COMPANY_TYPE_INSURE = "04"; // 보험사
	public static final String COMPANY_TYPE_GO = "11"; // 정부 기관
	
	// 은행사 코드
	public static final String BANK_KB = "004"; // 국민
	public static final String BANK_SH = "088"; // 신한
	public static final String BANK_NH = "011"; // 농협
	public static final String BANK_KE = "005"; // 외환
	public static final String BANK_SC = "023"; // SC
	public static final String BANK_WR = "020"; // 우리
	public static final String BANK_KF = "045"; // 새마을
	public static final String BANK_DB = "031"; // 대구
	public static final String BANK_PS = "032"; // 부산
	public static final String BANK_KD = "002"; // 산업
	public static final String BANK_SU = "007"; // 수협
	public static final String BANK_KN = "039"; // 경남
	public static final String BANK_CU = "048"; // 신협
	public static final String BANK_CT = "027"; // 씨티
	public static final String BANK_KJ = "034"; // 광주
	public static final String BANK_EP = "071"; // 우체국
	public static final String BANK_JB = "037"; // 전북
	public static final String BANK_IB = "003"; // 기업
	public static final String BANK_JJ = "035"; // 제주
	public static final String BANK_HN = "081"; // 하나
	
	public static final String[] COMPANY_BANK_LIST = {BANK_KB, 
														BANK_SH,
														BANK_NH, 
														BANK_KE, 
														BANK_SC, 
														BANK_WR, 
														BANK_KF, 
														BANK_DB, 
														BANK_PS, 
														BANK_KD, 
														BANK_SU, 
														BANK_KN, 
														BANK_CU, 
														BANK_CT, 
														BANK_KJ, 
														BANK_EP, 
														BANK_JB, 
														BANK_IB, 
														BANK_JJ, 
														BANK_HN};
	
	// 카드사 코드
	public static final String CARD_KB = "502"; // 국민
	public static final String CARD_NH = "514"; // 농협
	public static final String CARD_LT = "508"; // 롯데
	public static final String CARD_SS = "504"; // 삼성
	public static final String CARD_SH = "506"; // 신한
	public static final String CARD_CT = "501"; // 씨티
	public static final String CARD_WR = "511"; // 우리
	public static final String CARD_HD = "505"; // 현대
	public static final String CARD_BC = "503"; // 비씨
	public static final String CARD_HN = "509"; // 하나
	
	public static final String[] COMPANY_CARD_LIST = {CARD_KB,
													CARD_NH,
													CARD_LT,
													CARD_SS,
													CARD_SH,
													CARD_CT,
													CARD_WR,
													CARD_HD,
													CARD_BC,
													CARD_HN};
	
	// 현금영수증(국세청)
	public static final String CASH_HT = "054"; // 국세청
	
	public static final String[] COMPANY_GO_LIST = {CASH_HT};
	
	// 카테고리 갯수
	public static final int TOTAL_CATEGORY_COUNT = 19;
	
	// 머니캘린더 결제 수단
	public static final String MONEY_CALENDAR_PAY_TYPE_ACCOUNT = "01";
	public static final String MONEY_CALENDAR_PAY_TYPE_AUTO = "02";
	
	// 머니캘린더 반복
	public static final String MONEY_CALENDAR_REPEAT_1W = "1W";
	public static final String MONEY_CALENDAR_REPEAT_1M = "1M";
	public static final String MONEY_CALENDAR_REPEAT_3M = "3W";
	public static final String MONEY_CALENDAR_REPEAT_6M = "6W";
	public static final String MONEY_CALENDAR_REPEAT_1Y = "1Y";	
	
	// 투자 조회 개월 수
	public static final Integer INVEST_INQUIRY_MONTH = 3;
	
	// 알림 대분류 항목
	public static final String NOTIFICATION_LARGE_CATEGORY_EXPENSE = "01"; // 소비
	public static final String NOTIFICATION_LARGE_CATEGORY_INVEST = "02"; // 투자
	public static final String NOTIFICATION_LARGE_CATEGORY_CALENDAR = "03"; // 머니캘린더
	public static final String NOTIFICATION_LARGE_CATEGORY_CHALLENGE = "04"; // 챌린지
	public static final String NOTIFICATION_LARGE_CATEGORY_FINANCE = "05"; // 금융기관 연동
	public static final String NOTIFICATION_LARGE_CATEGORY_UPDATE = "06"; // 버전 업데이트
	
	// 알림 소분류 항목
	public static final String NOTIFICATION_SMALL_CATEGORY_EXPENSE_NON_CATEGORY = "0101"; // 카테고리 미분류
	public static final String NOTIFICATION_SMALL_CATEGORY_EXPENSE_MONTH_CATEGORY = "0102"; // 지난 달 미분류
	public static final String NOTIFICATION_SMALL_CATEGORY_EXPENSE_NEW_BUDGET = "0103"; // 새 예산 등록
	public static final String NOTIFICATION_SMALL_CATEGORY_EXPENSE_90PERCENT_BUDGET = "0104"; // 총 예산 90% 초과
	public static final String NOTIFICATION_SMALL_CATEGORY_EXPENSE_OVER_BUDGET = "0105"; // 총 예산 오버
	public static final String NOTIFICATION_SMALL_CATEGORY_EXPENSE_CATEGORY_OVER_BUDGET = "0106"; // 카테고리 예산 오버
	public static final String NOTIFICATION_SMALL_CATEGORY_INVEST_STOCK = "0201"; // 주식 확인
	public static final String NOTIFICATION_SMALL_CATEGORY_CALENDAR_ADD = "0301"; // 최초 머니캘린더 자동 등록 
	public static final String NOTIFICATION_SMALL_CATEGORY_CALENDAR_TOMMOROW = "0302"; // 내일 머니캘린더 일정 있음
	public static final String NOTIFICATION_SMALL_CATEGORY_CALENDAR_TODAY = "0303"; // 오늘 머니캘린더 일정 있음
	public static final String NOTIFICATION_SMALL_CATEGORY_CALENDAR_PAID = "0304"; // 머니캘린더 납부 완료
	public static final String NOTIFICATION_SMALL_CATEGORY_CALENDAR_UNPAID = "0305"; // 머니캘린더 미납
	public static final String NOTIFICATION_SMALL_CATEGORY_CHALLENGE_PROCESS = "0401"; // 챌린지 계좌 변동 사항 있음(진행 중)
	public static final String NOTIFICATION_SMALL_CATEGORY_CHALLENGE_COMPLETE = "0402"; // 챌린지 달성
	public static final String NOTIFICATION_SMALL_CATEGORY_CHALLENGE_NEED_ACCOUNT = "0403"; // 챌린지 계좌 등록

	// 알림 조회 초기화 일 수
	public static final Integer NOTIFICATION_NON_CATEGORY_RESET_DATE = 60; // 카테고리 미분류 60일
	public static final Integer NOTIFICATION_INVEST_RESET_DATE = 1; // 주식 확인 1일(매일)
	public static final Integer NOTIFICATION_CHALLENGE_NEED_ACCOUNT_RESET_DATE = 60; // 카테고리 미분류 60일
	
	// 일정 명 빈칸
	public static final String NOTIFICATION_TITLE_BLANK = "OOO";
	public static final String NOTIFICATION_PERCENT_BLANK = "NN";
	
	// 주식 거래내역 최대 범위
	public static final Integer STOCK_DETAIL_MAX_PERIOD = 365 * 3; // 3년
	
	// 챌린지 은행 계좌 종류
	public static final Integer CHALLENGE_BANK_TYPE_ALL = 0; // 전체
	public static final Integer CHALLENGE_BANK_TYPE_DEPOSIT = 1; // 예금/적금
	public static final Integer CHALLENGE_BANK_TYPE_DEBT = 2; // 대출
	
	
}
