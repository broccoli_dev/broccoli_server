package com.pam.service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.constant.Const;
import com.pam.dao.InvestDao;
import com.pam.entity.InvestEarning;
import com.pam.entity.InvestEstimate;
import com.pam.entity.InvestMain;
import com.pam.entity.StockDate;
import com.pam.entity.StockPrice;
import com.pam.manager.scraping.ScrapingUtil;
import com.pam.mapper.TblChallengeMapper;
import com.pam.mapper.TblMoneyCalendarMapper;
import com.pam.mapper.TblStockTransactionMapper;
import com.pam.mapper.domain.TblStockTransaction;
import com.pam.mapper.domain.TblStockTransactionExample;

@Service
public class InvestService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    AssetService assetService;
    
    @Autowired
    TblMoneyCalendarMapper moneyCalendarMapper;
    
    @Autowired
    TblChallengeMapper challengeMapper;
    
    @Autowired
    TblStockTransactionMapper stockTransactionMapper;
    
    @Autowired
    InvestDao investDao;

    /**
     * 투자 메인 조회
     * @param userId
     * @return
     * @throws MySQLIntegrityConstraintViolationException
     * @throws RuntimeException
     */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public InvestMain getInvestMain(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		
		return this.getInvestMainInfo(userId);
	}

	/**
	 * 투자 일별 평가액 조회
	 * @param userId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	private InvestMain getInvestMainInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);		
		InvestMain ret = new InvestMain();
		
		List<InvestEstimate> investEstimateList = new ArrayList<InvestEstimate>();
		List<InvestEarning> investEarningList = new ArrayList<InvestEarning>();
		int interval = Const.INVEST_INQUIRY_MONTH * 30;
		
		// 주식 코드 목록을 가져온다.
		List<String> stockCodeList = investDao.getUserStockList(userId);

		// interval 만큼 주식 거래일 리스트를 만든다.
		List<String> dateList = investDao.getStockDateList(interval);

		Long lastEstimate = 0L;
		Long prevEstimate = 0L;
		
		String codeSql = "(";
		if(stockCodeList != null && stockCodeList.size() > 0) {
			// 주식 별 총 수량
			HashMap<String, Long> totalAmtMap = new HashMap<String, Long>();

			// 조회 시작일 이전 거래 내역이 있으면 totalAmt에 추가한다.
			String startDate = dateList.get(0);
			startDate = startDate.replaceAll("-", "");
			logger.info("startDate = {}", startDate);
			
			
			for(String stockCode : stockCodeList) {
				codeSql += "b.code = '" + stockCode + "' or ";
				
				TblStockTransaction trans = new TblStockTransaction();
				trans.setUserId(userId);
				trans.setStockCode(stockCode);
				trans.setTransDate(startDate);
				
				Long startAmt = investDao.getStockAmtBeforeStartDate(trans);
				if(startAmt == null) {
					startAmt = 0L;
				}
				
				totalAmtMap.put(stockCode, startAmt);
			}			
			codeSql = codeSql.substring(0, codeSql.length() - 4);
			codeSql += ")";
			
			// 최근 6개월 가격 정보를 가져온다.
			HashMap<String, String> sqlMap = new HashMap<String, String>();
			sqlMap.put("codeSql", codeSql);
			sqlMap.put("interval", String.valueOf(interval+1));			
			List<StockPrice> stockPriceList = investDao.getDailyStockPriceInfo(sqlMap);
			
			// <주식날짜, 종가> 맵을 만든다.
			HashMap<StockDate, Long> endPriceMap = new HashMap<StockDate, Long>();
			// <주식날짜, 시작가> 맵을 만든다.
			HashMap<StockDate, Long> startPriceMap = new HashMap<StockDate, Long>();
			for(StockPrice stockPrice : stockPriceList) {
				StockDate stockDate = new StockDate();
				String stockCode = stockPrice.getStockCode();
				String date = stockPrice.getDate();
				Long endPrice = ScrapingUtil.getLongValueFromString(stockPrice.getEndPrice());
				Long startPrice = ScrapingUtil.getLongValueFromString(stockPrice.getStartPrice());
				stockDate.setStockCode(stockCode);
				stockDate.setDate(date);
				endPriceMap.put(stockDate, endPrice);
				startPriceMap.put(stockDate, startPrice);
			}
			

			// 전체 주식 거래 내역을 가져온다.
			TblStockTransactionExample example = new TblStockTransactionExample();		
			example.createCriteria().andUserIdEqualTo(userId);
			List<TblStockTransaction> stockTransactionList = stockTransactionMapper.selectByExample(example);
			
			// HashMap<StockDate, 주식수 변동>를 만든다.
			HashMap<StockDate, Long> stockAmtMap = new HashMap<StockDate, Long>();
			// HashMap<StockDate, 매수 합계>를 만든다.
			HashMap<StockDate, Long> stockInSumMap = new HashMap<StockDate, Long>();
			// HashMap<StockDate, 매도 합계>를 만든다.
			HashMap<StockDate, Long> stockOutSumMap = new HashMap<StockDate, Long>();
			
			for(TblStockTransaction stockTransaction : stockTransactionList) {
				StockDate stockDate = new StockDate();
				String stockCode = stockTransaction.getStockCode();
				String date = stockTransaction.getTransDate();
				Long price = stockTransaction.getPrice();
				stockDate.setStockCode(stockCode);
				stockDate.setDate(date);
				Long amt = stockTransaction.getAmt();
				Long inSum = 0L;
				Long outSum = 0L;
				
				if(amt > 0) {
					inSum = amt * price;
				} else {
					outSum = amt * price;
				}
				
				// 당일 중복 거래의 경우 총 주식 수를 계산한다.
				if(stockAmtMap.containsKey(stockDate)) {
					Long prevAmt = stockAmtMap.get(stockDate);
					amt += prevAmt;
				} 
				
				if(amt != null) {
					stockAmtMap.put(stockDate, amt);
				}
								
				// 당일 중복 매수의 경우 매수액을 합산한다.
				if(stockInSumMap.containsKey(stockDate)) {
					Long prevInSum = stockInSumMap.get(stockDate);
					inSum += prevInSum;
				} 
				
				if(inSum != 0L) {
					stockInSumMap.put(stockDate, inSum);
				}
				
				// 당일 중복 매도의 경우 매도액을 합산한다.
				if(stockOutSumMap.containsKey(stockDate)) {
					Long prevOutSum = stockOutSumMap.get(stockDate);
					outSum += prevOutSum;
				} 
				
				if(outSum != 0L) {
					stockOutSumMap.put(stockDate, outSum);
				}
			}
						
			int count = 1;
			int size = dateList.size();
			
			Long maxEstimate = 0L;
			Long minEstimate = 99999999999L;
			
			// 최근 6개월을 평가액을 계산한다.
			for(String date : dateList) {
				date = date.replace("-", "");
				InvestEstimate investEstimate = new InvestEstimate();
				InvestEarning investEarning = new InvestEarning();
				
				// 기말 자산
				Long todayTotalEstimate = 0L;
				// 기초 자산
				Long totalStartEstimate = 0L;
				// 오늘 거래 량
				Long todayAmt = 0L;
				// 입금고 누계
				Long totalIn = 0L;
				// 출금고 누계
				Long totalOut = 0L;
				// 오늘 주식 시세 정보가 있는 지 여부
				boolean haveTodayPrice = true;
				
				for(String stockCode : stockCodeList) {
					StockDate stockDate = new StockDate();
					stockDate.setStockCode(stockCode);
					stockDate.setDate(date);
					
					Long totalAmt = totalAmtMap.get(stockCode);
					if(totalAmt == null) {
						totalAmt = 0L;
					}
					
					todayAmt = stockAmtMap.get(stockDate);
					if(todayAmt == null) {
						todayAmt = 0L;
					}
					totalAmt += todayAmt;
					totalAmtMap.put(stockCode, totalAmt);
					
					// 오늘 시작가를 가져온다.
					Long todayStartPrice = startPriceMap.get(stockDate);
					if(todayStartPrice == null) {
						todayStartPrice = 0L;
					}
					
					// 오늘 가격(종가)을 가져온다.
					Long todayPrice = endPriceMap.get(stockDate);	
					if(todayPrice == null) {
						todayPrice = 0L;
					}
					
					// 오늘 시작가 평가액을 구한다.
					Long todayStartEstimate = (totalAmt - todayAmt) * todayStartPrice;
					// 오늘 기초 자산에 합산한다.
					totalStartEstimate += todayStartEstimate;
					
					// 오늘 매수가 있었으면
					if(stockInSumMap.containsKey(stockDate)) {
						totalIn = stockInSumMap.get(stockDate);
					} 			

					// 오늘 매도가 있었으면
					if(stockOutSumMap.containsKey(stockDate)) {
						totalOut = stockOutSumMap.get(stockDate);
					} 						
										
					// 오늘 평가액(종가)을 구한다.
					Long todayEstimate = totalAmt * todayPrice;
					// 오늘 총 평가액(종가)에 합산한다.
					todayTotalEstimate += todayEstimate;
					
					// 오늘 시세 정보가 없으면
					if(todayStartPrice == 0L && todayPrice == 0L) {
						haveTodayPrice = false;
					}
				}
				
				investEstimate.setDate(date);
				investEstimate.setEstimate(String.valueOf(todayTotalEstimate));
				investEstimateList.add(investEstimate);
				
				Float earningRate = 0F;
				investEarning.setDate(date);
				
				// 오늘 수익률을 계산한다.
				// 오늘 시작가와 종가 정보가 없으면(우리가 스크래핑 후에 새로 생긴 주식 종목을 과거에 매수 한 경우) 수익률을 표시하지 않는다.
				if(haveTodayPrice) {
					if(totalStartEstimate - totalIn != 0) {
						earningRate = (((todayTotalEstimate - totalOut - totalStartEstimate - totalIn) / (float) (totalStartEstimate + totalIn)) * 100);
						investEarning.setEarningRate(String.format("%.2f", earningRate));
					} else {
						investEarning.setEarningRate("");					
					}
				} else {
					investEarning.setEarningRate("");						
				}
				
				investEarningList.add(investEarning);
				
				// 마지막 전날 계산
				if(count == size - 1) {
					prevEstimate = todayTotalEstimate;
				} 
				
				// 마지막날 계산
				if(count == size) {
					lastEstimate = todayTotalEstimate;
				}
				
				// 3개월 최고 평가액
				if(todayTotalEstimate > maxEstimate) {
					maxEstimate = todayTotalEstimate;
				}
				
				// 3개월 최저 평가액
				if(todayTotalEstimate < minEstimate) {
					minEstimate = todayTotalEstimate;
				}
				
				count++;
			}
		}	
		
		ret.setDailyEstimate(investEstimateList);
		ret.setDailyEarningRate(investEarningList);
		ret.setStock(assetService.getAssetMainStockInfo(userId));
		ret.setTotalEstimate(String.valueOf(lastEstimate));
		ret.setDifference(String.valueOf(lastEstimate - prevEstimate));
		if(lastEstimate != 0L) {
			Float rate = ((lastEstimate - prevEstimate) / (float) lastEstimate) * 100;
			ret.setRate(String.format("%.2f", rate));
		} else {
			ret.setRate("");
		}
		
		return ret;
	}

}
