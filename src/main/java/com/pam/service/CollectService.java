package com.pam.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.constant.Const;
import com.pam.dao.BankDao;
import com.pam.dao.CardDao;
import com.pam.dao.CashReceiptDao;
import com.pam.dao.CategoryDao;
import com.pam.dao.CategoryTagDao;
import com.pam.dao.ExpenseDao;
import com.pam.dao.MoneyCalDao;
import com.pam.dao.PropertyDao;
import com.pam.entity.Category;
import com.pam.entity.CategoryTag;
import com.pam.entity.MoneyCalendarTag;
import com.pam.mapper.TblBankAccountBalanceMapper;
import com.pam.mapper.TblBankAccountMapper;
import com.pam.mapper.TblBankTransactionMapper;
import com.pam.mapper.TblCardBillMapper;
import com.pam.mapper.TblCardBillSummaryMapper;
import com.pam.mapper.TblCardMapper;
import com.pam.mapper.TblCategoryMapper;
import com.pam.mapper.TblCollectLogMapper;
import com.pam.mapper.TblErrorLogMapper;
import com.pam.mapper.domain.TblBankAccount;
import com.pam.mapper.domain.TblBankAccountBalance;
import com.pam.mapper.domain.TblBankAccountExample;
import com.pam.mapper.domain.TblBankTransaction;
import com.pam.mapper.domain.TblCard;
import com.pam.mapper.domain.TblCardApproval;
import com.pam.mapper.domain.TblCardBill;
import com.pam.mapper.domain.TblCardBillDetail;
import com.pam.mapper.domain.TblCardBillExample;
import com.pam.mapper.domain.TblCardBillMapping;
import com.pam.mapper.domain.TblCardBillSummary;
import com.pam.mapper.domain.TblCardExample;
import com.pam.mapper.domain.TblCardLimit;
import com.pam.mapper.domain.TblCashReceipt;
import com.pam.mapper.domain.TblCategory;
import com.pam.mapper.domain.TblCollectLog;
import com.pam.mapper.domain.TblErrorLog;
import com.pam.mapper.domain.TblExpense;
import com.pam.mapper.domain.TblMoneyCalendar;
import com.pam.util.Global;
import com.pam.util.Utils;


@Service
public class CollectService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    TblBankAccountMapper bankAccountMapper;
    
    @Autowired
    TblBankTransactionMapper bankTransactionMapper;
    
    @Autowired
    TblCardMapper cardMapper;
    
    @Autowired
    TblCardBillSummaryMapper cardBillSummaryMapper;
    
    @Autowired
    TblBankAccountBalanceMapper bankAccountBalanceMapper;
    
    @Autowired
    TblCollectLogMapper collectLogMapper;
    
    @Autowired
    TblCardBillMapper cardBillMapper;
    
    @Autowired
    BankDao bankDao;
    
    @Autowired
    CardDao cardDao;	
    
    @Autowired
    CashReceiptDao cashReceiptDao;
    
    @Autowired
    ExpenseDao expenseDao;
    
    @Autowired
    PropertyDao propertyDao;
    
    @Autowired
    CategoryDao categoryDao;
    
    @Autowired
    CategoryTagDao categoryTagDao;
    
    @Autowired
    TblCategoryMapper categoryMapper;
    
    @Autowired
    TblErrorLogMapper errorLogMapper;
    
    @Autowired
    MoneyCalDao moneyCalDao;
        
    /**
     * 은행 계좌를 등록한다.
     * @param list
     * @return
     * @throws MySQLIntegrityConstraintViolationException
     * @throws RuntimeException
     */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public List<TblBankAccountBalance> addBankAccounts(List<TblBankAccount> list) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("List<TblBankAccount> = {}", Utils.toJsonString(list));
		List<TblBankAccountBalance> ret = new ArrayList<TblBankAccountBalance>();
		Date now = Calendar.getInstance(Locale.KOREA).getTime();
		
		for(TblBankAccount bankAccount : list) {
			logger.info("TblBankAccount = {}", Utils.toJsonString(bankAccount));
			Long accntId = bankAccount.getAccntId();
			Long curBlnc = bankAccount.getCurBlnc();
			
			// userId, cmpnyCode, accntNum으로 기존 계좌가 있으면 accnt_id를 가져온다.
			// use_yn = "N" 삭제된 계좌도 가져와서 Y로 수정해서 새로 사용한다.
			accntId = this.checkAccountDuplication(bankAccount);			
			if(accntId == 0L) { // 처음이면 등록한다.
				bankAccount.setRegDate(now);
				accntId = bankDao.inserBankAccount(bankAccount);
			} else { // 기존 계좌면 업데이트 한다.(계좌 종류가 잔액에 따라 예금/대출 상호간에 변경될 수 있음. 삭제된 계좌도 update되어 사용할 수 있다)
				bankAccount.setAccntId(accntId);
				bankAccountMapper.updateByPrimaryKeySelective(bankAccount);
			}
			
			// 계좌 잔액 정보를 등록한다.
			TblBankAccountBalance bankAccountBalance = new TblBankAccountBalance();
			bankAccountBalance.setAccntId(accntId);
			String blncDate = Utils.getCurrentDate("yyyymmdd");
			bankAccountBalance.setBlncDate(blncDate);
			bankAccountBalance.setBlncAmt(curBlnc);
			bankAccountBalance.setRegDate(now);
			bankAccountBalance.setModDate(now);
			ret.add(bankAccountBalance);
		}
		
		return ret;
	}

	/**
	 * 은행 계좌 잔액을 등록한다.
	 * @param list
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	public void addBankAccountBalance(List<TblBankAccountBalance> list) throws MySQLIntegrityConstraintViolationException, RuntimeException {	
		logger.info("List<TblBankAccountBalance> = {}", Utils.toJsonString(list));
		String sql = "";
		for(TblBankAccountBalance bankAccountBalance : list) {
			String tmp = "(";
			Long accntId = bankAccountBalance.getAccntId();	
			String blncDate = bankAccountBalance.getBlncDate();
			Long blncAmt = bankAccountBalance.getBlncAmt();
			
			tmp += (accntId + ", ");
			tmp += ("'" + blncDate + "', ");
			tmp += (blncAmt + ", ");
			tmp += ("now(), ");
			tmp += ("now()), ");
			sql += tmp;
		}

		if(list.size() > 0) {
			sql = sql.substring(0, sql.length() - 2);
			logger.info("sql={}", sql);
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("sqlString", sql);
			bankDao.inserBankAccountBalance(map);
		}
	}

	/**
	 * 은행 거래 내역을 등록한다.
	 * @param bankTransaction
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addBankTransaction(TblBankTransaction bankTransaction) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblBankTransaction = {}", Utils.toJsonString(bankTransaction));
		
//		bankTransactionMapper.insert(bankTransaction);
		bankDao.inserBankTransaction(bankTransaction);
	}

	/**
	 * 은행 거래 내역을 insert 한번에 등록한다.
	 * @param list
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	public void addBankTransactionInOneSql(List<TblBankTransaction> list) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("List<TblBankTransaction> = {}", Utils.toJsonString(list));
		String sql = "";
		for(TblBankTransaction bankAccount : list) {
			String tmp = "(";
			Long accntId = bankAccount.getAccntId();
			String transDate = bankAccount.getTransDate();
			String ctgryCode = bankAccount.getCtgryCode();
			Long sum = bankAccount.getSum();
			Integer transType = bankAccount.getTransType();
			Long afterValue = bankAccount.getAfterValue();
			String oppent = bankAccount.getOppnnt();
			oppent = oppent.replace("\'", "\\'"); // 상대방 이름에 ' 있을 시 오류 수정
			String transMethod = bankAccount.getTransMethod();
			String dstrb = bankAccount.getDstrb();
			
			tmp += (accntId + ", ");
			tmp += ("'" + transDate + "', ");
			tmp += ("'" + ctgryCode + "', ");
			tmp += (sum + ", ");
			tmp += (transType + ", ");
			tmp += (afterValue + ", ");
			tmp += ("'" + oppent + "', ");
			tmp += ("'" + transMethod + "', ");
			tmp += ("'" + dstrb + "', ");
			tmp += ("now(), ");
			tmp += ("now()), ");
			sql += tmp;
		}
		
		if(list.size() > 0) {
			sql = sql.substring(0, sql.length() - 2);
			logger.info("sql={}", sql);
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("sqlString", sql);
			bankDao.inserBankTransactionInOneSql(map);
		}
	}

	/**
	 * 소비 내역을 insert 한번에 등록한다.
	 * @param list
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	public void addExpenseInOneSql(List<TblExpense> list) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("List<TblExpense> = {}", Utils.toJsonString(list));
		String sql = "";
		for(TblExpense expense : list) {
			String tmp = "(";
			String userId = expense.getUserId();
			String expnsDate = expense.getExpnsDate();
			String oppnt = expense.getOppnnt();
			oppnt = oppnt.replace("\'", "\\'"); // 상대방 이름에 ' 있을 시 오류 수정
			Long sum = expense.getSum();
			String ctgryCode = expense.getCtgryCode();
			String payType = expense.getPayType();
			String editYn = expense.getEditYn();
			String cancelYn = expense.getCancelYn();
			String companyCode = expense.getCmpnyCode();
			String useYn = expense.getUseYn();
			
			tmp += ("'" + userId + "', ");
			tmp += ("'" + expnsDate + "', ");
			tmp += ("'" + oppnt + "', ");
			tmp += (sum + ", ");
			tmp += ("'" + ctgryCode + "', ");
			tmp += ("'" + payType + "', ");
			tmp += ("'" + editYn + "', ");
			tmp += ("'" + cancelYn + "', ");
			tmp += ("'" + companyCode + "', ");
			tmp += ("'" + useYn + "', ");
			tmp += ("now(), ");
			tmp += ("now()), ");
			sql += tmp;
		}

		if(list.size() > 0) {
			sql = sql.substring(0, sql.length() - 2);
			logger.info("sql={}", sql);
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("sqlString", sql);
			// 중복 데이터는 use_yn = "Y"로 변경한다.
			expenseDao.insertExpenseInOneSql(map);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addBankProperty(String userId, Long accountId, List<TblBankTransaction> bankTransList) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblBankTransaction = {}", Utils.toJsonString(bankTransList));
		
		// <날짜, <시간, 금액>> 테이블
		HashMap<String, HashMap<String, Long>> map = new HashMap<String, HashMap<String, Long>>();
		
		for(TblBankTransaction bankTransaction : bankTransList) {
			String transDate = bankTransaction.getTransDate();
			String transDay = transDate.substring(0, 8);
			Long afterValue = bankTransaction.getAfterValue();
			Long sum = bankTransaction.getSum();
			// 계좌 잔액 = 사용 후 금액 - 사용 금액
			Long dailyVal = afterValue - sum;
			HashMap<String, Long> tmp = map.get(transDay);
			
			if(tmp == null) {
				HashMap<String, Long> mp = new HashMap<String, Long>();
				mp.put(transDate, dailyVal);
				map.put(transDay, mp);
			} else {
				HashMap<String, Long> mp = new HashMap<String, Long>();
				String oldTransDate = tmp.keySet().iterator().next();
				
				// 일별 최초 계좌 잔액을 구한다.
				if(Long.parseLong(transDate) < Long.parseLong(oldTransDate)) {
					mp.put(transDate, dailyVal);
					map.put(transDay, mp);
				}
			}
		}

		String sql = "";
		for(String day : map.keySet()) {
			HashMap<String, Long> aaa = map.get(day);
			
			for(String date : aaa.keySet()) {
				Long value = aaa.get(date);
				String tmp = "(";				
				tmp += ("'" + userId + "', ");
				tmp += ("'" + day + "', ");
				tmp += ("'01', ");
				tmp += (accountId + ", ");
				tmp += (value + ", ");
				tmp += ("now(), ");
				tmp += ("now()), ");
				sql += tmp;
			}
		}

		if(map.size() > 0) {
			sql = sql.substring(0, sql.length() - 2);
			logger.info("sql={}", sql);
			HashMap<String, String> sqlMap = new HashMap<String, String>();
			sqlMap.put("sqlString", sql);
			propertyDao.insertProperty(sqlMap);
		}
	}
	
	/**
	 * 카드를 등록한다.
	 * @param card
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Long addCard(TblCard card) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblCard = {}", Utils.toJsonString(card));
		
		// key 중복인 경우 use_yn = "Y"로 변경하여 카드를 사용함으로 바꾼다.
		return cardDao.insertCard(card);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addCardApproval(TblCardApproval cardApproval) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblCard = {}", Utils.toJsonString(cardApproval));
	
		cardDao.insertCardApproval(cardApproval);
	}

	/**
	 * 카드 한도 추가
	 * @param cardLimit
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Integer addCardLimit(TblCardLimit cardLimit) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblCardLimit = {}", Utils.toJsonString(cardLimit));
	
		// 증복인 경우 use_yn = "Y"로 변경하여 사용함으로 만든다.
		return cardDao.insertCardLimit(cardLimit);
	}

	public void addCardApprovalInOneSql(List<TblCardApproval> list) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("List<TblCardApproval> = {}", Utils.toJsonString(list));
		String sql = "";
		for(TblCardApproval cardApproval : list) {
			String tmp = "(";
			Long cardId = cardApproval.getCardId();
			String istmYn = cardApproval.getIstmYn();
			String aprvlDate = cardApproval.getAprvlDate();
			String aprvlNum = cardApproval.getAprvlNum();
			String aprvlyn = cardApproval.getAprvlYn();
			Integer istmMon = cardApproval.getIstmMon();
			Long aprvlAmt = cardApproval.getAprvlAmt();
			String storeName = cardApproval.getStoreName();
			storeName = storeName.replace("\'", "\\'"); // 상대방 이름에 ' 있을 시 오류 수정
			String crncyType = cardApproval.getCrncyType();
			String storeNum = cardApproval.getStoreNum();
			String ctgryCode = cardApproval.getCtgryCode();
			String storeTel = cardApproval.getStoreTel();
			String storeAddr = cardApproval.getStoreAddr();
			String storeHostName = cardApproval.getStoreHostName();
			
			
			tmp += (cardId + ", ");
			tmp += ("'" + istmYn + "', ");
			tmp += ("'" + aprvlDate + "', ");
			tmp += ("'" + aprvlNum + "', ");
			tmp += ("'" + aprvlyn + "', ");
			tmp += (istmMon + ", ");
			tmp += (aprvlAmt + ", ");
			tmp += ("'" + storeName + "', ");
			tmp += ("'" + crncyType + "', ");
			tmp += ("'" + storeNum + "', ");
			tmp += ("'" + ctgryCode + "', ");
			tmp += ("'" + storeTel + "', ");
			tmp += ("'" + storeAddr + "', ");
			tmp += ("'" + storeHostName + "', ");
			tmp += ("now(), ");
			tmp += ("now()), ");
			sql += tmp;
		}
		
		if(list.size() > 0) {
			sql = sql.substring(0, sql.length() - 2);
			logger.info("sql={}", sql);
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("sqlString", sql);
			cardDao.insertCardApprovalInOneSql(map);
		}
	}

	public void addCardBillDetailInOneSql(List<TblCardBillDetail> list) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("List<TblCardBillDetail> = {}", Utils.toJsonString(list));
		String sql = "";
		for(TblCardBillDetail cardBillDetail : list) {
			String tmp = "(";
			Long billId = cardBillDetail.getBillId();
			Long cardId = cardBillDetail.getCardId();
			String istmYn = cardBillDetail.getIstmYn();
			String istmPeriod = cardBillDetail.getIstmPeriod();
			Integer istmMonth = cardBillDetail.getIstmMonth();
			String useDate = cardBillDetail.getUseDate();
			String storeName = cardBillDetail.getStoreName();
			storeName = storeName.replace("\'", "\\'"); // 상대방 이름에 ' 있을 시 오류 수정
			String aprvlNum = cardBillDetail.getAprvlNum();
			Long srtx = cardBillDetail.getSrtx();
			Long tip = cardBillDetail.getTip();
			Float excgDate = cardBillDetail.getExcgRate();
			String storeNum = cardBillDetail.getStoreNum();
			String ctgryCode = cardBillDetail.getCtgryCode();
			String storeAddr = cardBillDetail.getStoreAddr();
			String storeTel = cardBillDetail.getStoreTel();
			String frgnYn = cardBillDetail.getFrgnYn();
			String storeHostName = cardBillDetail.getStoreHostName();
			String crncyCode = cardBillDetail.getCrncyCode();
			Long koSum = cardBillDetail.getKoSum();
			Long frgnSum = cardBillDetail.getFrgnSum();
			Long fee = cardBillDetail.getFee();			
			
			tmp += (billId + ", ");
			tmp += (cardId + ", ");
			tmp += ("'" + istmYn + "', ");
			tmp += ("'" + istmPeriod + "', ");
			tmp += (istmMonth + ", ");
			tmp += ("'" + useDate + "', ");
			tmp += ("'" + storeName + "', ");
			tmp += ("'" + aprvlNum + "', ");
			tmp += (srtx + ", ");
			tmp += (tip + ", ");
			tmp += (excgDate + ", ");
			tmp += ("'" + storeNum + "', ");
			tmp += ("'" + ctgryCode + "', ");
			tmp += ("'" + storeAddr + "', ");
			tmp += ("'" + storeTel + "', ");
			tmp += ("'" + frgnYn + "', ");
			tmp += ("'" + storeHostName + "', ");
			tmp += ("'" + crncyCode + "', ");
			tmp += (koSum + ", ");
			tmp += (frgnSum + ", ");
			tmp += (fee + ", ");
			tmp += ("now(), ");
			tmp += ("now()), ");
			sql += tmp;
		}

		if(list.size() > 0) {
			sql = sql.substring(0, sql.length() - 2);
			logger.info("sql={}", sql);
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("sqlString", sql);
			cardDao.insertCardBillDetailInOneSql(map);
		}
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addCardBill(TblCardBill cardBill) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblCardBill = {}", Utils.toJsonString(cardBill));
	
		// key가 중복인 경우 use_yn = "Y"로 변경하여 사용 카드로 만든다.
		cardDao.insertCardBill(cardBill);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Long getBillId(TblCardBill cardBill) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblCardBill = {}", Utils.toJsonString(cardBill));
		TblCardBillExample example = new TblCardBillExample();
		example.createCriteria().andUserIdEqualTo(cardBill.getUserId()).andCmpnyCodeEqualTo(cardBill.getCmpnyCode()).andPayDateEqualTo(cardBill.getPayDate()).andUseYnEqualTo(cardBill.getUseYn());
		List<TblCardBill> cardBillList = cardBillMapper.selectByExample(example);
	
		return cardBillList.get(0).getBillId();
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addCardBillMapping(TblCardBillMapping cardBillMapping) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblCardBillMapping = {}", Utils.toJsonString(cardBillMapping));
	
		cardDao.insertCardBillMapping(cardBillMapping);
	}

	public void addCardBillSummary(List<TblCardBillSummary> list) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("List<TblCardBillSummary> = {}", Utils.toJsonString(list));
		
		String sql = "";
		for(TblCardBillSummary cardBillSummary : list) {
			String tmp = "(";
			Long billId = cardBillSummary.getBillId();
			String smryName = cardBillSummary.getSmryName();
			Long sum = cardBillSummary.getSum();
			
			tmp += (billId + ", ");
			tmp += ("'" + smryName + "', ");
			tmp += (sum + ", ");
			tmp += ("now(), ");
			tmp += ("now()), ");
			sql += tmp;
		}

		if(list.size() > 0) {
			sql = sql.substring(0, sql.length() - 2);
			logger.info("sql={}", sql);
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("sqlString", sql);
			cardDao.insertCardBillSummaryInOneSql(map);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Long addCardBillDetail(TblCardBillDetail cardBillDetail) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblCardBillDetail = {}", Utils.toJsonString(cardBillDetail));
	
		return cardDao.insertCardBillDetail(cardBillDetail);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Long addCashReceipt(TblCashReceipt cashReceipt) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblCashReceipt = {}", Utils.toJsonString(cashReceipt));
	
		return cashReceiptDao.inserCashReceipt(cashReceipt);
	}

	public void addCashReceiptInOneSql(List<TblCashReceipt> list) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("List<TblCashReceipt> = {}", Utils.toJsonString(list));
		String sql = "";
		for(TblCashReceipt cashReceipt : list) {
			String tmp = "(";
			String userId = cashReceipt.getUserId();
			String transDate = cashReceipt.getTransDate();
			String storeName = cashReceipt.getStoreName();
			storeName = storeName.replace("\'", "\\'"); // 상대방 이름에 ' 있을 시 오류 수정
			Long sum = cashReceipt.getSum();
			String ctgryCode = cashReceipt.getCtgryCode();
			String aprvlNum = cashReceipt.getAprvlNum();
			String idnty = cashReceipt.getIdnty();
			String aprvlYn = cashReceipt.getAprvlYn();
			String ddctYn = cashReceipt.getDdctYn();
			Integer issueType = cashReceipt.getIssueType();
			String expense = cashReceipt.getExpense();
			
			tmp += ("'" + userId + "', ");
			tmp += ("'" + transDate + "', ");
			tmp += ("'" + storeName + "', ");
			tmp += (sum + ", ");
			tmp += ("'" + ctgryCode + "', ");
			tmp += ("'" + aprvlNum + "', ");
			tmp += ("'" + idnty + "', ");
			tmp += ("'" + aprvlYn + "', ");
			tmp += ("'" + ddctYn + "', ");
			tmp += (issueType + ", ");
			tmp += ("'" + expense + "', ");
			tmp += ("now(), ");
			tmp += ("now()), ");
			sql += tmp;
		}

		if(list.size() > 0) {
			sql = sql.substring(0, sql.length() - 2);
			logger.info("sql={}", sql);
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("sqlString", sql);
			cashReceiptDao.insertCashReceiptInOneSql(map);
		}
	}

	/**
	 * 은행 계좌 중복 확인
	 * @param bankAccount
	 * @return
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Long checkAccountDuplication(TblBankAccount bankAccount) {
		logger.info("TblBankAccount = {}", Utils.toJsonString(bankAccount));
		
		Long ret = 0L;
		TblBankAccountExample example = new TblBankAccountExample();
		String userId = bankAccount.getUserId();
		String cmpnyCode = bankAccount.getCmpnyCode();
		String accntNum = bankAccount.getAccntNum();
		example.createCriteria().andUserIdEqualTo(userId).andCmpnyCodeEqualTo(cmpnyCode).andAccntNumEqualTo(accntNum);
		List<TblBankAccount> list = bankAccountMapper.selectByExample(example);
		
		if(list.size() > 0) {
			ret = list.get(0).getAccntId();
		}
		
		return ret;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void checkAccountList(String userId, String cmpnyCode, List<String> accntNumList) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}, compnycode = {}, List<String> = {}", userId, cmpnyCode, Utils.toJsonString(accntNumList));
		TblBankAccountExample example = new TblBankAccountExample();
		example.createCriteria().andUserIdEqualTo(userId).andCmpnyCodeEqualTo(cmpnyCode).andUseYnEqualTo("Y").andAccntNumNotIn(accntNumList);
		TblBankAccount record = new TblBankAccount();
		record.setUseYn("N");
		bankAccountMapper.updateByExampleSelective(record, example);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Long getAccntId(String userId, String cmpnyCode, String accntNum) {
		logger.info("userId = {}, compnycode = {}, accntNum = {}", userId, cmpnyCode, accntNum);
		Long ret = null;
		
		TblBankAccountExample example = new TblBankAccountExample();
		example.createCriteria().andUserIdEqualTo(userId).andCmpnyCodeEqualTo(cmpnyCode).andAccntNumEqualTo(accntNum).andUseYnEqualTo("Y");
		List<TblBankAccount> list = bankAccountMapper.selectByExample(example);
		if(list.size() > 0) {
			TblBankAccount bankAccount = list.get(0);
			ret = bankAccount.getAccntId();
		}
		
		return ret;
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public String getCategoryCode(boolean inquire, String storeNum, String storeTel, String desc) {
		logger.info("inquire = {}, storeNum = {}, storeTel = {}, desc={}", inquire, storeNum, storeTel, desc);
		String ret = Const.CATEGORY_CODE_NOT_DEFINED; // set default code(etc)
		
		if(inquire) {
			// 1. store info matching
			boolean matched = false;
			String numPattern = "[^0-9]";
			
			HashMap<String, String> map = new HashMap<String, String>();
			if(!storeNum.equals("")){
				String storeNumRemoved = storeNum.replaceAll(numPattern, "");
				map.put("storeNum", storeNumRemoved);
			}
			if(!storeTel.equals("")){
				String storeTelRemoved = storeTel.replaceAll(numPattern, "");
				map.put("storeTel", storeTelRemoved);
			}
//			String descReplaced = "%"+desc.replaceAll(" ", " ").replace("（","(").replace("）",")").replaceAll("주식회사|\\(\\s?주\\s?\\)|주\\s?\\)|\\(\\s?유\\s?\\)|유\\s?\\)|#", " ").replaceAll("\\s\\s+", " ").trim().replaceAll(" ", "%")+"%";
//			map.put("storeName", descReplaced);
			map.put("storeName", desc);
			
			List<Category> list = categoryDao.getMatchedCategoryList(map);
			if(list.size() > 0){
				matched = true;
				Category category = list.get(0);
				ret = category.getCtgryCode();
			}
			
			// 2. tag equals matching
			List<CategoryTag> tagList = Global.tagList;
			if(!matched){
				for (CategoryTag categoryTag: tagList){
					String tag = categoryTag.getTag();
					String ctgryCode = categoryTag.getCtgryCode();
					
					if(desc.equals(tag)){
						matched = true;
						TblCategory category = categoryMapper.selectByPrimaryKey(ctgryCode);
						if(category != null) {
							ret = category.getCtgryCode();
						} else {
							ret = Const.CATEGORY_CODE_NOT_DEFINED;
						}
						logger.info("In categoryTag = {}", ret);
					}
				}
			}
			
			// 3. tag contains matching
			if(!matched){
				for (CategoryTag categoryTag: tagList){
					String tag = categoryTag.getTag();
					String ctgryCode = categoryTag.getCtgryCode();
					
					if(desc.contains(tag)){
						matched=true;
						TblCategory category = categoryMapper.selectByPrimaryKey(ctgryCode);
						ret = category.getCtgryCode();
						logger.info("In categoryTag = {}", ret);
					}
				}
			}
			
			logger.info("category code result = {}", ret);
		}
		
		return ret;
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Long getCardId(TblCard card) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblCard = {}", Utils.toJsonString(card));
		Long ret = null;
		String userId = card.getUserId();
		String cmpnyCode = card.getCmpnyCode();
		String cardNum = card.getCardNum();
		String billYn = card.getBillYn();
		String useYn = card.getUseYn();

		TblCardExample example = new TblCardExample();
		example.createCriteria().andUserIdEqualTo(userId).andCmpnyCodeEqualTo(cmpnyCode).andCardNumEqualTo(cardNum).andBillYnEqualTo(billYn).andUseYnEqualTo(useYn);
		List<TblCard> list = cardMapper.selectByExample(example);
		
		if(list != null && list.size() > 0) {
			ret = list.get(0).getCardId();
		}
		
		return ret;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addLog(TblCollectLog log) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblCollectLog = {}", Utils.toJsonString(log));
		collectLogMapper.insertSelective(log);
	}
	
	public MoneyCalendarTag getMoneyCalendarTag(String oppnnt) {
		logger.info("oppnnt = {}", oppnnt);
		
		MoneyCalendarTag ret = new MoneyCalendarTag();
		List<MoneyCalendarTag> mcList = Global.mcList;
		if (mcList != null && mcList.size() >0) {
			for (MoneyCalendarTag moneyCalendarTag : mcList){
				String tag = moneyCalendarTag.getTag();
				if(oppnnt.contains(tag)){
					ret = moneyCalendarTag;
					break;
				}
			}
		}
		return ret;
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public long addMoneyCal(List<TblMoneyCalendar> list) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("List<TblMoneyCalendar> = {}", Utils.toJsonString(list));
		String sql = "";
		
		for(TblMoneyCalendar moneyCalendar : list) {
			String tmp = "(";
			String userId = moneyCalendar.getUserId();
			String title = moneyCalendar.getTitle();
			String opponent = moneyCalendar.getOppnnt();
			opponent = opponent.replace("\'", "\\'"); // 상대방 이름에 ' 있을 시 오류 수정
			Long amt = moneyCalendar.getAmt();
			String payType = moneyCalendar.getPayType();
			String payDate = moneyCalendar.getPayDate();
			String payDay = moneyCalendar.getPayDay();
			String repeatType = moneyCalendar.getRptType();
			String useYn = moneyCalendar.getUseYn();
			String expectYn = moneyCalendar.getExptYn();
			String matchedTag = moneyCalendar.getMtchdTag();
			
			tmp += ("'" + userId + "', ");
			tmp += ("'" + title + "', ");
			tmp += ("'" + opponent + "', ");
			tmp += (amt + ", ");
			tmp += ("'" + payType + "', ");
			tmp += ("'" + payDate + "', ");
			tmp += ("'" + payDay + "', ");
			tmp += ("'" + repeatType + "', ");
			tmp += ("'" + useYn + "', ");
			tmp += ("'" + expectYn + "', ");
			if (matchedTag == null)
				tmp += ("null, ");
			else
				tmp += ("'" + matchedTag + "', ");
			tmp += ("now(), ");
			tmp += ("now()), ");
			sql += tmp;
		}

		if(list.size() > 0) {
			sql = sql.substring(0, sql.length() - 2);
			logger.info("sql={}", sql);
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("sqlString", sql);
			return moneyCalDao.insertMoneyCalFromBrankTransaction(map);
		}
		else return 0;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addErrorLog(TblErrorLog errorLog) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("errorLog = {}", Utils.toJsonString(errorLog));
		
		errorLogMapper.insertSelective(errorLog);
	}
}
