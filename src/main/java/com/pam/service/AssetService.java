package com.pam.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.constant.Const;
import com.pam.dao.AssetDao;
import com.pam.dao.CardDao;
import com.pam.entity.AssetEtc;
import com.pam.entity.Bank;
import com.pam.entity.BankDetail;
import com.pam.entity.Car;
import com.pam.entity.CarMarketPrice;
import com.pam.entity.Card;
import com.pam.entity.CardBill;
import com.pam.entity.CardBillDetail;
import com.pam.entity.CardBillMore;
import com.pam.entity.Debt;
import com.pam.entity.AssetMain;
import com.pam.entity.AssetMonthlyProperty;
import com.pam.entity.RealEstate;
import com.pam.entity.Stock;
import com.pam.entity.StockAmt;
import com.pam.entity.StockDailyPrice;
import com.pam.entity.StockDetail;
import com.pam.entity.StockDetailItem;
import com.pam.entity.StockDetailOfDate;
import com.pam.entity.StockItem;
import com.pam.mapper.TblBankAccountMapper;
import com.pam.mapper.TblBankTransactionMapper;
import com.pam.mapper.TblCarMapper;
import com.pam.mapper.TblCardBillDetailMapper;
import com.pam.mapper.TblCardBillMapper;
import com.pam.mapper.TblCardMapper;
import com.pam.mapper.TblRealEstateMapper;
import com.pam.mapper.TblStockTransactionMapper;
import com.pam.mapper.TblUserMapper;
import com.pam.mapper.domain.TblBankTransaction;
import com.pam.mapper.domain.TblBankTransactionExample;
import com.pam.mapper.domain.TblCar;
import com.pam.mapper.domain.TblCarExample;
import com.pam.mapper.domain.TblCardApproval;
import com.pam.mapper.domain.TblCardBill;
import com.pam.mapper.domain.TblCardBillExample;
import com.pam.mapper.domain.TblCardExample;
import com.pam.mapper.domain.TblRealEstate;
import com.pam.mapper.domain.TblRealEstateExample;
import com.pam.mapper.domain.TblStockTransaction;
import com.pam.mapper.domain.TblStockTransactionExample;
import com.pam.util.Utils;


@Service
public class AssetService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    InquiryService inquiryService;
    
    @Autowired
    TblUserMapper userMapper;
    
    @Autowired
    TblStockTransactionMapper stockTransactionMapper;
    
    @Autowired
    TblRealEstateMapper realEstateMapper;
    
    @Autowired
    TblCarMapper carMapper;
    
    @Autowired
    TblCardMapper cardMapper;
    
    @Autowired
    TblBankAccountMapper bankAccountMapper;
    
    @Autowired
    TblBankTransactionMapper bankTransactionMapper;
    
    @Autowired
    TblCardBillMapper cardBillMapper;
    
    @Autowired
    TblCardBillDetailMapper cardBillDetailMapper;
        
    @Autowired
    AssetDao assetDao;
    
    @Autowired
    CardDao cardDao;
    
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public AssetMain getAssetMainInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		AssetMain ret = new AssetMain();
		ret.setMonthlyProperty(this.getMonthlyProperty(userId));
		ret.setMonthlyDebt(this.getMonthlyDebt(userId));
		ret.setCard(this.getAssetMainCardInfo(userId));
		ret.setBank(this.getAssetMainBankInfo(userId));
		ret.setStock(this.getAssetMainStockInfo(userId));
		ret.setDebt(this.getAssetMainDebtInfo(userId));
		ret.setRealEstate(this.getAssetMainRealEstateInfo(userId));
		ret.setCar(this.getAssetMainCarInfo(userId));
		
		return ret;
	}	

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void setStockTransInfo(TblStockTransaction stockTransaction) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblStockTransaction = {}", Utils.toJsonString(stockTransaction));
		Date now = new Date();
		Long transId = stockTransaction.getTransId();
		
		// transId == 0이면 추가, 아니면 수정
		if(transId == 0) {
			stockTransaction.setTransId(null);
			stockTransaction.setRegDate(now);
			stockTransaction.setModDate(now);			
			stockTransactionMapper.insert(stockTransaction);
		} else {
			stockTransaction.setModDate(now);
			stockTransactionMapper.updateByPrimaryKeySelective(stockTransaction);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void setRealEstateInfo(TblRealEstate realEstate) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblRealEstate = {}", Utils.toJsonString(realEstate));
		Date now = new Date();
		Long estateId = realEstate.getEstateId();
		
		// transId == 0이면 추가, 아니면 수정
		if(estateId == 0) {
			realEstate.setEstateId(null);
			realEstate.setRegDate(now);
			realEstate.setModDate(now);			
			realEstateMapper.insert(realEstate);
		} else {
			realEstate.setModDate(now);
			realEstateMapper.updateByPrimaryKeySelective(realEstate);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void setCarInfo(TblCar car) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblCar = {}", Utils.toJsonString(car));
		Date now = new Date();
		Long carId = car.getCarId();
		
		// transId == 0이면 추가, 아니면 수정
		if(carId == 0) {
			car.setCarId(null);
			car.setRegDate(now);
			car.setModDate(now);			
			carMapper.insert(car);
		} else {
			car.setModDate(now);
			carMapper.updateByPrimaryKeySelective(car);
		}
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteStockTransInfo(TblStockTransaction stockTransaction) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblStockTransaction = {}", Utils.toJsonString(stockTransaction));
		Long transId = stockTransaction.getTransId();
		
		stockTransactionMapper.deleteByPrimaryKey(transId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteRealEstateInfo(TblRealEstate realEstate) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblRealEstate = {}", Utils.toJsonString(realEstate));
		Long estateId = realEstate.getEstateId();
		
		realEstateMapper.deleteByPrimaryKey(estateId);
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteCarInfo(TblCar car) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblCar = {}", Utils.toJsonString(car));
		Long carId = car.getCarId();
		
		carMapper.deleteByPrimaryKey(carId);
	}

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public String getStockNameFromCode(String stockCode) {
		logger.info("stockCode = {}", stockCode);
		return assetDao.getStockNameFromCode(stockCode);
	}

	/**
	 * 은행 거래내역 상세를 가져온다.
	 * @param userId
	 * @param accountId
	 * @param startDate
	 * @param endDate
	 * @param mode
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException, RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<BankDetail> getBankDetailInfo(String userId, String accountId, String startDate, String endDate, Integer mode) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}, accountId = {}, startDate = {}, endDate = {}, mode", 
				userId, accountId, startDate, endDate, mode);
		List<BankDetail> ret = new ArrayList<BankDetail>();
		TblBankTransactionExample example = new TblBankTransactionExample();
		Long accntId = Long.parseLong(accountId);
		startDate = startDate + "000000";
		endDate = endDate + "240000";
		
		if(mode == 0) {		
			example.createCriteria().andAccntIdEqualTo(accntId).andTransDateBetween(startDate, endDate);
		} else {	
			example.createCriteria().andAccntIdEqualTo(accntId).andTransDateBetween(startDate, endDate).andTransTypeEqualTo(mode);			
		}
		example.setOrderByClause("trans_date desc");
		
		List<TblBankTransaction> bankTransactionList = bankTransactionMapper.selectByExample(example);
		
		for(TblBankTransaction bankTransaction : bankTransactionList) {
			String transDate = bankTransaction.getTransDate();
			String transMethod = bankTransaction.getTransMethod();
			String opponent = bankTransaction.getOppnnt();
			String sum = String.valueOf(bankTransaction.getSum());
			BankDetail assetBankDetail = new BankDetail();
			assetBankDetail.setTransDate(transDate);
			assetBankDetail.setTransMethod(transMethod);
			assetBankDetail.setOpponent(opponent);
			assetBankDetail.setSum(sum);			
			ret.add(assetBankDetail);
		}
		
		return ret;
	}

	/**
	 * 이번 달 카드 청구서 목록을 가져온다.
	 * @param userId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException, RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<CardBill> getCardBillList(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		/*logger.info("userId = {}", userId);
		List<CardBill> ret = assetDao.getCardBillList(userId);
		
		return ret;*//*
		logger.info("userId = {}", userId);
		List<HashMap<String, Object>> retList = new ArrayList<HashMap<String, Object>>();
		
		List<CardBill> cardBillList = assetDao.getCardBillList(userId);	

		if(cardBillList != null && cardBillList.size() > 0) {
			for(CardBill cardBill : cardBillList) {	
				HashMap<String, Object> retMap = new HashMap<String, Object>();
				String companyCode = cardBill.getCompanyCode();
				Integer cardCount = cardBill.getCardCount();
				String date = cardBill.getDate();
				String sum = cardBill.getSum();
				
				List<HashMap<String, Object>> billList = new ArrayList<HashMap<String, Object>>();
				HashMap<String, Object> billMap = new HashMap<String, Object>();
				billMap.put("cardCount", cardCount);
				billMap.put("date", date);
				billMap.put("sum", sum);
				billList.add(billMap);
				
				retMap.put("companyCode", companyCode);
				retMap.put("list", billList);
				retList.add(retMap);
			}
		}*/

		logger.info("userId = {}", userId);
		List<CardBill> ret = new ArrayList<CardBill>();
				
		// 전체 청구서 목록을 가져온다(max 1년 전).
		List<CardBill> cardBillList = assetDao.getCardBillList(userId);	
		
		if(cardBillList != null && cardBillList.size() >0) {
			// <카드사, 청구서 리스트> 의 맵을 만든다.
			HashMap<String, List<CardBill>> companyMap = new HashMap<String, List<CardBill>>();
			for(CardBill cardBill : cardBillList) {
				String companyCode = cardBill.getCompanyCode();
				List<CardBill> tmpList = null;
				if(companyMap.containsKey(companyCode)) {
					tmpList = companyMap.get(companyCode);
				} else {
					tmpList = new ArrayList<CardBill>();
				}
				
				tmpList.add(cardBill);				
				companyMap.put(companyCode, tmpList);
			}
			
			// 카드사 별로 최신 청구 청구서를 찾는다.
			for(String cmpnyCode : companyMap.keySet()) {
				List<CardBill> tmpList = companyMap.get(cmpnyCode);
				// 청구일 목록
				List<String> dateList = new ArrayList<String>();
				
				// 1. 가장 큰 pay_date를 찾는다.
				if(tmpList != null && tmpList.size() > 0) {
					// 오늘
					Date now = Calendar.getInstance(Locale.KOREA).getTime();
					String nowDate = Utils.getDateString(now);
					nowDate = nowDate.substring(0, 8);
					
					CardBill tmpBill = tmpList.get(0);
					
					// 청구일 리스트를 구한다.
					for(CardBill cardBill : tmpList) {
						String payDate = cardBill.getDate();
						dateList.add(payDate);
					}
					
					tmpBill.setList(dateList);
					ret.add(tmpBill);
				}
			}
			
		}
		
		return ret;
	}

	/**
	 * 카드 청구서 상세 내역을 가져온다.
	 * @param userId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException, RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public CardBillMore getCardBillDetail(String userId, String companyCode, String date) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {} companyCode = {}, date = {}", userId, companyCode, date);
		CardBillMore ret = new CardBillMore();
		
		List<CardBillDetail> detailList = new ArrayList<CardBillDetail>();
		TblCardBillExample example = new TblCardBillExample();
		example.createCriteria().andUserIdEqualTo(userId).andCmpnyCodeEqualTo(companyCode).andPayDateEqualTo(date).andUseYnEqualTo("Y");
		List<TblCardBill> cardBillList = cardBillMapper.selectByExample(example);
		
		if(cardBillList.size() > 0) {
			TblCardBill retBill = cardBillList.get(0);
			Long billId = retBill.getBillId();
			Long sum = retBill.getPayAmt();
			ret.setSum(String.valueOf(sum));

			TblCardBill cardBill = new TblCardBill();
			cardBill.setBillId(billId);
			cardBill.setUserId(userId);
			detailList = assetDao.getCardBillDetail(cardBill); 
			ret.setList(detailList);
		} else {
			ret.setSum("0");
			ret.setList(detailList);
		}
		
		return ret;
	}

	/**
	 * 주식 거래 상세 내역을 가져 온다.
	 * 주식 시세 정보 테이블 구현 후 가능
	 * @param userId
	 * @param stockCode
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException, RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public StockDetail getStockDetailInfo(String userId, String stockCode) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {} stockCode = {}", userId, stockCode);
		StockDetail ret = new StockDetail();
		List<StockDetailItem> stockDetailItemList = new ArrayList<StockDetailItem>();
		
		// 현재 주식 수
		Long count = 0L;
		
		TblStockTransactionExample example = new TblStockTransactionExample();		
		example.createCriteria().andUserIdEqualTo(userId).andStockCodeEqualTo(stockCode);		
		example.setOrderByClause("trans_date desc");
		
		List<TblStockTransaction> stockTransactionList = stockTransactionMapper.selectByExample(example);
		
		for(TblStockTransaction stockTransaction : stockTransactionList) {
			Long transId = stockTransaction.getTransId();
			Integer transType = stockTransaction.getTransType();
			String transDate = stockTransaction.getTransDate();
			Long price = stockTransaction.getPrice();
			Long amount = stockTransaction.getAmt();
			Long sum = price * amount;
			if(transType == 1) { // 매수
				count += amount;
			} else { // 매도
				count -= amount;
			}
			
			StockDetailItem stockDetailItem = new StockDetailItem();
			stockDetailItem.setTransId(String.valueOf(transId));
			stockDetailItem.setTransType(transType);
			stockDetailItem.setTransDate(transDate);
			stockDetailItem.setPrice(String.valueOf(price));
			stockDetailItem.setAmount(String.valueOf(amount));
			stockDetailItem.setSum(String.valueOf(sum));
			stockDetailItemList.add(stockDetailItem);
		}

		// 종목 주식 시세 테이블에서 최신 데이터와 그 전날 데이터 중 주식 수, 종가를 가져온다.
		List<StockDailyPrice> dailyPriceList = this.getStockDailyPriceList(stockCode);
		List<StockDailyPrice> list = this.getStockPriceInfoLast2Days(stockCode);
		StockDetailOfDate stockDetailOfDate = this.getStockDetailOfDate(stockCode);
		String price = list.get(0).getPrice();
		String prevPrice = list.get(1).getPrice();
		Long totalSum = count * Long.parseLong(price);
		ret.setCount(String.valueOf(count));
		ret.setTotalSum(String.valueOf(totalSum));
		ret.setNowPrice(price);
		ret.setPrevPrice(prevPrice);
		ret.setDailyPrice(dailyPriceList);
		ret.setList(stockDetailItemList);
		ret.setHighPrice(stockDetailOfDate.getHighPrice());
		ret.setLowPrice(stockDetailOfDate.getLowPrice());
		ret.setTotalPrice(stockDetailOfDate.getTotalPrice());
		ret.setTradeAmount(stockDetailOfDate.getTradeAmount());
		ret.setTotalAmount(stockDetailOfDate.getTotalAmount());
		ret.setForeignerPercent(stockDetailOfDate.getForeignerPercent());
		ret.setPer(stockDetailOfDate.getPer());
		ret.setPbr(stockDetailOfDate.getPbr());
		return ret;
	}

	/**
	 * 주식 리스트를 가져온다.
	 * @param userId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public Stock getStockList(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		Stock ret = new Stock();
		
		List<String> last2Days = assetDao.getLast2Day();
		String nowDay = null;
		String prevDay = null;
		
		if(last2Days != null && last2Days.size() == 2) {
			nowDay = last2Days.get(0);
			nowDay = nowDay.replace("-", "");
			prevDay = last2Days.get(1);
			prevDay = prevDay.replace("-", "");
		} else {
			nowDay = Utils.getPrevNextDateString("2", -1, "yyyymmdd");
			prevDay = Utils.getPrevNextDateString("2", -2, "yyyymmdd");			
		}
		String dayTime = nowDay + "1500";
		ret.setDate(dayTime);
		
		List<StockAmt> stockAmtList = assetDao.getStockAmtList(userId);
		List<StockItem> list = new ArrayList<StockItem>();
		
		if(stockAmtList != null && stockAmtList.size() > 0) {
			for(StockAmt stockAmt : stockAmtList) {
				Long price = 0L;
				Long prevPrice = 0L;
				StockItem item1 = new StockItem();
				String stockCode = stockAmt.getStockCode();
				Long amt = stockAmt.getAmt();
				String stockName = this.getStockNameFromCode(stockCode);
				HashMap<String, String> map = new HashMap<String,String>();
				map.put("stockCode", stockCode);
				map.put("day", nowDay);
				map.put("prevDay", prevDay);
				List<StockDailyPrice> stockDailyPriceList = assetDao.getStockDatePrice2DaysList(map);
				for (StockDailyPrice stockDailyPrice : stockDailyPriceList){
					String d = stockDailyPrice.getDate().replaceAll("-", "");
					String p = stockDailyPrice.getPrice();
					if (d.equals(nowDay)) {
						price = Long.parseLong(p);
					}
					else if (d.equals(prevDay)) {
						prevPrice = Long.parseLong(p);
					}
				}
				item1.setStockCode(stockCode);
				item1.setStockName(stockName);
				item1.setSum(String.valueOf(amt * price));
				item1.setPrevSum(String.valueOf(amt * prevPrice));
				list.add(item1);
			}
		}
				
		ret.setList(list);
		
		return ret;
	}
	
	/**
	 * 기타 자산(부동산, 자동차) 정보를 가져온다.
	 * @param userId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public AssetEtc getAssetEtcInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		AssetEtc ret = new AssetEtc();
		ret.setRealEstate(this.getAssetMainRealEstateInfo(userId));
		ret.setCar(this.getAssetMainCarInfo(userId));
		
		return ret;		
	}

	/**
	 * 월별 재산 내역을 가져온다.
	 * @param userId
	 * @return
	 */
	private List<AssetMonthlyProperty> getMonthlyProperty(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		List<AssetMonthlyProperty> ret = new ArrayList<AssetMonthlyProperty>();
		List<AssetMonthlyProperty> monthList = new ArrayList<AssetMonthlyProperty>();
		
		// 최근 6개월을 설정한다.
		for(int i=0; i<6; i++) {
			String date = Utils.getPrevNextDateString("1", (-5 + i), "yyyymm");
			AssetMonthlyProperty property = new AssetMonthlyProperty();
			property.setDate(date);
//			property.setSum("0");
			property.setSum(String.valueOf(100000 * i));
			monthList.add(property);
		}
		
		// 일별 은행 잔고 내역을 가져온다.
		List<AssetMonthlyProperty> bankPropertyList = assetDao.getDailyBankBalanceInfo(userId);
		
		List<AssetMonthlyProperty> monthlyPropertyList = new ArrayList<AssetMonthlyProperty>();
		
		if(bankPropertyList != null  && bankPropertyList.size() > 0) {			
			// 중간에 빠진 날이 있으면 앞날 값으로 채워 넣는다.
			// 리스트를 맵으로 변환한다.
			HashMap<String, String> bankPropertyMap = new HashMap<String, String>();
			for(AssetMonthlyProperty property : bankPropertyList) {
				bankPropertyMap.put(property.getDate(), property.getSum());
			}
			
			// 첫날, 마지막날을 구한다.
			String firstDay = bankPropertyList.get(0).getDate().substring(0, 8);
			String lastDay = bankPropertyList.get(bankPropertyList.size() -1).getDate().substring(0,  8);
			String today = firstDay;
			String tmpSum = "";
			
			// 결과 리스트를 만든다.
			List<AssetMonthlyProperty> dailyPropertyList = new ArrayList<AssetMonthlyProperty>();
			
			// 마지막 날까지 채운 리스트를 만든다.
			lastDay = Utils.getPrevNextDateString("2", 1, "yyyymmdd", Utils.convertStringToDate(lastDay, "2"));
			while(!today.equals(lastDay)) {
				AssetMonthlyProperty tmp = new AssetMonthlyProperty();
				tmp.setDate(today);
				
				if(bankPropertyMap.containsKey(today)) {
					tmp.setSum(bankPropertyMap.get(today));
					tmpSum = bankPropertyMap.get(today);
				} else {
					tmp.setSum(tmpSum);
				}
				
				dailyPropertyList.add(tmp);				
				today = Utils.getPrevNextDateString("2", 1, "yyyymmdd", Utils.convertStringToDate(today, "2"));
			}
			
			int count = 1;
			int monthCount = 0;
			int size = dailyPropertyList.size();
			AssetMonthlyProperty firstProperty = new AssetMonthlyProperty();
			AssetMonthlyProperty lastProperty = new AssetMonthlyProperty();
			
			for(AssetMonthlyProperty property : dailyPropertyList) {
				String date = property.getDate();
				String sum = property.getSum();
				
				// 첫날 데이터를 구한다.
				if(count == 1) {
					firstProperty.setDate(date.substring(0, 6));
					firstProperty.setSum(sum);
					
					// 첫날이 1일이 아니면 리스트에 넣는다.
					if(!date.endsWith("01")) {
						monthlyPropertyList.add(firstProperty);
						++monthCount;						
					}
				}
				
				if(count == size) { // 마지막 데이터를 구한다.
					AssetMonthlyProperty tmp = new AssetMonthlyProperty();
					tmp.setDate(date.substring(0, 6));
					tmp.setSum(sum);
					lastProperty = tmp;
				}
				
				if(date.endsWith("01")) { // 매월 1일 만 남긴다.
					AssetMonthlyProperty tmp = new AssetMonthlyProperty();
					tmp.setDate(date.substring(0, 6));
					tmp.setSum(sum);
					monthlyPropertyList.add(tmp);
					++monthCount;
				} 
				
				++count;
			}
			
			// 마지막달 1일을 삭제하고 최신 값을 넣는다.
			if(monthCount != 0) {
				monthlyPropertyList.remove(monthCount - 1);
			}
			monthlyPropertyList.add(lastProperty);
		}

		// 맵 형태로 만든다.
		HashMap<String, AssetMonthlyProperty> bankMap = new HashMap<String, AssetMonthlyProperty>();
		for(AssetMonthlyProperty property : monthlyPropertyList) {
			bankMap.put(property.getDate(), property);
		}

		for(int i=0; i<6; i++) {
			AssetMonthlyProperty main = monthList.get(i);
			String date = main.getDate();
			Long bankVal = 0L;
			
			if(bankMap.containsKey(date)) {
				bankVal = Long.parseLong(bankMap.get(date).getSum());
			}
									
			AssetMonthlyProperty property = new AssetMonthlyProperty();
			property.setDate(date);
			property.setSum(String.valueOf(bankVal));
			
			ret.add(property);
		}
				
		return ret;
	}
	
	/**
	 * 월별 재산 내역을 가져온다.
	 * @param userId
	 * @return
	 */
	/*private List<AssetMonthlyProperty> getMonthlyProperty(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		List<AssetMonthlyProperty> ret = new ArrayList<AssetMonthlyProperty>();
		HashMap<BankBalanceKey, Long> balanceMap = new HashMap<BankBalanceKey, Long>();
		
		// 전체 계좌 리스트를 가져온다.
		TblBankAccountExample example = new TblBankAccountExample();
		example.createCriteria().andUserIdEqualTo(userId);
		List<TblBankAccount> bankAccountList = bankAccountMapper.selectByExample(example);
		
		
		// 최근 7개월을 설정한다.
		for(int i=0; i<210; i++) {
			String date = Utils.getPrevNextDateString("2", -(i+1), "yyyymmdd");
			for(TblBankAccount bankAccount : bankAccountList) {
				Long accountId = bankAccount.getAccntId();				
				BankBalanceKey balanceKey = new BankBalanceKey();
				balanceKey.setAccountId(accountId);
				balanceKey.setDate(date);
				balanceMap.put(balanceKey, 0L);
			}
		}
		
		// 은행 잔액 정보를 가져온다.
		List<BankBalance> balanceList = assetDao.getDailyBankBalanceInfo(userId);
		
		// 거래 내역에서 잔액 정보를 가져온다.
		List<BankBalance> propertyList = assetDao.getDailyBankPropertyInfo(userId);
		
		// 전체 재산을 월별로 합산한다.
		ret = this.getAssetMonthlyPropertySum(bankAccountList, balanceMap, balanceList, propertyList);
		
		return ret;
	}*/

	/**
	 * 월별 부채 내역을 가져온다.
	 * @param userId
	 * @return
	 */
	private List<AssetMonthlyProperty> getMonthlyDebt(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		List<AssetMonthlyProperty> monthList = new ArrayList<AssetMonthlyProperty>();
		
		// 최근 6개월을 설정한다.
		for(int i=0; i<6; i++) {
			String date = Utils.getPrevNextDateString("1", (-5 + i), "yyyymm");
			AssetMonthlyProperty property = new AssetMonthlyProperty();
			property.setDate(date);
//			property.setSum("0");
			property.setSum(String.valueOf(100000 * i));
			monthList.add(property);
		}
		
		// 월별 은행 부채 내역을 가져온다.
//		List<AssetMonthlyProperty> bankDebtList = assetDao.getMonthlyBankDebtInfo(userId);
		List<AssetMonthlyProperty> bankDebtList = assetDao.getDailyBankDebtInfo(userId);
		List<AssetMonthlyProperty> monthlyDebtList = new ArrayList<AssetMonthlyProperty>();
		
		if(bankDebtList != null  && bankDebtList.size() > 0) {		
			// 중간에 빠진 날이 있으면 앞날 값으로 채워 넣는다.
			// 리스트를 맵으로 변환한다.
			HashMap<String, String> bankDebtMap = new HashMap<String, String>();
			for(AssetMonthlyProperty property : bankDebtList) {
				bankDebtMap.put(property.getDate(), property.getSum());
			}
			
			// 첫날, 마지막날을 구한다.
			String firstDay = bankDebtList.get(0).getDate().substring(0, 8);
			String lastDay = bankDebtList.get(bankDebtList.size() -1).getDate().substring(0,  8);
			String today = firstDay;
			String tmpSum = "";
			
			// 결과 리스트를 만든다.
			List<AssetMonthlyProperty> dailyDebtList = new ArrayList<AssetMonthlyProperty>();
			
			// 마지막 날까지 채운 리스트를 만든다.
			lastDay = Utils.getPrevNextDateString("2", 1, "yyyymmdd", Utils.convertStringToDate(lastDay, "2"));
			while(!today.equals(lastDay)) {
				AssetMonthlyProperty tmp = new AssetMonthlyProperty();
				tmp.setDate(today);
				
				if(bankDebtMap.containsKey(today)) {
					tmp.setSum(bankDebtMap.get(today));
					tmpSum = bankDebtMap.get(today);
				} else {
					tmp.setSum(tmpSum);
				}
				
				dailyDebtList.add(tmp);				
				today = Utils.getPrevNextDateString("2", 1, "yyyymmdd", Utils.convertStringToDate(today, "2"));
			}
			
			int count = 1;
			int monthCount = 0;
			int size = dailyDebtList.size();
			AssetMonthlyProperty firstProperty = new AssetMonthlyProperty();
			AssetMonthlyProperty lastProperty = new AssetMonthlyProperty();
			
			for(AssetMonthlyProperty property : dailyDebtList) {
				String date = property.getDate();
				String sum = property.getSum();

				// 첫날 데이터를 구한다.
				if(count == 1) {
					firstProperty.setDate(date.substring(0, 6));
					firstProperty.setSum(sum);
					
					// 첫날이 1일이 아니면 리스트에 넣는다.
					if(!date.endsWith("01")) {
						monthlyDebtList.add(firstProperty);
						++monthCount;						
					}
				}
				
				if(count == size) { // 마지막 데이터를 구한다.
					AssetMonthlyProperty tmp = new AssetMonthlyProperty();
					tmp.setDate(date.substring(0, 6));
					tmp.setSum(sum);
					lastProperty = tmp;
				}
				
				if(date.endsWith("01")) { // 매월 1일 만 남긴다.
					AssetMonthlyProperty tmp = new AssetMonthlyProperty();
					tmp.setDate(date.substring(0, 6));
					tmp.setSum(sum);
					monthlyDebtList.add(tmp);
					++monthCount;
				}
				
				++count;
			}
			
			if(monthCount != 0) {
				monthlyDebtList.remove(monthCount - 1);
			}
			monthlyDebtList.add(lastProperty);
		}
		
		HashMap<String, AssetMonthlyProperty> bankMap = new HashMap<String, AssetMonthlyProperty>();
		for(AssetMonthlyProperty property : monthlyDebtList) {
			bankMap.put(property.getDate(), property);
		}
		
		// 월별 카드 부채 내역을 가져온다.
		List<AssetMonthlyProperty> cardDebtList = assetDao.getMonthlyCardDebtInfo(userId);
		HashMap<String, AssetMonthlyProperty> cardMap = new HashMap<String, AssetMonthlyProperty>();
		for(AssetMonthlyProperty property : cardDebtList) {
			cardMap.put(property.getDate(), property);
		}
		
		return this.getAssetMonthlyDebtSum(monthList, bankMap, cardMap);
	}

	/**
	 * 은행 계좌별 잔액과 최종 거래 내역을 가져온다.
	 * @param userId
	 * @return
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	private List<Bank> getAssetMainBankInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {	
		logger.info("userId = {}", userId);	
		List<Bank> ret = new ArrayList<Bank>();
		
		// 계좌별 최신 잔액 정보를 가져온다.
		List<Bank> bankList = assetDao.getAssetBankInfoList(userId);
		
		if(bankList != null && bankList.size() > 0) {		
			for(Bank bank : bankList) {
				Bank retBank = bank;
				String accountId = bank.getAccountId();
				Long accntId = Long.parseLong(accountId);
				TblBankTransactionExample example = new TblBankTransactionExample();
				example.createCriteria().andAccntIdEqualTo(accntId);
				example.setOrderByClause("trans_date desc");
				
				List<TblBankTransaction> bankTransactionList = bankTransactionMapper.selectByExample(example);
				String lastTransDate = "";
				String lastTransOpponent = "";
				String lastTransSum = "";
				if(bankTransactionList != null && bankTransactionList.size() > 0) {
					TblBankTransaction bankTransaction = bankTransactionList.get(0);
					lastTransDate = bankTransaction.getTransDate();
					lastTransOpponent = bankTransaction.getOppnnt();
					lastTransSum = String.valueOf(bankTransaction.getSum());
				} 
				
				retBank.setLastTransDate(lastTransDate);
				retBank.setLastTransOpponent(lastTransOpponent);
				retBank.setLastTransSum(lastTransSum);
				
				ret.add(retBank);
			}
		}
		
 		return ret;
	}

	/**
	 * 자산 메인 화면 카드 정보를 가져온다.
	 * @param userId
	 * @return
	 */
	private Card getAssetMainCardInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {	
		logger.info("userId = {}", userId);	
		return this.getAssetMainCard(userId);
	}

	/**
	 * 보유 주식 정보를 가져온다.
	 * 현재 주가 정보 연동이 안되므로 샘플 데이터를 만든다.
	 * @param userId
	 * @return
	 */
	public Stock getAssetMainStockInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		return this.getStockList(userId);
	}

	/**
	 * 대출 정보를 가져온다.
	 * 현재 대출 정보 연동이 안되므로 샘플 데이터를 만든다.
	 * @param userId
	 * @return
	 */
	private List<Debt> getAssetMainDebtInfo(String userId) {
		logger.info("userId = {}", userId);
		return this.makeSampleAssetDebt(userId);
	}

	/**
	 * 부동산 정보를 가져온다.
	 * @param userId
	 * @return
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	private List<RealEstate> getAssetMainRealEstateInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		TblRealEstateExample example = new TblRealEstateExample();
		example.createCriteria().andUserIdEqualTo(userId);
		List<TblRealEstate> list = realEstateMapper.selectByExample(example);
		List<RealEstate> ret = new ArrayList<RealEstate>();
		
		for(TblRealEstate realEstate : list) {
			Long estateId = realEstate.getEstateId();
			String estateName = realEstate.getEstateName();
			String estateType = realEstate.getEstateType();
			String dealType = realEstate.getDealType();
			Long price = realEstate.getPrice();
			
			RealEstate assetRealEstate = new RealEstate();
			assetRealEstate.setEstateId(String.valueOf(estateId));
			assetRealEstate.setEstateName(estateName);
			assetRealEstate.setEstateType(estateType);
			assetRealEstate.setDealType(dealType);
			assetRealEstate.setPrice(String.valueOf(price));
			
			ret.add(assetRealEstate);
		}
		
		return ret;
	}

	/**
	 * 자산 메인 카드 정보
	 * @param userId
	 * @return
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	private Card getAssetMainCard(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		Card ret = new Card();
		
		// 전체 카드 갯수 조회(청구서 카드만 조회한다.)
		TblCardExample example = new TblCardExample();
		example.createCriteria().andUserIdEqualTo(userId).andBillYnEqualTo("Y").andUseYnEqualTo("Y");
		int cardCount = cardMapper.countByExample(example);
		ret.setCardCount(cardCount);
		
		// 최신 청구 금액 합계 계산
		// 전체 청구서 목록을 가져온다.
		TblCardBillExample billExample = new TblCardBillExample();
		billExample.createCriteria().andUserIdEqualTo(userId).andUseYnEqualTo("Y");
		billExample.setOrderByClause("pay_date asc");
		List<TblCardBill> cardBillList = cardBillMapper.selectByExample(billExample);
		
		// 청구서 금액 합계
		Long accVal = 0L;
		
		if(cardBillList != null && cardBillList.size() >0) {
			// <카드사, 청구서 리스트> 의 맵을 만든다.
			HashMap<String, List<TblCardBill>> companyMap = new HashMap<String, List<TblCardBill>>();
			for(TblCardBill cardBill : cardBillList) {
				String companyCode = cardBill.getCmpnyCode();
				List<TblCardBill> tmpList = null;
				if(companyMap.containsKey(companyCode)) {
					tmpList = companyMap.get(companyCode);
				} else {
					tmpList = new ArrayList<TblCardBill>();
				}
				
				tmpList.add(cardBill);				
				companyMap.put(companyCode, tmpList);
			}
			
			// 카드사 별로 최신 청구 금액을 구해서 합산한다.
			for(String cmpnyCode : companyMap.keySet()) {
				List<TblCardBill> tmpList = companyMap.get(cmpnyCode);
				// 1. 오늘 이후 가장 큰 payDate 금액
				// 2. 1이 없으면 오늘 이전 가장 가까운 payDate 금액
				if(tmpList != null && tmpList.size() > 0) {
					// 오늘
					Date now = Calendar.getInstance(Locale.KOREA).getTime();
					String nowDate = Utils.getDateString(now);
					nowDate = nowDate.substring(0, 8);
					Integer nowInt = Integer.parseInt(nowDate);
					
					Long tmpAmt = 0L;
					boolean haveAmt = false;
					
					for(TblCardBill cardBill : tmpList) {
						String payDate = cardBill.getPayDate();
						Long payAmt = cardBill.getPayAmt();
						Integer payInt = Integer.parseInt(payDate);
						
						if(payInt >= nowInt) { // 오늘과 같거나 큰 청구일이면 선택
							tmpAmt = payAmt;
							haveAmt = true;
						}
					}
					
					if(!haveAmt) { // 오늘과 같거나 큰 청구일이 없으면
						for(TblCardBill cardBill : tmpList) {
							String payDate = cardBill.getPayDate();
							Long payAmt = cardBill.getPayAmt();
							Integer payInt = Integer.parseInt(payDate);
							
							if(payInt >= nowInt) {
								continue;
							} else {
								tmpAmt = payAmt;							
							}
						}						
					}
					
					accVal += tmpAmt;
				}
			}
			
		}
		
		ret.setSum(String.valueOf(accVal));
		
		// 카드 최신 승인 내역을 가져온다.
		TblCardApproval cardApproval = cardDao.getLastCardApproval(userId);
		if(cardApproval != null) {		
			String lastTransDate = cardApproval.getAprvlDate();
			String lastTransOpponent = cardApproval.getStoreName();
			String lastSum = String.valueOf(cardApproval.getAprvlAmt());
			ret.setLastTransDate(lastTransDate);
			ret.setLastTransOpponent(lastTransOpponent);
			ret.setLastSum(lastSum);
		} else {
			ret.setLastTransDate("");
			ret.setLastTransOpponent("");
			ret.setLastSum("");
		}
		
		return ret;
	}
	
	/**
	 * 자동차 정보를 가져온다.
	 * @param userId
	 * @return
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	private List<Car> getAssetMainCarInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		TblCarExample example = new TblCarExample();
		example.createCriteria().andUserIdEqualTo(userId);
		List<TblCar> list = carMapper.selectByExample(example);
		List<Car> ret = new ArrayList<Car>();
		
		for(TblCar car : list) {
			Long carId = car.getCarId();
			String makerCode = car.getMakerCode();
			String carCode = car.getCarCode();
			String madeYear = car.getMadeYear();
			String subCode = car.getSubCode();

			CarMarketPrice price = inquiryService.getCarMarketPrice(userId, makerCode, carCode, subCode, madeYear);
			
			Car assetCar = new Car();
			assetCar.setCarId(String.valueOf(carId));
			assetCar.setMakerCode(makerCode);
			assetCar.setCarCode(carCode);
			assetCar.setMadeYear(madeYear);
			assetCar.setSubCode(subCode);
			assetCar.setMarketPrice(price.getMarketPrice());
			
			ret.add(assetCar);
		}
		
		return ret;
	}

	private List<Debt> makeSampleAssetDebt(String userId) {
		logger.info("userId = {}", userId);
		List<Debt> ret = assetDao.getDebtInfo(userId);
		return ret;
	}

	/**
	 * 월별 부채 내역 합계 리스트를 만든다.
	 * @param monthList
	 * @param bankList
	 * @param cardList
	 * @return
	 */
	private List<AssetMonthlyProperty> getAssetMonthlyDebtSum(List<AssetMonthlyProperty> monthList, 
			HashMap<String, AssetMonthlyProperty> bankMap, 
			HashMap<String, AssetMonthlyProperty> cardMap) {
		logger.info("monthList = {}, bankMap = {}, cardMap = {}", Utils.toJsonString(monthList), Utils.toJsonString(bankMap), Utils.toJsonString(cardMap));
		List<AssetMonthlyProperty> ret = new ArrayList<AssetMonthlyProperty>();
		
		for(int i=0; i<6; i++) {
			AssetMonthlyProperty main = monthList.get(i);
			String date = main.getDate();
			Long bankVal = 0L;
			Long cardVal = 0L;
			
			if(bankMap.containsKey(date)) {
				bankVal = Long.parseLong(bankMap.get(date).getSum());
			}

			if(cardMap.containsKey(date)) {
				cardVal = Long.parseLong(cardMap.get(date).getSum());
			}
			
			Long sum = bankVal + cardVal;
						
			AssetMonthlyProperty property = new AssetMonthlyProperty();
			property.setDate(date);
			property.setSum(String.valueOf(sum));
			
			ret.add(property);
		}
		
		return ret;
	}
			
	/**
	 * 최근 2일간 주식 가격을 가져온다.
	 * @param stockCode
	 * @return
	 */
	private List<StockDailyPrice> getStockPriceInfoLast2Days(String stockCode) {
		logger.info("stockCode = {}", stockCode);
		List<StockDailyPrice> ret = new ArrayList<StockDailyPrice>();

		List<String> last2Days = assetDao.getLast2Day();
		String nowDay = null;
		String prevDay = null;
		
		if(last2Days != null && last2Days.size() == 2) {
			nowDay = last2Days.get(0);
			nowDay = nowDay.replace("-", "");
			prevDay = last2Days.get(1);
			prevDay = prevDay.replace("-", "");
		} else {
			nowDay = Utils.getPrevNextDateString("2", -1, "yyyymmdd");
			prevDay = Utils.getPrevNextDateString("2", -2, "yyyymmdd");			
		}
		
		String price = "0";
		String price2 = "0";
		
		HashMap<String, String> map = new HashMap<String,String>();
		map.put("stockCode", stockCode);
		map.put("day", nowDay);
		map.put("prevDay", prevDay);
		List<StockDailyPrice> stockDailyPriceList = assetDao.getStockDatePrice2DaysList(map);
		for (StockDailyPrice stockDailyPrice : stockDailyPriceList){
			String d = stockDailyPrice.getDate().replaceAll("-", "");
			String p = stockDailyPrice.getPrice();
			if (d.equals(nowDay)) {
				price = p;
			}
			else if (d.equals(prevDay)) {
				price2 = p;
			}
		}
		
		StockDailyPrice dailyPrice1 = new StockDailyPrice();
		dailyPrice1.setStockCode(stockCode);
		dailyPrice1.setDate(nowDay);
		dailyPrice1.setPrice(price);
		ret.add(dailyPrice1);

		StockDailyPrice dailyPrice2 = new StockDailyPrice();
		dailyPrice2.setStockCode(stockCode);
		dailyPrice2.setDate(prevDay);
		dailyPrice2.setPrice(price2);
		ret.add(dailyPrice2);
		
		return ret;
	}
	
	public List<StockDailyPrice> getStockDailyPriceList(String stockCode) {
		logger.info("stockCode = {}", stockCode);
		List<StockDailyPrice> ret = new ArrayList<StockDailyPrice>();

		List<String> last2Days = assetDao.getLast2Day();
		String nowDay = null;
		String prevDay = null;
		
		if(last2Days != null && last2Days.size() == 2) {
			nowDay = last2Days.get(0);
			nowDay = nowDay.replace("-", "");
			prevDay = last2Days.get(1);
			prevDay = prevDay.replace("-", "");
		} else {
			nowDay = Utils.getPrevNextDateString("2", -1, "yyyymmdd");
			prevDay = Utils.getPrevNextDateString("2", -2, "yyyymmdd");			
		}
		
		
		String dateFrom = Utils.getPrevNextDateString("2", -1 * Const.STOCK_DETAIL_MAX_PERIOD, "yyyymmdd");
		String dateTo = nowDay;
		HashMap<String, String> map = new HashMap<String,String>();
		map.put("stockCode", stockCode);
		map.put("dateFrom", dateFrom);
		map.put("dateTo", dateTo);
		List<StockDailyPrice> stockDailyPriceList = assetDao.getStockDailyPriceList(map);
		if (stockDailyPriceList != null && stockDailyPriceList.size() > 0){
			ret = stockDailyPriceList;
		}
		
		return ret;
	}
	
	public StockDetailOfDate getStockDetailOfDate(String stockCode) {
		logger.info("stockCode = {}", stockCode);
		StockDetailOfDate ret = new StockDetailOfDate();

		List<String> last2Days = assetDao.getLast2Day();
		String nowDay = null;
		
		if(last2Days != null && last2Days.size() == 2) {
			nowDay = last2Days.get(0);
			nowDay = nowDay.replace("-", "");
		} else {
			nowDay = Utils.getPrevNextDateString("2", -1, "yyyymmdd");
		}
		
		HashMap<String, String> map = new HashMap<String, String>();
		
		map.put("date", nowDay);
		map.put("stockCode", stockCode);
		ret = assetDao.getStockDetailOfDate(map);
		
		if (ret == null) {
			StockDetailOfDate ret2 = new StockDetailOfDate();
			ret2.setForeignerPercent(0f);
			ret2.setHighPrice("0");
			ret2.setLowPrice("0");
			ret2.setPbr(0f);
			ret2.setPer(0f);
			ret2.setTotalAmount("0");
			ret2.setTotalPrice("0");
			ret2.setTradeAmount("0");
			ret = ret2;
		}
		
		return ret;
	}
}
