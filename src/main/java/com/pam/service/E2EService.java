package com.pam.service;

import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pam.service.ServerEncryption;
import com.pam.security.Contents;
import com.pam.security.E2EProtocol;
import com.pam.security.Session;
import com.pam.service.SessionHandler;
import com.pam.util.ByteUtil;
import com.pam.util.Global;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@Service
public class E2EService {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    SecureSessionService secureSessionService;
    
    @Autowired
    ServerEncryption serverEncryption;
    
    @Autowired
    SessionHandler sessionHandler;
	
	private String httpSessionId;
		
	public String getHttpSessionId() {
		return this.httpSessionId;
	}
	
	public void setHttpSessionId(String httpSessionId) {
		this.httpSessionId = httpSessionId;
	}
	
	public E2EProtocol doHandShake(E2EProtocol protocol) throws BadPaddingException
	, InvalidKeyException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeySpecException, NoSuchAlgorithmException, RuntimeException {		
		// add session
		String rcvSessionId = protocol.getSessionId();
		String sessionId = "";
		E2EProtocol ret = new E2EProtocol();
		
		if(rcvSessionId.equals("")) {
			sessionId = ServerEncryption.genSessionId();
		} else {
			sessionId = rcvSessionId;
		}
		
		ret.setSessionId(sessionId);
		
		List<Contents> contentsList = protocol.getContents();
		
		for(Contents contents : contentsList) {
			String type = contents.getType();
			if(type.equals("01")) { // clientHello 받음
				logger.debug("Receive client hello(01) from {}", httpSessionId);
				sessionHandler.updateSessionInfo(httpSessionId, sessionId, contents);
				
				// serverHello
				Contents serverHelloContents = new Contents();
				serverHelloContents.setType("02");
				String random = ServerEncryption.genServerRandom();
				serverHelloContents.setRandom(random);
				List<String> cipherSuitesList = contents.getCipherSuitesList();
				String cipherSuitesSelected = cipherSuitesList.get(0);
				cipherSuitesSelected = "010103";
				serverHelloContents.setCipherSuitesSelected(cipherSuitesSelected);
				serverHelloContents.setServerName("www.nocosecurity.com");
				ret.getContents().add(serverHelloContents);
				sessionHandler.updateSessionInfo(httpSessionId, sessionId, serverHelloContents);
				
				// certificate
				Contents certContents = new Contents();
				certContents.setType("03");		
				
				// 인증서 즉석 생성
				Session session = secureSessionService.readSession(httpSessionId);
				String publicAlg = session.getPublicKeyAlgorithm();
				String hashAlg = session.getHashAlgorithm();
				KeyPair keyPair = ServerEncryption.genKeyPair(publicAlg, 2048);
				byte[] pub = keyPair.getPublic().getEncoded();
				logger.debug("publicKey = {}", ByteUtil.byteArrayToHex(pub));
				String cert = ServerEncryption.createCert(keyPair.getPublic(), keyPair.getPrivate(), hashAlg, session);

				certContents.setCertificate(cert);
				ret.getContents().add(certContents);
				logger.debug("Send server hello(02 and certificate(03) to {}", httpSessionId);
				sessionHandler.updateSessionInfo(httpSessionId, sessionId, certContents);
				String keyBytes = ByteUtil.byteArrayToHex(keyPair.getPrivate().getEncoded());
				sessionHandler.updatePrivateKey(httpSessionId, keyBytes);
				
				// 파일에서 읽어옴
				/*String[] cert = ServerEncryption.getCertificate("D:\noco.p12");
				certContents.setCertificate(cert[0]);
				ret.getContents().add(certContents);
				sessionHandler.updateSessionInfo(httpSessionId, sessionId, certContents);
				sessionHandler.updatePrivateKey(httpSessionId, cert[1]);*/
			} else if(type.equals("04")) { // keyChange 받음
				logger.debug("Receive key exchange(04) from {}", httpSessionId);
				String keyExchange = contents.getKeyExchange();
				
				// httpSession으로 정보를 검색하여 정상이면 기록하고 없거나 이상이면 오류를 return 한다.
				if(!serverEncryption.genSessionKey(httpSessionId, keyExchange, type)) { 
					Contents cont = new Contents();
					cont.setType("99"); // 오류 발생
					cont.setServerName("Session expired/invalid or decryption error");
					ret.getContents().add(cont);
				}
			} else if(type.equals("05")) { // client finished 받음
				logger.debug("Receive client finished(05) from {}", httpSessionId);
    			String finished = contents.getFinished();
    			
				Session session = secureSessionService.readSession(httpSessionId);
				
				if(serverEncryption.verifyClientFinished(httpSessionId, finished, type)) {
					// server finished
					Contents fContents = new Contents();
					fContents.setType("06");
					String serverFinished = ServerEncryption.genServerFinished(session);
					fContents.setFinished(serverFinished);
					ret.getContents().add(fContents);

					logger.debug("Send server finished(06) to {}", httpSessionId);
					sessionHandler.updateCurrentStatus(httpSessionId, "06");
					sessionHandler.updateHttpSessionTimeout(httpSessionId, Global.HTTP_SESSION_EXPIRE_MIN);
				} else {
					logger.debug("client finished is not verified");	
					Contents cont = new Contents();
					cont.setType("99"); // 오류 발생
					cont.setServerName("Session expired or invalid");
					ret.getContents().add(cont);							
				}
			}
		}
		
//		Global.printSessionList();
		
		return ret;
	}
	
	public boolean sessionExpired(String httpSessionId) {
		boolean ret = true;
		
		Session session = secureSessionService.readSession(httpSessionId);
		
		// session expire timeout을 reset한다. 
		if(session != null) {
			secureSessionService.write(httpSessionId, session);
			ret = false;
		}
		
		return ret;
	}

	public String getJsonStringFromHttpServlet(HttpServletRequest request) {
		String ret = null;
		HttpSession httpSession = request.getSession();
		String httpSessionId = httpSession.getId();		
		String recvType = request.getContentType();		
		byte[] in = null;
		
		try {
			InputStream is = request.getInputStream();
			in = ByteUtil.inputStreamToByteArray(is);

			if(recvType != null && recvType.contains("x-secure-")) {
				Session session = secureSessionService.readSession(httpSessionId);
			
				if(session != null) {
					ret = new String(ServerEncryption.genDecryptedBytes(in, session));
				}
			} else {
				ret = new String(in, "utf-8");
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			logger.info("e2e error = {}", e1.toString());
			e1.printStackTrace();
		}
		
		return ret;
	}
	
	public void sendData(HttpServletRequest request, HttpServletResponse response, Object ret) {
		HttpSession httpSession = request.getSession();
		String httpSessionId = httpSession.getId();
		String sendType = request.getHeader("Accept");
		String sendJson = this.getJsonStringFromObject(ret);
		OutputStream oStream = null;
		byte[] out = null;
		
		try {
			if(ret == null) { // session expired
				response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
			} else {
				response.setHeader("Content-Type", sendType);
				
				if(sendType != null && sendType.contains("x-secure-")) {
					Session session = secureSessionService.readSession(httpSessionId);
					
					if(session != null) {
						out = sendJson.getBytes("utf-8");	
						out = ServerEncryption.genEncryptedBytes(out, session);
					} else {/*
						HashMap<String, String> map = new HashMap<String, String>();
						map.put("code", "101");
						map.put("desc", "expired");
						Gson gson = new Gson(); 
						String tmp = gson.toJson(map);
						out = tmp.getBytes("utf-8");*/
					}				
					
				} else {
					out = sendJson.getBytes("utf-8");
				}	
			}

			oStream = response.getOutputStream();
			if(out != null) {
				oStream.write(out);
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getJsonStringFromObject(Object obj) {
		String ret = null;
		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		
		try {
			ret = ow.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}
}
