package com.pam.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.constant.Const;
import com.pam.dao.MoneyCalDao;
import com.pam.dao.NotificationDao;
import com.pam.entity.ChallengeList;
//import com.pam.entity.MoneyCalList;
import com.pam.entity.Notification;
import com.pam.mapper.TblChallengeMapper;
import com.pam.mapper.TblExpenseMapper;
import com.pam.mapper.TblNotificationMapper;
import com.pam.mapper.TblNotificationSmallCategoryMapper;
import com.pam.mapper.TblStockTransactionMapper;
import com.pam.mapper.domain.TblExpenseExample;
import com.pam.mapper.domain.TblNotification;
import com.pam.mapper.domain.TblNotificationExample;
import com.pam.mapper.domain.TblNotificationSmallCategory;
import com.pam.mapper.domain.TblStockTransactionExample;
import com.pam.util.Utils;



@Service
public class NotificationService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    TblExpenseMapper expenseMapper;

    @Autowired
    TblStockTransactionMapper stockTransactionMapper;
    
    @Autowired
    TblNotificationSmallCategoryMapper notificationSmallCategoryMapper;
    
    @Autowired
    TblNotificationMapper notificationMapper;
    
    @Autowired
    TblChallengeMapper challengeMapper;
       
    @Autowired
    NotificationDao notificationDao;
    
    @Autowired
    MoneyCalDao moneyCalDao;
    
    @Autowired
    EtcService etcService;

    /**
     * 전체 알림을 체크한다.
     * @param userId
     */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public void makeNotification(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);		
    	
		// 소비 - 카테고리 미분류, 지난달 미분류 알림 생성
		this.makeNotificationAboutNonCategory(userId);
		// 소비 - 예산 관련 알림 생성
		this.makeNotificationAboutBudget(userId);
		// 투자 관련 알림 생성
		this.makeNotificationAboutInvest(userId);
		// 머니캘린더 관련 알림 생성
		this.makeNotificationAboutMoneyCalendar(userId);
		// 챌린지 관련 알림 생성
		this.makeNotificationAboutChallenge(userId);
    }
    
    /**
     * 알림 리스트를 가져온다.
     * @param userId
     * @return
     */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public List<Notification> getNotificationList(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);		
		
    	List<Notification> ret = new ArrayList<Notification>();
    	
    	// 등록일이 최근 2개월인 알림 리스트를 가져온다.
    	Date baseDate = Utils.getPrevNextDate("1", -2);
    	TblNotificationExample example = new TblNotificationExample();
    	example.createCriteria().andUserIdEqualTo(userId).andRegDateGreaterThan(baseDate);
    	List<TblNotification> list = notificationMapper.selectByExample(example);
    	
    	if(list != null && list.size() > 0) {
    		for(TblNotification notification : list) {
    			Long notificationId = notification.getNotiId();
    			String categoryCode = notification.getSmallCtgryCode();
    			String message = notification.getMsg();
    			Date regDate = notification.getRegDate();
    			String date = Utils.getDateString(regDate);
    			String readYn = notification.getReadYn();
    			
    			Notification noti = new Notification();
    			noti.setNotificationId(notificationId);
    			noti.setCategoryCode(categoryCode);
    			noti.setMessage(message);
    			noti.setDate(date);
    			noti.setReadYn(readYn);
    			ret.add(noti);
    		}
    	}
    	
    	return ret;
    }

	/**
	 * 알림 내역을 읽음으로 수정한다.
	 * @param userId
	 * @param list
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Long setNotificationRead(String userId, ArrayList<String> list) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}, ArrayList<String> = {}", userId, Utils.toJsonString(list));
		Long ret = 0L;
		
		String sql = "";
		for(String notiId : list) {
			String tmp = "noti_id = " + notiId + " or ";
			sql += tmp;
		}		

		if(list.size() > 0) {
			sql = sql.substring(0, sql.length() - 4);
			logger.info("sql={}", sql);
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("sqlString", sql);
			ret = notificationDao.makeNotificationRead(map);
		}
		
		return ret;
	}

	/**
	 * 알림 추가
	 * @param notification
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addNotification(TblNotification notification) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblNotification = {}", Utils.toJsonString(notification));
		
		notificationMapper.insertSelective(notification);
	}
	
    /**
     * 카테고리 미분류 알림을 확인한다.
     * @param userId
     */
    private void makeNotificationAboutNonCategory(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);		
		Date now = Calendar.getInstance(Locale.KOREA).getTime();
		String date = Utils.getDateString(now);
		String makeMonth = date.substring(0, 6);
		String makeDay = date.substring(6, 8);
    	
		// 3개이상 카테고리 미분류 확인
		TblExpenseExample example = new TblExpenseExample();
		example.createCriteria().andUserIdEqualTo(userId).andCtgryCodeEqualTo(Const.CATEGORY_CODE_NOT_DEFINED);
		Integer count = expenseMapper.countByExample(example);
		
		// 3개이상 있으면
		if(count > 2) {			
			TblNotificationSmallCategory notificationSmallCategory = notificationSmallCategoryMapper.selectByPrimaryKey(Const.NOTIFICATION_SMALL_CATEGORY_EXPENSE_NON_CATEGORY);
			String message = notificationSmallCategory.getMsg();
			
			TblNotification noti = new TblNotification();
			noti.setUserId(userId);
			noti.setSmallCtgryCode(Const.NOTIFICATION_SMALL_CATEGORY_EXPENSE_NON_CATEGORY);
			noti.setMsg(message);
			noti.setMakeMonth(makeMonth);
			noti.setMakeDay(makeDay);
			noti.setReadYn("N");
			noti.setRegDate(now);
			noti.setModDate(now);
			
			// 중복이 아니면 등록한다.
			if(!this.isNotificationDuplication(noti)) {
				notificationMapper.insert(noti);
			}
		}
		
		// 월 1일, 지난 달까지 3개 이상 미분류 확인
		// 오늘이 1일이면
		String today = Utils.getCurrentDate("yyyymmdd");
		if(today.endsWith("01")) {
			TblExpenseExample example2 = new TblExpenseExample();
			example2.createCriteria().andUserIdEqualTo(userId).andCtgryCodeEqualTo(Const.CATEGORY_CODE_NOT_DEFINED).andExpnsDateLessThan(today);
			Integer count2 = expenseMapper.countByExample(example2);	

			// 3개이상 있으면
			if(count2 > 2) {			
				TblNotificationSmallCategory notificationSmallCategory = notificationSmallCategoryMapper.selectByPrimaryKey(Const.NOTIFICATION_SMALL_CATEGORY_EXPENSE_MONTH_CATEGORY);
				String message = notificationSmallCategory.getMsg();
				
				TblNotification noti = new TblNotification();
				noti.setUserId(userId);
				noti.setSmallCtgryCode(Const.NOTIFICATION_SMALL_CATEGORY_EXPENSE_MONTH_CATEGORY);
				noti.setMsg(message);
				noti.setReadYn("N");
				noti.setRegDate(now);
				noti.setModDate(now);
				
				notificationMapper.insert(noti);
			}
		}
    }
    
    private void makeNotificationAboutBudget(String userId) {
    	
    }
    
    /**
     * 투자자산 확인 유도 알림을 확인한다.
     * @param userId
     */
    private void makeNotificationAboutInvest(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
    	logger.info("userId = {}", userId);
    	
    	TblStockTransactionExample example = new TblStockTransactionExample();
		example.createCriteria().andUserIdEqualTo(userId);
		Integer count = stockTransactionMapper.countByExample(example);
    	
		// 주식 거래내역이 존재하면
    	if(count>0){
    		Date now = Calendar.getInstance(Locale.KOREA).getTime();
    		String date = Utils.getDateString(now);
    		String makeMonth = date.substring(0, 6);
    		String makeDay = date.substring(6, 8);
    		
    		TblNotificationSmallCategory notificationSmallCategory = notificationSmallCategoryMapper.selectByPrimaryKey(Const.NOTIFICATION_SMALL_CATEGORY_INVEST_STOCK);
    		String message = notificationSmallCategory.getMsg();
    		
    		TblNotification noti = new TblNotification();
    		noti.setUserId(userId);
    		noti.setSmallCtgryCode(Const.NOTIFICATION_SMALL_CATEGORY_INVEST_STOCK);
    		noti.setMsg(message);
    		noti.setMakeMonth(makeMonth);
    		noti.setMakeDay(makeDay);
    		noti.setReadYn("N");
    		noti.setRegDate(now);
    		noti.setModDate(now);
    		
    		// 중복이 아니면 등록한다.
    		if(!this.isNotificationDuplication(noti)) {
    			notificationMapper.insert(noti);
    		}
    	}
    }
    
	/**
     * 머니캘린더 알림을 확인한다.
     * @param userId
     */
    private void makeNotificationAboutMoneyCalendar(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
    	logger.info("userId = {}", userId);
    	this.makeNotificationCalendarSchedule(userId);
    	this.makeNotificationCalendarPaid(userId);
    }
    
    /**
     * 머니캘린더 예정 알림을 생성한다.
     * @param userId
     */
    private void makeNotificationCalendarSchedule(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException{
    	/*List<MoneyCalList> moneyCalList = moneyCalDao.getMoneyCalList(userId);
		
		// 머니캘린더 내역이 존재하면
		if(moneyCalList != null && moneyCalList.size() >0) {
			Date now = Calendar.getInstance(Locale.KOREA).getTime();
    		String nowDate = Utils.getDateString(now);
    		String nowYearMonth = nowDate.substring(0, 6);
    		String nowDay = nowDate.substring(6, 8);
    		Date convNowDate = Utils.convertStringToDate(nowYearMonth+nowDay, "2");
			
			for (MoneyCalList moneyCal : moneyCalList){
				etcService.setNextMoneyCalendarList(userId, moneyCalList);
				
				String calId = moneyCal.getCalendarId();
				String title = moneyCal.getTitle();
				String payDate = moneyCal.getPayDate();
				String payYearMonth = payDate.substring(0, 6);
				String payDay = payDate.substring(6, 8);
				Date convPayDate = Utils.convertStringToDate(payYearMonth+payDay, "2");
				
				TblNotification noti = new TblNotification();
				TblNotificationSmallCategory notificationSmallCategory = new TblNotificationSmallCategory();
				String smallCtgryCode = "";
				
				Long diffDay = (convNowDate.getTime() - convPayDate.getTime()) / (24 * 60 * 60 * 1000);
				logger.info("title = {}", title);
				logger.info("diffDay = {}", diffDay);
				logger.info("nowDate = {}", convNowDate);
				logger.info("payDate = {}", convPayDate);
				// 하루 전 머니캘린더 일정 알림
				if(diffDay == -1){
					smallCtgryCode = Const.NOTIFICATION_SMALL_CATEGORY_CALENDAR_TOMMOROW;
					notificationSmallCategory = notificationSmallCategoryMapper.selectByPrimaryKey(smallCtgryCode);
				}
				// 당일 머니캘린더 일정 알림
				else if(diffDay == 0){
					smallCtgryCode = Const.NOTIFICATION_SMALL_CATEGORY_CALENDAR_TODAY;
					notificationSmallCategory = notificationSmallCategoryMapper.selectByPrimaryKey(smallCtgryCode);
				}
				else{
					continue;
				}
				String message = notificationSmallCategory.getMsg().replaceAll(Const.NOTIFICATION_TITLE_BLANK, title);
	    		
	    		noti.setUserId(userId);
	    		noti.setSmallCtgryCode(smallCtgryCode);
	    		noti.setMsg(message);
	    		noti.setMakeMonth(payYearMonth);
	    		noti.setMakeDay(payDay);
	    		noti.setReadYn("N");
	    		noti.setRegDate(now);
	    		noti.setModDate(now);
	    		noti.setIdentifier(calId);
	    		
	    		// 중복이 아니면 등록한다.
	    		if(!this.isNotificationDuplication(noti)) {
	    			notificationMapper.insert(noti);
	    		}
			}
		}*/
    }
    
    /**
     * 머니캘린더 납부 완료/미납 알림을 생성한다.
     * @param userId
     */
    private void makeNotificationCalendarPaid(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException{
    	/*Date now = Calendar.getInstance(Locale.KOREA).getTime();
    	String nowDate = Utils.getDateString(now).substring(0, 8);
		String nowYearMonth = nowDate.substring(0, 6);
		String nowDay = nowDate.substring(6, 8);
		Date convNowDate = Utils.convertStringToDate(nowYearMonth+nowDay, "2");
		
		List<MoneyCalList> moneyCalList = etcService.getMonthlyMoneyCalendarList(userId, nowDate);
		if(moneyCalList != null && moneyCalList.size() >0) {
			for (MoneyCalList moneyCal : moneyCalList){
				String calId = moneyCal.getCalendarId();
				String title = moneyCal.getTitle();
				String payDate = moneyCal.getPayDate();
				String payYearMonth = payDate.substring(0, 6);
				String payDay = payDate.substring(6, 8);
				Date convPayDate = Utils.convertStringToDate(payYearMonth+payDay, "2");
				
				TblNotification noti = new TblNotification();
				TblNotificationSmallCategory notificationSmallCategory = new TblNotificationSmallCategory();
				String smallCtgryCode = "";
				if (moneyCal.getPayYn().equals("Y")){
					smallCtgryCode = Const.NOTIFICATION_SMALL_CATEGORY_CALENDAR_PAID;
					notificationSmallCategory = notificationSmallCategoryMapper.selectByPrimaryKey(smallCtgryCode);
				} else if (moneyCal.getPayYn().equals("N")){
					Long diffDay = (convNowDate.getTime() - convPayDate.getTime()) / (24 * 60 * 60 * 1000);
					if(diffDay > 3){
						smallCtgryCode = Const.NOTIFICATION_SMALL_CATEGORY_CALENDAR_UNPAID;
						notificationSmallCategory = notificationSmallCategoryMapper.selectByPrimaryKey(smallCtgryCode);
					}
					else continue;
				}
				String message = notificationSmallCategory.getMsg().replaceAll(Const.NOTIFICATION_TITLE_BLANK, title);
				noti.setUserId(userId);
	    		noti.setSmallCtgryCode(smallCtgryCode);
	    		noti.setMsg(message);
	    		noti.setMakeMonth(payYearMonth);
	    		noti.setMakeDay(payDay);
	    		noti.setReadYn("N");
	    		noti.setRegDate(now);
	    		noti.setModDate(now);
	    		noti.setIdentifier(calId);
	    		
	    		// 중복이 아니면 등록한다.
	    		if(!this.isNotificationDuplication(noti)) {
	    			notificationMapper.insert(noti);
	    		}
			}
		}*/
    }
    
    /**
     * 챌린지 알림을 생성한다.
     * @param userId
     */
    private void makeNotificationAboutChallenge(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException{
    	logger.info("userId = {}", userId);
    	
		List<ChallengeList> challengeList = etcService.getChallengeList(userId);
		
		// 챌린지 내역이 존재하면
    	if(challengeList != null && challengeList.size() >0){
    		Date now = Calendar.getInstance(Locale.KOREA).getTime();
    		String date = Utils.getDateString(now);
    		String makeMonth = date.substring(0, 6);
    		String makeDay = date.substring(6, 8);
    		for (ChallengeList challenge : challengeList){
    			TblNotification noti = new TblNotification();
				String smallCtgryCode = "";
				String message = "";
				
				String title = challenge.getTitle();
    			String challengeId = challenge.getChallengeId();
    			String amt = challenge.getAmt();
    			String goalAmt = challenge.getGoalAmt();
    			String accountId = challenge.getAccountId();
    			Double percent = (Double)((Double.parseDouble(amt)/Double.parseDouble(goalAmt)) * 100);
    			if (Utils.isStringEmpty(accountId)){
    				smallCtgryCode = Const.NOTIFICATION_SMALL_CATEGORY_CHALLENGE_NEED_ACCOUNT;
					message = notificationSmallCategoryMapper.selectByPrimaryKey(smallCtgryCode).getMsg();
    			}
    			else if (percent > 0 && percent < 100){
    				String strPercent = String.format("%.0f",Math.floor(percent));
    				smallCtgryCode = Const.NOTIFICATION_SMALL_CATEGORY_CHALLENGE_PROCESS;
					message = notificationSmallCategoryMapper.selectByPrimaryKey(smallCtgryCode).getMsg().replaceAll(Const.NOTIFICATION_TITLE_BLANK, title).replaceAll(Const.NOTIFICATION_PERCENT_BLANK, strPercent);
					noti.setIdentifier(challengeId+"&"+strPercent);
    			}
    			else if (percent >= 100){
    				smallCtgryCode = Const.NOTIFICATION_SMALL_CATEGORY_CHALLENGE_COMPLETE;
					message = notificationSmallCategoryMapper.selectByPrimaryKey(smallCtgryCode).getMsg().replaceAll(Const.NOTIFICATION_TITLE_BLANK, title);
					noti.setIdentifier(challengeId);
    			}
    			
    			noti.setUserId(userId);
	    		noti.setSmallCtgryCode(smallCtgryCode);
	    		noti.setMsg(message);
	    		noti.setMakeMonth(makeMonth);
	    		noti.setMakeDay(makeDay);
	    		noti.setReadYn("N");
	    		noti.setRegDate(now);
	    		noti.setModDate(now);
	    		
	    		// 중복이 아니면 등록한다.
	    		if(!this.isNotificationDuplication(noti)) {
	    			notificationMapper.insert(noti);
	    		}
    		}
    	}
    }
    
    /**
     * 알림이 중복인지 확인한다.
     * @param noti
     * @param notiSmallCategoryCode
     * @param resetDate
     * @return true:중복, false:중복아님
     */
    private boolean isNotificationDuplication(TblNotification noti) {
    	boolean ret = true;

    	String userId = noti.getUserId();
    	String categoryCode = noti.getSmallCtgryCode();
    	String largeCategoryCode = categoryCode.substring(0, 2);
    	
    	TblNotificationExample example = new TblNotificationExample();    	
    	
    	// 카테고리 미분류는 초기화 날짜, 사용자, 카테고리 코드로 중복 체크한다.
    	if(categoryCode.equals(Const.NOTIFICATION_SMALL_CATEGORY_EXPENSE_NON_CATEGORY)) {
    		Date resetDate = Utils.getPrevNextDate("2", (-1) * Const.NOTIFICATION_NON_CATEGORY_RESET_DATE);
    		example.createCriteria().andUserIdEqualTo(userId).andSmallCtgryCodeEqualTo(categoryCode).andRegDateGreaterThanOrEqualTo(resetDate);
    		Integer count = notificationMapper.countByExample(example);
    		
    		if(count == 0) {
    			ret = false;
    		}
    	}
    	// 투자 확인 유도는 초기화 날짜, 사용자, 카테고리 코드로 중복 체크한다.
    	else if(categoryCode.equals(Const.NOTIFICATION_SMALL_CATEGORY_INVEST_STOCK)) {
    		Date resetDate = Utils.getPrevNextDate("2", (-1) * Const.NOTIFICATION_INVEST_RESET_DATE);
    		example.createCriteria().andUserIdEqualTo(userId).andSmallCtgryCodeEqualTo(categoryCode).andRegDateGreaterThan(resetDate);
    		Integer count = notificationMapper.countByExample(example);
    		
    		if(count == 0) {
    			ret = false;
    		}
    	}
    	// 머니캘린더 납부완료 알림 중복체크 (사용자 id, 카테고리코드, 납부년월일, 머니캘린더id).
    	else if(largeCategoryCode.equals(Const.NOTIFICATION_LARGE_CATEGORY_CALENDAR)) {
    		String moneyCalId = noti.getIdentifier();
    		String payYearMonth = noti.getMakeMonth();
    		String payDay = noti.getMakeDay();
    		example.createCriteria().andUserIdEqualTo(userId).andSmallCtgryCodeEqualTo(categoryCode).andMakeMonthEqualTo(payYearMonth).andMakeDayEqualTo(payDay).andIdentifierEqualTo(moneyCalId);
    		Integer count = notificationMapper.countByExample(example);
    		
    		if(count == 0) {
    			ret = false;
    		}
    	}
    	// 챌린지 계좌 연동 촉구 알림 중복 체크 (초기화날짜, 사용자, 카테고리 코드)
    	else if(categoryCode.equals(Const.NOTIFICATION_SMALL_CATEGORY_CHALLENGE_NEED_ACCOUNT)) {
    		Date resetDate = Utils.getPrevNextDate("2", (-1) * Const.NOTIFICATION_CHALLENGE_NEED_ACCOUNT_RESET_DATE);
    		example.createCriteria().andUserIdEqualTo(userId).andSmallCtgryCodeEqualTo(categoryCode).andRegDateGreaterThanOrEqualTo(resetDate);
    		Integer count = notificationMapper.countByExample(example);
    		
    		if(count == 0) {
    			ret = false;
    		}
    	}
    	// 챌린지 달성, 변동사항 알림 중복 체크 (사용자id, 카테고리코드, 챌린지id(&달성율))
    	else if(categoryCode.equals(Const.NOTIFICATION_SMALL_CATEGORY_CHALLENGE_PROCESS) ||
    			categoryCode.equals(Const.NOTIFICATION_SMALL_CATEGORY_CHALLENGE_COMPLETE)){
    		String identifier = noti.getIdentifier();
    		example.createCriteria().andUserIdEqualTo(userId).andSmallCtgryCodeEqualTo(categoryCode).andIdentifierEqualTo(identifier);
    		Integer count = notificationMapper.countByExample(example);
    		
    		if(count == 0) {
    			ret = false;
    		}
    	}
    	
    	
    	return ret;
    }
}
