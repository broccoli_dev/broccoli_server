package com.pam.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.dao.CarMarketPriceDao;
import com.pam.dao.CardListDao;
import com.pam.dao.CollectLogDao;
import com.pam.dao.StockDao;
import com.pam.entity.CarMadeYear;
import com.pam.entity.CarMaker;
import com.pam.entity.CarMarketPrice;
import com.pam.entity.CarModel;
import com.pam.entity.CarSubModel;
import com.pam.entity.CardBenefit;
import com.pam.entity.CardBenefitLargeCategory;
import com.pam.entity.CardList;
import com.pam.entity.CollectSuccessInfo;
import com.pam.entity.NoticeEmergency;
import com.pam.entity.Qna;
import com.pam.entity.StockCodeName;
import com.pam.mapper.TblCollectLogMapper;
import com.pam.mapper.TblNoticeMapper;
import com.pam.mapper.TblQnaMapper;
import com.pam.mapper.TblVersionMapper;
import com.pam.mapper.domain.TblCollectLog;
import com.pam.mapper.domain.TblCollectLogExample;
import com.pam.mapper.domain.TblNotice;
import com.pam.mapper.domain.TblNoticeExample;
import com.pam.mapper.domain.TblQna;
import com.pam.mapper.domain.TblQnaExample;
import com.pam.mapper.domain.TblVersion;
import com.pam.mapper.domain.TblVersionExample;


@Service
public class InquiryService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    TblCollectLogMapper collectLogMapper;
    
    @Autowired
    TblVersionMapper versionMapper;
    
    @Autowired
    TblNoticeMapper noticeMapper;
    
    @Autowired
    TblQnaMapper qnaMapper;
    
    @Autowired
    CollectLogDao collectLogDao;
    
    @Autowired
    StockDao stockDao;
    
    @Autowired
	CardListDao cardListDao;
    
    @Autowired
	CarMarketPriceDao carMarketPriceDao;
    
    /**
     * 클라이언트 최신 버전을 가져온다.
     * @return
     * @throws MySQLIntegrityConstraintViolationException
     * @throws RuntimeException
     */
    public HashMap<String, String> getClientLatestVersion() throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("");
    	
		HashMap<String, String> ret = new HashMap<String, String>();
		
		TblVersionExample example = new TblVersionExample();
		List<TblVersion> list = versionMapper.selectByExample(example);
		
		if(list != null && list.size() > 0) {
			TblVersion version = list.get(0);
			ret.put("version", version.getVersion());
			ret.put("url", version.getUrl());
		} else {
			ret.put("version", "");
			ret.put("url", "");
		}

		TblNoticeExample noticeExample = new TblNoticeExample();
		noticeExample.createCriteria().andEmgncYnEqualTo("Y").andUseYnEqualTo("Y");
		
		List<TblNotice> noticeList = noticeMapper.selectByExample(noticeExample);
		if(noticeList != null && noticeList.size() > 0) {
			TblNotice notice = noticeList.get(0);
			String contents = notice.getContents();
			ret.put("notice", contents);
		} else {
			ret.put("notice", "");
		}
		
		return ret;
    }
    
    /**
     * 타입별로 스크래핑 데이터 수집 성공 로그 정보를 가져온다.
     * @param userId
     * @return
     * @throws MySQLIntegrityConstraintViolationException
     * @throws RuntimeException
     */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public List<CollectSuccessInfo> getCollectLogInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		List<CollectSuccessInfo> ret = new ArrayList<CollectSuccessInfo>();
		List<CollectSuccessInfo> list = collectLogDao.getCollectLogInfo(userId);
		HashMap<Integer, String> map = new HashMap<Integer, String>();
		for(CollectSuccessInfo collectSuccessInfo : list) {
			map.put(collectSuccessInfo.getType(), collectSuccessInfo.getUploadId());
		}
		
		for(int i=0; i<5; i++) {
			CollectSuccessInfo info = new CollectSuccessInfo();
			Integer type = i + 1;
			info.setType(type);
			String uploadId = map.get(type);
			
			if(uploadId != null) {
				info.setUploadId(uploadId);
			} else {
				info.setUploadId("");
			}
			
			ret.add(info);
		}
				
		return ret;
	}	
	
	/**
	 * uploadId로 성공 여부를 확인한다.
	 * @param userId
	 * @param uploadId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	public HashMap<String, String> getCollectResult(String userId, String uploadId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		HashMap<String, String> ret = new HashMap<String, String>();
		
		TblCollectLogExample example = new TblCollectLogExample();
		example.createCriteria().andUserIdEqualTo(userId).andUploadIdEqualTo(uploadId);
		List<TblCollectLog> list = collectLogMapper.selectByExample(example);
		if(list != null && list.size() > 0) {
			TblCollectLog collectLog = list.get(0);
			String result = collectLog.getResCode();
			ret.put("result", result);
		} else {
			ret.put("result", "");
		}
		
		return ret;
	}

	/**
	 * 키워드로 주식 종목을 검색한다.
	 * @param keyWord
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	public List<StockCodeName> getStockMap(String keyWord) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		String item = "'%" + keyWord + "%'";
		HashMap<String, String> itemMap = new HashMap<String, String>();
		itemMap.put("item", item);
		
		List<StockCodeName> list = stockDao.selectStockCodeName(itemMap);
		
		return list;
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<CardList> getCardList(String userId, String companyCode) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("companyCode", companyCode);
		List<CardList> list = cardListDao.getCardList(map);
		return list;
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public CardBenefit getCardBenefit(String userId, String cardCode) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("cardCode", cardCode);
		CardBenefit ret = new CardBenefit();
		ret = cardListDao.getCardBenefit(map);
		ret.setList(this.getCardBenefitLargeCategoryList(cardCode));
		
		return ret;
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<CardBenefitLargeCategory> getCardBenefitLargeCategoryList(String cardCode) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("cardCode", cardCode);
		List<CardBenefitLargeCategory> ret = cardListDao.getCardBenefitLargeCategoryList(map);
		for (CardBenefitLargeCategory cardBenefitLargeCategory: ret){
			String benefitLargeCategoryCode = cardBenefitLargeCategory.getBenefitLargeCategoryCode();
			cardBenefitLargeCategory.setDetail(this.getCardBenefitDetailList(cardCode, benefitLargeCategoryCode));
		}
		
		return ret;
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<String> getCardBenefitDetailList(String cardCode, String benefitLargeCategoryCode) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("cardCode", cardCode);
		map.put("benefitLargeCategoryCode", benefitLargeCategoryCode);
		List<String> ret = cardListDao.getCardBenefitDetailList(map);
		
		return ret;
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<CarMaker> getCarMakerList(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		List<CarMaker> ret = carMarketPriceDao.getCarMakerList();
		
		return ret;
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<CarModel> getCarModelList(String userId, String makerCode) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("makerCode", makerCode);
		List<CarModel> ret = carMarketPriceDao.getCarModelList(map);
		
		return ret;
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<CarSubModel> getCarSubModelList(String userId, String makerCode, String carCode) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("makerCode", makerCode);
		map.put("carCode", carCode);
		List<CarSubModel> ret = carMarketPriceDao.getCarSubModelList(map);
		
		return ret;
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<CarMadeYear> getCarMadeYearList(String userId, String makerCode, String carCode, String subCode) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("makerCode", makerCode);
		map.put("carCode", carCode);
		map.put("subCode", subCode);
		List<CarMadeYear> ret = carMarketPriceDao.getCarMadeYearList(map);
		
		return ret;
	}
	
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public CarMarketPrice getCarMarketPrice(String userId, String makerCode, String carCode, String subCode, String madeYear) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("makerCode", makerCode);
		map.put("carCode", carCode);
		map.put("subCode", subCode);
		map.put("madeYear", madeYear);
		
		CarMarketPrice ret = new CarMarketPrice();
		ret = carMarketPriceDao.getCarMarketPrice(map);
		
		return ret;
	}

	/**
	 * 긴급공지 조회
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public List<NoticeEmergency> getEmergencyNotice() throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("");
    	
		List<NoticeEmergency> ret = new ArrayList<NoticeEmergency>();
		TblNoticeExample example = new TblNoticeExample();
		example.createCriteria().andEmgncYnEqualTo("Y").andUseYnEqualTo("Y");
		
		List<TblNotice> noticeList = noticeMapper.selectByExample(example);
		if(noticeList != null && noticeList.size() > 0) {
			TblNotice notice = noticeList.get(0);
			String subject = notice.getSubject();
			String contents = notice.getContents();

			NoticeEmergency noticeEmergency = new NoticeEmergency();
			noticeEmergency.setSubject(subject);
			noticeEmergency.setContents(contents);
			
			ret.add(noticeEmergency);
		}
		
		return ret;
    }

	/**
	 * qna 조회
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public List<Qna> getQnaList() throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("");
    	
		List<Qna> ret = new ArrayList<Qna>();
		
		TblQnaExample example = new TblQnaExample();
		example.createCriteria();
		example.setOrderByClause("qna_id asc");
		
		List<TblQna> qnaList = qnaMapper.selectByExample(example);
		
		if(qnaList != null && qnaList.size() > 0) {
			for(TblQna qna : qnaList) {
				Long qnaId = qna.getQnaId();
				String question = qna.getQstn();
				String answer = qna.getAnsr();
				
				Qna tmp = new Qna();
				tmp.setQnaId(String.valueOf(qnaId));
				tmp.setQuestion(question);
				tmp.setAnswer(answer);
				
				ret.add(tmp);
			}
		}
		
		return ret;
    }
}
