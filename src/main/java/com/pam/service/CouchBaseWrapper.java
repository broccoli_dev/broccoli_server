package com.pam.service;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PreDestroy;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pam.config.CouchbaseConfig;
import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;

@Service
public class CouchBaseWrapper {
	
	protected final Log _logger = LogFactory.getLog( getClass() );
	
	private Bucket bucket;
	private Cluster cluster;
	private int expire;
	
	@Autowired
	public CouchBaseWrapper(final CouchbaseConfig config) {
		try{
			this.cluster = CouchbaseCluster.create(config.getUrl());
			this.bucket = cluster.openBucket(config.getBucket(),config.getPasswd(), 3, java.util.concurrent.TimeUnit.MINUTES);
			
		} catch (Exception e){
			e.printStackTrace();
		}
	}
	
	@PreDestroy
	public void preDestroy() {
		if (this.cluster != null) {
			this.cluster.disconnect();
		}
	}
	 /**
     * Prepare a new JsonDocument with some JSON content
     */
    public JsonDocument createDocument(String id, JsonObject content, String type) {
    	if("SMS".equals(type) && expire > 0){
    		return JsonDocument.create(id, expire, content);
    	} else{
    		return JsonDocument.create(id, content);
    	}
    }
    
    public static JsonDocument createDocument(String id, int expire, JsonObject content) {
        return JsonDocument.create(id, expire, content);
    }

    public static JsonDocument createDocument(String id, JsonObject content) {
        return JsonDocument.create(id, content);
    }
    /**
     * CREATE the document in database
     * @return the created document, with up to date metadata
     */
    public JsonDocument insert(JsonDocument doc) {
        return bucket.insert(doc);
    }
    /**
     * READ the document from database
     */
    public JsonDocument read(String id) {
        return bucket.get(id);
    }
    /**
     * UPDATE the document in database
     * @return the updated document, with up to date metadata
     */
    public JsonDocument update(JsonDocument doc) {
        return bucket.replace(doc);
    }
    /**
     * DELETE the document from database
     * @return the deleted document, with only metadata (since content has been deleted)
     */
    public JsonDocument delete(String id) {
        return bucket.remove(id);
    }
    public List<JsonDocument> delete(List<String> ids) {
        List<JsonDocument> docs = new ArrayList<JsonDocument>();
        try {
            for (String id : ids) {
                docs.add(bucket.remove(id));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return docs;
    }
}