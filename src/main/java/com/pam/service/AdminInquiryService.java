package com.pam.service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.constant.Const;
import com.pam.dao.CardListDao;
import com.pam.dao.CategoryDao;
import com.pam.dao.CategoryTagDao;
import com.pam.dao.UserDao;
import com.pam.entity.AdminList;
import com.pam.entity.CardAllList;
import com.pam.entity.CardBenefitCode;
import com.pam.entity.CardBenefitLargeCode;
import com.pam.entity.CardBrandCode;
import com.pam.entity.CardCodes;
import com.pam.entity.CardCompanyCode;
import com.pam.entity.CardInfo;
import com.pam.entity.CardInfoBenefit;
import com.pam.entity.CardTypeCode;
import com.pam.entity.LargeCategory;
import com.pam.entity.NonCategory;
import com.pam.entity.StoreNameMatchCategory;
import com.pam.entity.Tag;
import com.pam.entity.UserDB;
import com.pam.mapper.TblAdminAuthMapper;
import com.pam.mapper.TblAdminMapper;
import com.pam.mapper.TblExpenseMapper;
import com.pam.mapper.domain.TblAdmin;
import com.pam.mapper.domain.TblAdminExample;
import com.pam.mapper.domain.TblExpense;
import com.pam.mapper.domain.TblExpenseExample;
import com.pam.security.PasswordHash;
import com.pam.util.Utils;


@Service
public class AdminInquiryService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    TblExpenseMapper expenseMapper;
    
    @Autowired
	CardListDao cardListDao;
    
    @Autowired
	CategoryTagDao categoryTagDao;
    
    @Autowired
	CategoryDao categoryDao;
    
    @Autowired
	UserDao userDao;
    
    @Autowired
	TblAdminMapper adminMapper;
    
    @Autowired
	TblAdminAuthMapper adminAuthMapper;
	
	/**
	 * 태그 목록 조회 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<Tag> getTag() throws MySQLIntegrityConstraintViolationException, RuntimeException {
		List<Tag> ret = categoryTagDao.getAllTagInfo();
		return ret;
	}
	
	/**
	 * 카테고리 목록 조회 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<LargeCategory> getCategory() throws MySQLIntegrityConstraintViolationException, RuntimeException {
		List<LargeCategory> ret = categoryDao.getCategoryList();
		return ret;
	}
	
	/**
	 * 태그 목록 업데이트 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void saveTag(Map<String, ?> requestParam)
			throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("params : {}", requestParam);
		List<HashMap<String,String>> added = (List<HashMap<String,String>>) requestParam.get("added");
		List<HashMap<String,String>> removed = (List<HashMap<String,String>>) requestParam.get("removed");
		List<HashMap<String,String>> changed = (List<HashMap<String,String>>) requestParam.get("changed");
		
		for (HashMap<String, String> addTag : added){
			categoryTagDao.insertNewTag(addTag);
			
			TblExpense record = new TblExpense();
			record.setCtgryCode(addTag.get("ctgryCode"));
			
			TblExpenseExample example = new TblExpenseExample();
			example.createCriteria().andCtgryCodeEqualTo(Const.CATEGORY_CODE_NOT_DEFINED).andOppnntLike("%"+addTag.get("tag")+"%");
			expenseMapper.updateByExampleSelective(record, example);
		}
		for (HashMap<String, String> removeTag : removed){
			categoryTagDao.deleteTag(removeTag);
		}
		for (HashMap<String, String> changeTag : changed){
			categoryTagDao.updateTag(changeTag);
		}
		return;
	}
	
	/**
	 * 미분류 카테고리 목록 조회 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public HashMap<String,List<NonCategory>> getNonCategory() throws MySQLIntegrityConstraintViolationException, RuntimeException {
		HashMap<String,List<NonCategory>> ret = new HashMap<String,List<NonCategory>>();
		List<NonCategory> list = categoryDao.getNonCategoryList();
		ret.put("data", list);
		return ret;
	}
	
	/**
	 * 미분류 항목 태그 추가 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addNonCategoryTag(String tag, String ctgryCode, Integer isMoneyCalendar)
			throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("params : {}, {}, {}", tag, ctgryCode, isMoneyCalendar);
		
		Integer ctgryPriority = categoryTagDao.getLastCtgryPriority() + 1;
		logger.info("priority : {}", ctgryPriority);
		
		HashMap<String,String> map = new HashMap<String,String>();
		map.put("tag", tag);
		map.put("ctgryCode", ctgryCode);
		map.put("isMoneyCalendar", isMoneyCalendar.toString());
		map.put("ctgryPriority", ctgryPriority.toString());
		
		categoryTagDao.insertNewTag(map);
		
		TblExpense record = new TblExpense();
		record.setCtgryCode(ctgryCode);
		
		TblExpenseExample example = new TblExpenseExample();
		example.createCriteria().andCtgryCodeEqualTo(Const.CATEGORY_CODE_NOT_DEFINED).andOppnntEqualTo(tag);
		expenseMapper.updateByExampleSelective(record, example);
		
		return;
	}
	
	/**
	 * 모든 카드 리스트 검색 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<CardAllList> getCardAllList() throws MySQLIntegrityConstraintViolationException, RuntimeException {
		List<CardAllList> list = cardListDao.getCardAllList();
		return list;
	}
	
	/**
	 * 카드 삭제 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteCard(String cardCode) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("cardCode : {}", cardCode);
		HashMap<String,String> map = new HashMap<String,String>();
		map.put("cardCode", cardCode);
		
		cardListDao.deleteCardBrandByCardCode(map);
		cardListDao.deleteCardBenefitByCardCode(map);
		cardListDao.deleteCard(map);
		
		return;
	}
	
	/**
	 * 카드 회사,타입,브랜드,카테고리 코드 요청 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public CardCodes getCardCodes() throws MySQLIntegrityConstraintViolationException, RuntimeException {
		CardCodes ret = new CardCodes();
		List<CardCompanyCode> companyList = cardListDao.getCardCompanyCode();
		List<CardTypeCode> typeList = cardListDao.getCardTypeCode();
		List<CardBrandCode> brandList = cardListDao.getCardBrandCode();
		List<CardBenefitLargeCode> largeBenefitList = cardListDao.getCardBenefitLargeCode();
		for (int i=0; i<largeBenefitList.size(); i++){			
			CardBenefitLargeCode large = largeBenefitList.get(i);
			String benefitLargeCtgryCode = large.getBenefitLargeCtgryCode();
			List<CardBenefitCode> benefit = cardListDao.getCardBenefitCode(benefitLargeCtgryCode);
			large.setBenefit(benefit);
			largeBenefitList.set(i, large);
		}
		
		ret.setCompany(companyList);
		ret.setType(typeList);
		ret.setBrand(brandList);
		ret.setLargeBenefit(largeBenefitList);
		
		return ret;
	}
	
	/**
	 * 카드 회사,타입,브랜드,카테고리 코드 요청 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public CardInfo getCardInfo(String cardCode) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		CardInfo ret = new CardInfo();
		CardInfo info = cardListDao.getCardInfo(cardCode);
		List<String> brand = cardListDao.getCardInfoBrand(cardCode);
		List<CardInfoBenefit> benefit = cardListDao.getCardInfoBenefit(cardCode);
		info.setBrand(brand);
		info.setBenefit(benefit);
		ret = info;
		return ret;
	}
	
	/**
	 * 카드 추가,업데이트 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void saveCard(String cardCode, CardInfo cardData)
			throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("{}", Utils.toJsonString(cardData));
		if (cardCode.equals("add")){
			String lastCardCode = cardListDao.getLatestManualInsertedCardCode(cardData.getCompanyCode());
			if (Utils.isStringEmpty(lastCardCode)){
				cardCode = cardData.getCompanyCode()+"1000";
			} else {
				Integer nextCodeNum = Integer.parseInt(lastCardCode.substring(2, 6))+1;
				cardCode = cardData.getCompanyCode()+nextCodeNum.toString();
			}
		}
		
		HashMap<String, String> map1 = new HashMap<String, String>();
		map1.put("cardCode", cardCode);
		map1.put("companyCode", cardData.getCompanyCode());
		map1.put("cardType", cardData.getCardType());
		map1.put("cardName", cardData.getCardName());
		map1.put("internalAnnualFee", cardData.getInternalAnnualFee());
		map1.put("foreignAnnualFee", cardData.getForeignAnnualFee());
		map1.put("cardImg", cardData.getCardImg());
		map1.put("requirement", cardData.getRequirement());
		map1.put("headcopy", cardData.getHeadcopy());
		cardListDao.insertManualCardData(map1);
		
		List<String> brand = cardData.getBrand();
		if (brand != null && brand.size()>0){
			String brandInsertStr = "";
			String brandDeleteStr = "(";
			for (int i=0; i<brand.size();i++){
				brandInsertStr += "(\""+cardCode+"\", \""+brand.get(i)+"\")";
				
				brandDeleteStr += "\""+brand.get(i)+"\"";
				if (i < brand.size()-1){
					brandInsertStr += ",";
					brandDeleteStr += ",";
				}
			}
			brandDeleteStr += ")";
			HashMap<String,String> mapinsert = new HashMap<String,String>();
			mapinsert.put("brandInsertStr", brandInsertStr);
			cardListDao.insertManualCardBrand(mapinsert);
			
			HashMap<String, String> map2 = new HashMap<String,String>();
			map2.put("cardCode", cardCode);
			map2.put("brandDeleteStr", brandDeleteStr);
			cardListDao.deleteManualOldCardBrand(map2);
		}
		List<CardInfoBenefit> benefit = cardData.getBenefit();
		HashMap<String,String> mapbenefit = new HashMap<String,String>();
		mapbenefit.put("cardCode", cardCode);
		cardListDao.deleteManualOldCardBenefit(mapbenefit);
		if (benefit != null && benefit.size()>0){
			for (int i=0;i<benefit.size();i++){
				String benefitCode = cardCode+String.format("%02d", i);
				
				CardInfoBenefit b = benefit.get(i);
				String benefitLargeCtgry = b.getBenefitLargeCtgry();
				String benefitCtgry = b.getBenefitCtgry();
				String benefitName = b.getBenefitName();
				
				HashMap<String,String> map3 = new HashMap<String,String>();
				map3.put("cardCode", cardCode);
				map3.put("benefitLargeCtgry", benefitLargeCtgry);
				String benefitSeq = cardListDao.getBenefitSeq(map3);
				if (Utils.isStringEmpty(benefitSeq)){
					HashMap<String,String> mapbenefitseq = new HashMap<String,String>();
					mapbenefitseq.put("cardCode", cardCode);
					String lastBenefitSeq = cardListDao.getLatestManualInsertedBenefitSeq(mapbenefitseq);
					if (Utils.isStringEmpty(lastBenefitSeq)){
						benefitSeq = "1";
					} else{
						benefitSeq = Integer.toString(Integer.parseInt(lastBenefitSeq)+1);
					}	
				}
				HashMap<String,String> map4 = new HashMap<String,String>();
				map4.put("benefitCode", benefitCode);
				map4.put("cardCode", cardCode);
				map4.put("benefitSeq", benefitSeq);
				map4.put("benefitLargeCtgry", benefitLargeCtgry);
				map4.put("benefitCtgry", benefitCtgry);
				map4.put("benefitName", benefitName);
				cardListDao.insertManualCardBenefit(map4);
			}
		}
		return;
	}
	
	/**
	 * 회원DB 요청 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<UserDB> getUserDB(HashMap<String,String> map) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		List<UserDB> ret = userDao.getUserDB(map);
		return ret;
	}
	
	/**
	 * 스토어 이름-카테고리 매치 검색 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<StoreNameMatchCategory> getStoreNameMatchCategory(String storeName) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		HashMap<String,String> map = new HashMap<String,String>();
		map.put("storeName", storeName);
		List<StoreNameMatchCategory> list = categoryDao.getStoreNameMatchCategoryList(map);
		return list;
	}
	
	/**
	 * 스토어 삭제 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteStore(String storeName, String storeTel, String source, String categoryRaw) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		HashMap<String,String> map = new HashMap<String,String>();
		map.put("storeName", storeName);
		map.put("storeTel", storeTel);
		map.put("source", source);
		map.put("categoryRaw", categoryRaw);
		
		categoryDao.deleteStore(map);
		
		return;
	}
	
	/**
	 * 관리자 리스트 조회 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<AdminList> getAdminList() throws MySQLIntegrityConstraintViolationException, RuntimeException {
		List<AdminList> list = new ArrayList<AdminList>();
		
		TblAdminExample e = new TblAdminExample();
		e.createCriteria().andAdminIdNotEqualTo("F");
		List<TblAdmin> admins = adminMapper.selectByExample(e);
		for(TblAdmin admin:admins){
			AdminList a = new AdminList();
			a.setAdminId(admin.getAdminId());
			a.setAuthCode(admin.getAuthCode());
			a.setRegDate(Utils.convertDateFormat(admin.getRegDate(), "1"));
			a.setModDate(Utils.convertDateFormat(admin.getModDate(), "1"));
			list.add(a);
		}
		
		return list;
	}
	
	/**
	 * 관리자 계정 추가 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchAlgorithmException 
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addAdminAccount(String adminId,String adminAuth,String password) throws MySQLIntegrityConstraintViolationException, RuntimeException, NoSuchAlgorithmException, InvalidKeySpecException {
		logger.info("params : {}, {}, {}", adminId, adminAuth, password);
		
		Date now = Calendar.getInstance(Locale.KOREA).getTime();
		String newPassHash= PasswordHash.createHash(password, now);
		
		TblAdmin admin = new TblAdmin();
		admin.setAdminId(adminId);
		admin.setAuthCode(adminAuth);
		admin.setPassword(newPassHash);
		admin.setRegDate(now);
		admin.setModDate(now);
		
		adminMapper.insert(admin);
		
		return;
	}
	
	/**
	 * 관리자 계정 삭제 (관리자 페이지 용)
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteAdminAccount(String adminId, String adminAuth) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		TblAdminExample e = new TblAdminExample();
		e.createCriteria().andAdminIdEqualTo(adminId).andAuthCodeEqualTo(adminAuth);
		adminMapper.deleteByExample(e);
		
		return;
	}
}
