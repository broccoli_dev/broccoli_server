package com.pam.service;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.constant.Const;
import com.pam.dao.EtcDao;
import com.pam.dao.MoneyCalDao;
import com.pam.entity.BankTransactionOfMoneyCalendar;
import com.pam.entity.ChallengeBank;
import com.pam.entity.ChallengeList;
import com.pam.entity.EtcMain;
import com.pam.entity.MoneyCalList;
import com.pam.entity.MoneyCalendarMain;
import com.pam.entity.MoneyCalendarTransaction;
import com.pam.manager.scraping.ScrapingUtil;
import com.pam.mapper.TblBankAccountMapper;
import com.pam.mapper.TblChallengeMapper;
import com.pam.mapper.TblMoneyCalendarMapper;
import com.pam.mapper.domain.TblBankTransaction;
import com.pam.mapper.domain.TblChallenge;
import com.pam.mapper.domain.TblChallengeExample;
import com.pam.mapper.domain.TblExpense;
import com.pam.mapper.domain.TblMoneyCalendar;
import com.pam.mapper.domain.TblMoneyCalendarExample;
import com.pam.util.Utils;


@Service
public class EtcService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    TblMoneyCalendarMapper moneyCalendarMapper;
    
    @Autowired
    TblChallengeMapper challengeMapper;
    
    @Autowired
    TblBankAccountMapper bankAccountMapper;
    
    @Autowired
    EtcDao etcDao;
    
    @Autowired
    MoneyCalDao moneyCalDao;
    
    @Autowired
    CollectService collectService;

    /**
     * 기타 메인 조회
     * @param userId
     * @return
     * @throws MySQLIntegrityConstraintViolationException
     * @throws RuntimeException
     */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public EtcMain getEtcMain(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);		
		EtcMain ret = new EtcMain();
		
		// 챌린지 정보 조회
		List<ChallengeList> challengeList = new ArrayList<ChallengeList>(); 
		List<ChallengeList> retChallengeList = etcDao.getChallengeList(userId);
		if(retChallengeList != null && retChallengeList.size() > 0) {
			challengeList = retChallengeList;
		}
		
		ret.setChallenge(challengeList);
		
		// 머니 캘린더 정보 조회
		MoneyCalendarMain moneyCalendarMain = new MoneyCalendarMain();
		
		List<MoneyCalList> moneyCalList = new ArrayList<MoneyCalList>();
		List<TblMoneyCalendar> retMoneyCalendarList = moneyCalDao.getNextMoneyCalendarList(userId);
		if(retMoneyCalendarList != null && retMoneyCalendarList.size() > 0){
			moneyCalendarMain.setTotalCount(retMoneyCalendarList.size());
			if (retMoneyCalendarList.size() > 4){
				retMoneyCalendarList = retMoneyCalendarList.subList(0, 4);
			}
			
			for (TblMoneyCalendar m : retMoneyCalendarList){
				MoneyCalList moneyCal = new MoneyCalList();
				moneyCal.setCalendarId(m.getCalId().toString());
				moneyCal.setTitle(m.getTitle());
				moneyCal.setOpponent(m.getOppnnt());
				moneyCal.setAmt(m.getAmt().toString());
				moneyCal.setPayType(m.getPayType());
				moneyCal.setExpectOrPaid((m.getExptYn().equals("Y")) ? "EY":"EN");
				moneyCal.setPayDate(m.getPayDate());
				moneyCal.setRepeatType(m.getRptType());
				moneyCal.setMatchedTag(m.getMtchdTag());
				moneyCalList.add(moneyCal);
			}
		}
		else {
			moneyCalendarMain.setTotalCount(0);
		}
		
		moneyCalendarMain.setList(moneyCalList);
		ret.setMoneyCalendar(moneyCalendarMain);

		return ret;
	}	

	/**
	 * 월별 머니 캘린더 조회
	 * @param moneyCalendar
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<MoneyCalList> getMonthlyMoneyCalendarList(String userId, String date) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}, date = {}", userId, date);
		String year = date.substring(0, 4);
		String month = date.substring(4, 6);
		HashMap<String,String> map = new HashMap<String,String>();
		map.put("userId", userId);
		map.put("year", year);
		map.put("month", month);
		List<MoneyCalList> ret = new ArrayList<MoneyCalList>();
		List<TblMoneyCalendar> retMoneyCalendarList = moneyCalDao.getMonthlyMoneyCalendarList(map);
		if(retMoneyCalendarList != null && retMoneyCalendarList.size() >0) {
			Date now = Calendar.getInstance(Locale.KOREA).getTime();
			
			// 조회 월 거래내역 조회
			String startDate = year + month + "01000000";
			String endDate = year + month + "31240000";
			
			HashMap<String,String> map2 = new HashMap<String,String>();
			map2.put("userId", userId);
			map2.put("startDate", startDate);
			map2.put("endDate", endDate);
			List<TblBankTransaction> monthlyBTList = moneyCalDao.getMonthlyBankTransaction(map2);
			for(TblMoneyCalendar m : retMoneyCalendarList){
				String eop = "";
				if (Utils.convertStringToDate(m.getPayDate(), "2").compareTo(now) > 0){
					// 예상 or 확정
					eop = (m.getExptYn().equals("Y")) ? "EY":"EN";
				}
				else{
					// 완료 or 미납
					String con = "";
					if (m.getMtchdTag() != null) {
						con = m.getMtchdTag();
					} else{
						con = m.getOppnnt();
					}
					
					eop = "PN";
					for(TblBankTransaction transaction: monthlyBTList){
						if (transaction.getOppnnt().contains(con)){
							eop = "PY";
							break;
						}
					}
				}
				
				MoneyCalList moneyCal = new MoneyCalList();
				moneyCal.setCalendarId(m.getCalId().toString());
				moneyCal.setTitle(m.getTitle());
				moneyCal.setOpponent(m.getOppnnt());
				moneyCal.setAmt(m.getAmt().toString());
				moneyCal.setPayType(m.getPayType());
				moneyCal.setExpectOrPaid(eop);
				moneyCal.setPayDate(m.getPayDate());
				moneyCal.setRepeatType(m.getRptType());
				moneyCal.setMatchedTag(m.getMtchdTag());
				ret.add(moneyCal);
			}
		}
		return ret;
	}
	
	/**
	 * 머니캘린더 청구기관 등록 용 거래 내역 조회
	 * @param userId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<MoneyCalendarTransaction> getMoneyCalendarTransactionList(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		List<MoneyCalendarTransaction> ret = new ArrayList<MoneyCalendarTransaction>();
		
		// 거래 내역을 가져온다.
		List<MoneyCalendarTransaction> transList = etcDao.getMoneyCalendarTransactionList(userId);
		
		// 기존 등록된 머니 캘린더의 opponnent와 동일한 거래 내역이있는 지 확인한다.
		TblMoneyCalendarExample example = new TblMoneyCalendarExample();
		example.createCriteria().andUserIdEqualTo(userId).andUseYnEqualTo("Y");
		List<TblMoneyCalendar> moneyCalendarList = moneyCalendarMapper.selectByExample(example);
		for(MoneyCalendarTransaction calendarTransaction : transList) {
			String transOpponent = calendarTransaction.getOpponent();
			String addedYn = "N";
			for(TblMoneyCalendar moneyCalendar : moneyCalendarList) {
				String opponnent = moneyCalendar.getOppnnt();
				if(transOpponent.equals(opponnent)) {
					addedYn = "Y";
					break;
				} 
			}
			
			calendarTransaction.setAddedYn(addedYn);
			ret.add(calendarTransaction);
		}
		
		return ret;
	}	

    /**
     * 머니 캘린더 등록/수정
     * @param calendar
     * @throws MySQLIntegrityConstraintViolationException
     * @throws RuntimeException
     */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void setMoneyCalendar(TblMoneyCalendar moneyCalendar) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblMoneyCalendar = {}", Utils.toJsonString(moneyCalendar));

		Date now = Calendar.getInstance(Locale.KOREA).getTime();
		Long calendarId = moneyCalendar.getCalId();
		
		// transId == 0이면 추가, 아니면 수정
		if(calendarId == 0L) {
			if (moneyCalendar.getRptType().equals("NO")){
				// 반복 없음 캘린더 추가
				moneyCalendar.setCalId(null);
				moneyCalendar.setRegDate(now);
				moneyCalendar.setModDate(now);
				moneyCalDao.insertMoneyCal(moneyCalendar);
			}
			else{
				// 반복 캘린더 추가
				List<TblMoneyCalendar> moneyCalendarList = new ArrayList<TblMoneyCalendar>();
				String strPayDate = moneyCalendar.getPayDate();
				for(int i = 0;i<120;i++){
					Date conPayDate = Utils.convertStringToDate(strPayDate, "2");
					String payDate = Utils.getPrevNextDateString("1", i, "yyyymmdd", conPayDate);
					
					TblMoneyCalendar m = new TblMoneyCalendar();
					m.setUserId(moneyCalendar.getUserId());
					m.setTitle(moneyCalendar.getTitle());
					m.setOppnnt(moneyCalendar.getOppnnt());
					m.setAmt(moneyCalendar.getAmt());
					m.setPayType(moneyCalendar.getPayType());
					m.setPayDate(payDate);
					m.setPayDay(moneyCalendar.getPayDay());
					m.setRptType(moneyCalendar.getRptType());
					m.setUseYn(moneyCalendar.getUseYn());
					m.setExptYn(moneyCalendar.getExptYn());
					
					moneyCalendarList.add(m);
				}
				collectService.addMoneyCal(moneyCalendarList);
			}
			
			// 머니캘린더 등록 시 소비 항목으로
			HashMap<String, String> map = new HashMap<String,String>();
			map.put("userId", moneyCalendar.getUserId());
			map.put("oppnnt", moneyCalendar.getOppnnt());
			map.put("payDate", moneyCalendar.getPayDate());
			map.put("amt", String.valueOf((-1)*moneyCalendar.getAmt()));
			
			List<BankTransactionOfMoneyCalendar> btList = etcDao.getBankTransactionOfMoneyCalendar(map);
			for (BankTransactionOfMoneyCalendar bt : btList){
				TblExpense expense = new TblExpense();
				expense.setUserId(bt.getUserId());
				expense.setExpnsDate(bt.getTransDate().substring(0, 12)); // 소비 시간은 yyyyMMddHHmm(분)까지 표시
				expense.setOppnnt(bt.getOppnnt());
				expense.setCtgryCode(bt.getCtgryCode());
				String payType = ScrapingUtil.getPayTypeFromTransDes(bt.getTransMethod());
				expense.setPayType(payType);
				expense.setEditYn("N");
				expense.setCancelYn("N");
				expense.setCmpnyCode(bt.getCmpnyCode());
				expense.setUseYn("Y");
				
				if(bt.getSum() < 0) {
					bt.setSum((-1)*bt.getSum());
				}
				expense.setSum(bt.getSum());
				etcDao.insertExpenseFromMoneyCalendar(expense);
			}
		} else {
			TblMoneyCalendar orgCal = moneyCalendarMapper.selectByPrimaryKey(calendarId);
			Date conPayDate = Utils.convertStringToDate(orgCal.getPayDate(), "2");
			if(conPayDate.compareTo(now) >= 0){
				if (orgCal.getRptType().equals(moneyCalendar.getRptType())){
					// 반복 주기가 변경되지 않았다면
					if (moneyCalendar.getRptType().equals("NO")){
						// 수동 입력된 반복 없음 캘린더 수정
						moneyCalendar.setModDate(now);
						moneyCalDao.updateMoneyCal(moneyCalendar);
					}
					else {
						List<TblMoneyCalendar> list = new ArrayList<TblMoneyCalendar>();
						if (orgCal.getMtchdTag() == null){
							// 수동입력된 반복 캘린더 수정
							TblMoneyCalendarExample example = new TblMoneyCalendarExample();
							example.createCriteria().andUserIdEqualTo(orgCal.getUserId()).andOppnntEqualTo(orgCal.getOppnnt()).andPayDayEqualTo(orgCal.getPayDay()).andPayDateGreaterThanOrEqualTo(orgCal.getPayDate());
							list = moneyCalendarMapper.selectByExample(example);
						}
						else{
							// 자동입력된 반복 캘린더 수정
							TblMoneyCalendarExample example = new TblMoneyCalendarExample();
							example.createCriteria().andUserIdEqualTo(orgCal.getUserId()).andPayDayEqualTo(orgCal.getPayDay()).andMtchdTagEqualTo(orgCal.getMtchdTag()).andPayDateGreaterThanOrEqualTo(orgCal.getPayDate());
							list = moneyCalendarMapper.selectByExample(example);
						}
						
						for (TblMoneyCalendar m : list){
							m.setTitle(moneyCalendar.getTitle());
							m.setOppnnt(moneyCalendar.getOppnnt());
							m.setAmt(moneyCalendar.getAmt());
							m.setPayType(moneyCalendar.getPayType());
							m.setPayDate(m.getPayDate().substring(0, 6)+moneyCalendar.getPayDay());
							m.setPayDay(moneyCalendar.getPayDay());
							m.setExptYn(moneyCalendar.getExptYn());
							m.setModDate(now);
							moneyCalendarMapper.updateByPrimaryKeySelective(m);
						}
					}
				}
				else{
					// 반복 주기가 변경되었다면
					if (moneyCalendar.getRptType().equals("NO")){
						if (orgCal.getMtchdTag() == null){
							// 수동입력된 캘린더 반복주기 1M -> NO 수정
							TblMoneyCalendarExample example = new TblMoneyCalendarExample();
							example.createCriteria().andUserIdEqualTo(orgCal.getUserId()).andOppnntEqualTo(orgCal.getOppnnt()).andPayDayEqualTo(orgCal.getPayDay()).andPayDateGreaterThan(orgCal.getPayDate());
							moneyCalendarMapper.deleteByExample(example);
						}
						else {
							// 자동 입력된 캘린더 반복주기 1M -> NO 수정
							TblMoneyCalendarExample example = new TblMoneyCalendarExample();
							List<TblMoneyCalendar> list = new ArrayList<TblMoneyCalendar>();
							example.createCriteria().andUserIdEqualTo(orgCal.getUserId()).andPayDayEqualTo(orgCal.getPayDay()).andMtchdTagEqualTo(orgCal.getMtchdTag()).andPayDateGreaterThan(orgCal.getPayDate());
							list = moneyCalendarMapper.selectByExample(example);
							for (TblMoneyCalendar m : list){
								m.setTitle(moneyCalendar.getTitle());
								m.setOppnnt(moneyCalendar.getOppnnt());
								m.setAmt(moneyCalendar.getAmt());
								m.setPayType(moneyCalendar.getPayType());
								m.setPayDate(m.getPayDate().substring(0, 6)+moneyCalendar.getPayDay());
								m.setPayDay(moneyCalendar.getPayDay());
								m.setExptYn(moneyCalendar.getExptYn());
								m.setUseYn("N");
								m.setModDate(now);
								m.setDelDate(now);
								moneyCalendarMapper.updateByPrimaryKeySelective(m);
							}
						}
						moneyCalendar.setModDate(now);
						moneyCalDao.updateMoneyCal(moneyCalendar);
					}
					else{
						if (orgCal.getMtchdTag() == null){
							// 수동입력된 캘린더 반복주기 NO -> 1M 수정
							List<TblMoneyCalendar> moneyCalendarList = new ArrayList<TblMoneyCalendar>();
							for(int i = 0;i<120;i++){
								String payDate = Utils.getPrevNextDateString("1", i, "yyyymmdd", conPayDate);
								
								TblMoneyCalendar m = new TblMoneyCalendar();
								m.setUserId(moneyCalendar.getUserId());
								m.setTitle(moneyCalendar.getTitle());
								m.setOppnnt(moneyCalendar.getOppnnt());
								m.setAmt(moneyCalendar.getAmt());
								m.setPayType(moneyCalendar.getPayType());
								m.setPayDate(payDate);
								m.setPayDay(moneyCalendar.getPayDay());
								m.setRptType(moneyCalendar.getRptType());
								m.setUseYn(moneyCalendar.getUseYn());
								m.setExptYn(moneyCalendar.getExptYn());
								
								moneyCalendarList.add(m);
							}
							collectService.addMoneyCal(moneyCalendarList);
						}
						else {
							// 자동 입력된 캘린더 반복주기 NO -> 1M 수정
							List<TblMoneyCalendar> list = new ArrayList<TblMoneyCalendar>();
							TblMoneyCalendarExample example = new TblMoneyCalendarExample();
							example.createCriteria().andUserIdEqualTo(orgCal.getUserId()).andPayDayEqualTo(orgCal.getPayDay()).andMtchdTagEqualTo(orgCal.getMtchdTag()).andPayDateGreaterThan(orgCal.getPayDate());
							list = moneyCalendarMapper.selectByExample(example);
							for (TblMoneyCalendar m : list){
								m.setTitle(moneyCalendar.getTitle());
								m.setOppnnt(moneyCalendar.getOppnnt());
								m.setAmt(moneyCalendar.getAmt());
								m.setPayType(moneyCalendar.getPayType());
								m.setPayDate(m.getPayDate().substring(0, 6)+moneyCalendar.getPayDay());
								m.setPayDay(moneyCalendar.getPayDay());
								m.setExptYn(moneyCalendar.getExptYn());
								m.setUseYn("Y");
								m.setModDate(now);
								m.setDelDate(null);
								moneyCalendarMapper.updateByPrimaryKeySelective(m);
							}
						}
					}
				}
			}
		}
	}
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Integer deleteMoneyCaleldar(String userId, Long calendarId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}, calendarId = {}", userId, calendarId);
		
		TblMoneyCalendar calendar = moneyCalendarMapper.selectByPrimaryKey(calendarId);
		if (calendar == null) return 0;
		
		if (calendar.getRptType().equals("NO")){
			// 수동입력된 반복 없음 캘린더의 삭제 처리
			return moneyCalendarMapper.deleteByPrimaryKey(calendarId);
		}
		else if (calendar.getMtchdTag() == null){
			// 수동입력된 반복 캘린더의 삭제 처리
			TblMoneyCalendarExample example = new TblMoneyCalendarExample();
			example.createCriteria().andUserIdEqualTo(userId).andOppnntEqualTo(calendar.getOppnnt()).andPayDayEqualTo(calendar.getPayDay()).andPayDateGreaterThanOrEqualTo(calendar.getPayDate());
			return moneyCalendarMapper.deleteByExample(example);
		}
		else{
			// 자동입력된 캘린더의 삭제 처리
			return moneyCalDao.deleteRepeatedMoneyCal(calendar);
		}
	}

    /**
     * 챌린지 리스트를 가져온다.
     * @param userId
     * @return
     * @throws MySQLIntegrityConstraintViolationException
     * @throws RuntimeException
     */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<ChallengeList> getChallengeList(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);		
		
		return etcDao.getChallengeList(userId);
	}	

	/**
	 * 챌린지 계좌 정보를 가져온다.
	 * @param userId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<ChallengeBank> getChallengeBankAccount(String userId, Integer accountType) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		List<ChallengeBank> ret = new ArrayList<ChallengeBank>();
		logger.info("userId = {}, accountType = {}", userId, accountType);
		
		// 현재 내 챌린지에 등록된 계좌 아이디를 가져온다.
		TblChallengeExample example = new TblChallengeExample();
		example.createCriteria().andUserIdEqualTo(userId);
		List<TblChallenge> challengeList = challengeMapper.selectByExample(example);
		
		userId = "'" + userId + "'";
		
		HashMap<String, String> paramMap = new HashMap<String, String>();
		paramMap.put("userId", userId);
		String categoryCode = "";
				
		if(accountType == Const.CHALLENGE_BANK_TYPE_DEBT) {
			categoryCode = "a.small_ctgry_code = '" + Const.ACCT_TYPE_LOAN + "'";	
		} else if(accountType == Const.CHALLENGE_BANK_TYPE_DEPOSIT) {		
			categoryCode = "(a.small_ctgry_code = '" + Const.ACCT_TYPE_DEPOSIT + "' or a.small_ctgry_code = '" + Const.ACCT_TYPE_ISTMNT_SAVINGS + "')";
		} else if(accountType == Const.CHALLENGE_BANK_TYPE_ALL) {
			categoryCode = "1 = 1";
		}
		
		paramMap.put("categoryCode", categoryCode);
		// 선택한 계좌 리스트를 가져온다.
		List<ChallengeBank> challengeBankList = etcDao.getChallengeBankInfo(paramMap);
		
		if(challengeBankList != null && challengeBankList.size() > 0) {
			for(ChallengeBank challengeBank : challengeBankList) {
				String addedYn = "N";
				String accountId = challengeBank.getAccountId();
				
				for(TblChallenge challenge : challengeList) {
					Long accntId = challenge.getAccntId();
					String completeYn = challenge.getCmpltYn();
					
					// 기존 챌린지에 등록된 계좌인지 확인한다.
					if(accountId.equals(String.valueOf(accntId))) {
						if(completeYn.equals("N")) { // 등록되어 있고 완료되지 않은 챌린지 계좌만 등록됨으로 표시한다.
							addedYn = "Y";
						}
						break;
					}
				}
				
				challengeBank.setAddedYn(addedYn);
				ret.add(challengeBank);
			}
		}
		
		return ret;
	}	
	
    /**
     * 챌린지 등록/수정
     * @param challenge
     * @throws MySQLIntegrityConstraintViolationException
     * @throws RuntimeException
     */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void setChallenge(TblChallenge challenge) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblChallenge = {}", Utils.toJsonString(challenge));

		Date now = Calendar.getInstance(Locale.KOREA).getTime();
		Long challengeId = challenge.getChlngId();
		
		// transId == 0이면 추가, 아니면 수정
		if(challengeId == 0) {
			challenge.setChlngId(null);
			challenge.setRegDate(now);
			challenge.setModDate(now);			
			challengeMapper.insertSelective(challenge);
		} else {
			challenge.setModDate(now);
			challengeMapper.updateByPrimaryKeySelective(challenge);
		}
		
	}	

	/**
	 * 챌린지 삭제
	 * @param challenge
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Integer deleteChallenge(TblChallenge challenge) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblChallenge = {}", Utils.toJsonString(challenge));		
		Long challengeId = challenge.getChlngId();
		
		return challengeMapper.deleteByPrimaryKey(challengeId);
	}
}
