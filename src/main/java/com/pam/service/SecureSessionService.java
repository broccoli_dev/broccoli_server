package com.pam.service;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pam.security.Session;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.error.DocumentAlreadyExistsException;
import com.couchbase.client.java.error.DocumentDoesNotExistException;
import com.google.gson.Gson;

@Service
public class SecureSessionService {
    private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private CouchBaseWrapper couchBaseWrapper;
	
	private int secureExpire = 30 * 60;
	
	public boolean isValid(String sessionId) {
		boolean ret = false;
		Session session = this.readSession(sessionId);
		if(session !=null) {
			ret = true;
		}
		
		return ret;
	}
	
	public int write(String sessionId, JsonObject value) {
		int ret = 0;

    	JsonDocument doc = CouchBaseWrapper.createDocument(sessionId, this.secureExpire, value);
        try{
        	couchBaseWrapper.insert(doc);
        	ret = 1;
        }catch(DocumentAlreadyExistsException e){
        	couchBaseWrapper.update(doc);
        }
        
		return ret;
	}
	
	public int write(String sessionId, JsonObject value, int expireTimeout) {
		int ret = 0;

    	JsonDocument doc = CouchBaseWrapper.createDocument(sessionId, expireTimeout, value);
        try{
        	couchBaseWrapper.insert(doc);
        	ret = 1;
        }catch(DocumentAlreadyExistsException e){
        	couchBaseWrapper.update(doc);
        }
        
		return ret;
	}
	
	public JsonObject read(String sessionId) {
		JsonObject ret = null;
		
		JsonDocument doc = couchBaseWrapper.read(sessionId);
		if(doc != null) {
			ret = doc.content();
			String tmp = String.format("[READ] sessionId = %s, value = %s", sessionId, ret.toString());
			logger.debug(tmp);
		}
		
		return ret;
	}

	public Session readSession(String sessionId) {
		Session ret = null;
		
		JsonDocument doc = couchBaseWrapper.read(sessionId);
		if(doc != null) {
			JsonObject jsonObject = doc.content();
			String json = jsonObject.toString();
			
			Gson gson = new Gson();
			ret = gson.fromJson(json, Session.class);

			String tmp = String.format("[READ] sessionId = %s, value = %s", sessionId, json.toString());
			logger.debug(tmp);
		}
		
		return ret;
	}
	
	public int write(String sessionId, Session session) throws DocumentAlreadyExistsException{
		int ret = 1;

		Gson gson = new Gson();
		String json = gson.toJson(session);
		JsonObject obj = JsonObject.fromJson(json);
		
		this.write(sessionId, obj);
		
		String tmp = String.format("[WRITE] sessionId = %s, value = %s", sessionId, obj.toString());
		logger.debug(tmp);
		
		return ret;
	}

	public int write(String sessionId, Session session, int expireTimeout) throws DocumentAlreadyExistsException{
		int ret = 1;

		Gson gson = new Gson();
		String json = gson.toJson(session);
		JsonObject obj = JsonObject.fromJson(json);
		
		this.write(sessionId, obj, expireTimeout);

		String tmp = String.format("[WRITE] sessionId = %s, value = %s", sessionId, obj.toString());
		logger.debug(tmp);
		
		return ret;
	}
	
	public void delete(String sessionId) {
		try{
			couchBaseWrapper.delete(sessionId);
		}catch(DocumentDoesNotExistException e){
		}
	}
}
