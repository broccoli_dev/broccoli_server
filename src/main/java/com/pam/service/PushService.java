package com.pam.service;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.entity.Payload;
import com.pam.entity.PushData;
import com.pam.mapper.TblUserMapper;
import com.pam.mapper.domain.TblUser;



@Service
public class PushService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    TblUserMapper userMapper;
    private static Properties properties = null;
    	
	public void SendPush(String userId, String deviceType, String message) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}, message = {}", userId, message);

		List<PushData> dataList = new ArrayList<PushData>();
    	usingProperties();
    	String deviceToken = "";
    	
		deviceToken = this.getPushDeviceKey(userId);
    	
    	PushData pushData = new PushData();
    	pushData.setDeviceToken(deviceToken);
    	pushData.setDeviceType(deviceType);
    	Payload payload = new Payload();
    	payload.setBadge(1);
    	payload.setAlert(message);
    	pushData.setPayload(payload);
    	dataList.add(pushData);
    	
		Gson gson = new GsonBuilder().create();
		

		String data = gson.toJson(dataList);
		logger.info("pushData = {}" + data);
			
		if(data !=null ) {
			String ip = properties.getProperty("PUSH_SERVER_IP");
			int port = Integer.parseInt(properties.getProperty("PUSH_SERVER_PORT"));
			this.sendPush(ip, port, data);
		}
	}

	public void usingProperties() {
		ClassLoader cl;
		cl = Thread.currentThread().getContextClassLoader();
		if( cl == null )
			cl = ClassLoader.getSystemClassLoader();
		
		properties = new Properties();
		try {
			InputStream is = cl.getResourceAsStream("resource.properties");
			properties.load(is);

		} catch (IOException e) {
			logger.info("error = {}", e.toString());
		}

	}

    /**
     * userId로 deviceKey를 가져온다.
     * @param userId
     */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    private String getPushDeviceKey(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);		
		
		String ret = "";
		
		TblUser user = userMapper.selectByPrimaryKey(userId);
		
		if(user != null) {
			ret = user.getPushKey();
		}
		
		return ret;
    }

	private void sendPush(String ip, int port, String message) {		
		Socket socket = null;
		DataOutputStream os = null;
		
		try {
	        socket = new Socket(InetAddress.getByName(ip), port);
	        os = new DataOutputStream(socket.getOutputStream());	
	        byte[] b = message.getBytes("utf-8");
	        os.write(b);
	        
			os.close();
	    } catch (IOException e) {
	        logger.info("error = {}", e.toString());
	    } 
	}
}
