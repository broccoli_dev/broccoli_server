package com.pam.service;

import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.constant.Const;
import com.pam.entity.UserId;
import com.pam.entity.UserIdNameDate;
import com.pam.entity.UserInfo;
import com.pam.mapper.TblBankAccountBalanceMapper;
import com.pam.mapper.TblBankAccountMapper;
import com.pam.mapper.TblBankTransactionMapper;
import com.pam.mapper.TblBudgetListMapper;
import com.pam.mapper.TblBudgetMapper;
import com.pam.mapper.TblCarMapper;
import com.pam.mapper.TblCardApprovalMapper;
import com.pam.mapper.TblCardBillDetailMapper;
import com.pam.mapper.TblCardBillMapper;
import com.pam.mapper.TblCardBillMappingMapper;
import com.pam.mapper.TblCardBillSummaryMapper;
import com.pam.mapper.TblCardLimitMapper;
import com.pam.mapper.TblCardMapper;
import com.pam.mapper.TblCashReceiptMapper;
import com.pam.mapper.TblChallengeMapper;
import com.pam.mapper.TblCollectLogMapper;
import com.pam.mapper.TblDailyAssetMapper;
import com.pam.mapper.TblErrorLogMapper;
import com.pam.mapper.TblExpenseMapper;
import com.pam.mapper.TblFinaceCompanyRegistMapper;
import com.pam.mapper.TblMoneyCalendarMapper;
import com.pam.mapper.TblMonthlyAssetMapper;
import com.pam.mapper.TblNotificationMapper;
import com.pam.mapper.TblPropertyMapper;
import com.pam.mapper.TblRealEstateMapper;
import com.pam.mapper.TblStockTransactionMapper;
import com.pam.mapper.TblUserMapper;
import com.pam.mapper.domain.TblBankAccount;
import com.pam.mapper.domain.TblBankAccountBalanceExample;
import com.pam.mapper.domain.TblBankAccountExample;
import com.pam.mapper.domain.TblBankTransactionExample;
import com.pam.mapper.domain.TblBudget;
import com.pam.mapper.domain.TblBudgetExample;
import com.pam.mapper.domain.TblBudgetListExample;
import com.pam.mapper.domain.TblCarExample;
import com.pam.mapper.domain.TblCard;
import com.pam.mapper.domain.TblCardApprovalExample;
import com.pam.mapper.domain.TblCardBill;
import com.pam.mapper.domain.TblCardBillDetailExample;
import com.pam.mapper.domain.TblCardBillExample;
import com.pam.mapper.domain.TblCardBillMappingExample;
import com.pam.mapper.domain.TblCardBillSummaryExample;
import com.pam.mapper.domain.TblCardExample;
import com.pam.mapper.domain.TblCardLimit;
import com.pam.mapper.domain.TblCardLimitExample;
import com.pam.mapper.domain.TblCashReceiptExample;
import com.pam.mapper.domain.TblChallengeExample;
import com.pam.mapper.domain.TblCollectLogExample;
import com.pam.mapper.domain.TblDailyAssetExample;
import com.pam.mapper.domain.TblErrorLogExample;
import com.pam.mapper.domain.TblExpense;
import com.pam.mapper.domain.TblExpenseExample;
import com.pam.mapper.domain.TblFinaceCompanyRegistExample;
import com.pam.mapper.domain.TblMoneyCalendarExample;
import com.pam.mapper.domain.TblMonthlyAssetExample;
import com.pam.mapper.domain.TblNotificationExample;
import com.pam.mapper.domain.TblPropertyExample;
import com.pam.mapper.domain.TblRealEstateExample;
import com.pam.mapper.domain.TblStockTransactionExample;
import com.pam.mapper.domain.TblUser;
import com.pam.mapper.domain.TblUserExample;
import com.pam.security.PasswordHash;
import com.pam.util.ByteUtil;
import com.pam.util.SecureUtil;
import com.pam.util.Utils;


@Service
public class UserService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    TblUserMapper userMapper;
    
    @Autowired
    TblBankAccountMapper bankAccountMapper;
    
    @Autowired
    TblBankAccountBalanceMapper bankAccountBalanceMapper;
    
    @Autowired
    TblBankTransactionMapper bankTransactionMapper;
    
    @Autowired
    TblBudgetMapper budgetMapper;
    
    @Autowired
    TblBudgetListMapper budgetListMapper;
    
    @Autowired
    TblCarMapper carMapper;
    
    @Autowired
    TblErrorLogMapper errorLogMapper;
    
    @Autowired
    TblExpenseMapper expenseMapper;
    
    @Autowired
    TblCardMapper cardMapper;
    
    @Autowired
    TblCardApprovalMapper cardApprovalMapper;
    
    @Autowired
    TblCardBillMapper cardBillMapper;
    
    @Autowired
    TblCardBillSummaryMapper cardBillSummaryMapper;
    
    @Autowired
    TblCardBillMappingMapper cardBillMappingMapper;
    
    @Autowired
    TblCardBillDetailMapper cardBillDetailMapper;
    
    @Autowired
    TblCardLimitMapper cardLimitMapper;
    
    @Autowired
    TblCashReceiptMapper cashReceiptMapper;
    
    @Autowired
    TblChallengeMapper challengeMapper;
    
    @Autowired
    TblCollectLogMapper collectLogMapper;
    
    @Autowired
    TblDailyAssetMapper dailyAssetMapper;
    
    @Autowired
    TblFinaceCompanyRegistMapper finaceCompanyregistMapper;
    
    @Autowired
    TblMoneyCalendarMapper moneyCalendarMapper;
    
    @Autowired
    TblMonthlyAssetMapper monthlyAssetMapper;
    
    @Autowired
    TblNotificationMapper notificationMapper;
    
    @Autowired
    TblPropertyMapper propertyMapper;
    
    @Autowired
    TblRealEstateMapper realEstateMapper;
    
    @Autowired
    TblStockTransactionMapper stockTransactionMapper;
    
    /**
     * 사용자 등록
     * @param user
     * @return
     * @throws MySQLIntegrityConstraintViolationException
     * @throws RuntimeException
     */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String addUser(TblUser user) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblUser = {}", Utils.toJsonString(user));
		
		String userId = this.genUserId();
		String useYellopassYn = "N";
		user.setUserId(userId);
		user.setUseYellopassYn(useYellopassYn);
		
		userMapper.insertSelective(user);
		
		return userId;
	}	
	
	/**
     * 사용자 정보 요청
     * @param userId
     * @return
     * @throws MySQLIntegrityConstraintViolationException
     * @throws RuntimeException
     */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public UserInfo getUserInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", Utils.toJsonString(userId));

		TblUser tblUser = userMapper.selectByPrimaryKey(userId);
		
		Date now = tblUser.getRegDate();
		String key = ByteUtil.byteArrayToHex(SecureUtil.genSaltFromDate(now));
		String decryptedName = SecureUtil.decryptString(tblUser.getName(), key);
		UserInfo ret = new UserInfo();
		ret.setUserName(decryptedName);
		ret.setBirthDate(tblUser.getBirthDate());
		ret.setGender(tblUser.getGender());
		ret.setMarriedYn(tblUser.getMrrdYn());
		ret.setPushYn(tblUser.getPushYn());
		
		return ret;
	}	
	
	/**
     * 사용자 아이디 조회
     * @param yellopassId
     * @return
     * @throws MySQLIntegrityConstraintViolationException
     * @throws RuntimeException
     */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public UserId getUserId(String yellopassId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("yellopassId = {}", Utils.toJsonString(yellopassId));
		
		UserId ret = new UserId();
		
		TblUserExample example = new TblUserExample();
		example.createCriteria().andYellopassIdEqualTo(Long.parseLong(yellopassId));
		
		List<TblUser> list = userMapper.selectByExample(example);
		if(list != null && list.size() > 0) {
			TblUser user = list.get(0);
			ret.setUserId(user.getUserId());
		}
		
		return ret;
	}
	
	/**
	 * 푸쉬 사용여부 수정
	 * @param user
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Integer setPush(String userId, String pushYn) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("pushYn = {}", Utils.toJsonString(pushYn));
		
		TblUser user = new TblUser();
		user.setUserId(userId);
		user.setPushYn(pushYn);
		
		return userMapper.updateByPrimaryKeySelective(user);
	}

	/**
	 * 사용자 정보 수정
	 * @param user
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Integer setUser(TblUser user) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblUser = {}", Utils.toJsonString(user));
		
		Date now = new Date();
		user.setModDate(now);
		
		return userMapper.updateByPrimaryKeySelective(user);
	}	

	/**
	 * 비밀번호 수정
	 * @param user
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Integer setPassword(TblUser user) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblUser = {}", Utils.toJsonString(user));
		
		TblUser tmpUser = userMapper.selectByPrimaryKey(user.getUserId());
		Date regDate = tmpUser.getRegDate();
		String password = user.getPassword();
		
		String passHash = "";
		
		try {
			passHash = PasswordHash.createHash(password, regDate);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Date now = Calendar.getInstance(Locale.KOREA).getTime();
		user.setPassword(passHash);
		user.setModDate(now);
		
		return userMapper.updateByPrimaryKeySelective(user);
	}	

	/**
	 * 로그인
	 * @param user
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchAlgorithmException 
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public boolean authUser(TblUser user) throws MySQLIntegrityConstraintViolationException, RuntimeException, NoSuchAlgorithmException, InvalidKeySpecException {
		logger.info("TblUser = {}", Utils.toJsonString(user));
		
		boolean ret = false;
		
		String userId = user.getUserId();
		TblUser result = userMapper.selectByPrimaryKey(userId);
		
		if(result != null) {
			String password = user.getPassword();
			String pass = result.getPassword();
	
			Date regDate = result.getRegDate();
			boolean valid = PasswordHash.validatePassword(password, pass, regDate);
			
			// 사용자 이름 복호화 테스트
			String userName = result.getName();
			String key = ByteUtil.byteArrayToHex(SecureUtil.genSaltFromDate(regDate));
			String decryptedName = SecureUtil.decryptString(userName, key);
			logger.info("decryptedName={}", decryptedName);
			
			if(valid) {
				// os type, push key 등록
				Date now = new Date();
				result.setModDate(now);
				result.setOsType(user.getOsType());
				result.setPushKey(user.getPushKey());
				userMapper.updateByPrimaryKey(result);
				
				ret = true;
			} 
		}
		
		return ret;
	}	

	/**
	 * 업체 공인인증서 삭제
	 * @param userId
	 * @param companyType
	 * @param companyCode
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteCompanyData(String userId, String companyType, String companyCode) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}, type = {}, companyCode = {}", userId, companyType, companyCode);

		TblExpense expense = new TblExpense();
		expense.setUseYn("N");
		TblExpenseExample expenseExample = new TblExpenseExample();
		
		if(companyType.equals(Const.COMPANY_TYPE_BANK)) { // 은행 공인인증서 삭제 시 - tbl_bank_account, tbl_expense 삭제
			// 은행 계좌 삭제
			TblBankAccount bankAccount = new TblBankAccount();
			bankAccount.setUseYn("N");			
			TblBankAccountExample bankAccountExample = new TblBankAccountExample();
			bankAccountExample.createCriteria().andUserIdEqualTo(userId).andCmpnyCodeEqualTo(companyCode);
			bankAccountMapper.updateByExampleSelective(bankAccount, bankAccountExample);	

			// 소비 삭제
			expenseExample.createCriteria().andUserIdEqualTo(userId).andCmpnyCodeEqualTo(companyCode);
			expenseMapper.updateByExampleSelective(expense, expenseExample);
		} else if(companyType.equals(Const.COMPANY_TYPE_CARD)) { // 카드사 공인인증서 삭제 시 - tbl_card, tbl_card_bill, tbl_card_limit, tbl_expense 삭제
			// 카드 삭제
			TblCard card = new TblCard();
			card.setUseYn("N");
			TblCardExample cardExample = new TblCardExample();
			cardExample.createCriteria().andUserIdEqualTo(userId).andCmpnyCodeEqualTo(companyCode);
			cardMapper.updateByExampleSelective(card, cardExample);
			
			// 카드 청구서 삭제
			TblCardBill cardBill = new TblCardBill();
			cardBill.setUseYn("N");
			TblCardBillExample cardBillExample = new TblCardBillExample();
			cardBillExample.createCriteria().andUserIdEqualTo(userId).andCmpnyCodeEqualTo(companyCode);
			cardBillMapper.updateByExampleSelective(cardBill, cardBillExample);
			
			// 카드 한도 삭제
			TblCardLimit cardLimit = new TblCardLimit();
			cardLimit.setUseYn("N");
			TblCardLimitExample cardLimitExample = new TblCardLimitExample();
			cardLimitExample.createCriteria().andUserIdEqualTo(userId).andCmpnyCodeEqualTo(companyCode);
			cardLimitMapper.updateByExampleSelective(cardLimit, cardLimitExample);

			// 소비 삭제
			expenseExample.createCriteria().andUserIdEqualTo(userId).andCmpnyCodeEqualTo(companyCode);
			expenseMapper.updateByExampleSelective(expense, expenseExample);
		} else if(companyType.equals(Const.COMPANY_TYPE_GO)) { // 현금영수증 공인인증서 삭제 시 - tbl_expense		
			// 소비 삭제
			expenseExample.createCriteria().andUserIdEqualTo(userId).andCmpnyCodeEqualTo(Const.CASH_HT); // 현금 영수증은 국세청 고정
			expenseMapper.updateByExampleSelective(expense, expenseExample);	
		} else {
			logger.info("invalid companyType");
			return;
		}
	}	

	private String genUserId() {
		String ret = "";
		long time = System.currentTimeMillis() / 1000L;
		ByteBuffer buffer = ByteBuffer.allocate(8);
	    buffer.putLong(time);

        ret += SecureUtil.genRandom(28);
	    ret += ByteUtil.byteArrayToHex(buffer.array()).substring(8);
		
		return ret;		
	}
	
	/**
     * 이름으로 사용자 아이디 조회
     * @param userName
     * @return
     * @throws MySQLIntegrityConstraintViolationException
     * @throws RuntimeException
     */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public List<UserIdNameDate> getDecryptUser(String userName) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userName = {}", Utils.toJsonString(userName));
		
		List<UserIdNameDate> ret = new ArrayList<UserIdNameDate>();
		
		TblUserExample example = new TblUserExample();
		example.createCriteria();
		example.setOrderByClause("reg_date desc");
		List<TblUser> list = userMapper.selectByExample(example);
		
		for(TblUser user : list){
			Date now = user.getRegDate();
			String key = ByteUtil.byteArrayToHex(SecureUtil.genSaltFromDate(now));
			String name = user.getName();
			
			if(name.length() == 24 && key.length() == 16) {
				String decryptedName = SecureUtil.decryptString(name, key);
				if(decryptedName.equals(userName)){
					UserIdNameDate u = new UserIdNameDate();
					u.setUserId(user.getUserId());
					u.setUserName(decryptedName);
					u.setRegDate(Utils.getDateString(user.getRegDate()));
					ret.add(u);
				}
			}
		}
		
		return ret;
	}
	
	/**
	 * 암호화 키를 만든다.
	 * @param userId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 * @throws InvalidKeySpecException 
	 * @throws NoSuchAlgorithmException 
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public String getSecretKey(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException, NoSuchAlgorithmException, InvalidKeySpecException {
		logger.info("userId = {}", Utils.toJsonString(userId));
		String ret = "";
		
		TblUser user = userMapper.selectByPrimaryKey(userId);
		if(user != null) {
			Date regDate = user.getRegDate();			
			ret = PasswordHash.createHash(userId, regDate);			
		} 
		
		return ret;
	}
	
	/**
     * 회원 탈퇴 (사용자 삭제)
     * @param userId
     * @return
     * @throws MySQLIntegrityConstraintViolationException
     * @throws RuntimeException
     */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteUser(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", Utils.toJsonString(userId));
		
		TblUser user = userMapper.selectByPrimaryKey(userId);
		if (user != null){
			// 은행 계좌, 계좌 잔액, 거래내역 삭제
			TblBankAccountExample baExample = new TblBankAccountExample();
			baExample.createCriteria().andUserIdEqualTo(userId);
			List<TblBankAccount> baList = bankAccountMapper.selectByExample(baExample);
			for(TblBankAccount ba:baList){
				TblBankTransactionExample btExample = new TblBankTransactionExample();
				btExample.createCriteria().andAccntIdEqualTo(ba.getAccntId());
				bankTransactionMapper.deleteByExample(btExample);
				
				TblBankAccountBalanceExample babExample = new TblBankAccountBalanceExample();
				babExample.createCriteria().andAccntIdEqualTo(ba.getAccntId());
				bankAccountBalanceMapper.deleteByExample(babExample);
			}
			bankAccountMapper.deleteByExample(baExample);
			
			// 예산, 예산 내역 삭제
			TblBudgetExample bgExample = new TblBudgetExample();
			bgExample.createCriteria().andUserIdEqualTo(userId);
			List<TblBudget> bgList = budgetMapper.selectByExample(bgExample);
			for(TblBudget bg:bgList){
				TblBudgetListExample bglExample = new TblBudgetListExample();
				bglExample.createCriteria().andBdgtIdEqualTo(bg.getBdgtId());
				budgetListMapper.deleteByExample(bglExample);
			}
			budgetMapper.deleteByExample(bgExample);
			
			// 차 보유 내역 삭제
			TblCarExample carExample = new TblCarExample();
			carExample.createCriteria().andUserIdEqualTo(userId);
			carMapper.deleteByExample(carExample);

			// 카드, 카드 승인 내역, 카드 청구서, 카드 청구서 내역, 카드 청구서 매핑, 카드 청구셔 요약 삭제
			TblCardExample cardExample = new TblCardExample();
			cardExample.createCriteria().andUserIdEqualTo(userId);
			List<TblCard> cardList = cardMapper.selectByExample(cardExample);
			
			TblCardBillExample cardBillExample = new TblCardBillExample();
			cardBillExample.createCriteria().andUserIdEqualTo(userId);
			List<TblCardBill> cardBillList = cardBillMapper.selectByExample(cardBillExample);
			for(TblCard card:cardList){
				for(TblCardBill cardBill:cardBillList){
					TblCardBillMappingExample cardBillMappingExample = new TblCardBillMappingExample();
					cardBillMappingExample.createCriteria().andCardIdEqualTo(card.getCardId()).andBillIdEqualTo(cardBill.getBillId());
					cardBillMappingMapper.deleteByExample(cardBillMappingExample);
					
					TblCardBillDetailExample cardBillDetailExample = new TblCardBillDetailExample();
					cardBillDetailExample.createCriteria().andCardIdEqualTo(card.getCardId()).andBillIdEqualTo(cardBill.getBillId());
					cardBillDetailMapper.deleteByExample(cardBillDetailExample);
					
					TblCardBillSummaryExample cardBillSummaryExample = new TblCardBillSummaryExample();
					cardBillSummaryExample.createCriteria().andBillIdEqualTo(cardBill.getBillId());
					cardBillSummaryMapper.deleteByExample(cardBillSummaryExample);
				}
				TblCardApprovalExample caExample = new TblCardApprovalExample();
				caExample.createCriteria().andCardIdEqualTo(card.getCardId());
				cardApprovalMapper.deleteByExample(caExample);
			}
			cardMapper.deleteByExample(cardExample);
			cardBillMapper.deleteByExample(cardBillExample);
			
			// 카드 한도 삭제
			TblCardLimitExample cardLimitExample = new TblCardLimitExample();
			cardLimitExample.createCriteria().andUserIdEqualTo(userId);
			cardLimitMapper.deleteByExample(cardLimitExample);
			
			// 현금 영수증 삭제
			TblCashReceiptExample crExample = new TblCashReceiptExample();
			crExample.createCriteria().andUserIdEqualTo(userId);
			cashReceiptMapper.deleteByExample(crExample);
			
			// 챌린지 삭제
			TblChallengeExample challengeExample = new TblChallengeExample();
			challengeExample.createCriteria().andUserIdEqualTo(userId);
			challengeMapper.deleteByExample(challengeExample);
			
			// 수집 로그 삭제
			TblCollectLogExample clExample = new TblCollectLogExample();
			clExample.createCriteria().andUserIdEqualTo(userId);
			collectLogMapper.deleteByExample(clExample);
			
			// 일별 개인 자산 삭제  -----필요?
			TblDailyAssetExample daExample = new TblDailyAssetExample();
			daExample.createCriteria().andUserIdEqualTo(userId);
			dailyAssetMapper.deleteByExample(daExample);
			
			// 에러 로그 삭제
			TblErrorLogExample elExample = new TblErrorLogExample();
			elExample.createCriteria().andUserIdEqualTo(userId);
			errorLogMapper.deleteByExample(elExample);
			
			// 소비 삭제
			TblExpenseExample eExample = new TblExpenseExample();
			eExample.createCriteria().andUserIdEqualTo(userId);
			expenseMapper.deleteByExample(eExample);
			
			// 사용자 금융기관 등록 삭제 -----필요?
			TblFinaceCompanyRegistExample fcrExample = new TblFinaceCompanyRegistExample();
			fcrExample.createCriteria().andUserIdEqualTo(userId);
			finaceCompanyregistMapper.deleteByExample(fcrExample);
			
			// 머니캘린더 삭제
			TblMoneyCalendarExample mcExample = new TblMoneyCalendarExample();
			mcExample.createCriteria().andUserIdEqualTo(userId);
			moneyCalendarMapper.deleteByExample(mcExample);
			
			// 월별 개인 자산 삭제 ------필요?
			TblMonthlyAssetExample maExample = new TblMonthlyAssetExample();
			maExample.createCriteria().andUserIdEqualTo(userId);
			monthlyAssetMapper.deleteByExample(maExample);
			
			// 알림 삭제
			TblNotificationExample nExample = new TblNotificationExample();
			nExample.createCriteria().andUserIdEqualTo(userId);
			notificationMapper.deleteByExample(nExample);
			
			// 재산 삭제
			TblPropertyExample pExample = new TblPropertyExample();
			pExample.createCriteria().andUserIdEqualTo(userId);
			propertyMapper.deleteByExample(pExample);
			
			// 부동산 삭제
			TblRealEstateExample reExample = new TblRealEstateExample();
			reExample.createCriteria().andUserIdEqualTo(userId);
			realEstateMapper.deleteByExample(reExample);
			
			// 개인 주식 거래 내역 삭제
			TblStockTransactionExample stExample = new TblStockTransactionExample();
			stExample.createCriteria().andUserIdEqualTo(userId);
			stockTransactionMapper.deleteByExample(stExample);
			
			// 사용자 삭제
			userMapper.deleteByPrimaryKey(userId);
		}
		return;
	}
}
