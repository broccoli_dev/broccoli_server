package com.pam.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.constant.Const;
import com.pam.entity.CardIdBean;
import com.pam.entity.MoneyCalendarTag;
import com.pam.manager.scraping.ScrapingUtil;
import com.pam.mapper.TblMoneyCalendarMapper;
import com.pam.mapper.domain.TblBankAccount;
import com.pam.mapper.domain.TblBankAccountBalance;
import com.pam.mapper.domain.TblBankTransaction;
import com.pam.mapper.domain.TblCard;
import com.pam.mapper.domain.TblCardApproval;
import com.pam.mapper.domain.TblCardBill;
import com.pam.mapper.domain.TblCardBillDetail;
import com.pam.mapper.domain.TblCardBillMapping;
import com.pam.mapper.domain.TblCardBillSummary;
import com.pam.mapper.domain.TblCardLimit;
import com.pam.mapper.domain.TblCashReceipt;
import com.pam.mapper.domain.TblCollectLog;
import com.pam.mapper.domain.TblExpense;
import com.pam.mapper.domain.TblMoneyCalendar;
import com.pam.mapper.domain.TblMoneyCalendarExample;
import com.pam.queue.MQService;
import com.pam.util.Utils;

public class MQServiceImpl implements MQService 
{
    private final Logger logger = LoggerFactory.getLogger(getClass());
	@Autowired
	private JmsTemplate jmsTemplate;

    @Autowired
    CollectService collectService;
    
    @Autowired
    TblMoneyCalendarMapper moneyCalendarMapper;
	
	/* 
	 * 메시지 수신
	 */
	public void receive(Map<String, ?> data) {
		logger.info("receive data={}", Utils.toJsonString(data));
		
		TblCollectLog log = new TblCollectLog();
		Integer type = (Integer) data.get("TYPE");
		String userId = (String) data.get("USERID");
		
		if(type != null) {
			if(type == Const.COLLECT_TYPE_BANK_ACCOUNT) {// 은행 계좌 리스트
				log = this.bankAccountProccess(data);
			} else if(type == Const.COLLECT_TYPE_BANK_TRANSACTION) { // 은행 거래 내역
				log = this.bankTransProcess(data);
			} else if(type == Const.COLLECT_TYPE_CARD_APPROVAL) { // 카드 승인 내역
				log = this.cardApprovalProccess(data);
			} else if(type == Const.COLLECT_TYPE_CARD_BILL) { // 카드 청구서
				log = this.cardReceiptProccess(data);
			} else if(type == Const.COLLECT_TYPE_CARD_LIMIT) { // 카드 한도
				log = this.cardLimitProccess(data);
			} else if(type == Const.COLLECT_TYPE_CASH_RECEIPT) { // 현금 영수증
				log = this.cashProccess(data);
			} else if(type == Const.COLLECT_TYPE_CARD_SCHEDULE) { // 카드 결제 예정
				log = this.cardScheduleAmtProccess(data);
			} else {
				;
			}
		} else { // 기타 미분류
			Date now = Calendar.getInstance(Locale.KOREA).getTime();
			log.setUserId(userId);
			log.setCllctType(0);
			log.setResCode("603");
			log.setResDesc("Some essential parameters are empty.(TYPE)");
			log.setCllctType(0);
			log.setRegDate(now);
			log.setModDate(now);
		}
		
		try {
			collectService.addLog(log);
		} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
			// TODO Auto-generated catch block
			logger.info("error = {}", e.toString());
		}
	}

	/* 
	 * 메시지 송신
	 */
	public void send(Map<String, ?> data) {
		try {
			logger.info("send data={}", Utils.toJsonString(data));
		    
		    jmsTemplate.convertAndSend(data);
		} catch (Exception e) {
			logger.info("error = {}", e.toString());
			logger.error("send()", e);
		}
	}
	
	/**
	 * 은행 계좌 등록
	 * @param data
	 */
	private TblCollectLog bankAccountProccess(Map<String, ?> data) {	
		logger.info("data={}", Utils.toJsonString(data));	
		
		String userId = (String) data.get("USERID");
		Integer type = (Integer) data.get("TYPE");
		String cmpnyCode = (String) data.get("CMPNYCODE");
		String accntHostName = (String) data.get("ACCTNM");
		String uploadId = (String) data.get("UPLOADID");

		// 로그 등록
		TblCollectLog ret = new TblCollectLog();	
		Date now = Calendar.getInstance(Locale.KOREA).getTime();
		ret.setUserId(userId);
		ret.setCmpnyCode(cmpnyCode);
		ret.setCllctType(type);
		ret.setUploadId(uploadId);
		ret.setRegDate(now);
		ret.setModDate(now);

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(cmpnyCode) || Utils.isStringEmpty(uploadId)) {
			ret.setResCode("603");
			ret.setResDesc("Some essential parameters are empty.(USERID|CMPNYCODE|UPLOADID)");
		} else {			
			// companyCode가 은행사 리스트에 포함되는 지 확인. 아니면 무시한다.
			if(!ScrapingUtil.checkCompanyTypeCodes(Const.COMPANY_TYPE_BANK, cmpnyCode)) {
				ret.setResCode("601");
				ret.setResDesc("Not bank type company code.");		
				
				return ret;
			}
			
			@SuppressWarnings("unchecked")
			ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>>) data.get("LIST");
			List<String> accntNumList = new ArrayList<String>();
			List<TblBankAccount> bankAccountList = new ArrayList<TblBankAccount>();
			
			if(list != null && list.size() > 0) {					
				for(HashMap<String, String> map : list) {
					String accntNum = map.get("NUMBER");
					String accntKind = map.get("ACCTKIND");	
										
					// 필수 요소 확인
					if(Utils.isStringEmpty(accntNum) || Utils.isStringEmpty(accntKind)) {
						logger.info("Some essential parameters are empty.(NUMBER|ACCTKIND");
						logger.info("map = {}", Utils.toJsonString(map));
						continue;
					} else {
						String acctType = map.get("ACCTTYPE");
						String smallCtgryCode = ScrapingUtil.getSmallCtgryCodeFromAcctKindAndType(accntKind, acctType);
												
						String curbal = map.get("CURBAL");
						Long curBlnc = ScrapingUtil.getLongValueFromString(curbal);
						String enbbal = map.get("ENBBAL");
						Long pssblBlnc = ScrapingUtil.getLongValueFromString(enbbal);
						String openDate = map.get("OPENDATE");
						String closeDate = map.get("CLOSEDATE");
//						String accntName = map.get("ACCTNM");
						String curCd = map.get("CURRCD");

						// 예금/적금인데 잔고가 마이너스이면 대출로 분류하고 금액을 플러스로 바꾼다.
						if((smallCtgryCode.equals(Const.ACCT_TYPE_DEPOSIT) || smallCtgryCode.equals(Const.ACCT_TYPE_ISTMNT_SAVINGS)) && curBlnc < 0L) {
							smallCtgryCode = Const.ACCT_TYPE_LOAN;
							curBlnc = -1 * curBlnc;
						}

						TblBankAccount bankAccount = new TblBankAccount();
						bankAccount.setUserId(userId);
						bankAccount.setAccntNum(accntNum);
						bankAccount.setCmpnyCode(cmpnyCode);
						bankAccount.setAccntName(accntKind); // accntKind에 계좌 이름이 들어있는 경우가 많다.
						bankAccount.setAccntHostName(accntHostName);
						bankAccount.setSmallCtgryCode(smallCtgryCode);
						bankAccount.setCurBlnc(curBlnc);
						bankAccount.setPssblBlnc(pssblBlnc);
						bankAccount.setOpenDate(openDate);
						bankAccount.setCloseDate(closeDate);
						bankAccount.setUseYn("Y");
						bankAccount.setCurCd(curCd);
						bankAccount.setModDate(now);
						
						accntNumList.add(accntNum);
						bankAccountList.add(bankAccount);
					}
				}
				
				try {
					// 기존 계좌가 이번에 없으면 삭제한다.
					collectService.checkAccountList(userId, cmpnyCode, accntNumList);
					// 은행 계좌 정보를 등록한다.
					List<TblBankAccountBalance> bankAccountBalanceList = collectService.addBankAccounts(bankAccountList);
					// 은행 계좌 잔액을 등록한다.
					collectService.addBankAccountBalance(bankAccountBalanceList);

					ret.setResCode("100");
					ret.setResDesc("ok");
				} catch(MySQLIntegrityConstraintViolationException | RuntimeException e) {
					logger.info("error = {}", e.toString());
					ret.setResCode("501");
					ret.setResDesc("DB access error");		
				}
			} else {
				ret.setResCode("603");
				ret.setResDesc("No LISTs.");
			}
		}		
		
		return ret;
	}
	
	/**
	 * 은행 거래 내역 등록
	 * @param data
	 */
	private TblCollectLog bankTransProcess(Map<String, ?> data) {
		logger.info("data={}", Utils.toJsonString(data));	
		
		String userId = (String) data.get("USERID");
		Integer type = (Integer) data.get("TYPE");
		String cmpnyCode = (String) data.get("CMPNYCODE");
		String accntNum = (String) data.get("NUMBER");
		String uploadId = (String) data.get("UPLOADID");

		// 로그 등록
		TblCollectLog ret = new TblCollectLog();	
		Date now = Calendar.getInstance(Locale.KOREA).getTime();
		ret.setUserId(userId);
		ret.setCmpnyCode(cmpnyCode);
		ret.setCllctType(type);
		ret.setUploadId(uploadId);
		ret.setRegDate(now);
		ret.setModDate(now);

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(cmpnyCode) || Utils.isStringEmpty(uploadId) || Utils.isStringEmpty(accntNum)) {
			ret.setResCode("603");
			ret.setResDesc("Some essential parameters are empty.(USERID|CMPNYCODE|NUMBER|UPLOADID)");
			logger.info(Utils.toJsonString(data));
		} else {	
			// companyCode가 은행사 리스트에 포함되는 지 확인. 아니면 무시한다.
			if(!ScrapingUtil.checkCompanyTypeCodes(Const.COMPANY_TYPE_BANK, cmpnyCode)) {
				ret.setResCode("601");
				ret.setResDesc("Not bank type company code.");		
				
				return ret;
			}
			
			@SuppressWarnings("unchecked")
			ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>>) data.get("LIST");
			
			// 계좌 아이디를 찾는다.
			Long accntId = collectService.getAccntId(userId, cmpnyCode, accntNum);
			//계좌 아이디 등록
			ret.setAccdId(accntId);

			// 거래내역 리스트
			List<TblBankTransaction> bankTransList = new ArrayList<TblBankTransaction>();
			// 소비 리스트
			List<TblExpense> expenseList = new ArrayList<TblExpense>();
			// 머니 캘린더 리스트
			List<TblMoneyCalendar> moneyCalendarList = new ArrayList<TblMoneyCalendar>();
			
			if(accntId != null) {			
				if(list != null && list.size() > 0) {
					for(HashMap<String, String> map : list) {
						String tranDate = map.get("TRANDATE");
	//					String tranGb = map.get("TRANGB");
						String outBal = map.get("OUTBAL");
						String inBal = map.get("INBAL");
						String tranBal = map.get("TRANABAL");
						String jukyo = map.get("JUKYO");
						String tranDes = map.get("TRANDES");
						String tranDep = map.get("TRANDEP");
						String tranDt = map.get("TRANDT");
						
						// 필수 요소 확인
						if(/*Utils.isStringEmpty(jukyo) || */Utils.isStringEmpty(tranDes)) { // 거래 상대방 필수 요소에서 제외
							logger.info("Some essential parameters are empty.(JUKYO|TRANDES)");
							logger.info("map = {}", Utils.toJsonString(map));
							continue;
						} else {
							String transDate = ScrapingUtil.getAprvlDate(tranDate, tranDt);
							Long sum = ScrapingUtil.getSumFromInOutBal(inBal, outBal);
														
							Integer transType = (sum > 0) ? Const.BANK_TRANS_TYPE_IN : Const.BANK_TRANS_TYPE_OUT;
							Long afterValue = ScrapingUtil.getLongValueFromString(tranBal);
							String oppnnt = jukyo;
							oppnnt = ScrapingUtil.getNoSpaceString(oppnnt);
							//String ctgryCode = Const.CATEGORY_CODE_NOT_DEFINED;
							String ctgryCode = collectService.getCategoryCode(true, "", "", oppnnt);
							String transMethod = tranDes;
							String dstrb = tranDep;
							String useYn = "Y";
	
							// 은행 거래 내역을 리스트에 넣는다.
							TblBankTransaction bankTransaction = new TblBankTransaction();
							bankTransaction.setAccntId(accntId);
							bankTransaction.setTransDate(transDate);
							bankTransaction.setCtgryCode(ctgryCode);
							bankTransaction.setSum(sum);
							bankTransaction.setTransType(transType);
							bankTransaction.setAfterValue(afterValue);
							bankTransaction.setOppnnt(oppnnt);
							bankTransaction.setTransMethod(transMethod);
							bankTransaction.setDstrb(dstrb);
							bankTransList.add(bankTransaction);
							
							// 소비, 머니캘린더 플래그 계산
							Integer moneyCalendarFlag = 0;
							Integer notExpenseFlag = 0;
							Integer expectFlag = 0;
							Integer isExpenseFlag = 0;
							MoneyCalendarTag moneyCalendarTag = collectService.getMoneyCalendarTag(oppnnt);
							if (moneyCalendarTag != null) {
								String isMoneyCalendar = moneyCalendarTag.getIsMoneyCalendar();
								if (isMoneyCalendar != null){
									moneyCalendarFlag = Integer.valueOf(isMoneyCalendar)&1;	// x & 0001 : 첫번째 비트가 1이면 머니캘린더
									notExpenseFlag = Integer.valueOf(isMoneyCalendar)&2;	// x & 0010 : 두번째 비트가 1이면 소비제외
									expectFlag = Integer.valueOf(isMoneyCalendar)&4;		// x & 0100 : 세번째 비트가 1이면 확정머니캘린더
									isExpenseFlag = Integer.valueOf(isMoneyCalendar)&8;		// x & 1000 : 네번째 비트가 1이면 소비 항목 포함
								}
							}
							logger.info("mcFlag={}", moneyCalendarFlag);
							logger.info("neFlag={}", notExpenseFlag);
							logger.info("epFlag={}", expectFlag);
							logger.info("ieFlag={}", isExpenseFlag);
							
							// 소비 리스트에 넣는다.
							if (notExpenseFlag == 0 && transType == Const.BANK_TRANS_TYPE_OUT){
								boolean isExpense = false;
								String payType = ScrapingUtil.getPayTypeFromTransDes(transMethod);
								if(!payType.equals(Const.PAY_TYPE_ETC)) { // 소비수단이 기타(은행인데 체크카드 아닌 경우)가 아닌 경우 소비에 넣는다.
									isExpense = true;
								} else if(moneyCalendarFlag > 0) { // 기타지만 머니캘린더에 등록될 내역은 소비에 넣는다.
									isExpense = true;
								} else if(isExpenseFlag > 0){ // 기타지만 소비 항목으로 지정된 내역은 소비에 넣는다. 
									isExpense = true;
								}
								
								TblExpense expense = new TblExpense();
								expense.setUserId(userId);
								expense.setExpnsDate(transDate.substring(0, 12)); // 소비 시간은 yyyyMMddHHmm(분)까지 표시
								expense.setOppnnt(oppnnt);
								expense.setCtgryCode(ctgryCode);
								expense.setPayType(payType);
								expense.setEditYn("N");
								expense.setCancelYn("N");
								expense.setCmpnyCode(cmpnyCode);
								expense.setUseYn(useYn);
								
								if(sum < 0) {
									sum = -sum;
								}
								expense.setSum(sum);
								
								if(isExpense) {
									expenseList.add(expense);
								}
							}
							
							// 머니캘린더 리스트에 넣는다.
							if (moneyCalendarFlag > 0 && transType == Const.BANK_TRANS_TYPE_OUT){
								String strPayDate = transDate.substring(0, 8);
								String payDay = transDate.substring(6, 8);
								
								TblMoneyCalendarExample example = new TblMoneyCalendarExample();
								example.createCriteria().andUserIdEqualTo(userId).andPayDayEqualTo(payDay).andMtchdTagEqualTo(moneyCalendarTag.getTag());
								example.setOrderByClause("pay_date ASC");
								List<TblMoneyCalendar> moneyCalList = moneyCalendarMapper.selectByExample(example);
								if ((moneyCalList == null) || (moneyCalList.size() == 0)){
									// 머니캘린더 최초 등록시									
									for(int i = 0;i<120;i++){
										Date conPayDate = Utils.convertStringToDate(strPayDate, "2");
										String payDate = Utils.getPrevNextDateString("1", i, "yyyymmdd", conPayDate);
										TblMoneyCalendar m = new TblMoneyCalendar();
										
										m.setUserId(userId);
										m.setTitle(oppnnt);
										if(sum < 0) {
											sum = -sum;
										}
										m.setAmt(sum);
										m.setPayType(Const.MONEY_CALENDAR_PAY_TYPE_AUTO); // 매칭 등록은 자동 납부로 설정한다.
										m.setPayDate(payDate);
										m.setPayDay(payDay);
										m.setRptType(Const.MONEY_CALENDAR_REPEAT_1M); // default to 1M
										m.setUseYn("Y"); // default to y
										m.setExptYn((expectFlag == 0) ? "N":"Y");
										m.setMtchdTag(moneyCalendarTag.getTag());
										m.setOppnnt(oppnnt);
										moneyCalendarList.add(m);
									}
								}
								else{
									//이미 머니캘린더가 등록되어 있을 경우 가격 업데이트
									boolean foundDate = false; 
									for(TblMoneyCalendar m : moneyCalList){
										if(m.getPayDate().equals(strPayDate)){
											foundDate = true;
										}
										if (foundDate){
											if(sum < 0) {
												sum = -sum;
											}
											m.setAmt(sum);
											moneyCalendarList.add(m);
										}
									}
								}
							}
						}
					}
					
					try {
						// 은행 거래 내역을 저장한다.
						collectService.addBankTransactionInOneSql(bankTransList);
						// 소비 내역을 저장한다.
						collectService.addExpenseInOneSql(expenseList);
						// 재산내역에 저장한다.
						collectService.addBankProperty(userId, accntId, bankTransList);
						// 머니캘린더에 저장한다.
						collectService.addMoneyCal(moneyCalendarList);

						ret.setResCode("100");
						ret.setResDesc("ok");
					} catch(MySQLIntegrityConstraintViolationException | RuntimeException e) {
						logger.info("error = {}", e.toString());
						ret.setResCode("501");
						ret.setResDesc("DB access error");		
					}
				} else {
					ret.setResCode("603");
					ret.setResDesc("No LISTs.");
				}
			} else {
				ret.setResCode("601");
				ret.setResDesc("Unregistered account number.");
			}
		}
		
		return ret;		
	}

	/**
	 * 카드 승인 내역 등록
	 * @param data
	 */
	private TblCollectLog cardApprovalProccess(Map<String, ?> data) {	
		logger.info("data={}", Utils.toJsonString(data));	
		
		String userId = (String) data.get("USERID");
		Integer type = (Integer) data.get("TYPE");
		String cmpnyCode = (String) data.get("CMPNYCODE");
		String uploadId = (String) data.get("UPLOADID");

		// 로그 등록
		TblCollectLog ret = new TblCollectLog();	
		Date now = Calendar.getInstance(Locale.KOREA).getTime();
		ret.setUserId(userId);
		ret.setCmpnyCode(cmpnyCode);
		ret.setCllctType(type);
		ret.setUploadId(uploadId);
		ret.setRegDate(now);
		ret.setModDate(now);

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(cmpnyCode) || Utils.isStringEmpty(uploadId)) {
			ret.setResCode("603");
			ret.setResDesc("Some essential parameters are empty.(USERID|CMPNYCODE|UPLOADID)");
		} else {	
			// companyCode가 카드사 리스트에 포함되는 지 확인. 아니면 무시한다.
			if(!ScrapingUtil.checkCompanyTypeCodes(Const.COMPANY_TYPE_CARD, cmpnyCode)) {
				ret.setResCode("601");
				ret.setResDesc("Not card type company code.");		
				
				return ret;
			}
			
			@SuppressWarnings("unchecked")
			ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>>) data.get("LIST");

			// 카드 승인 내역 리스트
			List<TblCardApproval> cardApprovalList = new ArrayList<TblCardApproval>();
			HashMap<CardIdBean, Long> cardMap = new HashMap<CardIdBean, Long>();
			// 소비 리스트
			List<TblExpense> expenseList = new ArrayList<TblExpense>();
									
			if(list != null && list.size() > 0) {				
				for(HashMap<String, String> map : list) {		
					String useDis = map.get("USEDIS");
					String appNickName = map.get("APPNICKNAME");
					String appDate = map.get("APPDATE");
					String appTime = map.get("APPTIME");
					String appNo = map.get("APPNO");
					String appGubun = map.get("APPGUBUN");
					String appQuota = map.get("APPQUOTA");
					String appAmt = map.get("APPAMT");
					String appFranName = map.get("APPFRANNAME");
					String appTonghwa = map.get("APPTONGHWA");
					String appFranRegNum = map.get("APPFRANREGNUM");
//					String appFranType = map.get("APPFRANTYPE");
					String appFranTel = map.get("APPFRANTEL");
					String appFranAddr = map.get("APPFRANADDR");
					String appFranSajang = map.get("APPFRANSAJANG");
					
					// 필수 요소 확인
					if(Utils.isStringEmpty(appDate) || Utils.isStringEmpty(appNo) || Utils.isStringEmpty(appGubun) || Utils.isStringEmpty(appAmt)) {
						logger.info("Some essential parameters are empty.(APPDATE|APPNO|APPGUBUN|APPAMT");
						logger.info("map = {}", Utils.toJsonString(map));
						continue;
					} else {		
						String cardNum = appNickName;
						String nickName = appNickName;
						String billYn = "N";
						String useYn = "Y";
						
						// 카드 정보
						TblCard card = new TblCard();
						card.setUserId(userId);
						card.setCmpnyCode(cmpnyCode);
						card.setCardNum(cardNum);
						card.setNickName(nickName);
						card.setBillYn(billYn); // 승인 내역 카드 임
						card.setUseYn(useYn); // 사용하는 카드 임
						card.setRegDate(now);
						card.setModDate(now);
						
						Long cardId = null;
						CardIdBean bean = new CardIdBean();
						bean.setUserId(userId);
						bean.setCardNum(cardNum);
						bean.setCmpnyCode(cmpnyCode);
						bean.setBillYn(billYn);
						bean.setUseYn(useYn);
						
						// 카드 등록
						if(cardMap.containsKey(bean)) {
							cardId = cardMap.get(bean);
						} else {
							try {
								collectService.addCard(card);
								cardId = collectService.getCardId(card);
								cardMap.put(bean, cardId);
							} catch (MySQLIntegrityConstraintViolationException
									| RuntimeException e) {
								logger.info("error = {}", e.toString());
								ret.setResCode("501");
								ret.setResDesc("DB access error");	
								return ret;
							}
						}
						
						// 카드 아이디 등록
						ret.setAccdId(cardId);
						
						String istmYn = ScrapingUtil.getIstmYnFromUseDis(useDis);
						String aprvlDate = ScrapingUtil.getAprvlDate(appDate, appTime);
						String aprvlYn = ScrapingUtil.getAprvlYnFromAppGubun(appGubun);
						Integer istmMon = ScrapingUtil.getIstmMon(appQuota);
						Long aprvlAmt = ScrapingUtil.getLongValueFromString(appAmt);
						
						if(aprvlAmt == 0L) { // 금액이 0이면 무시한다.
							logger.info("Ignores this approval because amt equals 0.");
							continue;
						}
						
						String storeName = appFranName;
						storeName = ScrapingUtil.getNoSpaceString(storeName);
						String crncyType = appTonghwa;
						String storeNum = appFranRegNum;
						String storeTel = appFranTel;
						String storeAddr = appFranAddr;
						String ctgryCode = collectService.getCategoryCode(true, storeNum, storeTel, storeName);
						String storeHostName = appFranSajang;							
						
						// 거래 내역 등록
						TblCardApproval cardApproval = new TblCardApproval();
						cardApproval.setCardId(cardId);
						cardApproval.setIstmYn(istmYn);
						cardApproval.setAprvlDate(aprvlDate);
						cardApproval.setAprvlNum(appNo);
						cardApproval.setAprvlYn(aprvlYn);
						cardApproval.setIstmMon(istmMon);
						cardApproval.setAprvlAmt(aprvlAmt);
						cardApproval.setStoreName(storeName);
						cardApproval.setCrncyType(crncyType);
						cardApproval.setStoreNum(storeNum);
						cardApproval.setCtgryCode(Const.CATEGORY_CODE_NOT_DEFINED);
						cardApproval.setStoreTel(storeTel);
						cardApproval.setStoreAddr(storeAddr);
						cardApproval.setStoreHostName(storeHostName);
						cardApprovalList.add(cardApproval);

						// 소비 리스트에 넣는다.
						TblExpense expense = new TblExpense();
						expense.setUserId(userId);
						expense.setExpnsDate(aprvlDate.substring(0, 12)); // 소비 시간은 yyyyMMddHHmm(분)까지 표시
						expense.setOppnnt(storeName);
						// 취소의 경우 금액을 마이너스로 만들어서 넣는다. 소비 표시다 마이너스로 하고 사용액 계산 할 때에도 마이너스를 더하게 된다.
						if(aprvlYn.equals("0")) {
							aprvlAmt = -aprvlAmt;
						}
						expense.setSum(aprvlAmt);
						expense.setCtgryCode(ctgryCode);
						expense.setPayType(Const.PAY_TYPE_CARD); // 신용카드
						expense.setEditYn("N");
						expense.setCancelYn("N");
						expense.setCmpnyCode(cmpnyCode);
						expense.setUseYn(useYn);
						// 일단 전체를 넣는다.
						expenseList.add(expense);
					}
				}

				try {					
					// 카드 승인 내역을 저장한다.
					collectService.addCardApprovalInOneSql(cardApprovalList);
					// 소비 내역을 저장한다.
					collectService.addExpenseInOneSql(expenseList);

					ret.setResCode("100");
					ret.setResDesc("ok");
				} catch(MySQLIntegrityConstraintViolationException | RuntimeException e) {
					logger.info("error = {}", e.toString());
					ret.setResCode("501");
					ret.setResDesc("DB access error");		
				}
			} else {
				ret.setResCode("603");
				ret.setResDesc("No LISTs.");			
			}
		}
		
		return ret;		
	}
	
	/**
	 * 카드 청구 내역 등록
	 * @param data
	 */
	private TblCollectLog cardReceiptProccess(Map<String, ?> data) {
		logger.info("data={}", Utils.toJsonString(data));	
		
		String userId = (String) data.get("USERID");
		Integer type = (Integer) data.get("TYPE");
		String cmpnyCode = (String) data.get("CMPNYCODE");
		String uploadId = (String) data.get("UPLOADID");

		// 로그 등록
		TblCollectLog ret = new TblCollectLog();	
		Date now = Calendar.getInstance(Locale.KOREA).getTime();
		ret.setUserId(userId);
		ret.setCmpnyCode(cmpnyCode);
		ret.setCllctType(type);
		ret.setUploadId(uploadId);
		ret.setRegDate(now);
		ret.setModDate(now);

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(cmpnyCode) || Utils.isStringEmpty(uploadId)) {
			ret.setResCode("603");
			ret.setResDesc("Some essential parameters are empty.(USERID|CMPNYCODE|UPLOADID)");
		} else {	
			// companyCode가 카드사 리스트에 포함되는 지 확인. 아니면 무시한다.
			if(!ScrapingUtil.checkCompanyTypeCodes(Const.COMPANY_TYPE_CARD, cmpnyCode)) {
				ret.setResCode("601");
				ret.setResDesc("Not card type company code.");		
				
				return ret;
			}
			
			@SuppressWarnings("unchecked")
			ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>>) data.get("LIST");
			// 카드 청구내역 리스트
			List<TblCardBillDetail> cardBillDetaillList = new ArrayList<TblCardBillDetail>();
			// 카드 청구서 요약 리스트
			List<TblCardBillSummary> cardBillSummaryList = new ArrayList<TblCardBillSummary>();
			// 카드 ID 맵
			HashMap<CardIdBean, Long> cardMap = new HashMap<CardIdBean, Long>();

			if(list != null && list.size() > 0) {
				// 카드 청구서 등록
				String payDate = (String) data.get("PAYDATE");
				String payMonth = payDate.substring(0, 6);
				String payAmt = (String) data.get("PAYAMT");
				String useYn = "Y";
				
				TblCardBill cardBill = new TblCardBill();
				cardBill.setUserId(userId);
				cardBill.setCmpnyCode(cmpnyCode);
				cardBill.setPayDate(payDate);
				cardBill.setPayMonth(payMonth);
				cardBill.setPayAmt(ScrapingUtil.getLongValueFromString(payAmt));
				cardBill.setUseYn(useYn);
				cardBill.setRegDate(now);
				cardBill.setModDate(now);
				
				Long billId = null;
				try {
					collectService.addCardBill(cardBill);
					billId = collectService.getBillId(cardBill);
				} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
					logger.info("error = {}", e.toString());
					ret.setResCode("501");
					ret.setResDesc("DB access error");	
					return ret;
				}
				
				// 청구서 요약정보 등록			
				@SuppressWarnings("unchecked")
				ArrayList<HashMap<String, String>> sumlist = (ArrayList<HashMap<String, String>>) data.get("SUMLIST");
				if(sumlist != null && sumlist.size() > 0) {
					for(HashMap<String, String> map : sumlist) {		
						String fieldName = map.get("FIELDNAME");
						String value1 = map.get("VALUE1");
						TblCardBillSummary cardBillSummary = new TblCardBillSummary();
						cardBillSummary.setBillId(billId);
						cardBillSummary.setSmryName(fieldName);
						cardBillSummary.setSum(ScrapingUtil.getLongValueFromString(value1));
						cardBillSummary.setRegDate(now);
						cardBillSummary.setModDate(now);
						cardBillSummaryList.add(cardBillSummary);
					}

					try {
						collectService.addCardBillSummary(cardBillSummaryList);
					} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
						logger.info("error = {}", e.toString());
						ret.setResCode("501");
						ret.setResDesc("DB access error");	
						return ret;
					}
				}

				// LIST에서 내역을 꺼낸다.
				for(HashMap<String, String> map : list) {		
					String useDis = map.get("USEDIS");
					String useDate = map.get("USEDATE");
					useDate = ScrapingUtil.makeUseDate(useDate);
					String franName = map.get("FRANNAME");
					String appNum = map.get("APPNUM");
					if(appNum == null || appNum.equals("")) appNum = "0";
					String appNickName = map.get("APPNICKNAME");
					String surTax = map.get("SURTAX");
					String tip = map.get("TIP");
					String fxDate = map.get("FXDATE");
					String regNum = map.get("REGNUM");
//					String workKind = map.get("WORKKIND");
					String addr1 = map.get("ADDR1");
					String addr2 = map.get("ADDR2");
					String tel = map.get("TEL");
					String fxUse = map.get("FXUSE");
					String preName = map.get("PRENAME");
					String fxCode = map.get("FXCODE");
					String useAmt = map.get("USEAMT");
					String fxAmt = map.get("FXAMT");
					String fee = map.get("FEE");
					String instrmNm = map.get("INSTRMNM");
					String instrmMon = map.get("INSTRMMON");
									
					// 필수 요소 확인
					if(Utils.isStringEmpty(useDate) || Utils.isStringEmpty(useAmt)) {
						logger.info("Some essential parameters are empty.(USEDATE|USEAMT)");
						//logger.info("map = {}", Utils.toJsonString(map));
						logger.info("useDate = {}, useAmt = {}", useDate, useAmt);
						continue;
					} else {		
						String cardNum = appNickName;
						String nickName = appNickName;
						String billYn = "Y";// 청구서 내역 카드 임
						
						// 카드 정보
						TblCard card = new TblCard();
						card.setUserId(userId);
						card.setCmpnyCode(cmpnyCode);
						card.setCardNum(cardNum);
						card.setNickName(nickName);
						card.setBillYn(billYn); // 청구서 내역 카드 임
						card.setUseYn(useYn);
						card.setRegDate(now);
						card.setModDate(now);
						
						// 카드 ID를 확인한다.
						Long cardId = null;
						CardIdBean bean = new CardIdBean();
						bean.setUserId(userId);
						bean.setCardNum(cardNum);
						bean.setCmpnyCode(cmpnyCode);
						bean.setBillYn(billYn);
						bean.setUseYn(useYn);
						
						// 카드 등록
						if(cardMap.containsKey(bean)) {
							cardId = cardMap.get(bean);
						} else {
							try {
								collectService.addCard(card);
								cardId = collectService.getCardId(card);
							} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
								logger.info("error = {}", e.toString());
								ret.setResCode("501");
								ret.setResDesc("DB access error");	
								return ret;
							}
							
							cardMap.put(bean, cardId);
						}		
						
						// 카드 아이디 로그 등록
						ret.setAccdId(cardId);
						
						String istmYn = ScrapingUtil.getIstmYnFromUseDis(useDis);
						String istmPeriod = instrmNm;
						Integer istmMonth = null;
						if(!Utils.isStringEmpty(instrmMon)) {
							istmMonth = Integer.parseInt(instrmMon);
						}
						
						String storeName = franName;
						storeName = ScrapingUtil.getNoSpaceString(storeName);
						String appvlNum = appNum;
						Long srtx = ScrapingUtil.getLongValueFromString(surTax);
						Long _tip = ScrapingUtil.getLongValueFromString(tip);
						Float excgRate = ScrapingUtil.getFloatValueFromString(fxDate);
						String storeNum = regNum;
						String storeTel = tel;
						String ctgryCode = collectService.getCategoryCode(false, storeNum, storeTel, storeName);
						String storeAddr = addr1 + " " + addr2;
						String frgnYn = ScrapingUtil.getFrgnYnFromFxUse(fxUse);
						String storeHostName = preName;
						String crncyCode = fxCode;
						Long koSum = ScrapingUtil.getLongValueFromString(useAmt);
						Long frgnSum = ScrapingUtil.getLongValueFromString(fxAmt);	

						if(koSum == 0L && frgnSum == 0L) { // 금액이 0이면 무시한다.
							logger.info("Ignores this transaction because sum equals 0.");
							continue;
						}
						
						Long _fee = ScrapingUtil.getLongValueFromString(fee);	
						
						// 청구서 상세 내역
						TblCardBillDetail cardBillDetail = new TblCardBillDetail();
						cardBillDetail.setBillId(billId);
						cardBillDetail.setCardId(cardId);
						cardBillDetail.setIstmYn(istmYn);
						cardBillDetail.setIstmPeriod(istmPeriod);
						cardBillDetail.setIstmMonth(istmMonth);
						cardBillDetail.setUseDate(useDate);
						cardBillDetail.setStoreName(storeName);
						cardBillDetail.setAprvlNum(appvlNum);
						cardBillDetail.setSrtx(srtx);
						cardBillDetail.setTip(_tip);
						cardBillDetail.setExcgRate(excgRate);
						cardBillDetail.setStoreNum(storeNum);
						cardBillDetail.setCtgryCode(ctgryCode);
						cardBillDetail.setStoreAddr(storeAddr);
						cardBillDetail.setStoreTel(storeTel);
						cardBillDetail.setFrgnYn(frgnYn);
						cardBillDetail.setStoreHostName(storeHostName);
						cardBillDetail.setCrncyCode(crncyCode);
						cardBillDetail.setKoSum(koSum);
						cardBillDetail.setFrgnSum(frgnSum);
						cardBillDetail.setFee(_fee);
						cardBillDetail.setRegDate(now);
						cardBillDetail.setModDate(now);
						cardBillDetaillList.add(cardBillDetail);
					}
				}

				// 카드 청구서 매핑 정보를 등록한다.
				for(Long cardId : cardMap.values()) {
					TblCardBillMapping cardBillMapping = new TblCardBillMapping();
					cardBillMapping.setCardId(cardId);
					cardBillMapping.setBillId(billId);
					cardBillMapping.setRegDate(now);
					cardBillMapping.setModDate(now);
					try {
						collectService.addCardBillMapping(cardBillMapping);
					} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
						logger.info("error = {}", e.toString());
						ret.setResCode("501");
						ret.setResDesc("DB access error");	
						return ret;
					}
				}

				try {					
					// 카드 청구 내역을 저장한다.
					collectService.addCardBillDetailInOneSql(cardBillDetaillList);

					ret.setResCode("100");
					ret.setResDesc("ok");
				} catch(MySQLIntegrityConstraintViolationException | RuntimeException e) {
					logger.info("error = {}", e.toString());
					ret.setResCode("501");
					ret.setResDesc("DB access error");	
					return ret;
				}		
			} else {
				ret.setResCode("603");
				ret.setResDesc("No LISTs.");		
			}
		}
		
		return ret;		
	}

	/**
	 * 카드 한도 조회
	 * @param data
	 * @return
	 */
	private TblCollectLog cardLimitProccess(Map<String, ?> data) {	
		logger.info("data={}", Utils.toJsonString(data));	
		
		String userId = (String) data.get("USERID");
		Integer type = (Integer) data.get("TYPE");
		String cmpnyCode = (String) data.get("CMPNYCODE");
		String uploadId = (String) data.get("UPLOADID");

		// 로그 등록
		TblCollectLog ret = new TblCollectLog();	
		Date now = Calendar.getInstance(Locale.KOREA).getTime();
		ret.setUserId(userId);
		ret.setCmpnyCode(cmpnyCode);
		ret.setCllctType(type);
		ret.setUploadId(uploadId);
		ret.setRegDate(now);
		ret.setModDate(now);

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(cmpnyCode) || Utils.isStringEmpty(uploadId)) {
			ret.setResCode("603");
			ret.setResDesc("Some essential parameters are empty.(USERID|CMPNYCODE|UPLOADID)");
		} else {
			// companyCode가 카드사 리스트에 포함되는 지 확인. 아니면 무시한다.
			if(!ScrapingUtil.checkCompanyTypeCodes(Const.COMPANY_TYPE_CARD, cmpnyCode)) {
				ret.setResCode("601");
				ret.setResDesc("Not card type company code.");		
				
				return ret;
			}
			
			@SuppressWarnings("unchecked")
			ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>>) data.get("LIST");
									
			if(list != null && list.size() > 0) {
				// 한도 내역은 1개만 처리한다.
				HashMap<String, String> map = list.get(0);	
				//String useDis = map.get("USEDIS");
				String limitAmount = map.get("USELIMIT");
				String useAmount = map.get("USEAMT");
				String possibleAmount = map.get("USEFULAMT");
				
				// 필수 요소 확인
				if(Utils.isStringEmpty(limitAmount)) {
					logger.info("Some essential parameters are empty.(USELIMIT)");
					logger.info("map = {}", Utils.toJsonString(map));
				} else {
					String useYn = "Y";
					
					TblCardLimit cardLimit = new TblCardLimit();
					cardLimit.setUserId(userId);
					cardLimit.setCmpnyCode(cmpnyCode);
					cardLimit.setLimitAmt(ScrapingUtil.getLongValueFromString(limitAmount));
					cardLimit.setUseAmt(ScrapingUtil.getLongValueFromString(useAmount));
					cardLimit.setPsblAmt(ScrapingUtil.getLongValueFromString(possibleAmount));
					cardLimit.setUseYn(useYn);
					cardLimit.setRegDate(now);
					cardLimit.setModDate(now);

					try {					
						// 카드 한도를 저장한다.
						Integer result = collectService.addCardLimit(cardLimit);
						
						if(result > 0) {
							ret.setResCode("100");
							ret.setResDesc("ok");						
						} else {
							ret.setResCode("503");
							ret.setResDesc("No such data");							
						}
					} catch(MySQLIntegrityConstraintViolationException | RuntimeException e) {
						logger.info("error = {}", e.toString());
						ret.setResCode("501");
						ret.setResDesc("DB access error");		
					}
				}
			} else {
				ret.setResCode("603");
				ret.setResDesc("No LISTs.");			
			}
		}
		
		return ret;		
	}

	/**
	 * 카드 결재 예상 금액 수집
	 * 카드 청구서와 동일한 테이블을 사용한다.
	 * @param data
	 * @return
	 */
	private TblCollectLog cardScheduleAmtProccess(Map<String, ?> data) {
		logger.info("data={}", Utils.toJsonString(data));	
		
		String userId = (String) data.get("USERID");
		Integer type = (Integer) data.get("TYPE");
		String cmpnyCode = (String) data.get("CMPNYCODE");
		String uploadId = (String) data.get("UPLOADID");

		// 로그 등록
		TblCollectLog ret = new TblCollectLog();	
		Date now = Calendar.getInstance(Locale.KOREA).getTime();
		ret.setUserId(userId);
		ret.setCmpnyCode(cmpnyCode);
		ret.setCllctType(type);
		ret.setUploadId(uploadId);
		ret.setRegDate(now);
		ret.setModDate(now);

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(cmpnyCode) || Utils.isStringEmpty(uploadId)) {
			ret.setResCode("603");
			ret.setResDesc("Some essential parameters are empty.(USERID|CMPNYCODE|UPLOADID)");
		} else {		
			// companyCode가 카드사 리스트에 포함되는 지 확인. 아니면 무시한다.
			if(!ScrapingUtil.checkCompanyTypeCodes(Const.COMPANY_TYPE_CARD, cmpnyCode)) {
				ret.setResCode("601");
				ret.setResDesc("Not card type company code.");		
				
				return ret;
			}
			
			@SuppressWarnings("unchecked")
			ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>>) data.get("PAYESTLIST");
			// 카드 결제 예정 내역
			List<TblCardBillDetail> cardBillDetaillList = new ArrayList<TblCardBillDetail>();
			// 카드 ID 맵
			HashMap<CardIdBean, Long> cardMap = new HashMap<CardIdBean, Long>();

			if(list != null && list.size() > 0) {
				// 카드 청구서 등록
				String payDate = (String) data.get("ESTDATE");
				String payMonth = payDate.substring(0, 6);
				String payAmt = (String) data.get("ESTAMT");
				String useYn = "Y";
				
				TblCardBill cardBill = new TblCardBill();
				cardBill.setUserId(userId);
				cardBill.setCmpnyCode(cmpnyCode);
				cardBill.setPayDate(payDate);
				cardBill.setPayMonth(payMonth);
				cardBill.setPayAmt(ScrapingUtil.getLongValueFromString(payAmt));
				cardBill.setUseYn(useYn);
				cardBill.setRegDate(now);
				cardBill.setModDate(now);
				
				Long billId = null;
				try {
					collectService.addCardBill(cardBill);
					billId = collectService.getBillId(cardBill);
				} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
					logger.info("error = {}", e.toString());
					ret.setResCode("501");
					ret.setResDesc("DB access error");	
					return ret;
				}
				
				// LIST에서 내역을 꺼낸다.
				for(HashMap<String, String> map : list) {		
					String useDis = map.get("PAYOPTION");
					String useDate = map.get("ASALEDATE");
					useDate = ScrapingUtil.makeUseDate(useDate);
					String franName = map.get("MBRMCHNAME");
					String appNum = "";
					if(appNum == null || appNum.equals("")) appNum = "0";
					String appNickName = map.get("CARDNUMBER");
					String surTax = "";
					String tip = "";
					String fxDate = "";
					String regNum = "";
//					String workKind = map.get("WORKKIND");
					String addr1 = "";
					String addr2 = "";
					String tel = "";
					String fxUse = "";
					String preName = "";
					String fxCode = "";
					String useAmt = map.get("ASALEAMT");
					String fxAmt = "";
					String fee = map.get("FEE");
					String instrmNm = "";
					String instrmMon = "";
									
					// 필수 요소 확인
					if(Utils.isStringEmpty(useDate) || Utils.isStringEmpty(useAmt)) {
						logger.info("Some essential parameters are empty.(USEDATE|USEAMT)");
						//logger.info("map = {}", Utils.toJsonString(map));
						logger.info("useDate = {}, useAmt = {}", useDate, useAmt);
						continue;
					} else {		
						String cardNum = appNickName;
						String nickName = appNickName;
						String billYn = "Y";// 청구서 내역 카드 임
						
						// 카드 정보
						TblCard card = new TblCard();
						card.setUserId(userId);
						card.setCmpnyCode(cmpnyCode);
						card.setCardNum(cardNum);
						card.setNickName(nickName);
						card.setBillYn(billYn); 
						card.setUseYn(useYn);
						card.setRegDate(now);
						card.setModDate(now);
						
						// 카드 ID를 확인한다.
						Long cardId = null;
						CardIdBean bean = new CardIdBean();
						bean.setUserId(userId);
						bean.setCardNum(cardNum);
						bean.setCmpnyCode(cmpnyCode);
						bean.setBillYn(billYn);
						bean.setUseYn(useYn);
						
						// 카드 등록
						if(cardMap.containsKey(bean)) {
							cardId = cardMap.get(bean);
						} else {
							try {
								collectService.addCard(card);
								cardId = collectService.getCardId(card);
							} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
								logger.info("error = {}", e.toString());
								ret.setResCode("501");
								ret.setResDesc("DB access error");	
								return ret;
							}
							
							cardMap.put(bean, cardId);
						}		
						
						// 카드 아이디 로그 등록
						ret.setAccdId(cardId);
						
						String istmYn = ScrapingUtil.getIstmYnFromUseDis(useDis);
						String istmPeriod = instrmNm;
						Integer istmMonth = null;
						if(!Utils.isStringEmpty(instrmMon)) {
							istmMonth = Integer.parseInt(instrmMon);
						}
						
						String storeName = franName;
						storeName = ScrapingUtil.getNoSpaceString(storeName);
						String appvlNum = appNum;
						Long srtx = ScrapingUtil.getLongValueFromString(surTax);
						Long _tip = ScrapingUtil.getLongValueFromString(tip);
						Float excgRate = ScrapingUtil.getFloatValueFromString(fxDate);
						String storeNum = regNum;
						String storeTel = tel;
						String ctgryCode = collectService.getCategoryCode(false, storeNum, storeTel, storeName);
						String storeAddr = addr1 + " " + addr2;
						String frgnYn = ScrapingUtil.getFrgnYnFromFxUse(fxUse);
						String storeHostName = preName;
						String crncyCode = fxCode;
						Long koSum = ScrapingUtil.getLongValueFromString(useAmt);
						Long frgnSum = ScrapingUtil.getLongValueFromString(fxAmt);	
						
						if(koSum == 0L && frgnSum == 0L) { // 금액이 0이면 무시한다.
							logger.info("Ignores this transaction because sum equals 0.");
							continue;
						}
						
						Long _fee = ScrapingUtil.getLongValueFromString(fee);	
						
						// 청구서 상세 내역
						TblCardBillDetail cardBillDetail = new TblCardBillDetail();
						cardBillDetail.setBillId(billId);
						cardBillDetail.setCardId(cardId);
						cardBillDetail.setIstmYn(istmYn);
						cardBillDetail.setIstmPeriod(istmPeriod);
						cardBillDetail.setIstmMonth(istmMonth);
						cardBillDetail.setUseDate(useDate);
						cardBillDetail.setStoreName(storeName);
						cardBillDetail.setAprvlNum(appvlNum);
						cardBillDetail.setSrtx(srtx);
						cardBillDetail.setTip(_tip);
						cardBillDetail.setExcgRate(excgRate);
						cardBillDetail.setStoreNum(storeNum);
						cardBillDetail.setCtgryCode(ctgryCode);
						cardBillDetail.setStoreAddr(storeAddr);
						cardBillDetail.setStoreTel(storeTel);
						cardBillDetail.setFrgnYn(frgnYn);
						cardBillDetail.setStoreHostName(storeHostName);
						cardBillDetail.setCrncyCode(crncyCode);
						cardBillDetail.setKoSum(koSum);
						cardBillDetail.setFrgnSum(frgnSum);
						cardBillDetail.setFee(_fee);
						cardBillDetail.setRegDate(now);
						cardBillDetail.setModDate(now);
						cardBillDetaillList.add(cardBillDetail);
					}
				}

				// 카드 청구서 매핑 정보를 등록한다.
				for(Long cardId : cardMap.values()) {
					TblCardBillMapping cardBillMapping = new TblCardBillMapping();
					cardBillMapping.setCardId(cardId);
					cardBillMapping.setBillId(billId);
					cardBillMapping.setRegDate(now);
					cardBillMapping.setModDate(now);
					try {
						collectService.addCardBillMapping(cardBillMapping);
					} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
						logger.info("error = {}", e.toString());
						ret.setResCode("501");
						ret.setResDesc("DB access error");	
						return ret;
					}
				}

				try {					
					// 카드 청구 내역을 저장한다.
					collectService.addCardBillDetailInOneSql(cardBillDetaillList);

					ret.setResCode("100");
					ret.setResDesc("ok");
				} catch(MySQLIntegrityConstraintViolationException | RuntimeException e) {
					logger.info("error = {}", e.toString());
					ret.setResCode("501");
					ret.setResDesc("DB access error");	
					return ret;
				}		
			} else {
				ret.setResCode("603");
				ret.setResDesc("No LISTs.");		
			}
		}
		
		return ret;		
	}
	
	/**
	 * 현금 영수증 내역 등록
	 * @param data
	 */
	private TblCollectLog cashProccess(Map<String, ?> data) {
		logger.info("data={}", Utils.toJsonString(data));	
		
		String userId = (String) data.get("USERID");
		Integer type = (Integer) data.get("TYPE");
		String uploadId = (String) data.get("UPLOADID");

		// 로그 등록
		TblCollectLog ret = new TblCollectLog();	
		Date now = Calendar.getInstance(Locale.KOREA).getTime();
		ret.setUserId(userId);
		ret.setCmpnyCode(Const.CASH_HT); // 국세청으로 등록
		ret.setCllctType(type);
		ret.setUploadId(uploadId);
		ret.setRegDate(now);
		ret.setModDate(now);

		// 필수 요소 확인
		if(Utils.isStringEmpty(userId) || Utils.isStringEmpty(uploadId)) {
			ret.setResCode("603");
			ret.setResDesc("Some essential parameters are empty.(USERID|UPLOADID)");
		} else {			
			@SuppressWarnings("unchecked")
			ArrayList<HashMap<String, String>> list = (ArrayList<HashMap<String, String>>) data.get("CASHLIST");
			// 현금 영수증 리스트
			List<TblCashReceipt> cashReceiptList = new ArrayList<TblCashReceipt>();
			// 소비 리스트
			List<TblExpense> expenseList = new ArrayList<TblExpense>();
			
			if(list != null && list.size() > 0) {
				for(HashMap<String, String> map : list) {
					String transDate = map.get("TRANSDATE");
					String transTime = map.get("TRANSTIME");
					String storeName = map.get("STORENAME");
					String useAmt = map.get("USEAMT");
					String appNumber = map.get("APPNUMBER");
					String identi = map.get("IDENTI");
					String transGubun = map.get("TRANSGUBUN");
					String deductYn = map.get("DEDUCTYN");
					String issueGubun = map.get("ISSUEGUBUN");
					String expense = map.get("EXPENSE");
					
					// 필수 요소 확인
					if(Utils.isStringEmpty(transDate) || Utils.isStringEmpty(transTime) || Utils.isStringEmpty(storeName)|| Utils.isStringEmpty(useAmt) || Utils.isStringEmpty(appNumber)) {
						logger.info("Some essential parameters are empty.(TRANSDATE|STORENAME|USEAMT|APPNUMBER)");
						logger.info("map = {}", Utils.toJsonString(map));
						continue;
					} else {
						Long sum = ScrapingUtil.getLongValueFromString(useAmt);
						
						if(sum == 0L) { // 금액이 0이면 무시한다.
							logger.info("Ignores this transaction because sum equals 0.");
							continue;
						}
						
						storeName = ScrapingUtil.getNoSpaceString(storeName);
						String ctgryCode = collectService.getCategoryCode(true, "", "", storeName);
						transDate = ScrapingUtil.getAprvlDate(transDate, transTime);
						String apvlNum = appNumber;
						String idnty = identi;
						String aprvlYn = ScrapingUtil.getAprvlYnFromAppGubun(transGubun);
						String ddctYn = ScrapingUtil.getDdctYnFromDeductYn(deductYn);
						Integer issueType = ScrapingUtil.getIssueType(issueGubun);		
						String useYn = "Y";
						
						// 현금 영수증 리스트에 넣는다.
						TblCashReceipt cashReceipt = new TblCashReceipt();
						cashReceipt.setUserId(userId);
						cashReceipt.setTransDate(transDate);
						cashReceipt.setStoreName(storeName);
						cashReceipt.setSum(sum);
						cashReceipt.setCtgryCode(ctgryCode);
						cashReceipt.setAprvlNum(apvlNum);
						cashReceipt.setIdnty(idnty);
						cashReceipt.setAprvlYn(aprvlYn);
						cashReceipt.setDdctYn(ddctYn);
						cashReceipt.setIssueType(issueType);
						cashReceipt.setExpense(expense);
						cashReceipt.setRegDate(now);
						cashReceipt.setModDate(now);						
						cashReceiptList.add(cashReceipt);

						// 소비 리스트에 넣는다.
						TblExpense tblExpense = new TblExpense();
						tblExpense.setUserId(userId);
						tblExpense.setExpnsDate(transDate.substring(0, 12)); // 소비 시간은 yyyyMMddHHmm(분)까지 표시
						tblExpense.setOppnnt(storeName);
						tblExpense.setSum(sum);
						tblExpense.setCtgryCode(ctgryCode);
						tblExpense.setPayType(Const.PAY_TYPE_CASH); // 현금
						tblExpense.setEditYn("N");
						tblExpense.setCancelYn("N");	
						tblExpense.setCmpnyCode(Const.CASH_HT); // 현금 영수증은 국세청으로 등록
						tblExpense.setUseYn(useYn);
						// 일단 전체를 넣는다.
						expenseList.add(tblExpense);
					}
				}

				try {
					// 현금 영수증 내역을 저장한다.
					collectService.addCashReceiptInOneSql(cashReceiptList);
					// 소비 내역을 저장한다.
					collectService.addExpenseInOneSql(expenseList);

					ret.setResCode("100");
					ret.setResDesc("ok");
				} catch(MySQLIntegrityConstraintViolationException | RuntimeException e) {
					logger.info("error = {}", e.toString());
					ret.setResCode("501");
					ret.setResDesc("DB access error");	
					return ret;
				}		
			} else {
				ret.setResCode("603");
				ret.setResDesc("No LISTs.");					
			}
		}
		
		return ret;
	}
}
