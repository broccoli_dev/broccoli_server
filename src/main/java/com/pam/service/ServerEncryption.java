package com.pam.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Security;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pam.util.ByteUtil;
import com.pam.util.Global;
import com.pam.security.Session;
import com.pam.service.SessionHandler;
import com.pam.service.SecureSessionService;

@Service
public class ServerEncryption {
    private static final Logger logger = LoggerFactory.getLogger(ServerEncryption.class);
	private final static String clientFinishedString = "hello world";
	private final static String serverFinishedString = "hello world too";
    private final static char[]   passwd = { '@', 'n', 'c', 'z', 'y', 'm', 'l'};
    private final static String keyAlias = "Noco's key";
    private final static String certPath = "/noco.p12";
    
    @Autowired
    SecureSessionService secureSessionService;
    
    @Autowired
    SessionHandler sessionHandler;
	
	/**
	 * generate sessoin id
	 * @return session id string 32
	 */
	public static String genSessionId() {		
		return genRandom(32);
	}

	/**
	 * generate server random value
	 * @return server random 32
	 */
	public static String genServerRandom() {
		String ret = "";
		long time = System.currentTimeMillis() / 1000L;
		ByteBuffer buffer = ByteBuffer.allocate(8);
	    buffer.putLong(time);
	    
	    ret = ByteUtil.byteArrayToHex(buffer.array()).substring(8);
        ret += genRandom(28);
		
		return ret;
	}
	
	public static String[] genPublicKeyPair(String algorithmNum, int length) {
		String[] ret = new String[2];
		String publicKeyAlgorithm = (String) Global.publicKeyEncryptionAlgorithm.get(algorithmNum);
		
		// create the keys
		
		KeyPairGenerator generator;
		try {
			generator = KeyPairGenerator.getInstance(publicKeyAlgorithm);
			generator.initialize(length, new SecureRandom());

			KeyPair pair = generator.generateKeyPair();
			Key pubKey = pair.getPublic();
			Key privKey = pair.getPrivate();

			// Send the public key bytes to the other party...
			byte[] publicKeyBytes = pubKey.getEncoded();
			//Convert Public key to String
			String pubKeyStr = ByteUtil.byteArrayToHex(publicKeyBytes);				
			
			byte[] privateKeyBytes = privKey.getEncoded();
			String privKeyStr = ByteUtil.byteArrayToHex(privateKeyBytes);
			
			ret[0] = pubKeyStr;
			ret[1] = privKeyStr;
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}

	public static KeyPair genKeyPair(String publicAlg, int length) {
		KeyPair ret = null;
		
		// create the keys
		KeyPairGenerator generator;
		try {
			generator = KeyPairGenerator.getInstance(publicAlg);
			generator.initialize(length, new SecureRandom());

			ret = generator.generateKeyPair();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public boolean genSessionKey(String httpSessionId, String keyExchange, String type) {
		logger.debug("httpSessionID = {}, keyExchange = {}, type = {}", httpSessionId, keyExchange, type);
		
		boolean ret = false;
		Session session = secureSessionService.readSession(httpSessionId);		
		if(session != null) {
			String privateKey = session.getPrivateKey();
			String algorithm = session.getPublicKeyAlgorithm();
			
			if(!privateKey.equals("") && privateKey != null) {
				String preMasterKey = ServerEncryption.getPreMasterKey(keyExchange, privateKey, algorithm);
				
				if(!preMasterKey.equals("")) {
					String serverRandom = session.getServerRandom();
					String clientRandom = session.getClientRandom();
					
					String sessionKey = "";
					sessionKey = this.genSessionKey(httpSessionId, preMasterKey, serverRandom, clientRandom);
					sessionHandler.updateSessionKey(httpSessionId, sessionKey, type);
					
					ret = true;
				}
			} 			
		} 
		
		return ret;
	}
	
	public boolean verifyClientFinished(String httpSessionId, String finished, String type) {
		boolean ret = false;
		Session session = secureSessionService.readSession(httpSessionId);		
		
		if(session != null) {
			String sessionKey = session.getSessionKey();
			String secretAlg = session.getSecretKeyAlgorithm();
			String keyAlg = "";
			if(secretAlg.contains("/")) {
				keyAlg = secretAlg.substring(0, secretAlg.indexOf("/"));
			} else {
				keyAlg = secretAlg;
			}		
			
			byte[] keyData = ByteUtil.hexToByteArray(sessionKey);
			SecretKey secretKey = new SecretKeySpec(keyData, keyAlg);
			try {
				Cipher cipher = Cipher.getInstance(secretAlg);
				try {
					cipher.init(Cipher.DECRYPT_MODE, secretKey);
				} catch (InvalidKeyException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				byte[] tmpArr = ByteUtil.hexToByteArray(finished);
				try {
					try {
						String decoded = new String(cipher.doFinal(tmpArr), "utf-8");
						if(decoded.equals(clientFinishedString)) {
							sessionHandler.updateCurrentStatus(httpSessionId, type);
							ret = true;
						}
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (IllegalBlockSizeException | BadPaddingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		
		return ret;
	}
	
	public static String genServerFinished(Session session) {
		String ret = "";
		String sessionKey = session.getSessionKey();
		String secretAlg = session.getSecretKeyAlgorithm();
		String keyAlg = "";
		if(secretAlg.contains("/")) {
			keyAlg = secretAlg.substring(0, secretAlg.indexOf("/"));
		} else {
			keyAlg = secretAlg;
		}		
		
		try {
			Cipher cipher = Cipher.getInstance(secretAlg);
			SecretKey secretKey = new SecretKeySpec(ByteUtil.hexToByteArray(sessionKey), keyAlg);
			try {
				cipher.init(Cipher.ENCRYPT_MODE, secretKey);
				try {
					byte[] encrypted = cipher.doFinal(serverFinishedString.getBytes());
					ret = ByteUtil.byteArrayToHex(encrypted);
				} catch (IllegalBlockSizeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (BadPaddingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}
	
	public static byte[] genEncryptedBytes(byte[] data, Session session) {
		byte[] ret = null;
		String sessionKey = session.getSessionKey();
		String secretAlg = session.getSecretKeyAlgorithm();
		String keyAlg = "";
		if(secretAlg.contains("/")) {
			keyAlg = secretAlg.substring(0, secretAlg.indexOf("/"));
		} else {
			keyAlg = secretAlg;
		}		
		
		try {
			Cipher cipher = Cipher.getInstance(secretAlg);
			SecretKey secretKey = new SecretKeySpec(ByteUtil.hexToByteArray(sessionKey), keyAlg);
			try {
				cipher.init(Cipher.ENCRYPT_MODE, secretKey);
				try {
					ret = cipher.doFinal(data);
				} catch (IllegalBlockSizeException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (BadPaddingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}

	public static byte[] genDecryptedBytes(byte[] data, Session session) {
		byte[] ret = null;

		String sessionKey = session.getSessionKey();
		String secretAlg = session.getSecretKeyAlgorithm();
		String keyAlg = "";
		if(secretAlg.contains("/")) {
			keyAlg = secretAlg.substring(0, secretAlg.indexOf("/"));
		} else {
			keyAlg = secretAlg;
		}		
		
		byte[] keyData = ByteUtil.hexToByteArray(sessionKey);
		SecretKey secretKey = new SecretKeySpec(keyData, keyAlg);
		try {
			Cipher cipher = Cipher.getInstance(secretAlg);
			try {
				cipher.init(Cipher.DECRYPT_MODE, secretKey);
			} catch (InvalidKeyException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {
				ret = cipher.doFinal(data);
			} catch (IllegalBlockSizeException | BadPaddingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}
	
	/**
	 * generate random value
	 * @param length random value length
	 * @return (length) bytes random value string
	 */
	private static String genRandom(int length) {
    	SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[length];
        random.nextBytes(bytes);
        
        return ByteUtil.byteArrayToHex(bytes);
	}
	
	private static String getPreMasterKey(String keyExchange, String privateKey, String algorithm) {
		logger.debug("keyExchange = {}, privateKey = {}, algorithm = {}", keyExchange, privateKey, algorithm);
		String ret = "";
		byte[] keyBytes = ByteUtil.hexToByteArray(keyExchange);
		byte[] privBytes = ByteUtil.hexToByteArray(privateKey);

	//  get public key
	    KeyFactory kf;
		try {
			kf = KeyFactory.getInstance(algorithm);
//		    get the private key  
			PKCS8EncodedKeySpec specPriv = new PKCS8EncodedKeySpec(privBytes);
		    
		    try {
				PrivateKey privKey = kf.generatePrivate(specPriv);				
				try {
					if(algorithm.startsWith("RSA")) {
						algorithm = algorithm + "/ECB/PKCS1Padding";
					}
					
					Cipher cipher = Cipher.getInstance(algorithm);
					try {
						cipher.init(Cipher.DECRYPT_MODE, privKey);
						try {
							byte[] secritKeyBytes = cipher.doFinal(keyBytes);
							ret = ByteUtil.byteArrayToHex(secritKeyBytes);
						} catch (IllegalBlockSizeException e) {
							// TODO Auto-generated catch block
							logger.debug("error = {}", e.toString());
							e.printStackTrace();
						} catch (BadPaddingException e) {
							// TODO Auto-generated catch block
							logger.debug("error = {}", e.toString());
							e.printStackTrace();
						}
					} catch (InvalidKeyException e) {
						// TODO Auto-generated catch block
						logger.debug("error = {}", e.toString());
						e.printStackTrace();
					}
				} catch (NoSuchPaddingException e) {
					// TODO Auto-generated catch block
					logger.debug("error = {}", e.toString());
					e.printStackTrace();
				}
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				logger.debug("error = {}", e.toString());
				e.printStackTrace();
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			logger.debug("error = {}", e.toString());
			e.printStackTrace();
		}
		
		return ret;
	}

	private String genSessionKey(String httpSessionId, String preMasterKey, String serverRandom, String clientRandom) {
		String ret = "";
		
		logger.debug("preMasterKey="+preMasterKey);
		logger.debug("seed="+serverRandom+clientRandom);
		
		byte[] secret = ByteUtil.hexToByteArray(preMasterKey);
		byte[] tmpBytes = ret.getBytes(); 
		byte[] seed = ByteUtil.concat(ByteUtil.hexToByteArray(serverRandom), ByteUtil.hexToByteArray(clientRandom));
		Session session = secureSessionService.readSession(httpSessionId);		
		String hashAlg = session.getHashAlgorithm();
		String hmacName = "Hmac" + hashAlg;		
		
		for(int i=0; i<4; i++) {
			tmpBytes = ServerEncryption.doMyHMac(secret, ByteUtil.concat(tmpBytes, seed), hmacName);
			logger.debug("H" + String.valueOf((i+1)) + ":" + ByteUtil.byteArrayToHex(tmpBytes));
			ret += ByteUtil.byteArrayToHex(tmpBytes).substring(i*4*2, (i+1)*4*2);
		}
		
		return ret;
	}
	
	private static byte[] doMyHMac(byte[] secret, byte[] message, String hmacName) {
		byte[] ret = null;

		Mac hmac = null;
		try {
			hmac = Mac.getInstance(hmacName);
			SecretKeySpec keySpec = new SecretKeySpec(secret, hmacName);
			try {
				hmac.init(keySpec);
			} catch (InvalidKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ret = hmac.doFinal(message);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ret;
	}

    /**
     * we generate a certificate signed by our CA's intermediate certificate
     */
    public static String createCert(PublicKey pubKey, PrivateKey privKey, String algorithm, Session session) {
    	String ret = "";
        //
        // signers name table.
        //
        X500NameBuilder issuerBuilder = new X500NameBuilder();

        issuerBuilder.addRDN(BCStyle.CN, "Noco Cert");
        issuerBuilder.addRDN(BCStyle.C, "KR");
        issuerBuilder.addRDN(BCStyle.O, "Nomadconnection");
        issuerBuilder.addRDN(BCStyle.EmailAddress, "zimly@nomadconnection.com");

        //
        // subjects name table.
        //
        X500NameBuilder subjectBuilder = new X500NameBuilder();

        subjectBuilder.addRDN(BCStyle.C, "KR");
        subjectBuilder.addRDN(BCStyle.O, "Nomadconnection");
        subjectBuilder.addRDN(BCStyle.L, "Seoul");
        subjectBuilder.addRDN(BCStyle.CN, "noco");
        subjectBuilder.addRDN(BCStyle.EmailAddress, "zimly@nomadconnection.com");

        //
        // create the certificate - version 3
        //
        X509v3CertificateBuilder v3Bldr = new JcaX509v3CertificateBuilder(issuerBuilder.build(), BigInteger.valueOf(3),
            new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 24 * 30), new Date(System.currentTimeMillis() + (1000L * 60 * 60 * 24 * 30)),
            subjectBuilder.build(), pubKey);

        //
        // extensions
        //
        JcaX509ExtensionUtils extUtils = null;
		try {
			extUtils = new JcaX509ExtensionUtils();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
/*
        v3Bldr.addExtension(
            Extension.subjectKeyIdentifier,
            false,
            extUtils.createSubjectKeyIdentifier(pubKey));

        v3Bldr.addExtension(
            Extension.authorityKeyIdentifier,
            false,
            extUtils.createAuthorityKeyIdentifier(caPubKey));
*/
        X509CertificateHolder certHldr = null;
		try {
			String secretAlg = session.getPublicKeyAlgorithm();
			String hashAlg = session.getHashAlgorithm();
			String sigAlg = hashAlg + "With" + secretAlg;
			certHldr = v3Bldr.build(new JcaContentSignerBuilder(sigAlg).setProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider()).build(privKey));
		} catch (OperatorCreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        X509Certificate cert = null;
		try {
			cert = new JcaX509CertificateConverter().setProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider()).getCertificate(certHldr);
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {
			cert.checkValidity(new Date());
		} catch (CertificateExpiredException | CertificateNotYetValidException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {
			cert.verify(pubKey);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        PKCS12BagAttributeCarrier   bagAttr = (PKCS12BagAttributeCarrier)cert;

        //
        // this is also optional - in the sense that if you leave this
        // out the keystore will add it automatically, note though that
        // for the browser to recognise the associated private key this
        // you should at least use the pkcs_9_localKeyId OID and set it
        // to the same as you do for the private key's localKeyId.
        //
        bagAttr.setBagAttribute(
            PKCSObjectIdentifiers.pkcs_9_at_friendlyName,
            new DERBMPString("Noco key"));
        bagAttr.setBagAttribute(
            PKCSObjectIdentifiers.pkcs_9_at_localKeyId,
            extUtils.createSubjectKeyIdentifier(pubKey));

        try {
			ret = ByteUtil.byteArrayToHex(cert.getEncoded());
		} catch (CertificateEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return ret;
    }

    /**
     * we generate a certificate signed by our CA's intermediate certificate
     */
    public static void saveCert(PublicKey pubKey, PrivateKey privKey, String hashAlg, String publicAlg) {
        Security.addProvider(new BouncyCastleProvider());
        //
        // signers name table.
        //
        X500NameBuilder issuerBuilder = new X500NameBuilder();

        issuerBuilder.addRDN(BCStyle.CN, "Noco Cert");
        issuerBuilder.addRDN(BCStyle.C, "KR");
        issuerBuilder.addRDN(BCStyle.O, "Nomadconnection");
        issuerBuilder.addRDN(BCStyle.EmailAddress, "zimly@nomadconnection.com");

        //
        // subjects name table.
        //
        X500NameBuilder subjectBuilder = new X500NameBuilder();

        subjectBuilder.addRDN(BCStyle.C, "KR");
        subjectBuilder.addRDN(BCStyle.O, "Nomadconnection");
        subjectBuilder.addRDN(BCStyle.L, "Seoul");
        subjectBuilder.addRDN(BCStyle.CN, "noco");
        subjectBuilder.addRDN(BCStyle.EmailAddress, "zimly@nomadconnection.com");

        //
        // create the certificate - version 3
        //
        X509v3CertificateBuilder v3Bldr = new JcaX509v3CertificateBuilder(issuerBuilder.build(), BigInteger.valueOf(3),
            new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 24 * 30), new Date(System.currentTimeMillis() + (1000L * 60 * 60 * 24 * 365 * 10)),
            subjectBuilder.build(), pubKey);

        //
        // extensions
        //
        JcaX509ExtensionUtils extUtils = null;
		try {
			extUtils = new JcaX509ExtensionUtils();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
/*
        v3Bldr.addExtension(
            Extension.subjectKeyIdentifier,
            false,
            extUtils.createSubjectKeyIdentifier(pubKey));

        v3Bldr.addExtension(
            Extension.authorityKeyIdentifier,
            false,
            extUtils.createAuthorityKeyIdentifier(caPubKey));
*/
        X509CertificateHolder certHldr = null;
		try {
			String sigAlg = hashAlg + "With" + publicAlg;
			certHldr = v3Bldr.build(new JcaContentSignerBuilder(sigAlg).setProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider()).build(privKey));
		} catch (OperatorCreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        X509Certificate cert = null;
		try {
			cert = new JcaX509CertificateConverter().setProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider()).getCertificate(certHldr);
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {
			cert.checkValidity(new Date());
		} catch (CertificateExpiredException | CertificateNotYetValidException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {
			cert.verify(pubKey);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        PKCS12BagAttributeCarrier   bagAttr = (PKCS12BagAttributeCarrier)cert;

        //
        // this is also optional - in the sense that if you leave this
        // out the keystore will add it automatically, note though that
        // for the browser to recognise the associated private key this
        // you should at least use the pkcs_9_localKeyId OID and set it
        // to the same as you do for the private key's localKeyId.
        //
        bagAttr.setBagAttribute(
            PKCSObjectIdentifiers.pkcs_9_at_friendlyName,
            new DERBMPString("Noco key"));
        bagAttr.setBagAttribute(
            PKCSObjectIdentifiers.pkcs_9_at_localKeyId,
            extUtils.createSubjectKeyIdentifier(pubKey));

        //
        // store the key and the certificate chain
        //
        KeyStore store = null;
		try {
			store = KeyStore.getInstance("PKCS12", "BC");
	        try {
				store.load(null, null);
			} catch (NoSuchAlgorithmException | CertificateException
					| IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (KeyStoreException | NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


        //
        // if you haven't set the friendly name and local key id above
        // the name below will be the name of the key
        //        
        Certificate[] chain = new Certificate[1];
        chain[0] = cert;

        try {
			store.setKeyEntry(keyAlias, privKey, null, chain);
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        FileOutputStream fOut = null;
		try {
			fOut = new FileOutputStream(certPath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        try {
			store.store(fOut, passwd);
		} catch (KeyStoreException | NoSuchAlgorithmException
				| CertificateException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	        try {
				fOut.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
    }
    
    public static String[] getCertificate(String path) {
    	String[] ret = new String[2];

        KeyStore p12 = null;
		try {
			p12 = KeyStore.getInstance("pkcs12");
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			p12.load(new FileInputStream(certPath), passwd);
		} catch (NoSuchAlgorithmException | CertificateException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			Certificate cert = (X509Certificate) p12.getCertificate(keyAlias);
			try {
				PrivateKey key = (PrivateKey)p12.getKey(keyAlias, passwd);
				ret[1] = ByteUtil.byteArrayToHex(key.getEncoded());
			} catch (UnrecoverableKeyException | NoSuchAlgorithmException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				ret[0] = ByteUtil.byteArrayToHex(cert.getEncoded());
			} catch (CertificateEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	return ret;
    }
}
