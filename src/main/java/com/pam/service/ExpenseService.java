package com.pam.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.pam.constant.Const;
import com.pam.dao.BudgetDao;
import com.pam.dao.CardListDao;
import com.pam.dao.ExpenseDao;
import com.pam.entity.AssetMonthlyProperty;
import com.pam.entity.Budget;
import com.pam.entity.BudgetExpense;
import com.pam.entity.BudgetList;
import com.pam.entity.BudgetMain;
import com.pam.entity.CardBenefit;
import com.pam.entity.CardUse;
import com.pam.entity.ExpenseCardList;
import com.pam.entity.ExpenseCategory;
import com.pam.entity.ExpenseCategoryMonth;
import com.pam.entity.ExpenseDetail;
import com.pam.entity.ExpenseList;
import com.pam.entity.ExpenseMain;
import com.pam.entity.ExpensePaytype;
import com.pam.entity.ExpenseSumList;
import com.pam.mapper.TblBudgetListMapper;
import com.pam.mapper.TblBudgetMapper;
import com.pam.mapper.TblCardLimitMapper;
import com.pam.mapper.TblCardMapper;
import com.pam.mapper.TblExpenseMapper;
import com.pam.mapper.domain.TblBudget;
import com.pam.mapper.domain.TblBudgetExample;
import com.pam.mapper.domain.TblBudgetList;
import com.pam.mapper.domain.TblBudgetListExample;
import com.pam.mapper.domain.TblCard;
import com.pam.mapper.domain.TblCardExample;
import com.pam.mapper.domain.TblCardLimit;
import com.pam.mapper.domain.TblCardLimitExample;
import com.pam.mapper.domain.TblExpense;
import com.pam.util.Utils;


@Service
public class ExpenseService {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    
    @Autowired
    TblBudgetMapper budgetMapper;
    
    @Autowired
    TblBudgetListMapper budgetListMapper;
    
    @Autowired
    TblExpenseMapper expenseMapper;
    
    @Autowired
    TblCardMapper cardMapper;
    
    @Autowired
    TblCardLimitMapper cardLimitMapper;
    
    @Autowired
    BudgetDao budgetDao;
    
    @Autowired
    ExpenseDao expenseDao;
    
    @Autowired
    CardListDao cardListDao;
    
    @Autowired
    InquiryService inquiryService;

	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public ExpenseMain getExpenseMainInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
    	ExpenseMain ret = new ExpenseMain();    	

    	List<AssetMonthlyProperty> dailyExpense = this.getDailyExpenseInfo(userId);
    	ExpenseList expenseList = this.getExpenseMainListInfo(userId);
    	BudgetMain budget = this.getBudgetMainInfo(userId);
    	List<Budget> expenseCategoryList = this.getExpenseCategoryListInfo(userId);
    	List<ExpensePaytype> expensePaytypeList = this.getExpensePaytypeListInfo(userId);
    	List<ExpenseCardList> expenseCardList = this.getExpenseCardListInfo(userId);
    	
    	ret.setDailyExpense(dailyExpense);
    	ret.setExpenseList(expenseList);
    	ret.setBudget(budget);
    	ret.setExpenseCategoryList(expenseCategoryList);
    	ret.setExpensePaytypeList(expensePaytypeList);
    	ret.setCardList(expenseCardList);
    	
    	return ret;
    }
    
    /**
     * 예산 상세 내역을 가져온다.
     * @param expense
     * @return
     * @throws MySQLIntegrityConstraintViolationException
     * @throws RuntimeException
     */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public ExpenseSumList getExpenseDetailList(TblExpense expense) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblExpense = {}", Utils.toJsonString(expense));
		ExpenseSumList ret = new ExpenseSumList();
		
		Long total = expenseDao.getExpenseMonthlySum(expense);
		List<ExpenseDetail> expenseList = expenseDao.getExpenseDetail(expense);
		
		ret.setTotalSum(String.valueOf(total));
		ret.setList(expenseList);
		
		return ret;
	}

	/**
	 * 소비 내역을 추가한다.
	 * @param expense
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addExpense(TblExpense expense) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblExpense = {}", Utils.toJsonString(expense));
		Date now = new Date();
		Long expenseId = expense.getExpnsId();
		
		// expenseId == 0이면 추가, 아니면 수정
		if(expenseId == 0) {
			// 예산 추가
			expense.setExpnsId(null);
			expense.setRegDate(now);
			expense.setModDate(now);			
			expenseMapper.insert(expense);
			
		} else {
			expense.setModDate(now);
			expenseMapper.updateByPrimaryKeySelective(expense);
		} 
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void deleteExpense(TblExpense expense) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblExpense = {}", Utils.toJsonString(expense));
		
		expenseMapper.updateByPrimaryKeySelective(expense);
	}

	/**
	 * 예산 상세 리스트를 가져온다.
	 * @param budget
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public BudgetList getBudgetDetailList(TblBudget budget) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblBudget = {}", Utils.toJsonString(budget));
		BudgetList ret = new BudgetList();
		Long budgetId = 0L;
		
		// budgetId를 가져온다.
		TblBudgetExample example = new TblBudgetExample();
		example.createCriteria().andUserIdEqualTo(budget.getUserId()).andBaseMonthEqualTo(budget.getBaseMonth());
		List<TblBudget> budgetList = budgetMapper.selectByExample(example);
		if(budgetList != null && budgetList.size() > 0) {
			budgetId = budgetList.get(0).getBdgtId();
		} 
		
		// 예산 리스트를 가져온다.
		BudgetExpense budgetExpense = new BudgetExpense();
		List<BudgetExpense> budgetExpenseList = budgetDao.getBudgetList(budget);
		if(budgetExpenseList != null && budgetExpenseList.size() == 20) {
			// 예산 합계를 꺼내고 지운다.
			budgetExpense = budgetExpenseList.remove(Const.TOTAL_CATEGORY_COUNT);
		} else {
			budgetExpense.setCategoryCode("00");
			budgetExpense.setBudget("0");
			budgetExpense.setExpense("0");
		}

		String totalBudget = budgetExpense.getBudget();
		String totalExpense = budgetExpense.getExpense();
		if(budgetId == 0L) {
			ret.setBudgetId("");
		} else {
			ret.setBudgetId(String.valueOf(budgetId));
		}
		ret.setBudgetSum(totalBudget);
		ret.setExpenseSum(totalExpense);
		ret.setList(budgetExpenseList);
		
		return ret;
	}
        
    /**
     * 예산을 저장한다.(등록/수정)
     * @param budget
     * @param budgetList
     * @throws MySQLIntegrityConstraintViolationException
     */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void addBudget(TblBudget budget, List<TblBudgetList> budgetList) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblBudget = {}, TblBudgetList = {}", Utils.toJsonString(budget), Utils.toJsonString(budgetList));
		Date now = new Date();
		Long budgetId = budget.getBdgtId();
		
		// budgetId == 0이면 추가, 아니면 수정
		if(budgetId == 0) {
			// 예산 추가
			budget.setBdgtId(null);
			budget.setRegDate(now);
			budget.setModDate(now);			
			budgetId = budgetDao.inserBudget(budget);
			
		} else {
			budget.setModDate(now);
			budgetMapper.updateByPrimaryKeySelective(budget);
			String userId = budget.getUserId();
			String baseMonth = budget.getBaseMonth();
			
			budgetId = this.getBudgetId(userId, baseMonth);
			
			// 기존 예산 내역 삭제 후 추가
			TblBudgetListExample example = new TblBudgetListExample();
			example.createCriteria().andBdgtIdEqualTo(budgetId);
			budgetListMapper.deleteByExample(example);
		} 
		
		// 예산 내역 추가
		String sql = "";
		for(TblBudgetList budget_list : budgetList) {
			String largeCategoryCode = budget_list.getLargeCtgryCode();
			Long sum = budget_list.getSum();
			
			String tmp = "(";		
			tmp += (budgetId + ", ");
			tmp += ("'" + largeCategoryCode + "', ");
			tmp += (sum + ", ");
			tmp += ("now(), ");
			tmp += ("now()), ");
			sql += tmp;
		}			

		if(budgetList.size() > 0) {
			sql = sql.substring(0, sql.length() - 2);
			logger.info("sql={}", sql);
			HashMap<String, String> sqlMap = new HashMap<String, String>();
			sqlMap.put("sqlString", sql);
			budgetDao.inserBudgetList(sqlMap);
		}
	}

	/**
	 * 소비 카테고리 별 금액과 퍼센트를 구한다.
	 * @param expense
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<ExpenseCategory> getExpenseCategoryList(TblExpense expense) throws MySQLIntegrityConstraintViolationException, RuntimeException {	
		logger.info("TblExpense = {}", Utils.toJsonString(expense));	
		List<ExpenseCategory> ret = new ArrayList<ExpenseCategory>();
		
		List<ExpenseCategory> categoryList = expenseDao.getExpensePerCategory(expense);
		/*ExpenseCategory tmp = new ExpenseCategory();*/
		
		if(categoryList != null && categoryList.size() > 0) {
			/*// 미분류 카테고리 항목을 꺼내서 맨 마지막에 넣는다.
			int cnt = 0;
			int tmpCnt = -1;
			
			for(ExpenseCategory category : categoryList) {
				String categoryCode = category.getCategoryCode();
				if(categoryCode.equals(Const.CATEGORY_CODE_NOT_DEFINED.substring(0, 2))) {
					tmpCnt = cnt;
					tmp = category;
					break;
				}
				
				cnt++;
			}
			
			if(tmpCnt != -1) { // 미분류를 마지막에 넣는다(0%)
				categoryList.remove(tmpCnt);
				tmp.setPercent("0");
				categoryList.add(tmp);
			}*/
			
			ret = categoryList;
		}
		
		return ret;
	}
	
	/**
	 * 소비 카테고리 별 상세 소비 내역을 가져온다.
	 * @param expense
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	public ExpenseCategoryMonth getExpenseDetail(TblExpense expense) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("TblExpense = {}", Utils.toJsonString(expense));	
		
		Long monthlyExpense = expenseDao.getMonthlyExpenseInfo(expense);
		Long monthlyBudget = expenseDao.getMonthlyBudgetInfo(expense);
		
		if(monthlyExpense == null) {
			monthlyExpense = 0L;
		}
		
		if(monthlyBudget == null) {
			monthlyBudget = 0L;
		}		
		
		List<ExpenseDetail> expenseDetailList = new ArrayList<ExpenseDetail>();		
		expenseDetailList = expenseDao.getExpenseCategoryDetailInfo(expense);
		
		ExpenseCategoryMonth ret = new ExpenseCategoryMonth();
		ret.setBudget(String.valueOf(monthlyBudget));
		ret.setExpense(String.valueOf(monthlyExpense));
		ret.setList(expenseDetailList);
		
		return ret;
	}

	/**
	 * 카드에 카드코드를 등록한다.
	 * @param card
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public Integer setCardCode(TblCard card) throws MySQLIntegrityConstraintViolationException, RuntimeException {	
		logger.info("TblCard = {}", Utils.toJsonString(card));	
		
		return cardMapper.updateByPrimaryKeySelective(card);
	}

	/**
	 * 카드별 사용 금액/한도/혜택을 구한다.
	 * @param userId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	public List<ExpenseCardList> getExpenseCardListInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		
		List<ExpenseCardList> ret = new ArrayList<ExpenseCardList>();
		
		// 전체 카드 리스트를 가져온다.(승인내역 카드)
		TblCardExample example = new TblCardExample();
		example.createCriteria().andUserIdEqualTo(userId).andBillYnEqualTo("N");
		List<TblCard> cardList = cardMapper.selectByExample(example);
		
		if(cardList != null && cardList.size() > 0) {
			for(TblCard card : cardList) {
				Long cardId = card.getCardId();
				String companyCode = card.getCmpnyCode();
				String cardName = card.getNickName();
				String cardCode = card.getCardCode();
				if(cardCode == null) {
					cardCode = "";
				}
				String isCredit = "Y"; // 신용카드다.
				
				// 이번달 사용액을 구한다.
				Long sum = expenseDao.getCardUseSum(cardId);
				
				// 한도를 구한다.
				String limit = "";
				TblCardLimitExample example2 = new TblCardLimitExample();
				example2.createCriteria().andUserIdEqualTo(userId).andCmpnyCodeEqualTo(companyCode);
				List<TblCardLimit> cardLimit = cardLimitMapper.selectByExample(example2);
				
				if(cardLimit != null && cardLimit.size() > 0) {
					// 1개만 존재한다.
					Long longLimit = cardLimit.get(0).getLimitAmt();
					limit = String.valueOf(longLimit);
				} // 한도가 없으면 ""로 표시한다.
				
				CardUse cardUse = new CardUse();
				cardUse.setCardId(String.valueOf(cardId));
				cardUse.setCompanyCode(companyCode);
				cardUse.setCardName(cardName);
				cardUse.setCardCode(cardCode);
				cardUse.setIsCredit(isCredit);
				cardUse.setSum(String.valueOf(sum));
				cardUse.setTotalLimit(limit);

				ExpenseCardList expenseCardList = new ExpenseCardList();
				expenseCardList.setCardUse(cardUse);
				
				if(!Utils.isStringEmpty(cardCode)) {
					CardBenefit cardBenifit = inquiryService.getCardBenefit(userId, cardCode);
					expenseCardList.setBenefit(cardBenifit);
				} 

				ret.add(expenseCardList);
			}
		}
		
		return ret;
	}

	/**
	 * 사용자 아이디와 해당 월로 예산 아이디를 가져온다.
	 * @param userId
	 * @param baseMonth
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 */
	@Transactional(readOnly = true, propagation = Propagation.REQUIRED)
	private Long getBudgetId(String userId, String baseMonth) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		
		logger.info("userId = {}, baseMonth = {}", userId, baseMonth);	
		Long ret = 0L;
		
		TblBudgetExample example = new TblBudgetExample();
		example.createCriteria().andUserIdEqualTo(userId).andBaseMonthEqualTo(baseMonth);
		List<TblBudget> budgetList = budgetMapper.selectByExample(example);
		
		if(budgetList != null && budgetList.size() > 0) {
			ret = budgetList.get(0).getBdgtId();
		}
		
		return ret;
	}

	/**
	 * 일별 소비 정보 누적값으로 가져온다.
	 * @param userId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	private List<AssetMonthlyProperty> getDailyExpenseInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		
		List<AssetMonthlyProperty> ret = new ArrayList<AssetMonthlyProperty>();
		
		// 일별 소비 내역 합계를 가져온다.
		TblExpense expense = new TblExpense();
		expense.setUserId(userId);
		expense.setCtgryCode(Const.CATEGORY_CODE_NOT_DEFINED.substring(0, 2));
		List<AssetMonthlyProperty> dailyExpenseList = expenseDao.getDailyExpenseSumInfo(expense);
		HashMap<String, String> expenseSumMap = new HashMap<String, String>();
		
		// 맵 형태로 변환한다.
		for(AssetMonthlyProperty expenseSum : dailyExpenseList) {
			expenseSumMap.put(expenseSum.getDate(), expenseSum.getSum());
		}
		
		// 오늘부터 이번달 1일까지 맵을 만들고 소비액 합계를 넣는다.
		String today = Utils.getCurrentDate("yyyymmdd");
//		String today = Utils.getPrevNextDateString("2", -1, "yyyymmdd");
		String firstDay = today.substring(0, 6) + "01";
		
		Long acc = 0L;
		while(Integer.parseInt(firstDay) <= Integer.parseInt(today)) {
			AssetMonthlyProperty property = new AssetMonthlyProperty();
			property.setDate(firstDay);
			Long sum = 0L;
			if(expenseSumMap.containsKey(firstDay)) {
				sum = Long.parseLong(expenseSumMap.get(firstDay));
			} 			
			acc += sum;
			property.setSum(String.valueOf(acc));
			firstDay = Utils.getPrevDate(firstDay, 1);
			ret.add(property);
		}
		
		return ret;
	}
	
	/**
	 * 이번 달 전체 소비 건 수와 최근 3건에 대한 정보를 가져온다.
	 * @param userId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	private ExpenseList getExpenseMainListInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		
		ExpenseList ret = new ExpenseList();
		
		Integer totalCount = expenseDao.getTotalExpenseCount(userId);
		// 최신 소비 이력은 이번달로 한정하지 않음
		List<ExpenseDetail> expenseList = expenseDao.getLast3Expenses(userId);
		
		ret.setTotalCount(totalCount);
		ret.setList(expenseList);
		
		return ret;
	}
	
	/**
	 * 예산 등록 건수와 소비/예산 비교 정보를 가져온다.
	 * @param userId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	private BudgetMain getBudgetMainInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		
		BudgetMain ret = new BudgetMain();
		TblExpense expense = new TblExpense();
		expense.setUserId(userId);
		expense.setCtgryCode(Const.CATEGORY_CODE_NOT_DEFINED.substring(0, 2)); // 미분류는 소비 합계에서 제외한다.
		
		Integer count = expenseDao.getBudgetCount(userId);
		Long expenseSum = expenseDao.getExpenseSum(expense);
		Long budgetSum = expenseDao.getBudgetSum(userId);
		
		if(expenseSum == null) {
			expenseSum = 0L;
		}

		if(budgetSum == null) {
			budgetSum = 0L;
		}
		
		ret.setCount(count);
		ret.setBudgetSum(String.valueOf(budgetSum));
		ret.setExpenseSum(String.valueOf(expenseSum));
		
		return ret;
	}
	
	/**
	 * 카테고리 별 소비 분류 정보를 가져온다.
	 * @param userId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	private List<Budget> getExpenseCategoryListInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		
		List<Budget> ret = expenseDao.getExpenseCategory(userId);
		
		return ret;
	}
	
	/**
	 * 소비 방법 별 소비액 합계 정보를 가져온다.
	 * @param userId
	 * @return
	 * @throws MySQLIntegrityConstraintViolationException
	 * @throws RuntimeException
	 */
	private List<ExpensePaytype> getExpensePaytypeListInfo(String userId) throws MySQLIntegrityConstraintViolationException, RuntimeException {
		logger.info("userId = {}", userId);
		
		List<ExpensePaytype> ret = expenseDao.getExpensePaytype(userId);
		
		return ret;
	}
	
}
