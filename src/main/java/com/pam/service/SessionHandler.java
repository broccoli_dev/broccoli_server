package com.pam.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pam.security.Contents;
import com.pam.security.Session;
import com.pam.service.SecureSessionService;
import com.pam.util.Global;

@Service
public class SessionHandler {
	@Autowired
	SecureSessionService secureSessionService;
	
	public void updateSessionInfo(String httpSessionId, String sessionId, Contents contents) {
		String type = contents.getType();	
		Session session = secureSessionService.readSession(httpSessionId);
		
		if(session != null) {
			// sending serverHello
			if(type.equals("02")) {
				session.setCurrentStatus(type);
				String serverRandom = contents.getRandom();
				session.setServerRandom(serverRandom);
				String cipherSuitesSelected = contents.getCipherSuitesSelected();
				String secretAlg = Global.secritKeyEncryptionAlgorithm.get(cipherSuitesSelected.substring(0, 2));
				session.setSecretKeyAlgorithm(secretAlg);
				String publicAlg = Global.publicKeyEncryptionAlgorithm.get(cipherSuitesSelected.substring(2, 4));
				session.setPublicKeyAlgorithm(publicAlg);
				String hashAlg = Global.hashAlgorithm.get(cipherSuitesSelected.substring(4, 6));
				session.setHashAlgorithm(hashAlg);
				String serverName = contents.getServerName();
				session.setServerName(serverName);
				
				secureSessionService.write(httpSessionId, session);
			} else if(type.equals("03")) {
				session.setCurrentStatus(type);
				String certificate = contents.getCertificate();
				session.setCertificate(certificate);

				secureSessionService.write(httpSessionId, session);
			}
		} else {
			// received clientHello
			if(type.equals("01")) {
				Session _session = new Session();
				_session.setCurrentStatus(type);
				_session.setSessionId(sessionId);
				String clientRandom = contents.getRandom();
				_session.setClientRandom(clientRandom);
				List<String> cipherSuitesList = contents.getCipherSuitesList();
				String cipherSuitesSelected = cipherSuitesList.get(0);
				String secretAlg = Global.secritKeyEncryptionAlgorithm.get(cipherSuitesSelected.substring(0, 2));
				_session.setSecretKeyAlgorithm(secretAlg);
				String publicAlg = Global.secritKeyEncryptionAlgorithm.get(cipherSuitesSelected.substring(2, 4));
				_session.setPublicKeyAlgorithm(publicAlg);
				String hashAlg = Global.secritKeyEncryptionAlgorithm.get(cipherSuitesSelected.substring(4, 6));
				_session.setHashAlgorithm(hashAlg);
				
				secureSessionService.write(httpSessionId, _session);
			}
		}
	}
	
	public void updatePrivateKey(String httpSessionId, String privateKey) {
		Session session = secureSessionService.readSession(httpSessionId);		
		
		if(session != null) {
			session.setPrivateKey(privateKey);
			secureSessionService.write(httpSessionId, session);
		}
	}
	
	public void updateSessionKey(String httpSessionId, String sessionKey, String type) {	
		Session session = secureSessionService.readSession(httpSessionId);		
		
		if(session != null) {
			session.setSessionKey(sessionKey);
			session.setCurrentStatus(type);			
			secureSessionService.write(httpSessionId, session);
		}		
	}
	
	public void updateCurrentStatus(String httpSessionId, String currentStatus) {		
		Session session = secureSessionService.readSession(httpSessionId);		
		
		if(session != null) {
			session.setCurrentStatus(currentStatus);
			secureSessionService.write(httpSessionId, session);
		}		
	}
	
	public void updateHttpSessionTimeout(String httpSessionId, Integer timeout) {		
		Session session = secureSessionService.readSession(httpSessionId);		
		
		if(session != null) {
			session.setTimeout(timeout);
			secureSessionService.write(httpSessionId, session);
		}
	}
}
