package com.pam.context;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.pam.dao.CategoryTagDao;
import com.pam.dao.UserDao;
import com.pam.util.Global;

public class PamContext {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    UserDao userDao;
    
    @Autowired
    CategoryTagDao categoryTagDao;
    /*
    @Autowired
    NotificationService notificationService;*/
	    
	public void initialize() {
        logger.info("Server starts");
		Global.tagList = categoryTagDao.getAllTag();
        Global.mcList = categoryTagDao.getAllMoneyCalendar();
        /*try {
			notificationService.makeNotification("e9cab3b0d8c7b9a494c69ab17f0e5dc13bda50927b6f342a1330c4eb56bde33f");
		} catch (MySQLIntegrityConstraintViolationException | RuntimeException e) {
			// TODO Auto-generated catch block
			logger.info("error = {}", e.toString());
		}*/
    	
		Global.secritKeyEncryptionAlgorithm = new HashMap<String, String>();
		Global.secritKeyEncryptionAlgorithm.put("01", "AES/ECB/PKCS5Padding");
				
		Global.publicKeyEncryptionAlgorithm = new HashMap<String, String>();
		Global.publicKeyEncryptionAlgorithm.put("01", "RSA");
		
		Global.hashAlgorithm = new HashMap<String, String>();
		Global.hashAlgorithm.put("01", "MD5");
		Global.hashAlgorithm.put("02", "SHA1");
		Global.hashAlgorithm.put("03", "SHA256");
	}
	
	public void finalize() {
		
	}
}
