package com.pam.queue;

import java.util.Map;

public interface MQService {
	public void send(Map<String, ?> data);
	public void receive(Map<String, ?> data);
}
