package com.pam.queue;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.connection.CachingConnectionFactory;

public class MQExceptionListener implements ExceptionListener 
{
    private final Logger logger = LoggerFactory.getLogger(getClass());
	private CachingConnectionFactory cachingConnectionFactory;

	public void onException(JMSException jmse) {
		if (logger.isDebugEnabled())
			logger.debug("onException() is called!");
		cachingConnectionFactory.onException(jmse);
	}

	public CachingConnectionFactory getCachingConnectionFactory() {
		return cachingConnectionFactory;
	}

	public void setCachingConnectionFactory(
			CachingConnectionFactory cachingConnectionFactory) {
		this.cachingConnectionFactory = cachingConnectionFactory;
	}
}
