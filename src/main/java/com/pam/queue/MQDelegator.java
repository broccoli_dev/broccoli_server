package com.pam.queue;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MQDelegator 
{
    private final Logger logger = LoggerFactory.getLogger(getClass());
	private MQService mqService;
	
	public void setMqService(MQService mqService) {
		this.mqService = mqService;
	}

	public void handleMessage(Map<String, ?> data) 
	{
		try {
		   if (logger.isDebugEnabled())
		      logger.debug("Queue 에 등록된 메세지 : " + data.toString());
			
			Thread.sleep(100);
			
			mqService.receive(data);
		} catch (InterruptedException e) {
			logger.info("error = {}", e.toString());
		} catch (Exception e) {
			logger.info("error = {}", e.toString());
		}
	}
}
