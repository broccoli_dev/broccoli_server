package com.pam.mapper;

import com.pam.mapper.domain.TblCardLimit;
import com.pam.mapper.domain.TblCardLimitExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TblCardLimitMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card_limit
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int countByExample(TblCardLimitExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card_limit
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByExample(TblCardLimitExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card_limit
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByPrimaryKey(Long limitId);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card_limit
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insert(TblCardLimit record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card_limit
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insertSelective(TblCardLimit record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card_limit
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */List<TblCardLimit> selectByExample(TblCardLimitExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card_limit
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	TblCardLimit selectByPrimaryKey(Long limitId);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card_limit
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExampleSelective(@Param("record") TblCardLimit record,@Param("example") TblCardLimitExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card_limit
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExample(@Param("record") TblCardLimit record,@Param("example") TblCardLimitExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card_limit
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int updateByPrimaryKeySelective(TblCardLimit record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card_limit
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int updateByPrimaryKey(TblCardLimit record);
}