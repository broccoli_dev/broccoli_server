package com.pam.mapper;

import com.pam.mapper.domain.TblBudgetList;
import com.pam.mapper.domain.TblBudgetListExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TblBudgetListMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_budget_list
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int countByExample(TblBudgetListExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_budget_list
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByExample(TblBudgetListExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_budget_list
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByPrimaryKey(Long listId);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_budget_list
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insert(TblBudgetList record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_budget_list
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insertSelective(TblBudgetList record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_budget_list
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */List<TblBudgetList> selectByExample(TblBudgetListExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_budget_list
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	TblBudgetList selectByPrimaryKey(Long listId);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_budget_list
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExampleSelective(@Param("record") TblBudgetList record,@Param("example") TblBudgetListExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_budget_list
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExample(@Param("record") TblBudgetList record,@Param("example") TblBudgetListExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_budget_list
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int updateByPrimaryKeySelective(TblBudgetList record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_budget_list
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int updateByPrimaryKey(TblBudgetList record);
}