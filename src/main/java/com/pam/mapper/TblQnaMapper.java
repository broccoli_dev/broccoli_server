package com.pam.mapper;

import com.pam.mapper.domain.TblQna;
import com.pam.mapper.domain.TblQnaExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TblQnaMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_qna
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int countByExample(TblQnaExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_qna
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByExample(TblQnaExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_qna
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByPrimaryKey(Long qnaId);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_qna
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insert(TblQna record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_qna
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insertSelective(TblQna record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_qna
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */List<TblQna> selectByExample(TblQnaExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_qna
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	TblQna selectByPrimaryKey(Long qnaId);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_qna
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExampleSelective(@Param("record") TblQna record,@Param("example") TblQnaExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_qna
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExample(@Param("record") TblQna record,@Param("example") TblQnaExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_qna
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int updateByPrimaryKeySelective(TblQna record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_qna
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int updateByPrimaryKey(TblQna record);
}