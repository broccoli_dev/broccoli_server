package com.pam.mapper;

import com.pam.mapper.domain.TblChallengeType;
import com.pam.mapper.domain.TblChallengeTypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TblChallengeTypeMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_challenge_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int countByExample(TblChallengeTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_challenge_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByExample(TblChallengeTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_challenge_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByPrimaryKey(String chlngType);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_challenge_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insert(TblChallengeType record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_challenge_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insertSelective(TblChallengeType record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_challenge_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */List<TblChallengeType> selectByExample(TblChallengeTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_challenge_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	TblChallengeType selectByPrimaryKey(String chlngType);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_challenge_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExampleSelective(@Param("record") TblChallengeType record,@Param("example") TblChallengeTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_challenge_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExample(@Param("record") TblChallengeType record,@Param("example") TblChallengeTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_challenge_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int updateByPrimaryKeySelective(TblChallengeType record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_challenge_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int updateByPrimaryKey(TblChallengeType record);
}