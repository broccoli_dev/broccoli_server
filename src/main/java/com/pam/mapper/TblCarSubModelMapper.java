package com.pam.mapper;

import com.pam.mapper.domain.TblCarSubModel;
import com.pam.mapper.domain.TblCarSubModelExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TblCarSubModelMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_sub_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	int countByExample(TblCarSubModelExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_sub_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	int deleteByExample(TblCarSubModelExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_sub_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	int deleteByPrimaryKey(Long subCode);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_sub_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	int insert(TblCarSubModel record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_sub_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	int insertSelective(TblCarSubModel record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_sub_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	List<TblCarSubModel> selectByExample(TblCarSubModelExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_sub_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	TblCarSubModel selectByPrimaryKey(Long subCode);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_sub_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	int updateByExampleSelective(@Param("record") TblCarSubModel record,
			@Param("example") TblCarSubModelExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_sub_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	int updateByExample(@Param("record") TblCarSubModel record, @Param("example") TblCarSubModelExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_sub_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	int updateByPrimaryKeySelective(TblCarSubModel record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_sub_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	int updateByPrimaryKey(TblCarSubModel record);
}