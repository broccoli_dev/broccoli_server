package com.pam.mapper;

import com.pam.mapper.domain.CategoryTag;
import com.pam.mapper.domain.CategoryTagExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CategoryTagMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table category_tag
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int countByExample(CategoryTagExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table category_tag
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByExample(CategoryTagExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table category_tag
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insert(CategoryTag record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table category_tag
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insertSelective(CategoryTag record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table category_tag
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */List<CategoryTag> selectByExample(CategoryTagExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table category_tag
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExampleSelective(@Param("record") CategoryTag record,@Param("example") CategoryTagExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table category_tag
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExample(@Param("record") CategoryTag record,@Param("example") CategoryTagExample example);
}