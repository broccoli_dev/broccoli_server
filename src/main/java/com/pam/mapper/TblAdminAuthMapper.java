package com.pam.mapper;

import com.pam.mapper.domain.TblAdminAuth;
import com.pam.mapper.domain.TblAdminAuthExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TblAdminAuthMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_admin_auth
     *
     * @mbggenerated Tue May 31 17:57:54 KST 2016
     */
    int countByExample(TblAdminAuthExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_admin_auth
     *
     * @mbggenerated Tue May 31 17:57:54 KST 2016
     */
    int deleteByExample(TblAdminAuthExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_admin_auth
     *
     * @mbggenerated Tue May 31 17:57:54 KST 2016
     */
    int deleteByPrimaryKey(String authCode);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_admin_auth
     *
     * @mbggenerated Tue May 31 17:57:54 KST 2016
     */
    int insert(TblAdminAuth record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_admin_auth
     *
     * @mbggenerated Tue May 31 17:57:54 KST 2016
     */
    int insertSelective(TblAdminAuth record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_admin_auth
     *
     * @mbggenerated Tue May 31 17:57:54 KST 2016
     */
    List<TblAdminAuth> selectByExample(TblAdminAuthExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_admin_auth
     *
     * @mbggenerated Tue May 31 17:57:54 KST 2016
     */
    TblAdminAuth selectByPrimaryKey(String authCode);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_admin_auth
     *
     * @mbggenerated Tue May 31 17:57:54 KST 2016
     */
    int updateByExampleSelective(@Param("record") TblAdminAuth record, @Param("example") TblAdminAuthExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_admin_auth
     *
     * @mbggenerated Tue May 31 17:57:54 KST 2016
     */
    int updateByExample(@Param("record") TblAdminAuth record, @Param("example") TblAdminAuthExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_admin_auth
     *
     * @mbggenerated Tue May 31 17:57:54 KST 2016
     */
    int updateByPrimaryKeySelective(TblAdminAuth record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_admin_auth
     *
     * @mbggenerated Tue May 31 17:57:54 KST 2016
     */
    int updateByPrimaryKey(TblAdminAuth record);
}