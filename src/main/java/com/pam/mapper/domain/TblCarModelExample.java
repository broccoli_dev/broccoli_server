package com.pam.mapper.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

public class TblCarModelExample {
    /**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	protected String orderByClause;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	protected boolean distinct;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public TblCarModelExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andCarCodeIsNull() {
			addCriterion("car_code is null");
			return (Criteria) this;
		}

		public Criteria andCarCodeIsNotNull() {
			addCriterion("car_code is not null");
			return (Criteria) this;
		}

		public Criteria andCarCodeEqualTo(Long value) {
			addCriterion("car_code =", value, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeNotEqualTo(Long value) {
			addCriterion("car_code <>", value, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeGreaterThan(Long value) {
			addCriterion("car_code >", value, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeGreaterThanOrEqualTo(Long value) {
			addCriterion("car_code >=", value, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeLessThan(Long value) {
			addCriterion("car_code <", value, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeLessThanOrEqualTo(Long value) {
			addCriterion("car_code <=", value, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeIn(List<Long> values) {
			addCriterion("car_code in", values, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeNotIn(List<Long> values) {
			addCriterion("car_code not in", values, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeBetween(Long value1, Long value2) {
			addCriterion("car_code between", value1, value2, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeNotBetween(Long value1, Long value2) {
			addCriterion("car_code not between", value1, value2, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarNameIsNull() {
			addCriterion("car_name is null");
			return (Criteria) this;
		}

		public Criteria andCarNameIsNotNull() {
			addCriterion("car_name is not null");
			return (Criteria) this;
		}

		public Criteria andCarNameEqualTo(String value) {
			addCriterion("car_name =", value, "carName");
			return (Criteria) this;
		}

		public Criteria andCarNameNotEqualTo(String value) {
			addCriterion("car_name <>", value, "carName");
			return (Criteria) this;
		}

		public Criteria andCarNameGreaterThan(String value) {
			addCriterion("car_name >", value, "carName");
			return (Criteria) this;
		}

		public Criteria andCarNameGreaterThanOrEqualTo(String value) {
			addCriterion("car_name >=", value, "carName");
			return (Criteria) this;
		}

		public Criteria andCarNameLessThan(String value) {
			addCriterion("car_name <", value, "carName");
			return (Criteria) this;
		}

		public Criteria andCarNameLessThanOrEqualTo(String value) {
			addCriterion("car_name <=", value, "carName");
			return (Criteria) this;
		}

		public Criteria andCarNameLike(String value) {
			addCriterion("car_name like", value, "carName");
			return (Criteria) this;
		}

		public Criteria andCarNameNotLike(String value) {
			addCriterion("car_name not like", value, "carName");
			return (Criteria) this;
		}

		public Criteria andCarNameIn(List<String> values) {
			addCriterion("car_name in", values, "carName");
			return (Criteria) this;
		}

		public Criteria andCarNameNotIn(List<String> values) {
			addCriterion("car_name not in", values, "carName");
			return (Criteria) this;
		}

		public Criteria andCarNameBetween(String value1, String value2) {
			addCriterion("car_name between", value1, value2, "carName");
			return (Criteria) this;
		}

		public Criteria andCarNameNotBetween(String value1, String value2) {
			addCriterion("car_name not between", value1, value2, "carName");
			return (Criteria) this;
		}

		public Criteria andMakerCodeIsNull() {
			addCriterion("maker_code is null");
			return (Criteria) this;
		}

		public Criteria andMakerCodeIsNotNull() {
			addCriterion("maker_code is not null");
			return (Criteria) this;
		}

		public Criteria andMakerCodeEqualTo(Long value) {
			addCriterion("maker_code =", value, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeNotEqualTo(Long value) {
			addCriterion("maker_code <>", value, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeGreaterThan(Long value) {
			addCriterion("maker_code >", value, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeGreaterThanOrEqualTo(Long value) {
			addCriterion("maker_code >=", value, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeLessThan(Long value) {
			addCriterion("maker_code <", value, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeLessThanOrEqualTo(Long value) {
			addCriterion("maker_code <=", value, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeIn(List<Long> values) {
			addCriterion("maker_code in", values, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeNotIn(List<Long> values) {
			addCriterion("maker_code not in", values, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeBetween(Long value1, Long value2) {
			addCriterion("maker_code between", value1, value2, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeNotBetween(Long value1, Long value2) {
			addCriterion("maker_code not between", value1, value2, "makerCode");
			return (Criteria) this;
		}

		public Criteria andDbVerIsNull() {
			addCriterion("db_ver is null");
			return (Criteria) this;
		}

		public Criteria andDbVerIsNotNull() {
			addCriterion("db_ver is not null");
			return (Criteria) this;
		}

		public Criteria andDbVerEqualTo(String value) {
			addCriterion("db_ver =", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerNotEqualTo(String value) {
			addCriterion("db_ver <>", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerGreaterThan(String value) {
			addCriterion("db_ver >", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerGreaterThanOrEqualTo(String value) {
			addCriterion("db_ver >=", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerLessThan(String value) {
			addCriterion("db_ver <", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerLessThanOrEqualTo(String value) {
			addCriterion("db_ver <=", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerLike(String value) {
			addCriterion("db_ver like", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerNotLike(String value) {
			addCriterion("db_ver not like", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerIn(List<String> values) {
			addCriterion("db_ver in", values, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerNotIn(List<String> values) {
			addCriterion("db_ver not in", values, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerBetween(String value1, String value2) {
			addCriterion("db_ver between", value1, value2, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerNotBetween(String value1, String value2) {
			addCriterion("db_ver not between", value1, value2, "dbVer");
			return (Criteria) this;
		}

		public Criteria andRegDateIsNull() {
			addCriterion("reg_date is null");
			return (Criteria) this;
		}

		public Criteria andRegDateIsNotNull() {
			addCriterion("reg_date is not null");
			return (Criteria) this;
		}

		public Criteria andRegDateEqualTo(Date value) {
			addCriterion("reg_date =", value, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateNotEqualTo(Date value) {
			addCriterion("reg_date <>", value, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateGreaterThan(Date value) {
			addCriterion("reg_date >", value, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateGreaterThanOrEqualTo(Date value) {
			addCriterion("reg_date >=", value, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateLessThan(Date value) {
			addCriterion("reg_date <", value, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateLessThanOrEqualTo(Date value) {
			addCriterion("reg_date <=", value, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateIn(List<Date> values) {
			addCriterion("reg_date in", values, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateNotIn(List<Date> values) {
			addCriterion("reg_date not in", values, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateBetween(Date value1, Date value2) {
			addCriterion("reg_date between", value1, value2, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateNotBetween(Date value1, Date value2) {
			addCriterion("reg_date not between", value1, value2, "regDate");
			return (Criteria) this;
		}

		public Criteria andModDateIsNull() {
			addCriterion("mod_date is null");
			return (Criteria) this;
		}

		public Criteria andModDateIsNotNull() {
			addCriterion("mod_date is not null");
			return (Criteria) this;
		}

		public Criteria andModDateEqualTo(Date value) {
			addCriterion("mod_date =", value, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateNotEqualTo(Date value) {
			addCriterion("mod_date <>", value, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateGreaterThan(Date value) {
			addCriterion("mod_date >", value, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateGreaterThanOrEqualTo(Date value) {
			addCriterion("mod_date >=", value, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateLessThan(Date value) {
			addCriterion("mod_date <", value, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateLessThanOrEqualTo(Date value) {
			addCriterion("mod_date <=", value, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateIn(List<Date> values) {
			addCriterion("mod_date in", values, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateNotIn(List<Date> values) {
			addCriterion("mod_date not in", values, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateBetween(Date value1, Date value2) {
			addCriterion("mod_date between", value1, value2, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateNotBetween(Date value1, Date value2) {
			addCriterion("mod_date not between", value1, value2, "modDate");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table tbl_car_model
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}

	/**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table tbl_car_model
     *
     * @mbggenerated do_not_delete_during_merge Thu Feb 25 18:21:52 KST 2016
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }
}