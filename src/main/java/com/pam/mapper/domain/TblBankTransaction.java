package com.pam.mapper.domain;

import java.util.Date;

public class TblBankTransaction {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_bank_transaction.trans_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Long transId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_bank_transaction.accnt_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Long accntId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_bank_transaction.trans_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String transDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_bank_transaction.ctgry_code
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String ctgryCode;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_bank_transaction.sum
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Long sum;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_bank_transaction.trans_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Integer transType;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_bank_transaction.after_value
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Long afterValue;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_bank_transaction.oppnnt
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String oppnnt;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_bank_transaction.trans_method
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String transMethod;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_bank_transaction.dstrb
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String dstrb;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_bank_transaction.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Date regDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_bank_transaction.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Date modDate;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_bank_transaction.trans_id
	 * @return  the value of tbl_bank_transaction.trans_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Long getTransId() {
		return transId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_bank_transaction.trans_id
	 * @param transId  the value for tbl_bank_transaction.trans_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setTransId(Long transId) {
		this.transId = transId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_bank_transaction.accnt_id
	 * @return  the value of tbl_bank_transaction.accnt_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Long getAccntId() {
		return accntId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_bank_transaction.accnt_id
	 * @param accntId  the value for tbl_bank_transaction.accnt_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setAccntId(Long accntId) {
		this.accntId = accntId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_bank_transaction.trans_date
	 * @return  the value of tbl_bank_transaction.trans_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getTransDate() {
		return transDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_bank_transaction.trans_date
	 * @param transDate  the value for tbl_bank_transaction.trans_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_bank_transaction.ctgry_code
	 * @return  the value of tbl_bank_transaction.ctgry_code
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getCtgryCode() {
		return ctgryCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_bank_transaction.ctgry_code
	 * @param ctgryCode  the value for tbl_bank_transaction.ctgry_code
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setCtgryCode(String ctgryCode) {
		this.ctgryCode = ctgryCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_bank_transaction.sum
	 * @return  the value of tbl_bank_transaction.sum
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Long getSum() {
		return sum;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_bank_transaction.sum
	 * @param sum  the value for tbl_bank_transaction.sum
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setSum(Long sum) {
		this.sum = sum;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_bank_transaction.trans_type
	 * @return  the value of tbl_bank_transaction.trans_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Integer getTransType() {
		return transType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_bank_transaction.trans_type
	 * @param transType  the value for tbl_bank_transaction.trans_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setTransType(Integer transType) {
		this.transType = transType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_bank_transaction.after_value
	 * @return  the value of tbl_bank_transaction.after_value
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Long getAfterValue() {
		return afterValue;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_bank_transaction.after_value
	 * @param afterValue  the value for tbl_bank_transaction.after_value
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setAfterValue(Long afterValue) {
		this.afterValue = afterValue;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_bank_transaction.oppnnt
	 * @return  the value of tbl_bank_transaction.oppnnt
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getOppnnt() {
		return oppnnt;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_bank_transaction.oppnnt
	 * @param oppnnt  the value for tbl_bank_transaction.oppnnt
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setOppnnt(String oppnnt) {
		this.oppnnt = oppnnt;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_bank_transaction.trans_method
	 * @return  the value of tbl_bank_transaction.trans_method
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getTransMethod() {
		return transMethod;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_bank_transaction.trans_method
	 * @param transMethod  the value for tbl_bank_transaction.trans_method
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setTransMethod(String transMethod) {
		this.transMethod = transMethod;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_bank_transaction.dstrb
	 * @return  the value of tbl_bank_transaction.dstrb
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getDstrb() {
		return dstrb;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_bank_transaction.dstrb
	 * @param dstrb  the value for tbl_bank_transaction.dstrb
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setDstrb(String dstrb) {
		this.dstrb = dstrb;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_bank_transaction.reg_date
	 * @return  the value of tbl_bank_transaction.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Date getRegDate() {
		return regDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_bank_transaction.reg_date
	 * @param regDate  the value for tbl_bank_transaction.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_bank_transaction.mod_date
	 * @return  the value of tbl_bank_transaction.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Date getModDate() {
		return modDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_bank_transaction.mod_date
	 * @param modDate  the value for tbl_bank_transaction.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setModDate(Date modDate) {
		this.modDate = modDate;
	}
}