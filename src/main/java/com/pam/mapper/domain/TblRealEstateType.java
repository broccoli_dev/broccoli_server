package com.pam.mapper.domain;

import java.util.Date;

public class TblRealEstateType {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_real_estate_type.estate_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String estateType;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_real_estate_type.info
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String info;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_real_estate_type.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Date regDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_real_estate_type.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Date modDate;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_real_estate_type.estate_type
	 * @return  the value of tbl_real_estate_type.estate_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getEstateType() {
		return estateType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_real_estate_type.estate_type
	 * @param estateType  the value for tbl_real_estate_type.estate_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setEstateType(String estateType) {
		this.estateType = estateType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_real_estate_type.info
	 * @return  the value of tbl_real_estate_type.info
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getInfo() {
		return info;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_real_estate_type.info
	 * @param info  the value for tbl_real_estate_type.info
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setInfo(String info) {
		this.info = info;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_real_estate_type.reg_date
	 * @return  the value of tbl_real_estate_type.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Date getRegDate() {
		return regDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_real_estate_type.reg_date
	 * @param regDate  the value for tbl_real_estate_type.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_real_estate_type.mod_date
	 * @return  the value of tbl_real_estate_type.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Date getModDate() {
		return modDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_real_estate_type.mod_date
	 * @param modDate  the value for tbl_real_estate_type.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setModDate(Date modDate) {
		this.modDate = modDate;
	}
}