package com.pam.mapper.domain;

import java.util.Date;

public class TblCalendarPayType {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_calendar_pay_type.pay_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String payType;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_calendar_pay_type.name
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String name;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_calendar_pay_type.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Date regDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_calendar_pay_type.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Date modDate;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_calendar_pay_type.pay_type
	 * @return  the value of tbl_calendar_pay_type.pay_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getPayType() {
		return payType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_calendar_pay_type.pay_type
	 * @param payType  the value for tbl_calendar_pay_type.pay_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setPayType(String payType) {
		this.payType = payType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_calendar_pay_type.name
	 * @return  the value of tbl_calendar_pay_type.name
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getName() {
		return name;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_calendar_pay_type.name
	 * @param name  the value for tbl_calendar_pay_type.name
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_calendar_pay_type.reg_date
	 * @return  the value of tbl_calendar_pay_type.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Date getRegDate() {
		return regDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_calendar_pay_type.reg_date
	 * @param regDate  the value for tbl_calendar_pay_type.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_calendar_pay_type.mod_date
	 * @return  the value of tbl_calendar_pay_type.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Date getModDate() {
		return modDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_calendar_pay_type.mod_date
	 * @param modDate  the value for tbl_calendar_pay_type.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setModDate(Date modDate) {
		this.modDate = modDate;
	}
}