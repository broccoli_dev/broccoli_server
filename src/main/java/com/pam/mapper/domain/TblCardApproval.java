package com.pam.mapper.domain;

import java.util.Date;

public class TblCardApproval {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.aprvl_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Long aprvlId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.card_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Long cardId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.istm_yn
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String istmYn;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.aprvl_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String aprvlDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.aprvl_num
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String aprvlNum;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.aprvl_yn
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String aprvlYn;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.istm_mon
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Integer istmMon;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.aprvl_amt
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Long aprvlAmt;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.store_name
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String storeName;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.crncy_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String crncyType;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.store_num
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String storeNum;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.ctgry_code
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String ctgryCode;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.store_tel
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String storeTel;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.store_host_name
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String storeHostName;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Date regDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Date modDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_card_approval.store_addr
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String storeAddr;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.aprvl_id
	 * @return  the value of tbl_card_approval.aprvl_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Long getAprvlId() {
		return aprvlId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.aprvl_id
	 * @param aprvlId  the value for tbl_card_approval.aprvl_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setAprvlId(Long aprvlId) {
		this.aprvlId = aprvlId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.card_id
	 * @return  the value of tbl_card_approval.card_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Long getCardId() {
		return cardId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.card_id
	 * @param cardId  the value for tbl_card_approval.card_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setCardId(Long cardId) {
		this.cardId = cardId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.istm_yn
	 * @return  the value of tbl_card_approval.istm_yn
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getIstmYn() {
		return istmYn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.istm_yn
	 * @param istmYn  the value for tbl_card_approval.istm_yn
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setIstmYn(String istmYn) {
		this.istmYn = istmYn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.aprvl_date
	 * @return  the value of tbl_card_approval.aprvl_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getAprvlDate() {
		return aprvlDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.aprvl_date
	 * @param aprvlDate  the value for tbl_card_approval.aprvl_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setAprvlDate(String aprvlDate) {
		this.aprvlDate = aprvlDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.aprvl_num
	 * @return  the value of tbl_card_approval.aprvl_num
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getAprvlNum() {
		return aprvlNum;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.aprvl_num
	 * @param aprvlNum  the value for tbl_card_approval.aprvl_num
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setAprvlNum(String aprvlNum) {
		this.aprvlNum = aprvlNum;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.aprvl_yn
	 * @return  the value of tbl_card_approval.aprvl_yn
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getAprvlYn() {
		return aprvlYn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.aprvl_yn
	 * @param aprvlYn  the value for tbl_card_approval.aprvl_yn
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setAprvlYn(String aprvlYn) {
		this.aprvlYn = aprvlYn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.istm_mon
	 * @return  the value of tbl_card_approval.istm_mon
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Integer getIstmMon() {
		return istmMon;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.istm_mon
	 * @param istmMon  the value for tbl_card_approval.istm_mon
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setIstmMon(Integer istmMon) {
		this.istmMon = istmMon;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.aprvl_amt
	 * @return  the value of tbl_card_approval.aprvl_amt
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Long getAprvlAmt() {
		return aprvlAmt;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.aprvl_amt
	 * @param aprvlAmt  the value for tbl_card_approval.aprvl_amt
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setAprvlAmt(Long aprvlAmt) {
		this.aprvlAmt = aprvlAmt;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.store_name
	 * @return  the value of tbl_card_approval.store_name
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getStoreName() {
		return storeName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.store_name
	 * @param storeName  the value for tbl_card_approval.store_name
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.crncy_type
	 * @return  the value of tbl_card_approval.crncy_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getCrncyType() {
		return crncyType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.crncy_type
	 * @param crncyType  the value for tbl_card_approval.crncy_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setCrncyType(String crncyType) {
		this.crncyType = crncyType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.store_num
	 * @return  the value of tbl_card_approval.store_num
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getStoreNum() {
		return storeNum;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.store_num
	 * @param storeNum  the value for tbl_card_approval.store_num
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setStoreNum(String storeNum) {
		this.storeNum = storeNum;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.ctgry_code
	 * @return  the value of tbl_card_approval.ctgry_code
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getCtgryCode() {
		return ctgryCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.ctgry_code
	 * @param ctgryCode  the value for tbl_card_approval.ctgry_code
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setCtgryCode(String ctgryCode) {
		this.ctgryCode = ctgryCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.store_tel
	 * @return  the value of tbl_card_approval.store_tel
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getStoreTel() {
		return storeTel;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.store_tel
	 * @param storeTel  the value for tbl_card_approval.store_tel
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setStoreTel(String storeTel) {
		this.storeTel = storeTel;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.store_host_name
	 * @return  the value of tbl_card_approval.store_host_name
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getStoreHostName() {
		return storeHostName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.store_host_name
	 * @param storeHostName  the value for tbl_card_approval.store_host_name
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setStoreHostName(String storeHostName) {
		this.storeHostName = storeHostName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.reg_date
	 * @return  the value of tbl_card_approval.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Date getRegDate() {
		return regDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.reg_date
	 * @param regDate  the value for tbl_card_approval.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.mod_date
	 * @return  the value of tbl_card_approval.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Date getModDate() {
		return modDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.mod_date
	 * @param modDate  the value for tbl_card_approval.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setModDate(Date modDate) {
		this.modDate = modDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_card_approval.store_addr
	 * @return  the value of tbl_card_approval.store_addr
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getStoreAddr() {
		return storeAddr;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_card_approval.store_addr
	 * @param storeAddr  the value for tbl_card_approval.store_addr
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setStoreAddr(String storeAddr) {
		this.storeAddr = storeAddr;
	}
}