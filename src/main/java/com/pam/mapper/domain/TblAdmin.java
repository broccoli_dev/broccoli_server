package com.pam.mapper.domain;

import java.util.Date;

public class TblAdmin {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_admin.admin_id
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	private String adminId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_admin.password
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	private String password;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_admin.auth_code
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	private String authCode;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_admin.reg_date
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	private Date regDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_admin.mod_date
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	private Date modDate;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_admin.admin_id
	 * @return  the value of tbl_admin.admin_id
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	public String getAdminId() {
		return adminId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_admin.admin_id
	 * @param adminId  the value for tbl_admin.admin_id
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_admin.password
	 * @return  the value of tbl_admin.password
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_admin.password
	 * @param password  the value for tbl_admin.password
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_admin.auth_code
	 * @return  the value of tbl_admin.auth_code
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	public String getAuthCode() {
		return authCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_admin.auth_code
	 * @param authCode  the value for tbl_admin.auth_code
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_admin.reg_date
	 * @return  the value of tbl_admin.reg_date
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	public Date getRegDate() {
		return regDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_admin.reg_date
	 * @param regDate  the value for tbl_admin.reg_date
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_admin.mod_date
	 * @return  the value of tbl_admin.mod_date
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	public Date getModDate() {
		return modDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_admin.mod_date
	 * @param modDate  the value for tbl_admin.mod_date
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	public void setModDate(Date modDate) {
		this.modDate = modDate;
	}
}