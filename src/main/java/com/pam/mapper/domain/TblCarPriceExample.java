package com.pam.mapper.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

public class TblCarPriceExample {
    /**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	protected String orderByClause;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	protected boolean distinct;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	protected List<Criteria> oredCriteria;

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public TblCarPriceExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public String getOrderByClause() {
		return orderByClause;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public boolean isDistinct() {
		return distinct;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	protected abstract static class GeneratedCriteria {
		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andPriceCodeIsNull() {
			addCriterion("price_code is null");
			return (Criteria) this;
		}

		public Criteria andPriceCodeIsNotNull() {
			addCriterion("price_code is not null");
			return (Criteria) this;
		}

		public Criteria andPriceCodeEqualTo(Long value) {
			addCriterion("price_code =", value, "priceCode");
			return (Criteria) this;
		}

		public Criteria andPriceCodeNotEqualTo(Long value) {
			addCriterion("price_code <>", value, "priceCode");
			return (Criteria) this;
		}

		public Criteria andPriceCodeGreaterThan(Long value) {
			addCriterion("price_code >", value, "priceCode");
			return (Criteria) this;
		}

		public Criteria andPriceCodeGreaterThanOrEqualTo(Long value) {
			addCriterion("price_code >=", value, "priceCode");
			return (Criteria) this;
		}

		public Criteria andPriceCodeLessThan(Long value) {
			addCriterion("price_code <", value, "priceCode");
			return (Criteria) this;
		}

		public Criteria andPriceCodeLessThanOrEqualTo(Long value) {
			addCriterion("price_code <=", value, "priceCode");
			return (Criteria) this;
		}

		public Criteria andPriceCodeIn(List<Long> values) {
			addCriterion("price_code in", values, "priceCode");
			return (Criteria) this;
		}

		public Criteria andPriceCodeNotIn(List<Long> values) {
			addCriterion("price_code not in", values, "priceCode");
			return (Criteria) this;
		}

		public Criteria andPriceCodeBetween(Long value1, Long value2) {
			addCriterion("price_code between", value1, value2, "priceCode");
			return (Criteria) this;
		}

		public Criteria andPriceCodeNotBetween(Long value1, Long value2) {
			addCriterion("price_code not between", value1, value2, "priceCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeIsNull() {
			addCriterion("maker_code is null");
			return (Criteria) this;
		}

		public Criteria andMakerCodeIsNotNull() {
			addCriterion("maker_code is not null");
			return (Criteria) this;
		}

		public Criteria andMakerCodeEqualTo(Long value) {
			addCriterion("maker_code =", value, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeNotEqualTo(Long value) {
			addCriterion("maker_code <>", value, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeGreaterThan(Long value) {
			addCriterion("maker_code >", value, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeGreaterThanOrEqualTo(Long value) {
			addCriterion("maker_code >=", value, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeLessThan(Long value) {
			addCriterion("maker_code <", value, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeLessThanOrEqualTo(Long value) {
			addCriterion("maker_code <=", value, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeIn(List<Long> values) {
			addCriterion("maker_code in", values, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeNotIn(List<Long> values) {
			addCriterion("maker_code not in", values, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeBetween(Long value1, Long value2) {
			addCriterion("maker_code between", value1, value2, "makerCode");
			return (Criteria) this;
		}

		public Criteria andMakerCodeNotBetween(Long value1, Long value2) {
			addCriterion("maker_code not between", value1, value2, "makerCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeIsNull() {
			addCriterion("car_code is null");
			return (Criteria) this;
		}

		public Criteria andCarCodeIsNotNull() {
			addCriterion("car_code is not null");
			return (Criteria) this;
		}

		public Criteria andCarCodeEqualTo(Long value) {
			addCriterion("car_code =", value, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeNotEqualTo(Long value) {
			addCriterion("car_code <>", value, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeGreaterThan(Long value) {
			addCriterion("car_code >", value, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeGreaterThanOrEqualTo(Long value) {
			addCriterion("car_code >=", value, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeLessThan(Long value) {
			addCriterion("car_code <", value, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeLessThanOrEqualTo(Long value) {
			addCriterion("car_code <=", value, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeIn(List<Long> values) {
			addCriterion("car_code in", values, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeNotIn(List<Long> values) {
			addCriterion("car_code not in", values, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeBetween(Long value1, Long value2) {
			addCriterion("car_code between", value1, value2, "carCode");
			return (Criteria) this;
		}

		public Criteria andCarCodeNotBetween(Long value1, Long value2) {
			addCriterion("car_code not between", value1, value2, "carCode");
			return (Criteria) this;
		}

		public Criteria andSubCodeIsNull() {
			addCriterion("sub_code is null");
			return (Criteria) this;
		}

		public Criteria andSubCodeIsNotNull() {
			addCriterion("sub_code is not null");
			return (Criteria) this;
		}

		public Criteria andSubCodeEqualTo(Long value) {
			addCriterion("sub_code =", value, "subCode");
			return (Criteria) this;
		}

		public Criteria andSubCodeNotEqualTo(Long value) {
			addCriterion("sub_code <>", value, "subCode");
			return (Criteria) this;
		}

		public Criteria andSubCodeGreaterThan(Long value) {
			addCriterion("sub_code >", value, "subCode");
			return (Criteria) this;
		}

		public Criteria andSubCodeGreaterThanOrEqualTo(Long value) {
			addCriterion("sub_code >=", value, "subCode");
			return (Criteria) this;
		}

		public Criteria andSubCodeLessThan(Long value) {
			addCriterion("sub_code <", value, "subCode");
			return (Criteria) this;
		}

		public Criteria andSubCodeLessThanOrEqualTo(Long value) {
			addCriterion("sub_code <=", value, "subCode");
			return (Criteria) this;
		}

		public Criteria andSubCodeIn(List<Long> values) {
			addCriterion("sub_code in", values, "subCode");
			return (Criteria) this;
		}

		public Criteria andSubCodeNotIn(List<Long> values) {
			addCriterion("sub_code not in", values, "subCode");
			return (Criteria) this;
		}

		public Criteria andSubCodeBetween(Long value1, Long value2) {
			addCriterion("sub_code between", value1, value2, "subCode");
			return (Criteria) this;
		}

		public Criteria andSubCodeNotBetween(Long value1, Long value2) {
			addCriterion("sub_code not between", value1, value2, "subCode");
			return (Criteria) this;
		}

		public Criteria andMadeYearIsNull() {
			addCriterion("made_year is null");
			return (Criteria) this;
		}

		public Criteria andMadeYearIsNotNull() {
			addCriterion("made_year is not null");
			return (Criteria) this;
		}

		public Criteria andMadeYearEqualTo(String value) {
			addCriterion("made_year =", value, "madeYear");
			return (Criteria) this;
		}

		public Criteria andMadeYearNotEqualTo(String value) {
			addCriterion("made_year <>", value, "madeYear");
			return (Criteria) this;
		}

		public Criteria andMadeYearGreaterThan(String value) {
			addCriterion("made_year >", value, "madeYear");
			return (Criteria) this;
		}

		public Criteria andMadeYearGreaterThanOrEqualTo(String value) {
			addCriterion("made_year >=", value, "madeYear");
			return (Criteria) this;
		}

		public Criteria andMadeYearLessThan(String value) {
			addCriterion("made_year <", value, "madeYear");
			return (Criteria) this;
		}

		public Criteria andMadeYearLessThanOrEqualTo(String value) {
			addCriterion("made_year <=", value, "madeYear");
			return (Criteria) this;
		}

		public Criteria andMadeYearLike(String value) {
			addCriterion("made_year like", value, "madeYear");
			return (Criteria) this;
		}

		public Criteria andMadeYearNotLike(String value) {
			addCriterion("made_year not like", value, "madeYear");
			return (Criteria) this;
		}

		public Criteria andMadeYearIn(List<String> values) {
			addCriterion("made_year in", values, "madeYear");
			return (Criteria) this;
		}

		public Criteria andMadeYearNotIn(List<String> values) {
			addCriterion("made_year not in", values, "madeYear");
			return (Criteria) this;
		}

		public Criteria andMadeYearBetween(String value1, String value2) {
			addCriterion("made_year between", value1, value2, "madeYear");
			return (Criteria) this;
		}

		public Criteria andMadeYearNotBetween(String value1, String value2) {
			addCriterion("made_year not between", value1, value2, "madeYear");
			return (Criteria) this;
		}

		public Criteria andMrktPriceHighIsNull() {
			addCriterion("mrkt_price_high is null");
			return (Criteria) this;
		}

		public Criteria andMrktPriceHighIsNotNull() {
			addCriterion("mrkt_price_high is not null");
			return (Criteria) this;
		}

		public Criteria andMrktPriceHighEqualTo(Long value) {
			addCriterion("mrkt_price_high =", value, "mrktPriceHigh");
			return (Criteria) this;
		}

		public Criteria andMrktPriceHighNotEqualTo(Long value) {
			addCriterion("mrkt_price_high <>", value, "mrktPriceHigh");
			return (Criteria) this;
		}

		public Criteria andMrktPriceHighGreaterThan(Long value) {
			addCriterion("mrkt_price_high >", value, "mrktPriceHigh");
			return (Criteria) this;
		}

		public Criteria andMrktPriceHighGreaterThanOrEqualTo(Long value) {
			addCriterion("mrkt_price_high >=", value, "mrktPriceHigh");
			return (Criteria) this;
		}

		public Criteria andMrktPriceHighLessThan(Long value) {
			addCriterion("mrkt_price_high <", value, "mrktPriceHigh");
			return (Criteria) this;
		}

		public Criteria andMrktPriceHighLessThanOrEqualTo(Long value) {
			addCriterion("mrkt_price_high <=", value, "mrktPriceHigh");
			return (Criteria) this;
		}

		public Criteria andMrktPriceHighIn(List<Long> values) {
			addCriterion("mrkt_price_high in", values, "mrktPriceHigh");
			return (Criteria) this;
		}

		public Criteria andMrktPriceHighNotIn(List<Long> values) {
			addCriterion("mrkt_price_high not in", values, "mrktPriceHigh");
			return (Criteria) this;
		}

		public Criteria andMrktPriceHighBetween(Long value1, Long value2) {
			addCriterion("mrkt_price_high between", value1, value2, "mrktPriceHigh");
			return (Criteria) this;
		}

		public Criteria andMrktPriceHighNotBetween(Long value1, Long value2) {
			addCriterion("mrkt_price_high not between", value1, value2, "mrktPriceHigh");
			return (Criteria) this;
		}

		public Criteria andMrktPriceMidIsNull() {
			addCriterion("mrkt_price_mid is null");
			return (Criteria) this;
		}

		public Criteria andMrktPriceMidIsNotNull() {
			addCriterion("mrkt_price_mid is not null");
			return (Criteria) this;
		}

		public Criteria andMrktPriceMidEqualTo(Long value) {
			addCriterion("mrkt_price_mid =", value, "mrktPriceMid");
			return (Criteria) this;
		}

		public Criteria andMrktPriceMidNotEqualTo(Long value) {
			addCriterion("mrkt_price_mid <>", value, "mrktPriceMid");
			return (Criteria) this;
		}

		public Criteria andMrktPriceMidGreaterThan(Long value) {
			addCriterion("mrkt_price_mid >", value, "mrktPriceMid");
			return (Criteria) this;
		}

		public Criteria andMrktPriceMidGreaterThanOrEqualTo(Long value) {
			addCriterion("mrkt_price_mid >=", value, "mrktPriceMid");
			return (Criteria) this;
		}

		public Criteria andMrktPriceMidLessThan(Long value) {
			addCriterion("mrkt_price_mid <", value, "mrktPriceMid");
			return (Criteria) this;
		}

		public Criteria andMrktPriceMidLessThanOrEqualTo(Long value) {
			addCriterion("mrkt_price_mid <=", value, "mrktPriceMid");
			return (Criteria) this;
		}

		public Criteria andMrktPriceMidIn(List<Long> values) {
			addCriterion("mrkt_price_mid in", values, "mrktPriceMid");
			return (Criteria) this;
		}

		public Criteria andMrktPriceMidNotIn(List<Long> values) {
			addCriterion("mrkt_price_mid not in", values, "mrktPriceMid");
			return (Criteria) this;
		}

		public Criteria andMrktPriceMidBetween(Long value1, Long value2) {
			addCriterion("mrkt_price_mid between", value1, value2, "mrktPriceMid");
			return (Criteria) this;
		}

		public Criteria andMrktPriceMidNotBetween(Long value1, Long value2) {
			addCriterion("mrkt_price_mid not between", value1, value2, "mrktPriceMid");
			return (Criteria) this;
		}

		public Criteria andDbVerIsNull() {
			addCriterion("db_ver is null");
			return (Criteria) this;
		}

		public Criteria andDbVerIsNotNull() {
			addCriterion("db_ver is not null");
			return (Criteria) this;
		}

		public Criteria andDbVerEqualTo(String value) {
			addCriterion("db_ver =", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerNotEqualTo(String value) {
			addCriterion("db_ver <>", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerGreaterThan(String value) {
			addCriterion("db_ver >", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerGreaterThanOrEqualTo(String value) {
			addCriterion("db_ver >=", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerLessThan(String value) {
			addCriterion("db_ver <", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerLessThanOrEqualTo(String value) {
			addCriterion("db_ver <=", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerLike(String value) {
			addCriterion("db_ver like", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerNotLike(String value) {
			addCriterion("db_ver not like", value, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerIn(List<String> values) {
			addCriterion("db_ver in", values, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerNotIn(List<String> values) {
			addCriterion("db_ver not in", values, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerBetween(String value1, String value2) {
			addCriterion("db_ver between", value1, value2, "dbVer");
			return (Criteria) this;
		}

		public Criteria andDbVerNotBetween(String value1, String value2) {
			addCriterion("db_ver not between", value1, value2, "dbVer");
			return (Criteria) this;
		}

		public Criteria andRegDateIsNull() {
			addCriterion("reg_date is null");
			return (Criteria) this;
		}

		public Criteria andRegDateIsNotNull() {
			addCriterion("reg_date is not null");
			return (Criteria) this;
		}

		public Criteria andRegDateEqualTo(Date value) {
			addCriterion("reg_date =", value, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateNotEqualTo(Date value) {
			addCriterion("reg_date <>", value, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateGreaterThan(Date value) {
			addCriterion("reg_date >", value, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateGreaterThanOrEqualTo(Date value) {
			addCriterion("reg_date >=", value, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateLessThan(Date value) {
			addCriterion("reg_date <", value, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateLessThanOrEqualTo(Date value) {
			addCriterion("reg_date <=", value, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateIn(List<Date> values) {
			addCriterion("reg_date in", values, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateNotIn(List<Date> values) {
			addCriterion("reg_date not in", values, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateBetween(Date value1, Date value2) {
			addCriterion("reg_date between", value1, value2, "regDate");
			return (Criteria) this;
		}

		public Criteria andRegDateNotBetween(Date value1, Date value2) {
			addCriterion("reg_date not between", value1, value2, "regDate");
			return (Criteria) this;
		}

		public Criteria andModDateIsNull() {
			addCriterion("mod_date is null");
			return (Criteria) this;
		}

		public Criteria andModDateIsNotNull() {
			addCriterion("mod_date is not null");
			return (Criteria) this;
		}

		public Criteria andModDateEqualTo(Date value) {
			addCriterion("mod_date =", value, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateNotEqualTo(Date value) {
			addCriterion("mod_date <>", value, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateGreaterThan(Date value) {
			addCriterion("mod_date >", value, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateGreaterThanOrEqualTo(Date value) {
			addCriterion("mod_date >=", value, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateLessThan(Date value) {
			addCriterion("mod_date <", value, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateLessThanOrEqualTo(Date value) {
			addCriterion("mod_date <=", value, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateIn(List<Date> values) {
			addCriterion("mod_date in", values, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateNotIn(List<Date> values) {
			addCriterion("mod_date not in", values, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateBetween(Date value1, Date value2) {
			addCriterion("mod_date between", value1, value2, "modDate");
			return (Criteria) this;
		}

		public Criteria andModDateNotBetween(Date value1, Date value2) {
			addCriterion("mod_date not between", value1, value2, "modDate");
			return (Criteria) this;
		}
	}

	/**
	 * This class was generated by MyBatis Generator. This class corresponds to the database table tbl_car_price
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public static class Criterion {
		private String condition;
		private Object value;
		private Object secondValue;
		private boolean noValue;
		private boolean singleValue;
		private boolean betweenValue;
		private boolean listValue;
		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			} else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}
	}

	/**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table tbl_car_price
     *
     * @mbggenerated do_not_delete_during_merge Thu Feb 25 18:21:52 KST 2016
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }
}