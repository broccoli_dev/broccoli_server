package com.pam.mapper.domain;

import java.util.Date;

public class TblChallenge {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_challenge.chlng_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Long chlngId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_challenge.user_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String userId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_challenge.chlng_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String chlngType;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_challenge.title
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String title;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_challenge.goal_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String goalDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_challenge.accnt_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Long accntId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_challenge.goal_amt
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Long goalAmt;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_challenge.monthly_amt
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Long monthlyAmt;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_challenge.cmplt_yn
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String cmpltYn;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_challenge.cmplt_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String cmpltDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_challenge.image_path
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private String imagePath;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_challenge.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Date regDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_challenge.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	private Date modDate;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_challenge.chlng_id
	 * @return  the value of tbl_challenge.chlng_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Long getChlngId() {
		return chlngId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_challenge.chlng_id
	 * @param chlngId  the value for tbl_challenge.chlng_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setChlngId(Long chlngId) {
		this.chlngId = chlngId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_challenge.user_id
	 * @return  the value of tbl_challenge.user_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_challenge.user_id
	 * @param userId  the value for tbl_challenge.user_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_challenge.chlng_type
	 * @return  the value of tbl_challenge.chlng_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getChlngType() {
		return chlngType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_challenge.chlng_type
	 * @param chlngType  the value for tbl_challenge.chlng_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setChlngType(String chlngType) {
		this.chlngType = chlngType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_challenge.title
	 * @return  the value of tbl_challenge.title
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_challenge.title
	 * @param title  the value for tbl_challenge.title
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_challenge.goal_date
	 * @return  the value of tbl_challenge.goal_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getGoalDate() {
		return goalDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_challenge.goal_date
	 * @param goalDate  the value for tbl_challenge.goal_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setGoalDate(String goalDate) {
		this.goalDate = goalDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_challenge.accnt_id
	 * @return  the value of tbl_challenge.accnt_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Long getAccntId() {
		return accntId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_challenge.accnt_id
	 * @param accntId  the value for tbl_challenge.accnt_id
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setAccntId(Long accntId) {
		this.accntId = accntId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_challenge.goal_amt
	 * @return  the value of tbl_challenge.goal_amt
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Long getGoalAmt() {
		return goalAmt;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_challenge.goal_amt
	 * @param goalAmt  the value for tbl_challenge.goal_amt
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setGoalAmt(Long goalAmt) {
		this.goalAmt = goalAmt;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_challenge.monthly_amt
	 * @return  the value of tbl_challenge.monthly_amt
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Long getMonthlyAmt() {
		return monthlyAmt;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_challenge.monthly_amt
	 * @param monthlyAmt  the value for tbl_challenge.monthly_amt
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setMonthlyAmt(Long monthlyAmt) {
		this.monthlyAmt = monthlyAmt;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_challenge.cmplt_yn
	 * @return  the value of tbl_challenge.cmplt_yn
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getCmpltYn() {
		return cmpltYn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_challenge.cmplt_yn
	 * @param cmpltYn  the value for tbl_challenge.cmplt_yn
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setCmpltYn(String cmpltYn) {
		this.cmpltYn = cmpltYn;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_challenge.cmplt_date
	 * @return  the value of tbl_challenge.cmplt_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getCmpltDate() {
		return cmpltDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_challenge.cmplt_date
	 * @param cmpltDate  the value for tbl_challenge.cmplt_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setCmpltDate(String cmpltDate) {
		this.cmpltDate = cmpltDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_challenge.image_path
	 * @return  the value of tbl_challenge.image_path
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_challenge.image_path
	 * @param imagePath  the value for tbl_challenge.image_path
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_challenge.reg_date
	 * @return  the value of tbl_challenge.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Date getRegDate() {
		return regDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_challenge.reg_date
	 * @param regDate  the value for tbl_challenge.reg_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_challenge.mod_date
	 * @return  the value of tbl_challenge.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public Date getModDate() {
		return modDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_challenge.mod_date
	 * @param modDate  the value for tbl_challenge.mod_date
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	public void setModDate(Date modDate) {
		this.modDate = modDate;
	}
}