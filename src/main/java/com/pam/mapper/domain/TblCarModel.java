package com.pam.mapper.domain;

import java.util.Date;

public class TblCarModel {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_car_model.car_code
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	private Long carCode;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_car_model.car_name
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	private String carName;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_car_model.maker_code
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	private Long makerCode;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_car_model.db_ver
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	private String dbVer;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_car_model.reg_date
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	private Date regDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column tbl_car_model.mod_date
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	private Date modDate;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_car_model.car_code
	 * @return  the value of tbl_car_model.car_code
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public Long getCarCode() {
		return carCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_car_model.car_code
	 * @param carCode  the value for tbl_car_model.car_code
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public void setCarCode(Long carCode) {
		this.carCode = carCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_car_model.car_name
	 * @return  the value of tbl_car_model.car_name
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public String getCarName() {
		return carName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_car_model.car_name
	 * @param carName  the value for tbl_car_model.car_name
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public void setCarName(String carName) {
		this.carName = carName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_car_model.maker_code
	 * @return  the value of tbl_car_model.maker_code
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public Long getMakerCode() {
		return makerCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_car_model.maker_code
	 * @param makerCode  the value for tbl_car_model.maker_code
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public void setMakerCode(Long makerCode) {
		this.makerCode = makerCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_car_model.db_ver
	 * @return  the value of tbl_car_model.db_ver
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public String getDbVer() {
		return dbVer;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_car_model.db_ver
	 * @param dbVer  the value for tbl_car_model.db_ver
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public void setDbVer(String dbVer) {
		this.dbVer = dbVer;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_car_model.reg_date
	 * @return  the value of tbl_car_model.reg_date
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public Date getRegDate() {
		return regDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_car_model.reg_date
	 * @param regDate  the value for tbl_car_model.reg_date
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column tbl_car_model.mod_date
	 * @return  the value of tbl_car_model.mod_date
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public Date getModDate() {
		return modDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column tbl_car_model.mod_date
	 * @param modDate  the value for tbl_car_model.mod_date
	 * @mbggenerated  Mon Feb 29 16:20:50 KST 2016
	 */
	public void setModDate(Date modDate) {
		this.modDate = modDate;
	}
}