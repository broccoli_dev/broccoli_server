package com.pam.mapper;

import com.pam.mapper.domain.TblCard;
import com.pam.mapper.domain.TblCardExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TblCardMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int countByExample(TblCardExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByExample(TblCardExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByPrimaryKey(Long cardId);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insert(TblCard record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insertSelective(TblCard record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */List<TblCard> selectByExample(TblCardExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	TblCard selectByPrimaryKey(Long cardId);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExampleSelective(@Param("record") TblCard record,@Param("example") TblCardExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExample(@Param("record") TblCard record,@Param("example") TblCardExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int updateByPrimaryKeySelective(TblCard record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_card
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int updateByPrimaryKey(TblCard record);
}