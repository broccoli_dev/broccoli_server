package com.pam.mapper;

import com.pam.mapper.domain.TblFinanceType;
import com.pam.mapper.domain.TblFinanceTypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TblFinanceTypeMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_finance_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int countByExample(TblFinanceTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_finance_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByExample(TblFinanceTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_finance_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByPrimaryKey(String fnncType);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_finance_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insert(TblFinanceType record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_finance_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insertSelective(TblFinanceType record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_finance_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */List<TblFinanceType> selectByExample(TblFinanceTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_finance_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	TblFinanceType selectByPrimaryKey(String fnncType);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_finance_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExampleSelective(@Param("record") TblFinanceType record,@Param("example") TblFinanceTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_finance_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExample(@Param("record") TblFinanceType record,@Param("example") TblFinanceTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_finance_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int updateByPrimaryKeySelective(TblFinanceType record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_finance_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int updateByPrimaryKey(TblFinanceType record);
}