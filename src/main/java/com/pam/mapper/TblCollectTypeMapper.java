package com.pam.mapper;

import com.pam.mapper.domain.TblCollectType;
import com.pam.mapper.domain.TblCollectTypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TblCollectTypeMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_collect_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int countByExample(TblCollectTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_collect_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByExample(TblCollectTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_collect_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int deleteByPrimaryKey(Integer cllctType);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_collect_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insert(TblCollectType record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_collect_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int insertSelective(TblCollectType record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_collect_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */List<TblCollectType> selectByExample(TblCollectTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_collect_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	TblCollectType selectByPrimaryKey(Integer cllctType);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_collect_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExampleSelective(@Param("record") TblCollectType record,@Param("example") TblCollectTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_collect_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */int updateByExample(@Param("record") TblCollectType record,@Param("example") TblCollectTypeExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_collect_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int updateByPrimaryKeySelective(TblCollectType record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_collect_type
	 * @mbggenerated  Wed Mar 30 15:57:54 KST 2016
	 */
	int updateByPrimaryKey(TblCollectType record);
}