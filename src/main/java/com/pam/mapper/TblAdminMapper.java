package com.pam.mapper;

import com.pam.mapper.domain.TblAdmin;
import com.pam.mapper.domain.TblAdminExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TblAdminMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_admin
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	int countByExample(TblAdminExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_admin
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	int deleteByExample(TblAdminExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_admin
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	int deleteByPrimaryKey(String adminId);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_admin
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	int insert(TblAdmin record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_admin
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	int insertSelective(TblAdmin record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_admin
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	List<TblAdmin> selectByExample(TblAdminExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_admin
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	TblAdmin selectByPrimaryKey(String adminId);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_admin
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	int updateByExampleSelective(@Param("record") TblAdmin record, @Param("example") TblAdminExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_admin
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	int updateByExample(@Param("record") TblAdmin record, @Param("example") TblAdminExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_admin
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	int updateByPrimaryKeySelective(TblAdmin record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table tbl_admin
	 * @mbggenerated  Tue May 31 17:57:54 KST 2016
	 */
	int updateByPrimaryKey(TblAdmin record);
}