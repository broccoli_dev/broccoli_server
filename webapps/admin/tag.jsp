<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Broccoli Admin Page</title>

    <!-- Bootstrap core CSS -->
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://cdn.datatables.net/t/dt/dt-1.10.11,rr-1.1.1/datatables.min.css" rel="stylesheet">
    <link href="/admin/css/bradmin.css" rel="stylesheet">
  </head>

  <body>
    <!-- top nav bar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/pam/admin/home">Broccoli Admin</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <!-- <li><a href="#user">회원관리</a></li>  -->
            <li class="active"><a href="/pam/admin/system">시스템관리</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span><span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#" data-toggle="modal" data-target="#change-pw-modal">비밀번호 변경</a></li>
                <li><a href="/pam/admin/logout">로그아웃</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


    <div class="container-fluid">
      <!-- container -->
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <!-- left nav -->
          <ul class="nav nav-sidebar">
            <li><a href="/pam/admin/system/store">스토어 관리</a></li>
            <li class="active"><a href="/pam/admin/system/tag">태그 관리 <span class="sr-only">(current)</span></a></li>
            <li><a href="/pam/admin/system/ncategory">미분류 카테고리 관리</a></li>
            <li><a href="/pam/admin/system/cardbenefit">카드 혜택 관리</a></li>
            <li><a href="/pam/admin/system/userdb">회원 DB 관리</a></li>
            <li><a href="/pam/admin/system/adminaccount">관리자 계정 관리</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <!-- main -->
          <div class="row table-icon">
            <div class="btn-group" role="group" aria-label="...">
              <button name="submit-change" type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-ok" aria-hidden="true" data-loading-text="Loading..."></span> 저장</button>
              <button name="revert-change" type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> 되돌리기</button>
            </div>
            <div class="btn-group" role="group" aria-label="...">
              <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#add-tag-modal" name="pop-add-tag">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 태그 추가
              </button>
            </div>
          </div>
          <div class="row">
            <table class="table" id="tag-table">
              <thead>
                <tr>
                  <th>태그 우선순위</th>
                  <th>태그</th>
                  <th>코드</th>
                  <th>카테고리 대분류</th>
                  <th>카테고리 소분류</th>
                  <th>머니캘린더</th>
                  <th>소비</th>
                  <th></th>
                  <th style="display:none;">i</th>
                </tr>
              </thead>
              <tbody><!-- tag table body --></tbody>
            </table>
          </div>
          <div class="row table-icon">
            <div class="btn-group" role="group" aria-label="...">
              <button name="submit-change" type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-ok" aria-hidden="true" data-loading-text="Loading..."></span> 저장</button>
              <button name="revert-change" type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> 되돌리기</button>
            </div>
            <div class="btn-group" role="group" aria-label="...">
              <button type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#add-tag-modal" name="pop-add-tag">
                <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 태그 추가
              </button>
            </div>
          </div>
        </div> <!-- /.main -->
      </div>
    </div><!-- /.container -->
    
    
    <!-- Modal -->
    <div class="modal fade" id="add-tag-modal" tabindex="-1" role="dialog" aria-labelledby="add-tag-ModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="add-tag-ModalLabel">태그 추가</h4>
          </div>
          <div class="modal-body">
            <div class="alert alert-danger" role="alert" id="new-tag-alert">
              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
              <span class="sr-only">Error:</span>
              alert
            </div>
            <form>
              <div class="form-group">
                <label for="new-tag-name">태그</label>
                <input type="text" class="form-control" id="new-tag-name" placeholder="태그 입력">
              </div>
              <div class="form-group">
                <label>코드</label>
                <p class="text-info" id="new-tag-code"></p>
              </div>
              <div class="form-group">
                <label for="new-tag-category-large">카테고리 대분류</label>
                <select id="new-tag-category-large" class="form-control">
                </select>
              </div>
              <div class="form-group">
                <label for="new-tag-category-small">카테고리 소분류</label>
                <select id="new-tag-category-small" class="form-control">
                </select>
              </div>
              <div class="form-group">
                <label for="new-tag-moneycal">머니캘린더</label>
                <select id="new-tag-moneycal" class="form-control">
                </select>
              </div>
              <div class="form-group">
                <label for="new-tag-expense">소비</label>
                <select id="new-tag-expense" class="form-control">
                </select>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
            <button type="button" class="btn btn-primary" id="add-tag" >추가</button>
          </div>
        </div>
      </div>
    </div> <!-- /.Modal -->
    
    <!-- Change PW Modal -->
    <div class="modal fade" id="change-pw-modal" tabindex="-1" role="dialog" aria-labelledby="change-pw-ModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="change-pw-ModalLabel">비밀번호 변경</h4>
          </div>
          <div class="modal-body">
            <div class="alert alert-danger" role="alert" id="change-pw-alert">
              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
              <span class="sr-only">Error:</span>
              alert
            </div>
            <form>
              <div class="form-group">
                <label for="now-pw">현재 비밀번호</label>
                <input type="password" class="form-control" id="now-pw" placeholder="현재 비밀번호 입력">
              </div>
              <div class="form-group">
                <label for="next-pw">새 비밀번호</label>
                <input type="password" class="form-control" id="new-pw" placeholder="새 비밀번호 입력">
              </div>
              <div class="form-group">
                <label for="next-pw-confirm">새 비밀번호 확인</label>
                <input type="password" class="form-control" id="new-pw-confirm" placeholder="새 비밀번호 확인">
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
            <button type="button" class="btn btn-primary" id="change-pw" >변경</button>
          </div>
        </div>
      </div>
    </div> <!-- /.Modal -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/t/dt/dt-1.10.11,rr-1.1.1/datatables.min.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script src="/admin/js/change_pw.js"></script>
    <script src="/admin/js/tag.js"></script>
  </body>
</html>
