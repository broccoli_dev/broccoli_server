<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Broccoli Admin Page</title>

    <!-- Bootstrap core CSS -->
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://cdn.datatables.net/t/dt/dt-1.10.11,rr-1.1.1/datatables.min.css" rel="stylesheet">
    <link href="/admin/css/bradmin.css" rel="stylesheet">
  </head>

  <body>
    <!-- top nav bar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/pam/admin/home">Broccoli Admin</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <!-- <li><a href="#user">회원관리</a></li>  -->
            <li class="active"><a href="/pam/admin/system">시스템관리</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span><span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#" data-toggle="modal" data-target="#change-pw-modal">비밀번호 변경</a></li>
                <li><a href="/pam/admin/logout">로그아웃</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


    <div class="container-fluid">
      <!-- container -->
      <div class="row">  
        <div class="col-sm-3 col-md-2 sidebar">
          <!-- left nav -->
          <ul class="nav nav-sidebar">
            <li><a href="/pam/admin/system/store">스토어 관리</a></li>
            <li><a href="/pam/admin/system/tag">태그 관리 </a></li>
            <li><a href="/pam/admin/system/ncategory">미분류 카테고리 관리</a></li>
            <li><a href="/pam/admin/system/cardbenefit">카드 혜택 관리</a></li>
            <li class="active"><a href="/pam/admin/system/userdb">회원 DB 관리<span class="sr-only">(current)</span></a></li>
            <li><a href="/pam/admin/system/adminaccount">관리자 계정 관리</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <!-- main -->
          <div class="jumbotron">
            <div class="form-horizontal">
              <div class="form-group form-inline">
                <label class="control-label col-md-1">연령</label>
                <div class="checkbox col-md-1">
                  <label>
                    <input type="checkbox" id="all-age-input" checked> 전체
                  </label>
                </div>
                <input type="number" class="form-control" id="min-age-input" name="age-input" placeholder="0" disabled>
                ~
                <input type="number" class="form-control" id="max-age-input" name="age-input" placeholder="100" disabled>
              </div>
              <div class="form-group form-inline">
                <label class="control-label col-md-1">성별</label>
                <div class="checkbox col-md-1">
                  <label>
                    <input type="checkbox" id="all-sex-input" checked> 전체
                  </label>
                </div>
                <label class="radio-inline">
                    <input type="radio" name="sex-input" value="M" disabled>남
                </label>
                <label class="radio-inline">
                    <input type="radio" name="sex-input" value="F" disabled>여
                </label>
              </div>
              <div class="form-group form-inline">
                <label class="control-label col-md-1">결혼 여부</label>
                <div class="checkbox col-md-1">
                  <label>
                    <input type="checkbox" id="all-married-input" checked> 전체
                  </label>
                </div> 
                <label class="radio-inline">
                  <input type="radio" name="married-input" value="Y" disabled>기혼
                </label>
                <label class="radio-inline">
                  <input type="radio" name="married-input" value="N" disabled>미혼
                </label>
              </div>
              <!-- <button type="submit" class="btn btn-primary" id="submit-button">조회</button> -->
              <a id="submit-button" class="btn btn-primary" href="#" role="button">조회</a>
            </div>
          </div>
        </div><!-- /.main -->
      </div>
    </div><!-- /.container -->
    
    <!-- Change PW Modal -->
    <div class="modal fade" id="change-pw-modal" tabindex="-1" role="dialog" aria-labelledby="change-pw-ModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="change-pw-ModalLabel">비밀번호 변경</h4>
          </div>
          <div class="modal-body">
            <div class="alert alert-danger" role="alert" id="change-pw-alert">
              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
              <span class="sr-only">Error:</span>
              alert
            </div>
            <form>
              <div class="form-group">
                <label for="now-pw">현재 비밀번호</label>
                <input type="password" class="form-control" id="now-pw" placeholder="현재 비밀번호 입력">
              </div>
              <div class="form-group">
                <label for="next-pw">새 비밀번호</label>
                <input type="password" class="form-control" id="new-pw" placeholder="새 비밀번호 입력">
              </div>
              <div class="form-group">
                <label for="next-pw-confirm">새 비밀번호 확인</label>
                <input type="password" class="form-control" id="new-pw-confirm" placeholder="새 비밀번호 확인">
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
            <button type="button" class="btn btn-primary" id="change-pw" >변경</button>
          </div>
        </div>
      </div>
    </div> <!-- /.Modal -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/t/dt/dt-1.10.11,rr-1.1.1/datatables.min.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script src="/admin/js/change_pw.js"></script>
    <script src="/admin/js/userdb.js"></script>
  </body>
</html>
