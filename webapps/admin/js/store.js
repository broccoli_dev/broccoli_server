$(document).ready(function() {
	store_table = $('#storelist-table').DataTable({
		paging: false,
		searching: false,
		data: [],
		columns: [{
			"data": "storeName"
		}, {
			"data": "storeTel"
		}, {
			"data": "source"
		}, {
			"data": "categoryRaw"
		}, {
			"data": "categoryName"
		}, {
			"data": null
		}],
		"columnDefs" : [ {
			"targets" : -1,
			"defaultContent" : '<button name="remove-store-button" type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#add-tag-modal">'
				+'<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
		} ]
	});
	$('#submit-button').on('click', ajax_table);
	$('#storelist-table').on('click', 'button[name="remove-store-button"]', remove_store);
});

function ajax_table(e){
	var store_name = $('#store-name-input').val();
	if (store_name == ''){
		alert('가맹점 이름을 입력하세요');
	} else{
		$('#submit-button').button('loading');
		$.ajax({
			url: "/pam/admininquiry/storematch",
			type: "post",
			data: JSON.stringify( {
				admin: "admin",
				storeName: store_name
			}),
			dataType : "json",
			success : function(data) {
				store_table.clear().draw();
				store_table.rows.add(data).draw();
				$('#submit-button').button('reset');
			},
			error : function(data) {
				alert(data['statusText']);
				$('#submit-button').button('reset');
			}
		});
	}
}

function remove_store(e){
	var store_name = $($(this).parent().parent().find("td")[0]).text();
	var store_tel = $($(this).parent().parent().find("td")[1]).text();
	var source = $($(this).parent().parent().find("td")[2]).text();
	var category_raw = $($(this).parent().parent().find("td")[3]).text();
	if(confirm('정말로 카드 데이터를 삭제 할까요?')){
		var result = false;
		$.ajax({
			async : false,
			url : "/pam/admininquiry/store/delete",
			type : "post",
			data : JSON.stringify( {
				admin:"admin",
				storeName: store_name,
				storeTel: store_tel,
				source: source,
				categoryRaw: category_raw
			}),
			dataType : "json",
			success : function(data) {
				if (data['code'] != '100') {
					result = [ false, data['desc'] ];
				} else {
					result = [ true, 'OK' ];
				}
			},
			error : function(data) {
				result = [ false, data['statusText'] ];
			}
		});
		if (result[0]) {
			location.reload();
		} else {
			alert('ERROR :'+result[1]);
		}
	}
}