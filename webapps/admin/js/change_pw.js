$(document).ready(function(){
	$('#change-pw').bind('click', change_pw);
	$('#change-pw-modal').on('show.bs.modal', function (event) {
		var modal = $(this);
		modal.find('#change-pw-alert').hide();
		modal.find('#now-pw').val("");
		modal.find('#new-pw').val("");
		modal.find('#new-pw-confirm').val("");
		modal.find('#new-pw').parent().removeClass('has-error')
		modal.find('#new-pw-confirm').parent().removeClass('has-error')
	});
});

function change_pw(e){
	var now_pw = $('#now-pw').val();
	var new_pw = $('#new-pw').val();
	var new_pw_confirm = $('#new-pw-confirm').val();
	var err_header = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>'
	
	if (new_pw != new_pw_confirm){
		$('#change-pw-alert').html(err_header+' 새 비밀번호가 일치하지 않습니다.');
		$('#change-pw-alert').show();
		$('#new-pw').val("");
		$('#new-pw-confirm').val("");
		$('#new-pw').parent().addClass('has-error')
		$('#new-pw-confirm').parent().addClass('has-error')
	} else {
		$('#change-pw-alert').hide();
		$('#new-pw').parent().removeClass('has-error')
		$('#new-pw-confirm').parent().removeClass('has-error')
		
		var result = save_pw(now_pw, new_pw);
		if (result[0]){
			switch(result[1]){
			case '100':
				alert('비밀번호를 변경하였습니다.');
				$('#change-pw-modal').modal('toggle');
				break;
			case '701':
				$('#change-pw-alert').html(err_header+' 현재 비밀번호가 일치하지 않습니다.');
				$('#change-pw-alert').show();
				$('#now-pw').val("");
				$('#new-pw').val("");
				$('#new-pw-confirm').val("");
				$('#now-pw').parent().addClass('has-error')
				break;
			}
		}
		else{
			alert(result[1]);
		}
	}
}

function save_pw(now_pw, new_pw){
	var send_data = {
		admin:"admin",
		nowPW:now_pw,
		newPW:new_pw
	}
	
	var result = false;
	$.ajax({
		async: false,
		url: "/pam/admin/changepw",
		type: "post",
		data: JSON.stringify(send_data),
		dataType: "json",
		success:function(data){
			result = [true, data['code']];
		},
		error:function(data){
			result = [false, data['statusText']];
		}
	});
	return result;
}