$(document).ready(function() {
	$('#save-button').on('click', save_card);
	$('#revert-button').on('click', function (){
		history.back();
	});
	$('#add-benefit-button').on('click', add_benefit);
	$('#cardbenefit-table tbody').on('change', 'tr select[name="card-benefit-large-select"]', set_benefit_category);
	$('#cardbenefit-table tbody').on('click', 'button[name="remove-benefit-button"]', remove_benefit);
	$.ajax({
		async : false,
		url : "/pam/admininquiry/card/codes",
		type : "post",
		data : JSON.stringify({admin : "admin"}),
		dataType : "json",
		success : function(data) {
			codes = data;
		}
	});
	console.log(codes); //////
	set_form(codes);
	var card_code = get_card_code();
	if (card_code == 'add'){
		add_benefit();
	} else{
		$('#card-company-select').attr('disabled', true);
		set_card_data(card_code, codes);
	}
});

function set_benefit_category(e){
	var large = $(this).val();
	
	var largeBenefits = codes['largeBenefit'];
	for (var i=0; i<largeBenefits.length; i++){
		if (largeBenefits[i]['benefitLargeCtgryCode']==large){
			var benefits = largeBenefits[i]['benefit'];
			var benefit_select = $(this).parent().parent().parent().find('td select[name="card-benefit-select"]');
			benefit_select.html('<option style="display:none;"></option>');
			for (var j=0; j<benefits.length; j++){
				benefit_select.append('<option value="'+benefits[j]['benefitCtgryCode']+'">'+benefits[j]['benefitCtgryName']+'</option>');
			}
			break;
		}
	}
}

function save_card(){
	if (!check_blank()){
		alert("입력되지 않은 항목이 있습니다.");
	}else{
		var company_code = $('#card-company-select').val();
		var card_type = $('#card-type-select').val();
		var card_name = $('#card-name-input').val();
		var internal_annual_fee = $('#inannual-input').val();
		var foreign_annual_fee = $('#foannual-input').val();
		var card_img = $('#img-url-input').val();
		var requirement = $('#requirement-input').val();
		var headcopy = $('#headcopy-input').val();
		var brand_list = [];
		$('input[name="card-brand-input"]:checked').each(function(){
			brand_list.push($(this).val())
		});
		var benefit_list = [];
		$('#cardbenefit-table tbody tr').each(function(){
			var large_ctgry = $(this).find('select[name="card-benefit-large-select"]').val();
			var ctgry = $(this).find('select[name="card-benefit-select"]').val();
			var name = $(this).find('input[name="benefit-name-input"]').val();
			benefit_list.push({
				benefitLargeCtgry:large_ctgry,
				benefitCtgry:ctgry,
				benefitName:name
			});
		});
		
		$.ajax({
			async : false,
			url : "/pam/admininquiry/card/save",
			type : "post",
			data : JSON.stringify({
				admin: "admin",
				cardCode: get_card_code(),
				cardData: {
					companyCode: company_code,
					cardType: card_type,
					cardName: card_name,
					internalAnnualFee: internal_annual_fee,
					foreignAnnualFee: foreign_annual_fee,
					cardImg: card_img,
					requirement: requirement,
					headcopy: headcopy,
					brand: brand_list,
					benefit: benefit_list
				}
			}),
			dataType : "json",
			success : function(data) {
				if (data['code'] != '100') {
					alert(data['desc']);
				} else {
					$(location).attr('href',"/pam/admin/system/cardbenefit/");
				}
			},
			error : function(data) {
				alert(data['statusText']);
			}
		});
	}
}

function check_blank(){
	var ret = true;
	$('input[type="text"]').each(function(){
		if ($(this).val() == ''){
			ret = false;
		}
	});
	$('input[type="number"]').each(function(){
		if ($(this).val() == ''){
			ret = false;
		}
	});
	$('select').each(function(){
		if ($(this).val() == ''){
			ret = false;
		}
	});
	if ($('input[name="card-brand-input"]:checked').length == 0){
		ret = false;
	}
	return ret;
}

function add_benefit(){
	$('#cardbenefit-table tbody').append(get_init_benefit_row());
}

function remove_benefit(){
	row = $(this).parent().parent();
	row.remove();
}

function get_init_benefit_row(){
	var benefit_tr = $.parseHTML('<tr>'
			+'<td><div class="form-group"><select class="form-control" name="card-benefit-large-select">'
			+'<option style="display:none;"></option></select></div></td>'
			+'<td><div class="form-group"><select class="form-control" name="card-benefit-select">'
			+'<option style="display:none;"></option></select></div></td>'
			+'<td><div class="form-group"><input type="text" class="form-control" name="benefit-name-input" placeholder="혜택설명"></div></td>'
			+'<td><button name="remove-benefit-button" type="button" class="btn btn-default"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>'
			+'</tr>');
	var largeBenefits = codes['largeBenefit'];
	for (var i=0; i<largeBenefits.length; i++){
		$(benefit_tr).find('select[name="card-benefit-large-select"]').append('<option value="'+largeBenefits[i]['benefitLargeCtgryCode']+'">'+largeBenefits[i]['benefitLargeCtgryName']+'</option>');
	}
	return benefit_tr;
}

function set_form(codes){
	var companies = codes['company'];
	for (var i=0; i<companies.length; i++){
		$('#card-company-select').append('<option value="'+companies[i]['companyCode']+'">'+companies[i]['companyName']+'</option>');
	}
	
	var types = codes['type'];
	for (var i=0; i<types.length; i++){
		$('#card-type-select').append('<option value="'+types[i]['cardType']+'">'+types[i]['typeName']+'</option>');
	}
}

function set_card_data(card_code, codes){
	var card_data;
	$.ajax({
		async : false,
		url : "/pam/admininquiry/card/info",
		type : "post",
		data : JSON.stringify({
			admin: "admin",
			cardCode: card_code
		}),
		dataType : "json",
		success : function(data) {
			card_data = data;
		}
	});
	console.log(card_data); ///////
	var company_code = card_data['companyCode'];
	var card_type = card_data['cardType'];
	var card_name = card_data['cardName'];
	var internal_annual_fee = card_data['internalAnnualFee'];
	var foreign_annual_fee = card_data['foreignAnnualFee'];
	var card_img = card_data['cardImg'];
	var requirement = card_data['requirement'];
	var headcopy = card_data['headcopy'];
	var brand_list = card_data['brand'];
	var benefit_list = card_data['benefit'];
	
	$('#card-company-select').val(company_code);
	$('#card-type-select').val(card_type);
	$('#card-name-input').val(card_name);
	$('#inannual-input').val(internal_annual_fee);
	$('#foannual-input').val(foreign_annual_fee);
	$('#img-url-input').val(card_img);
	$('#requirement-input').val(requirement);
	$('#headcopy-input').val(headcopy);
	$('input[name="card-brand-input"]').val(brand_list)
	for (var i=0; i<benefit_list.length;i++){
		var row = get_init_benefit_row();
		$('#cardbenefit-table tbody').append(row);
		$(row).find('select[name="card-benefit-large-select"]').val(benefit_list[i]['benefitLargeCtgry']).change();
		$(row).find('select[name="card-benefit-select"]').val(benefit_list[i]['benefitCtgry']);
		$(row).find('input[name="benefit-name-input"]').val(benefit_list[i]['benefitName']);
	}
}