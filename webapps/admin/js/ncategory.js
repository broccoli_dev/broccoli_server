$(document).ready(function() {
	var categories = get_category();
	largeCategory = categories[0];
	category = categories[1];

	ncategory_table = $('#ncategory-table').DataTable({
		"ajax" : {
			"url" : "/pam/admininquiry/noncategory",
			"type" : "POST",
			"data" : function() {
				return JSON.stringify({
					"admin" : "admin"
				})
			}
		},
		"columns" : [ {
			"data" : "oppnnt"
		}, {
			"data" : null
		} ],
		"columnDefs" : [ {
			"targets" : -1,
			"defaultContent" : '<button name="pop-add-tag" type="button" class="btn btn-default btn-sm" data-toggle="modal" data-target="#add-tag-modal"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button>'
		} ]
	});

	$('#ncategory-table tbody').on('click', 'button', init_add_tag_modal);
	$('#new-tag-category-large').bind('change', new_tag_large_ctgry_change);
	$('#new-tag-category-small').bind('change', new_tag_small_ctgry_change);
	$('#new-tag-moneycal').bind('change', new_tag_moneycal_change);
	$('#new-tag-expense').bind('change', new_tag_expense_change);
	$('#add-tag').bind('click', add_tag);
});

function get_category() {
	var lctgry = new Object();
	var ctgry = new Object();
	$.ajax({
		async : false,
		url : "/pam/admininquiry/category",
		type : "post",
		data : JSON.stringify({
			admin : "admin"
		}),
		dataType : "json",
		success : function(data) {
			for ( var name in data) {
				lc = data[name]['largeCtgryCode'];
				ln = data[name]['largeCtgryName'];
				c = data[name]['ctgryCode'];
				n = data[name]['ctgryName'];
				lctgry[lc] = ln;
				ctgry[c] = n;
			}
		}
	});
	return [ lctgry, ctgry ];
}
function init_add_tag_modal(e) {
	$('#new-tag-alert').hide();
	$('#new-tag-name').val($($(this).parent().parent().find("td")[0]).text());
	$('#new-tag-code').text('');
	var str = '<option style="display:none;" selected="selected"></option>';
	for ( var lcKey in largeCategory) {
		str += '<option>' + largeCategory[lcKey] + '</option>';
	}
	$('#new-tag-category-large').html(str);
	$('#new-tag-category-small').html(
			'<option style="display:none;" selected="selected"></option>');

	var mc_strs = [ 'X', '예상', '확정' ];
	str = '<option style="display:none;" selected="selected"></option>';
	for ( var i in mc_strs) {
		str += '<option>' + mc_strs[i] + '</option>';
	}
	$('#new-tag-moneycal').html(str);

	var exp_strs = [ '해당없음', '제외', '포함' ];
	str = '<option style="display:none;" selected="selected"></option>';
	for ( var i in exp_strs) {
		str += '<option>' + exp_strs[i] + '</option>';
	}
	$('#new-tag-expense').html(str);
}

function new_tag_large_ctgry_change(e) {
	var selected = $(this).find("option:selected");
	$(this).find('option[selected="selected"]').removeAttr('selected');
	selected.attr('selected', 'selected');
	$('#new-tag-code').text('');

	var largeCtgryName = $(this).val();
	var str = '';
	for ( var lcKey in largeCategory) {
		if (largeCategory[lcKey] == largeCtgryName) {
			str = '<option style="display:none;" selected="selected"></option>';
			for ( var cKey in category) {
				if (cKey.substring(0, 2) == lcKey) {
					str += '<option>' + category[cKey] + '</option>';
				}
			}
			break;
		}
	}
	$('#new-tag-category-small').html(str);
}

function new_tag_small_ctgry_change(e) {
	var selected = $(this).find("option:selected");
	$(this).find('option[selected="selected"]').removeAttr('selected');
	selected.attr('selected', 'selected');

	for ( var cKey in category) {
		if (category[cKey] == $(this).val()) {
			$('#new-tag-code').text(cKey);
		}
	}
}

function new_tag_moneycal_change(e) {
	var selected = $(this).find("option:selected");
	$(this).find('option[selected="selected"]').removeAttr('selected');
	selected.attr('selected', 'selected');
}

function new_tag_expense_change(e) {
	var selected = $(this).find("option:selected");
	$(this).find('option[selected="selected"]').removeAttr('selected');
	selected.attr('selected', 'selected');
}

function add_tag(e) {
	var check_result = check_new_tag_form();
	if (check_result == 'OK') {
		var imc = 0;
		switch ($('#new-tag-moneycal option[selected="selected"]').val()) {
		case 'X':
			imc += 0;
			break;
		case '예상':
			imc += 5;
			break;
		case '확정':
			imc += 1;
			break;
		}
		switch ($('#new-tag-not-expense input').val()) {
		case '해당없음':
			imc += 0;
			break;
		case '제외':
			imc += 2;
			break;
		case '포함':
			imc += 8;
			break;
		}
		var send_data = {
			admin : "admin",
			tag : $('#new-tag-name').val(),
			ctgryCode : $('#new-tag-code').text(),
			isMoneyCalendar : imc
		}
		result = send_new_tag(send_data);
		if (result[0]) {
			location.reload();
		} else {
			alert(result[1]);
		}
	} else {
		header = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>'
		if (check_result == 'BLANK') {
			$('#new-tag-alert').html(header + '입력되지 않은 항목이 있습니다.');
			$('#new-tag-alert').show();
		}
	}
}

function send_new_tag(send_data) {
	var result = false;
	$.ajax({
		async : false,
		url : "/pam/admininquiry/tag/addnoncategory",
		type : "post",
		data : JSON.stringify(send_data),
		dataType : "json",
		success : function(data) {
			if (data['code'] != '100') {
				result = [ false, data['desc'] ];
			} else {
				result = [ true, 'OK' ];
			}
		},
		error : function(data) {
			result = [ false, data['statusText'] ];
		}
	});
	return result;
}

function check_new_tag_form() {
	var tag_name = $('#new-tag-name').val();
	var tag_code = $('#new-tag-code').text();
	var tag_moneycal = $('#new-tag-moneycal').val();
	var tag_expense = $('#new-tag-expense').val();

	if (tag_name == '' || tag_code == '' || tag_moneycal == '' || tag_expense == '') {
		return 'BLANK';
	}
	return 'OK';
}