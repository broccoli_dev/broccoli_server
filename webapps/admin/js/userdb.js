$(document).ready(function() {
	$('#all-age-input').on('change', all_age_checked);
	$('#all-sex-input').on('change', all_sex_checked);
	$('#all-married-input').on('change', all_married_checked);
	$('#submit-button').on('click', userdb_submit);
});

function all_age_checked(e){
	if ($(this).is(":checked")){
		$('[name="age-input"]').prop('disabled',true);
	} else{
		$('[name="age-input"]').prop('disabled',false);
	}
}

function all_sex_checked(e){
	if ($(this).is(":checked")){
		$('[name="sex-input"]').prop('disabled',true);
	} else{
		$('[name="sex-input"]').prop('disabled',false);
	}
}

function all_married_checked(e){
	if ($(this).is(":checked")){
		$('[name="married-input"]').prop('disabled',true);
	} else{
		$('[name="married-input"]').prop('disabled',false);
	}
}

function userdb_submit(e){
	if (!check_blank()){
		alert("입력되지 않은 항목이 있습니다.");
	}else{
		var min_age, max_age, sex, married; 
		if (!$('#all-age-input').is(":checked")){
			min_age = $('#min-age-input').val();
			max_age = $('#max-age-input').val();
		} else{
			min_age = "ALL";
			max_age = "ALL";
		}
		if (!$('#all-sex-input').is(":checked")){
			sex = $('[name="sex-input"]:checked').val();
		} else{
			sex = "ALL";
		}
		if (!$('#all-married-input').is(":checked")){
			married = $('[name="married-input"]:checked').val();
		} else{
			married = "ALL";
		}
		
		var param_str = "?"+"minAge="+min_age+"&maxAge="+max_age+"&sex="+sex+"&married="+married;
		$(this).attr("href","/pam/admin/system/userdb/excel"+param_str);
	}
}

function check_blank(){
	if (!$('#all-age-input').is(":checked")){
		if (($('#min-age-input').val() == '') || ($('#max-age-input').val() == '')){
			return false;
		}
	} 
	if (!$('#all-sex-input').is(":checked")){
		if ($('[name="sex-input"]:checked').length == 0){
			return false;
		}
	}
	if (!$('#all-married-input').is(":checked")){
		 if ($('[name="married-input"]:checked').length == 0){
			return false;
		}
	} 
	return true;
}
