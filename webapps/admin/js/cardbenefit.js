$(document).ready(function() {
	ncategory_table = $('#cardlist-table').DataTable({
		"ajax" : {
			"url" : "/pam/admininquiry/card/alllist",
			"type" : "POST",
			"dataSrc" : "",
			"data" : function() {
				return JSON.stringify({
					"admin" : "admin"
				})
			}
		},
		"columns" : [ {
			"data" : "cardCode"
		}, {
			"data" : "companyName"
		}, {
			"data" : "cardType"
		}, {
			"data" : "cardName"
		}, {
			"data" : "internalAnnualFee"
		}, {
			"data" : "foreignAnnualFee"
		}, {
			"data" : "cardImg"
		}, {
			"data" : "requirement"
		}, {
			"data" : "headcopy"
		}, {
			"data" : "cardCode"
		}],
		"columnDefs" : [ {
			"targets" : 6,
			"render" : function(data) {
				return '<img src="' + data + '">';
			}
		}, {
			"targets" : -1,
			"render" : function(data){
				return '<a href="/pam/admin/system/cardbenefit/'+data+'" class="btn btn-default btn-sm" role="button">'+
				'<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>'+
				'<button name="delete-card" type="button" class="btn btn-default btn-sm">'+
				'<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
			}
		} ]
	});
	$('#cardlist-table tbody').on('click', 'button[name="delete-card"]', delete_card);
});

function delete_card(e){
	var card_code = $($(this).parent().parent().find("td")[0]).text();
	if(confirm('정말로 카드 데이터를 삭제 할까요?')){
		var result = false;
		$.ajax({
			async : false,
			url : "/pam/admininquiry/card/delete",
			type : "post",
			data : JSON.stringify( {
				admin:"admin",
				cardCode:card_code
			}),
			dataType : "json",
			success : function(data) {
				if (data['code'] != '100') {
					result = [ false, data['desc'] ];
				} else {
					result = [ true, 'OK' ];
				}
			},
			error : function(data) {
				result = [ false, data['statusText'] ];
			}
		});
		if (result[0]) {
			location.reload();
		} else {
			alert('ERROR :'+result[1]);
		}
	}
}