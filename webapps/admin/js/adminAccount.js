$(document).ready(function() {
	ncategory_table = $('#adminlist-table').DataTable({
		"ajax" : {
			"url" : "/pam/admininquiry/admin/list",
			"type" : "POST",
			"dataSrc" : "",
			"data" : function() {
				return JSON.stringify({
					"admin" : "admin"
				})
			}
		},
		"columns" : [ {
			"data" : "adminId"
		}, {
			"data" : "authCode"
		}, {
			"data" : "regDate"
		}, {
			"data" : "modDate"
		}, {
			"data" : "authCode"
		}],
		"columnDefs" : [ {
			"targets" : -1,
			"render" : function(data){
				var footer = ""
				if (data == "S"){
					footer = "disabled"
				}
				return '<button name="delete-admin-button" type="button" class="btn btn-default btn-sm" '+footer+'>'+
				'<span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>'
			}
		} ]
	});
	$('#adminlist-table tbody').on('click', 'button[name="delete-admin-button"]', delete_admin);
	$('#add-admin-modal').on('show.bs.modal', init_add_admin_modal);
	$('#add-admin-button').on('click', add_admin);
});

function init_add_admin_modal(e){
	var modal = $(this);
	modal.find('#admin-id-input').val("");
	modal.find('#admin-auth-select').val("");
	modal.find('#admin-pw-input').val("");
	modal.find('#admin-pw-confirm-input').val("");
}

function add_admin(e){
	if (!check_blank()){
		alert("입력되지 않은 항목이 있습니다.");
	} else if (!check_duplicate($('#admin-id-input').val())){
		alert('같은 ID가 있습니다. 다른 ID를 입력해주세요.');
	} else if ($('#admin-pw-input').val()!=$('#admin-pw-confirm-input').val()){
		alert("확인 비밀번호가 일치하지 않습니다.");
	} else{
		var send_data = {
			admin: "admin",
			adminId: $('#admin-id-input').val(),
			adminAuth: $('#admin-auth-select').val(),
			password: $('#admin-pw-input').val()
		}
		result = send_new_admin(send_data);
		if (result[0]) {
			location.reload();
		} else {
			alert(result[1]);
		}
	}
}

function check_blank(){
	if ($('#admin-di-input').val() == ""){
		return false;
	}
	if ($('#admin-auth-select').val() == ""){
		return false;
	}
	if ($('#admin-pw-input').val() == ""){
		return false;
	}
	if ($('#admin-pw-confirm-input').val() == ""){
		return false;
	}
	return true;
}

function check_duplicate(admin_id){
	var ids = $('#adminlist-table tbody tr');
	for (var i = 0, ien = ids.length; i < ien; i++) {
		var aid = $(ids[i]).find("td:first").text();
		if (aid == admin_id) {
			return false;
		}
	}
	return true;
}

function send_new_admin(send_data) {
	var result = false;
	$.ajax({
		async : false,
		url : "/pam/admininquiry/admin/add",
		type : "post",
		data : JSON.stringify(send_data),
		dataType : "json",
		success : function(data) {
			if (data['code'] != '100') {
				result = [ false, data['desc'] ];
			} else {
				result = [ true, 'OK' ];
			}
		},
		error : function(data) {
			result = [ false, data['statusText'] ];
		}
	});
	return result;
}

function delete_admin(e){
	if (confirm('정말로 이 계정을 삭제 할까요?')){
		var admin_id = $($(this).parent().parent().find("td")[0]).text();
		var admin_auth = $($(this).parent().parent().find("td")[1]).text();
		
		var result = false;
		$.ajax({
			async : false,
			url : "/pam/admininquiry/admin/delete",
			type : "post",
			data : JSON.stringify( {
				admin:"admin",
				adminId: admin_id,
				adminAuth: admin_auth
			}),
			dataType : "json",
			success : function(data) {
				if (data['code'] != '100') {
					result = [ false, data['desc'] ];
				} else {
					result = [ true, 'OK' ];
				}
			},
			error : function(data) {
				result = [ false, data['statusText'] ];
			}
		});
		if (result[0]) {
			location.reload();
		} else {
			alert('ERROR :'+result[1]);
		}
	}
}

