$(document).ready(function() {
	var categories = get_category();
	largeCategory = categories[0];
	category = categories[1];

	tag_data = get_tag();
	tag_table = render_tag_table(tag_data);
});

function get_category() {
	var lctgry = new Object();
	var ctgry = new Object();
	$.ajax({
		async : false,
		url : "/pam/admininquiry/category",
		type : "post",
		data : JSON.stringify({
			admin : "admin"
		}),
		dataType : "json",
		success : function(data) {
			for ( var name in data) {
				lc = data[name]['largeCtgryCode'];
				ln = data[name]['largeCtgryName'];
				c = data[name]['ctgryCode'];
				n = data[name]['ctgryName'];
				lctgry[lc] = ln;
				ctgry[c] = n;
			}
		}
	});
	return [ lctgry, ctgry ];
}
function get_tag() {
	var tdata;
	$.ajax({
		async : false,
		url : "/pam/admininquiry/tag/list",
		type : "post",
		data : JSON.stringify({
			admin : "admin"
		}),
		dataType : "json",
		success : function(data) {
			tdata = data;
		}
	});
	return tdata;
}
function render_tag_table(data) {
	var str = '';
	var mc_strs = [ 'X', '예상', '확정' ];
	var exp_strs = [ '해당없음', '제외', '포함' ];
	for ( var name in data) {
		var ctgryPriority = data[name]['ctgryPriority'];
		var tag = data[name]['tag'];
		var ctgryCode = data[name]['ctgryCode'];
		var largeCtgryCode = ctgryCode.substring(0, 2);
		var flag = data[name]['isMoneyCalendar'];
		var imc_flag = ((flag & 1) > 0);
		var emc_flag = ((flag & 4) > 0);
		if (!imc_flag) {
			var m_str = 'X';
		} else if (emc_flag) {
			var m_str = '예상';
		} else {
			var m_str = '확정';
		}
		var ne_flag = ((flag & 2) > 0);
		var ie_flag = ((flag & 8) > 0);
		if (ne_flag) {
			var exp_str = '제외';
		} else if (ie_flag) {
			var exp_str = '포함';
		} else {
			var exp_str = '해당없음';
		}

		str += '<tr>'
		str += '<td class="reorder">' + ctgryPriority + '</td>';
		str += '<td name="tag-name">' + tag + '</td>';
		str += '<td name="ctgry-code">' + ctgryCode + '</td>';
		str += '<td><select class="form-control input-sm" name="large-select"><option style="display:none;"></option>';
		for ( var lcKey in largeCategory) {
			if (lcKey == largeCtgryCode) {
				str += '<option selected="selected">' + largeCategory[lcKey]
						+ '</option>';
			} else {
				str += '<option>' + largeCategory[lcKey] + '</option>';
			}
		}
		str += '</select></td>';
		str += '<td><select class="form-control input-sm" name="small-select">';
		for ( var cKey in category) {
			if (cKey.substring(0, 2) == largeCtgryCode) {
				if (cKey == ctgryCode) {
					str += '<option selected="selected">' + category[cKey]
							+ '</option>';
				} else {
					str += '<option>' + category[cKey] + '</option>';
				}
			}
		}
		str += '</select></td>';
		str += '<td><select class="form-control input-sm" name="moneycal-select">';
		for ( var i in mc_strs) {
			var select_footer = (m_str == mc_strs[i]) ? ' selected="selected"'
					: '';
			str += '<option' + select_footer + '>' + mc_strs[i] + '</option>';
		}
		str += '</select></td>';
		str += '<td><select class="form-control input-sm" name="expense-select"><option style="display:none;"></option>';
		for ( var i in exp_strs) {
			var select_footer = (exp_str == exp_strs[i]) ? ' selected="selected"'
					: '';
			str += '<option' + select_footer + '>' + exp_strs[i] + '</option>';
		}
		str += '</select></td>';
		str += '<td><button name="rm-tag" type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>'
		str += '<td style="display:none;">' + name + '</td>'
		str += '</tr>'
	}
	$('#tag-table tbody').html(str);
	bind_events();

	var table = $('#tag-table').DataTable({
		paging : false,
		rowReorder : true
	});
	table.on('row-reordered', row_reordered);
	return table;
}

function large_ctgry_change(e) {
	var selected = $(this).find("option:selected");
	$(this).find('option[selected="selected"]').removeAttr('selected');
	selected.attr('selected', 'selected');
	var row = $(this).parent().parent();
	row.find('td[name="ctgry-code"]').text('');

	var smctgry = row.find('select[name="small-select"]')
	var largeCtgryName = $(this).val();
	var str = '';
	for ( var lcKey in largeCategory) {
		if (largeCategory[lcKey] == largeCtgryName) {
			str = '<option style="display:none;" selected="selected"></option>';
			for ( var cKey in category) {
				if (cKey.substring(0, 2) == lcKey) {
					str += '<option>' + category[cKey] + '</option>';
				}
			}
			break;
		}
	}
	smctgry.html(str);
}

function small_ctgry_change(e) {
	var selected = $(this).find("option:selected");
	$(this).find('option[selected="selected"]').removeAttr('selected');
	selected.attr('selected', 'selected');
	var row = $(this).parent().parent();

	for ( var cKey in category) {
		if (category[cKey] == $(this).val()) {
			row.find('td[name="ctgry-code"]').text(cKey);
		}
	}
	row.addClass('changed');
}

function moneycal_change(e) {
	var selected = $(this).find("option:selected");
	$(this).find('option[selected="selected"]').removeAttr('selected');
	selected.attr('selected', 'selected');
	var row = $(this).parent().parent();
	row.addClass('changed');
}

function expense_change(e) {
	var selected = $(this).find("option:selected");
	$(this).find('option[selected="selected"]').removeAttr('selected');
	selected.attr('selected', 'selected');
	var row = $(this).parent().parent();
	row.addClass('changed');
}

function submit_click(e) {
	$('button[name="submit-change"]').button('loading');
	check_result = check_tag_table()
	if (check_result == 'OK') {
		result = save_tag()
		if (result[0]) {
			location.reload();
		} else {
			alert(result[1]);
		}
	} else if (check_result == 'BLANK') {
		alert('아직 수정 중인 태그가 있습니다.');
	}
	$('button[name="submit-change"]').button('reset');
}

function save_tag() {
	var send_data = {
		admin : "admin",
		added : make_data($('#tag-table tr.added:not(.removed)')),
		removed : make_data($('#tag-table tr.removed:not(.added)')),
		changed : make_data($('#tag-table tr.changed:not(.added, .removed)'))
	}

	var result = false;
	$.ajax({
		async : false,
		url : "/pam/admininquiry/tag/save",
		type : "post",
		data : JSON.stringify(send_data),
		dataType : "json",
		success : function(data) {
			if (data['code'] != '100') {
				result = [ false, data['desc'] ];
			} else {
				result = [ true, 'OK' ];
			}
		},
		error : function(data) {
			result = [ false, data['statusText'] ];
		}
	});
	return result;
}

function make_data(elements) {
	var list = []
	for (var i = 0, ien = elements.length; i < ien; i++) {
		var tds = $(elements[i]).find('td');
		var imc = 0;
		switch ($(tds[5]).find('option[selected="selected"]').val()) {
		case 'X':
			imc += 0;
			break;
		case '예상':
			imc += 5;
			break;
		case '확정':
			imc += 1;
			break;
		}
		switch ($(tds[6]).find('option[selected="selected"]').val()) {
		case '해당없음':
			imc += 0;
			break;
		case '제외':
			imc += 2;
			break;
		case '포함':
			imc += 8;
			break;
		}
		list.push({
			tag : $(tds[1]).text(),
			ctgryCode : $(tds[2]).text(),
			isMoneyCalendar : imc,
			ctgryPriority : $(tds[0]).text()
		});
	}
	return list;
}

function check_tag_table() {
	ctgryCodes = $('td[name="ctgry-code"]');
	for (var i = 0, ien = ctgryCodes.length; i < ien; i++) {
		if ($(ctgryCodes[i]).text() == '') {
			var position = $(ctgryCodes[i]).offset();
			$('html, body').animate({
				scrollTop : (position.top - 400)
			}, 1000);
			return 'BLANK';
		}
	}
	return 'OK';
}

function revert_click(e) {
	unbind_events();
	$('#tag-table tbody').empty();
	tag_table.destroy();
	tag_table = render_tag_table(tag_data);
}

function bind_events() {
	$('#tag-table select[name="large-select"]').bind('change',
			large_ctgry_change);
	$('#tag-table select[name="small-select"]').bind('change',
			small_ctgry_change);
	$('#tag-table select[name="moneycal-select"]').bind('change',
			moneycal_change);
	$('#tag-table select[name="expense-select"]')
			.bind('change', expense_change);
	$('button[name="submit-change"]').bind('click', submit_click);
	$('button[name="revert-change"]').bind('click', revert_click);
	$('button[name="pop-add-tag"]').bind('click', init_add_tag_modal);
	$('button[name="rm-tag"]').bind('click', remove_row);
	$('#new-tag-category-large').bind('change', new_tag_large_ctgry_change);
	$('#new-tag-category-small').bind('change', new_tag_small_ctgry_change);
	$('#new-tag-moneycal').bind('change', new_tag_moneycal_change);
	$('#new-tag-expense').bind('change', new_tag_expense_change);
	$('#add-tag').bind('click', add_row);
}

function unbind_events() {
	$('#tag-table select[name="large-select"]').unbind('change',
			large_ctgry_change);
	$('#tag-table select[name="small-select"]').unbind('change',
			small_ctgry_change);
	$('#tag-table select[name="moneycal-select"]').unbind('change',
			moneycal_change);
	$('#tag-table input[name="expense-select"]').unbind('change',
			expense_change);
	$('button[name="submit-change"]').unbind('click', submit_click);
	$('button[name="revert-change"]').unbind('click', revert_click);
	$('button[name="pop-add-tag"]').unbind('click', init_add_tag_modal);
	$('button[name="rm-tag"]').unbind('click', remove_row);
	$('#new-tag-category-large').unbind('change', new_tag_large_ctgry_change);
	$('#new-tag-category-small').unbind('change', new_tag_small_ctgry_change);
	$('#new-tag-moneycal').unbind('change', new_tag_moneycal_change);
	$('#new-tag-expense').unbind('change', new_tag_expense_change);
	$('#add-tag').unbind('click', add_row);
}

function init_add_tag_modal(e) {
	$('#new-tag-alert').hide();
	$('#new-tag-name').val('');
	$('#new-tag-code').text('');
	var str = '<option style="display:none;" selected="selected"></option>';
	for ( var lcKey in largeCategory) {
		str += '<option>' + largeCategory[lcKey] + '</option>';
	}
	$('#new-tag-category-large').html(str);
	$('#new-tag-category-small').html(
			'<option style="display:none;" selected="selected"></option>');

	var mc_strs = [ 'X', '예상', '확정' ];
	str = '<option style="display:none;" selected="selected"></option>';
	for ( var i in mc_strs) {
		str += '<option>' + mc_strs[i] + '</option>';
	}
	$('#new-tag-moneycal').html(str);

	var exp_strs = [ '해당없음', '제외', '포함' ];
	str = '<option style="display:none;" selected="selected"></option>';
	for ( var i in exp_strs) {
		str += '<option>' + exp_strs[i] + '</option>';
	}
	$('#new-tag-expense').html(str);
}

function new_tag_large_ctgry_change(e) {
	var selected = $(this).find("option:selected");
	$(this).find('option[selected="selected"]').removeAttr('selected');
	selected.attr('selected', 'selected');
	$('#new-tag-code').text('');

	var largeCtgryName = $(this).val();
	var str = '';
	for ( var lcKey in largeCategory) {
		if (largeCategory[lcKey] == largeCtgryName) {
			str = '<option style="display:none;" selected="selected"></option>';
			for ( var cKey in category) {
				if (cKey.substring(0, 2) == lcKey) {
					str += '<option>' + category[cKey] + '</option>';
				}
			}
			break;
		}
	}
	$('#new-tag-category-small').html(str);
}

function new_tag_small_ctgry_change(e) {
	var selected = $(this).find("option:selected");
	$(this).find('option[selected="selected"]').removeAttr('selected');
	selected.attr('selected', 'selected');

	for ( var cKey in category) {
		if (category[cKey] == $(this).val()) {
			$('#new-tag-code').text(cKey);
		}
	}
}

function new_tag_moneycal_change(e) {
	var selected = $(this).find("option:selected");
	$(this).find('option[selected="selected"]').removeAttr('selected');
	selected.attr('selected', 'selected');
}

function new_tag_expense_change(e) {
	var selected = $(this).find("option:selected");
	$(this).find('option[selected="selected"]').removeAttr('selected');
	selected.attr('selected', 'selected');
}

function add_row(e) {
	var check_result = check_new_tag_form();
	if (check_result == 'OK') {
		var str = '';
		var mc_strs = [ 'X', '예상', '확정' ];
		var exp_strs = [ '해당없음', '제외', '포함' ];
		var ctgryPriority = parseInt($(
				'#tag-table tbody tr:not(.removed) td.reorder').last().text()) + 1;
		var tag = $('#new-tag-name').val();
		var ctgryCode = $('#new-tag-code').text();
		var largeCtgryCode = ctgryCode.substring(0, 2);
		var m_str = $('#new-tag-moneycal').val();
		var exp_str = $('#new-tag-expense').val();

		str += '<tr class="added">';
		str += '<td class="reorder">' + ctgryPriority + '</td>';
		str += '<td name="tag-name">' + tag + '</td>';
		str += '<td name="ctgry-code">' + ctgryCode + '</td>';
		str += '<td><select class="form-control input-sm" name="large-select"><option style="display:none;"></option>';
		for ( var lcKey in largeCategory) {
			if (lcKey == largeCtgryCode) {
				str += '<option selected="selected">' + largeCategory[lcKey]
						+ '</option>';
			} else {
				str += '<option>' + largeCategory[lcKey] + '</option>';
			}
		}
		str += '</select></td>';
		str += '<td><select class="form-control input-sm" name="small-select">';
		for ( var cKey in category) {
			if (cKey.substring(0, 2) == largeCtgryCode) {
				if (cKey == ctgryCode) {
					str += '<option selected="selected">' + category[cKey]
							+ '</option>';
				} else {
					str += '<option>' + category[cKey] + '</option>';
				}
			}
		}
		str += '</select></td>';
		str += '<td><select class="form-control input-sm" name="moneycal-select">';
		for ( var i in mc_strs) {
			var select_footer = (m_str == mc_strs[i]) ? ' selected="selected"'
					: '';
			str += '<option' + select_footer + '>' + mc_strs[i] + '</option>';
		}
		str += '</select></td>';
		str += '<td><select class="form-control input-sm" name="expense-select">';
		for ( var i in exp_strs) {
			var select_footer = (exp_str == exp_strs[i]) ? ' selected="selected"'
					: '';
			str += '<option' + select_footer + '>' + exp_strs[i] + '</option>';
		}
		str += '</select></td>';
		str += '<td><button name="rm-tag" type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button></td>';
		str += '<td style="display:none;">999</td>';
		str += '</tr>';
		tag_table.row.add($(str)).draw();
		unbind_events();
		bind_events();
		$('#add-tag-modal').modal('toggle');
	} else {
		header = '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span><span class="sr-only">Error:</span>'
		if (check_result == 'BLANK') {
			$('#new-tag-alert').html(header + '입력되지 않은 항목이 있습니다.');
			$('#new-tag-alert').show();
		} else if (check_result == 'DUPLICATE') {
			$('#new-tag-alert').html(header + '중복된 태그입니다.');
			$('#new-tag-alert').show();
		}
	}
}

function check_new_tag_form() {
	var tag_name = $('#new-tag-name').val();
	var tag_code = $('#new-tag-code').text();
	var tag_moneycal = $('#new-tag-moneycal').val();
	var tag_expense = $('#new-tag-expense').val();

	if (tag_name == '' || tag_code == '' || tag_moneycal == '' || tag_expense == '') {
		return 'BLANK';
	} else {
		var tags = $('#tag-table tr:not(.removed) td[name="tag-name"]');
		for (var i = 0, ien = tags.length; i < ien; i++) {
			var t = $(tags[i]).text();
			if (t == tag_name) {
				return 'DUPLICATE';
			}
		}
	}
	return 'OK';
}

function remove_row(e) {
	var row = $(this).parent().parent();
	var rm_priority = parseInt(row.find('td.reorder').text());

	var rows = row.nextAll(':not(.removed)');
	for (var i = 0, ien = rows.length; i < ien; i++) {
		$(rows[i]).find('.reorder').text(rm_priority);
		rm_priority += 1;
		$(rows[i]).addClass('changed');
	}
	row.addClass('removed');
}

function row_reordered(e, diff, edit) {
	for (var i = 0, ien = diff.length; i < ien; i++) {
		$(diff[i].node).addClass('changed');
	}
}