<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Broccoli Admin Page</title>

    <!-- Bootstrap core CSS -->
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://cdn.datatables.net/t/dt/dt-1.10.11,rr-1.1.1/datatables.min.css" rel="stylesheet">
    <link href="/admin/css/bradmin.css" rel="stylesheet">
  </head>

  <body>
    <!-- top nav bar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/pam/admin/home">Broccoli Admin</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <!-- <li><a href="#user">회원관리</a></li>  -->
            <li class="active"><a href="/pam/admin/system">시스템관리</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


    <div class="container-fluid">
      <!-- container -->
      <div class="row">  
        <div class="col-sm-3 col-md-2 sidebar">
          <!-- left nav -->
          <ul class="nav nav-sidebar">
            <li><a href="/pam/admin/system/store">스토어 관리</a></li>
            <li><a href="/pam/admin/system/tag">태그 관리 </a></li>
            <li><a href="/pam/admin/system/ncategory">미분류 카테고리 관리</a></li>
            <li class="active"><a href="/pam/admin/system/cardbenefit">카드 혜택 관리<span class="sr-only">(current)</span></a></li>
            <li><a href="/pam/admin/system/userdb">회원 DB 관리</a></li>
            <li><a href="/pam/admin/system/adminaccount">관리자 계정 관리</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <!-- main -->
          <div class="row table-icon">
            <div class="btn-group" role="group" aria-label="...">
              <button id="revert-button" type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> 취소</button>
              <button id="save-button" type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-ok" aria-hidden="true" data-loading-text="Loading..."></span> 저장</button>
            </div>
          </div>
          <div class="row">
            <form class="form-horizontal">
              <div class="form-group">
                <label class="control-label col-md-1" for="card-company-select">카드사</label>
                <div class="col-md-2">
                  <select class="form-control" id="card-company-select">
                    <option style="display:none;"></option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-1" for="card-type-select">타입</label>
                <div class="col-md-2">
                  <select class="form-control" id="card-type-select">
                    <option style="display:none;"></option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-1" for="card-name-input">이름</label>
                <div class="col-md-4">
                  <input type="text" class="form-control" id="card-name-input" placeholder="카드 이름">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-1" for="inannual-input">국내연회비</label>
                <div class="col-md-3">
                  <input type="number" class="form-control" id="inannual-input" placeholder="0">
                </div>
                <label class="control-label" for="inannual-input">원</label>
              </div>
              <div class="form-group">
                <label class="control-label col-md-1" for="foannual-input">해외연회비</label>
                <div class="col-md-3">
                  <input type="number" class="form-control" id="foannual-input" placeholder="0">
                </div>
                <label class="control-label" for="foannual-input">원</label>
              </div>
              <div class="form-group">
                <label class="control-label col-md-1" for="img-url-input">이미지주소</label>
                <div class="col-md-6">
                  <input type="text" class="form-control" id="img-url-input" placeholder="http://">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-1" for="requirement-input">전월실적</label>
                <div class="col-md-3">
                  <input type="number" class="form-control" id="requirement-input" placeholder="0">
                </div>
                <label class="control-label" for="requirement-input">원</label>
              </div>
              <div class="form-group">
                <label class="control-label col-md-1" for="headcopy-input">헤드카피</label>
                <div class="col-md-7">
                  <input type="text" class="form-control" id="headcopy-input" placeholder="HAPPY한 할인의 시작, XX 카드! 주유소 L당 00원 할인!엔진오일 무료교환, 차량정비 서비스도">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-md-1">카드브랜드</label>
                <div class="col-md-7">
                  <label class="checkbox-inline">
                    <input type="checkbox" name="card-brand-input" value="A"> AMEX
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="card-brand-input" value="B"> BC
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="card-brand-input" value="C"> CUP
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="card-brand-input" value="I"> 국내전용
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="card-brand-input" value="J"> JCB
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="card-brand-input" value="M"> MasterCard
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="card-brand-input" value="U"> UnionPay
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="card-brand-input" value="V"> VISA
                  </label>
                </div>
              </div>
            </form>
          </div>
          <div class="row table-icon">
            <button id="add-benefit-button" type="button" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 혜택추가</button>
          </div>
          <div class="row">
            <table class="table" id="cardbenefit-table">
              <thead>
                <tr>
                  <th>카테고리 대분류</th>
                  <th>카테고리 소분류</th>
                  <th>혜택 설명</th>
                  <th></th>
                </tr>
              </thead>
              <tbody><!-- card benefit table body --></tbody>
            </table>
          </div>
        </div><!-- /.main -->
      </div>
    </div><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdn.datatables.net/t/dt/dt-1.10.11,rr-1.1.1/datatables.min.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script src="/admin/js/cardbenefitEdit.js"></script>
    <script>
      function get_card_code(){
    	  return "${ cardCode }";
      }
    </script>
  </body>
</html>
