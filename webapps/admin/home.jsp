<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Broccoli Admin Page</title>

    <!-- Bootstrap core CSS -->
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/admin/css/bradmin.css" rel="stylesheet">
  </head>

  <body>
    <!-- top nav bar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/pam/admin/home">Broccoli Admin</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <!-- <li><a href="#user">회원관리</a></li>  -->
            <li><a href="/pam/admin/system">시스템관리</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span><span class="caret"></span>
              </a>
              <ul class="dropdown-menu">
                <li><a href="#" data-toggle="modal" data-target="#change-pw-modal">비밀번호 변경</a></li>
                <li><a href="/pam/admin/logout">로그아웃</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container m_container">
      <!-- container -->
      <div class="home">
        <h1>${ admin_id }님 환영합니다</h1>
        <p class="lead">브로콜리 관리자 페이지 입니다.</p>
      </div>
    </div><!-- /.container -->
    
    <!-- Change PW Modal -->
    <div class="modal fade" id="change-pw-modal" tabindex="-1" role="dialog" aria-labelledby="change-pw-ModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="change-pw-ModalLabel">비밀번호 변경</h4>
          </div>
          <div class="modal-body">
            <div class="alert alert-danger" role="alert" id="change-pw-alert">
              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
              <span class="sr-only">Error:</span>
              alert
            </div>
            <form>
              <div class="form-group">
                <label for="now-pw">현재 비밀번호</label>
                <input type="password" class="form-control" id="now-pw" placeholder="현재 비밀번호 입력">
              </div>
              <div class="form-group">
                <label for="next-pw">새 비밀번호</label>
                <input type="password" class="form-control" id="new-pw" placeholder="새 비밀번호 입력">
              </div>
              <div class="form-group">
                <label for="next-pw-confirm">새 비밀번호 확인</label>
                <input type="password" class="form-control" id="new-pw-confirm" placeholder="새 비밀번호 확인">
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
            <button type="button" class="btn btn-primary" id="change-pw" >변경</button>
          </div>
        </div>
      </div>
    </div> <!-- /.Modal -->
    

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="/admin/js/bootstrap.min.js"></script>
    <script src="/admin/js/change_pw.js"></script>
  </body>
</html>
